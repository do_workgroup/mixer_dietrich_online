# Project 'Mixer' (Dietrich Online) #

Project Mixer is a subproject of �Dietrich Online�, see http://www.dietrich.uni-trier.de/.

The purpose of this subproject is to develop a software platform, which can analyse the given text, divide it into segments, then save the data into the database. The generated data provide the background for the online search. 

Our platform is a Perl 5 object system with its own environment for running in the major OSs, including Windows, Mac OS X, Linux etc. It implements the Text Analysis Engine, State Pattern Engine and Business Rule Engine to discover and reveal the data structure and content. This is the preliminary work for presenting and using the data online.


# License & Authors #

**Manager:**

* Dr. Hans-Ulrich Seifert (seifert@uni-trier.de)


**Cooperators:** 

* Dr. Hagen Reinstein (reinstei@uni-trier.de)
* Julia Odenbreit M.A. MA (LIS) (odenbreit@uni-trier.de)
* Dipl.-Ing. Constanze Decker (decker@uni-trier.de)

**Author:** 

* Dipl.-Inf. Xin Zhou (zhouxinj@hotmail.com)



Copyright: 2015-2017, Xin Zhou, The University Library of Trier. All Rights Reserved.