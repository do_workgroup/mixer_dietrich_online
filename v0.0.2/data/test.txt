<strecke=a> <seite=77>
----
<a00001 74 77 li><l>Aachen</l>: Sichergsarbeiten an d. got. Chorhalle d. Münsterkirche in ~ (J. Buchkremer) <s>459a.</s> 30. J. 1-13.
| Aus d. Musikleben ~s (H. Jacobs) <s>1344.</s> 235
----
<a00002 74 77 li><l>Aal</l>: Vom ~, v. Buhnen, Buhnenfeldern u. d. Bremer Weserwehre (Löwe) <s>494a.</s> 59. J. 99-104
| Chem. Zusammensetzg. v. ~fett. (H. Wiehr) <s>1256.</s> 71
| ~seuchen in dtschn. Binnen- u. Küstengewässern 1930/33 (W. Schäperclaus) <s>1674.</s> 191-217
| ~stechen (Hans Domnick) <s>1422.</s> 90
----
<a00003 74 77 li><l>Aba-Novák, Wilh.</l> (N. Waissnix) <s>810ac.</s> VI. 16-19.
----
<a00004 74 77 li><l>Abbe</l> s. Duncker
