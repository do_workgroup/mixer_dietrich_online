package main;
use Moose;
use Moose::Util::TypeConstraints;

=encoding utf8

=head1 NAME

DietrichOnline::Main - Main Process of Project 'Dietrich Online'

=head1 VERSION

version 0.02

=head1 SYNOPSIS

my $mgr = Management::ProcessManagement->new;
my $bandName = "b74";
my $fileName = "d74.txt";
my $createTable = 1;  # for database
my $tableName = "entry"; # for database
#$mgr->main($bandName, \$fileName, $createTable, $tableName);

=head1 DESCRIPTION

This is a textual analysis engine for Project "Dietrich Online" of Library University Trier.

The main goal of this sub project is to analyse the given text file, separate it into its parts, in 
order to save in the database and mapping the data from other field.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyrigh 2016, Xin Zhou. All Rights Reserved.

=cut



use Data::Dumper;
#use namespace::autoclean;
#use FindBin;
#use lib "$FindBin::Bin/../../lib";
use 5.020;
use FindBin;
use lib "$FindBin::Bin/../lib";
use Carp;

use utf8;
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);       
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";


#Initializing

use DateTime;
#use Test::More;

use Coordinate::Point;
use Coordinate::TextIndex;
use Coordinate::TextPointer;
#use Utils::Object;
use Global::Values;
use Text::EntryText;
use Data::Dumper;
use Utils::IO;
use Coordinate::TextPointer;
use Algorithms::DOFSM;
use Algorithms::DOGraph;
use Algorithms::DOSets;
use Set::Scalar;

#use Engine::RegEngine;

#use Engine::DOModuleOutput;
#use Engine::RegModule;
#use Engine::DOModuleOutput;

#use Global::DOID;

# 0. init paths
Global::Values->initPaths;
# init id system

# Attention: IDGenerator is so important for 
# import Global::RowNumGenerator
use Engine::IDProcessor;
#use Global::RowNumGenerator;

use Database::MotDB;
use Database::MotDBProcessor;
use Algorithms::DORuleEngineClass;
use Database::DBProcessor;


use Management::ProcessManagement;


my $mgr = Management::ProcessManagement->new;

my $bandName = "b75";
my $fileName = "d75.txt";
my $createTable = 1;  # for database
my $tableName = "entries"; # for database
#my $sqliteFile = "sqlite_test2.db";

$mgr->main($bandName, \$fileName, $createTable, $tableName);
print Dumper("main() done");

#$mgr->addFileName(" hallo");
#$mgr->chompFileName();
#print Dumper($mgr->inputFileName);


#my $type = "ENTRY_TYPE_ENTRY";
#my $set = Global::Values->doSetGroups->{EntryText}->intersection(Global::Values->doSetGroups->{D74EntryText})->has($type);
#print $set;









#my $stmt = qq(
#  CREATE TABLE segment ( 
#          sid INTEGER  PRIMARY KEY NOT NULL,
#          name   TEXT
#  );
#);
#my $dbu = Database::DatabaseUtils->getInstance;



#$dbu->createTable("segment", \$stmt);
#my $schema = $dbu->curSchema;
#$schema->storage->debug(1);
##print Dumper($schema);
#my @seg = (['18', "hallo"]);
#
#my @arr = ({name=>"xiao"}, { name=>"lkn"});
#$schema->populate('Segment', \@arr);


#my $segment = $schema->resultset("Segment")->new({
#  name => "xin"
#});
#$segment->update;

#my $dbu = Database::DatabaseUtils->new;
#my $res =  $dbu->curSchema;
#print Dumper($res);



#my $str = "ENTRY_TYPE_LEMA";
#print Dumper(Global::Values->doSetGroups->{LemmaEntryText}->has($str));

# 1. update input file name and band name
#my $mdbApp = Database::MotDB_DBTableApp->new;
#$mdbApp->updateInputTextFile("d74_test.txt");
#$mdbApp->updateBandName("b74");
#print Dumper($mdbApp->readNewRow);


#use Analyse::AnalyseContentStrategies;
#my $hallo = Analyse::AnalyseContentRowType->new;
#my $str = "hallo";
#print "hallo " . $hallo->setText($str);
#print "hi";



#my $entryID = "b74=d74txt=r0000055";
#my $id = "b74=d74txt=r0000055=t00000001";
#my $id2 = "b74=d74txt=r0000055=t00000002";
#my $dbapp =  Database::MotDB_DBTableApp->new;
#$dbapp->getTableProcessor("IDMAP_TREE")->insert($entryID, $id);
#$dbapp->insert("IDMAP_TREE", $entryID, $id);

#Database::MotDB->addToIDMap(Global::Values::DO_IDMAP_TYPE->{TREE}, $entryID, $id);
#Database::MotDB->addToIDMap(Global::Values::DO_IDMAP_TYPE->{TREE}, $entryID, $id2);
#my $id3 = "hallo";
#Database::MotDB->rmFromIDMap(Global::Values::DO_IDMAP_TYPE->{TREE}, $entryID, $id2);
#print Dumper(Database::MotDB->getIDMapTree);

#Database::MotDB->setFileName("d74.txt");
#Database::MotDB->setBandName("b74");
#Database::MotDB->setRowNum(55);
#my $idg = Engine::IDGenerateApp->new;
#print Dumper($idg->generateEntryID);




#my $ga = Global::IDGenerateApp->new();



#my $rng = Global::RowNumGenerator->new;
#print $rng->generate;




#Utils::Tools->timer("start");
#my $value = "hallo";
#for my $key (1..100000) {
#  print "$key \n";
#  $value .= $key;
#  Database::MotDB->addToRowIDMap($key, $value);
#}
#Utils::Tools->timer("end");
#Utils::Tools->timer("start");
#sleep(1);
#Utils::Tools->timer("end");
#Utils::Tools->timer("start");
#sleep(1);
#Utils::Tools->timer("end");


#my $o = Engine::DOModuleOutput->new;
#my $hash = $o->newOutputUnit("MODULE_OUTPUT_TARGET_POS", "name1", "nihao", "12,32");
#$o->addToOutputTable($hash);



#my $index = Utils::Tools->setIndices(52,32);
#print Dumper(Utils::Tools->splitIndices($index));

#my $arr = [1, "hallo", 2, "ni"];
#my $str = "hall";
#print Dumper(Utils::Tools->isInArray($arr, \$str));



#Global::DOID->getEntryTextPartID('SIC_ELEMENT_22');

#Global::DOID->curTextPartType('ENTRY_ELEMENT');
#print Dumper(Global::DOID->curTextPartType);
#Global::DOID->resetCurTextPartType;
#print Dumper(Global::DOID->curTextPartType);


#Global::DOID->addToRowIDMap("1", "hallo_xin");
#Global::DOID->addToRowIDMap("2", "hallo_xi");
#Global::DOID->resetRowIDMap;
#print Dumper(Global::DOID->rowIDMap);

#Global::DOID->setIDParam("origPage", "32");

#Global::DOID->bandPartType(Global::Values::DO_BAND_PART->{AUTHOR_REGISTER});
#print Dumper(Global::DOID->bandPartType);
#Global::DOID->resetBandPartType;
#print Dumper(Global::DOID->bandPartType);


#print Dumper(Global::DOID->entryNumInBlock);
#Global::DOID->clearEntryNumInBlock;
#print Dumper(Global::DOID->entryNumInBlock);




#my $lem = "hallo";
#my $lem1 = "hallo";
#my $lem2 = "hlo";
#Global::DOID->updateLemma(\$lem);
#Global::DOID->updateLemma(\$lem1);
#Global::DOID->updateLemma(\$lem2);

#print Dumper(Global::DOID->bandPartType);


#my $m = Engine::RegModule->new;
#my $str = "hallo, Xin! Wie geht's dir?";
#$m->setModName(\$str);
#print Dumper($m->getModName);

#my $mo = Engine::DOModuleOutput->new;

#my $hash = {key => 1, value => 2};
#my @arr = ();
#push @arr, $hash;
#$mo->outputTable(\@arr);
#print Dumper($mo->outputTable);



#my $output = Engine::DOModuleOutput->new;

#my $str = "<>";
#my $hallo = Utils::Tools->specCharEscape($str);
#print Dumper(Utils::Tools->specCharUnEscape($hallo));


#$output->getOutputUnitType("MODULE_OUTPUT_MATCHED_BOOL");

#print Dumper(Global::Values->getOutputUnit("MODULE_OUTPUT_MATCHED_BOOL"));

#$output->outputType("MODULE_OUTPUT_MATCHED_BOOL");
#print $output->outputType;

#my $ept = Text::EntryPartText->new;
#$ept->reset;

#my $eText = Text::EntryText->new;
#my $str = "asdfasdfasdfd0a9=)( %& §§fa0s9df8ao dfkjpoas fd2";

#$eText->reset;



#$eText->setEntryAttr("originalText", \$str);
#print Dumper($eText->getEntryAttr("originalText"));

#$eText->setHIDNummer("p0001");
#print Dumper($eText->getHIDNummer);

#my $reg = Engine::RegEngine->new();
#print Dumper($reg->getStructCharReg);

#my $str = "hallo,<l>xin</l> hier ist <l>zhou</l>. Wie geht's dir? hoffe<l>dude</l> dir alles Gute! Bye.";
#my $pat = qr{<l[>]*>.*?</l>};
#print Dumper($reg->matchInfo(\$str, \$pat, 15));




#my $g = Algorithms::DOGraph->new;

#my $arr = $g->getVName("mod_entryType_check_1:streck_1");
#print Dumper($arr);








#print Dumper(Global::Values->SEPARATOR->{IN_INDEX});

#my $machine = Analyse::TextMachine->new();
#$machine->setInputTextFile("d74_test.txt");

#print Dumper($machine->entryArray->[0]);

#my $io = Utils::IO->new;
#$io->setDBConfig;
#$io->setRegExpInfos;
#my $io = Utils::IO->new;
#my $dofsm = Algorithms::DOFSM->new;
#$io->setFSMStatesFromConfig($dofsm->fsm, "fsm_states.yaml");
#$io->setFSMTransFromConfig($dofsm->fsm, "fsm_trans.yaml");
#$dofsm->run;
#my $str = $dofsm->fsm->generate_graphviz_code(name=>'fsmGraph', size=> 20);
#open my $fh, ">:utf8", "fsm.dot";
#print $fh $str;
#close $fh;
#my @args = (dot=>'-Tpng', "fsm.dot", "-ofsm.png");
#system @args;



#my $sets = Algorithms::DOSets->new();
#$sets->insert(Global::Values::CHARTYPES->{NORMAL}, "a");
#print Dumper($sets->groups->{Global::Values::CHARTYPES->{NORMAL}}->members);

#print Dumper($sets->inGroups("-"));

#my $ptr = Coordinate::TextPointer->new;
#$ptr->moveTo(10);
#$ptr->moveTo(12);
#$ptr->moveTo(22);
#print Dumper($ptr->toString);
#$ptr->reset;
#print $ptr->toString;



#my $g = Algorithms::DOGraph->new("directed");
#$g->addVertex("a", "b", "c", "d" , "e", "f","y");
#$g->add_vertex('a');
#$g->add_vertex('b');
#$g->add_vertex('c');
#$g->add_vertex('d');
#$g->addEdge("a", "b", 
#            "a", "c", 
#            "c", "d",
#            "c", "d",
#            "b", "d",
#            "d", "e",
#            "d", "y",
#            );
#$g->add_edge('a', 'c');
#$g->add_edge('a', 'b');
#$g->add_edge('c', 'd');
#$g->add_edge('d', 'a');
#print "has_a_cycle: ". $g->hasCycle . "\n";
#print "is dag: ". $g->isDag . "\n";
#print "is weakly connected: ". $g->graph->is_weakly_connected . "\n";
#print "is strongly connected: ". $g->graph->is_strongly_connected . "\n";
#my @e = $g->graph->edges_from("a");
#print Dumper(@e);
#$g->addRoots('a');
#$g->addRoots('f');
#$g->addRoots('f');
#my @arr = $g->edgesFrom("y");
#print Dumper(@arr);
#$g->markV('a', 1);
#print "is marked: " . $g->isMarked('a') . "\n";
#$g->setVMatchAttr('a', 0);
#print "get matched result: " . $g->getVMatchAttr('a') . "\n";
#$g->setEMatchAttr("a", "b", 1);
#print "get edge matched vale: " . $g->getEMatchAttr("a", "b") . "\n";
#print $g->unmarkedSuccessor("a") . "\n";
#print Dumper($g->graph->all_successors('a'));
#print "get predecessor: " . $g->findPredecessor("d") . "\n";
#print Dumper($g->getAllSuccessors("a"));

#print "g = " . $g->toString() ."\n";
#print Dumper($g);







#my $hash = {
#  "pat_mod_otherline_check_1" => { 
#      "content"    => "",
#      "rank"       => 0,
#      "array"      => ["arr1", "arr2"],
#  }
#};

#my $io = Utils::IO->new;
##$io->writeConfigFile("", "");
#my $fsmHashRef = $io->readConfigFile("fsm_trans.yaml");
#
#print Dumper($fsmHashRef);





#my $fsm = Algorithms::DOFSM->new();
#$fsm->addStates($fsmHashRef);
#$fsm->addtrans;
#$fsm->run;
#my $str = $fsm->fsm->generate_graphviz_code(name=>'fsmGraph', size=> 20);
#
#open my $fh, ">:utf8", "fsm.dot";
#print $fh $str;
#close $fh;

#my @args = (dot=>'-Tpng', "fsm.dot", "-ofsm.png");
#system @args;



#my @hallo = @{Global::Values->allDOTypes};
#print Dumper(@hallo);

#my $pter = Coordinate::TextPointer->new();
#my $pt1 = Coordinate::Point->new(2);
#my $pt2 = Coordinate::Point->new(3);
#my $pt3 = Coordinate::Point->new(12);
#my $pt4 = Coordinate::Point->new(33);
#my $str = "hallo";
#$pter->move($pt1);
#$pter->move($pt2);
#$pter->addToPTHistory($pt3);
#$pter->addToPTHistory($pt4);
#$pter->addToPTHistory($str);

#$pter->move($pt1);
#$pter->showPTHistroy;
#$pter->rmFromPTHistory(2);
#$pter->showPTHistroy;




#print Dumper($pter->getCurPos->getPosition);




#my $io = Utils::IO->new();
#$io->fetchTiedFileArr("d74.txt");
#
#print Dumper(scalar @{$io->tiedFile});


#print Dumper($io->tiedFile);
#$io->setCurFile("d74_test.txt");
#$io->tieCurFile($io->curFile);
#print Dumper(ref $io->tiedFile);


#my $text = Text::EntryText->new("hallo", "ENTRY_TYPE_UNKNOWN");
#$text->setEntryAttr("hasOK", 1);
#print Dumper($text->getEntryAttr("hasOK"));

#my $p1 = Coordinate::Point->new(22);
#my $p2 = Coordinate::Point->new(33);
#print Dumper(Global::Values->globVal->{designPath});

#my $str = "0";
#print $str;

#print Dumper($p1->meta->class_precedence_list);
#print Dumper($p1->meta->superclasses);
#print Dumper(ref $p1);
#my $index = Coordinate::TextIndex->new($p1, $p2);
#$index->setLength(555);
#print Dumper(($index->meta->class_precedence_list)[-1]);
#print Dumper($index->validParameters);
#print Dumper($index->getLength);
#my $str = "adfasdf";
#print Dumper(Utils::Object::isObject($str));

#my $text = TextField->new(name=>"hallo");
#$text->setAttribute({k1=>"v1", k2=>"v2"});
#my %hash = ();
#$text->setAttribute(\%hash);
#my $p2 = Coordinate::Point->new(33);
#print Dumper($text->getAttribute);
#$text->getTextIndex->setBeginIndex($p2);







#package Index;
#use Moose;
#with 'Coordinate::TextIndex';
#sub toString_TextIndex {}
#sub reset_TextIndex {}
#package main;
#use Coordinate::Point;
#my $index = Index->new();
#my $p1 = Coordinate::Point->new();
#$index->setEndIndex($p1);
##print Dumper($index->getEndIndex->getX);
#$p2->clear;
#print Dumper($p2->getX);

#use Coordinate::TextPointer;


#my $point1 = Moose::Mouse::Point3D->new(x=>5, y=>7, z=> 19);
#my $point1 = IceFir::Coordinate::PointX->new( {x => 5.2} );
#$point1->toString();
#$point1->clear();
#print $point1->hasX() ? "then" : "else";
#print $point1->getLocation();
#$point1->setLocation(3);
#$point1->toString();

#my $pt = IceFir::Coordinate::PointX->new();
#print $pt->getX();

#my $textPt = Coordinate::TextPointer->new();
#say $textPt->is_moved ? "is moved" : "not moved";
#$textPt->move;
#say $textPt->is_moved ? "is moved" : "not moved";
#say $textPt->does("Coordinate::Movable");



#package Shirt;
#use Moose;
#use namespace::autoclean;
#use Utils::Library;
#has 'color' => (
#  is => 'rw',
#  isa => 'ENTRY_TYPE',
#  reader => 'getColor',
#  writer => 'setColor',
#);
#package NewMain;
#use Moose;
#use namespace::autoclean;
#use Shirt;
#my $shirt = Shirt->new(color=>"ENTRY_TYPE_SEITE");
#print $shirt->getColor;

#print Utils::Library::ENTRY_PART->{COL};


#package TextField;
#use Moose;
##use MooseX::Types::Moose qw(Str Int HashRef ArrayRef);
##use MooseX::Types::Structured qw(Dict Tuple Optional);
#extends "Text::AbstractText";
#with 'Text::TextFieldRole';
#sub update {}
#sub remove {}
#sub clear {}
#sub toString {}
#sub reset {}
#package main;
#my $text = TextField->new({name => "hallo"});
#my $text1 = TextField->new ({name=> "hallo1"});
#$text->setAttribute({key1 =>'value1'});
##print Dumper($text->getAttribute());
#my $str = "strstrstr";
#my @arr;
#$arr[0] = 9;
#$arr[1] = $text1;

# @arr = (9, $text1, 10);
#my $hash = {k1 => 'v1', k2=>'v2'};
#my @arr = qw($str);
#$text->content([
#  id => 10,
#  color => "blue",
#  other_id => 1,
#]);


#$text->setName('nihao');
#$text->setContent(\@arr);
#$text->setContent1($hash);
#print ref($text->getContent1);
#print ref($textX);

#print $textX->getName();


#my $meta = ref($textX)->meta();
##print Dumper($meta);
##$meta->calculate_all_roles; #e.g. Text::Breakable|Coordinate::Movable
##$meta->superclasses; # Text::AbstractTextField
#for my $attr ($meta->get_all_attributes) {
#  print Dumper($attr->name), "\n";  
#}

#print Dumper($textX->getContent);


1;


#__DATA__
#<entry id="p00002" bd="73" se="523" sp="re" ze="06"><l>Pacht</l>: Ueb. d. Regelg. h> ulo d. ~zinses v. landw. ~gn. (Th. Haas) <s>490c.</s> XI. 214 <verweis"> -- s. a. Gewand</verweis>  <ok />



__END__

=pod

=encoding UTF-8






