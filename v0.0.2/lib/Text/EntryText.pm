# EntryText has interID as identity too.
package Text::EntryText;
=encoding utf8

=head1 NAME

Text::EntryText - Entry text object

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package extends Text::AbstrctText. It stores all useful information of row text to analyse.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use Carp ;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;
use Clone;

use Coordinate::TextIndex;
use Utils::Tools;
use Global::Values;

use Text::EntryPartText;

with 'Text::AbstractText';

enum 'ENTRY_TEXT_TYPE', [@{Global::Values->entryTextType}];

=head1 CONSTRUCTORS

=head2 BUILDARGS

In new.

=cut
around 'BUILDARGS' => sub {
  my $orig  = shift;
  my $class = shift;
  
  # There is no parameter
  if ( @_ == 0 ) {

    return $class->$orig(  id => "", type => "ENTRY_TYPE_UNKNOWN");
  }
  elsif ( @_ == 2 ) {

    # There is one parameter which 'x' is.
    my ($id, $type) = @_;   
    return $class->$orig( id => $id , type => $type);
  }
  else {
    # normal input
    # Point->new(@list_or hashref)
    return $class->$orig(@_);
  }
};

=head2 BUILD

After new.

=cut
sub BUILD {
  my $self = shift;
  # initialing Entry Attribute
  $self->initEntryAttr;
  $self->resetHeaderInfo;
}

=head2 _build_entryheaderinfo

=cut
sub _build_entryheaderinfo {
#  my $hash = Clone::clone(Global::Values->entryHeaderAttrHash);
  my $hash = Global::Values->entryHeaderAttrHash;
  return $hash;
}

=head1 ATTRIBUTES

=head2 headerInfo

For header information: #headerIDNummer, headerIDBand, headerIDSeite, headerIDSpalte, headerIDZeile 

=cut
has 'headerInfo' => (
  is => 'rw',
  isa => 'HashRef',
  predicate=> "hasHeaderInfo",
  clearer => 'resetHeaderInfo',
  reader => 'getHeaderInfo',
  writer => 'setHeaderInfo',
  builder => "_build_entryheaderinfo",
  lazy => 1,
);
=head2 extraInfo

for some special information.

=cut
has 'extraInfo' => (
  is => 'rw',
  isa => 'HashRef[Str]',
  #predicate => "hasExtraInfo",
  reader => 'getExtraInfo',
  writer => 'setExtraInfo',
  clearer => 'resetExtraInfo',
  default => sub {{}},
  lazy => 1,
);
=head2 entryType

Type of the class (ENTRY_TEXT_TYPE: Global::Values->entryTextType)

=cut
has 'entryType' => (
  is        => 'rw',
  isa       => 'ENTRY_TEXT_TYPE',
  predicate => 'hasEntryType',
  clearer   => 'resetEntryType',
  writer  => 'setEntryType',
  reader  => 'getEntryType',
  default => 'ENTRY_TYPE_UNKNOWN',
  lazy => 1,
);

=head2 origText

the original text to analyse

=cut
has 'origText' => (
  is        => 'rw',
  isa       => 'Str',
  predicate => 'hasOrigText',
  clearer   => 'resetOrigText',
  writer  => 'setOrigText',
  reader  => 'getOrigText',
);

=head2 origEntryText

the original text of entire entry element(s).

=cut
has 'origEntryText' => (
  is        => 'rw',
  isa       => 'Str',
  predicate => 'hasOrigEntryText',
  clearer   => 'resetOrigEntryText',
  writer  => 'setOrigEntryText',
  reader  => 'getOrigEntryText',
);

=head2 rowNumber

the row number in the text file

=cut
has 'rowNumber' => (
  is        => 'rw',
  isa       => 'Int',
  predicate => 'hasRowNumber',
  clearer   => 'resetRowNumber',
  writer  => 'setRowNumber',
  reader  => 'getRowNumber',
);

#has 'textPointer' => (
#  is => 'rw',
#  isa => 'Coordinate::TextPointer',
#  predicate => 'hasTextPointer',
#  clearer => 'resetTextPointer',
#  builder => '_build_textpointer',
#  lazy => 1,
#  writer => 'setTextPointer',
#  reader => 'getTextPointer',
#);
=head2 problemReports

The problem reports list

=cut
has 'problemReports' => (
  is => 'rw',
  isa => 'ArrayRef[Str]',
  predicate => 'hasProblemReports',
  clearer => 'resetProblemReports',
  default => sub {[]},
  lazy => 1,
  writer => 'setProblemReports',
  reader => 'getProblemReports',
);

=head2 content

=cut
has 'content' => (
  is        => 'rw',
  isa       => 'ArrayRef[Object|Str]',
  predicate => 'hasContent',
  clearer   => 'clearContent',
  writer    => "setContent",
  reader    => "getContent",
);

=head2 partTextMap

A hashmap for storaging the Objects (Text::EntryPartText) with their types.

For example: TAG_ELEMENT => [EntryPartText_1, EntryPartText_2, ...]

=cut
has 'partTextMap' => (
  is        => 'rw',
  isa       => 'HashRef',
  predicate => 'hasPartTextMap',
  clearer   => 'clearPartTextMap',
#  writer    => "setPartTextMap",
#  reader    => "getPartTextMap",
  default => sub {{}},
  lazy => 1,
);
=head2 severity

The severity level

=cut
has 'severity' => (
  is => 'rw',
  isa => 'Int',
  default => 0,
  lazy => 1,
  clearer => 'resetSeverity',
  writer => 'setSeverity',
  reader => 'getSeverity',
);
=head2 tempIndex

The temporary TextIndex

=cut
has 'tempIndex' => (
  is        => 'rw',
  isa       => 'Coordinate::TextIndex',
  predicate => 'hasTempIndex',
  clearer   => 'clearTempIndex',
  builder   => '_build_textindex',
  lazy      => 1,
  writer    => 'setTempIndex',
  reader    => 'getTempIndex',
);

=head1 METHODS


=head2 getTempIndices()

Returns the beginning and ending indices of its temp TextIndex system.

=cut
sub getTempIndices {
  my $self      = shift;
  return $self->getTempIndex->getTextIndices;
}

=head2 updateTempIndex($inputIndex)

Update the tempIndex with the given TextIndex.

=cut

sub updateTempIndex {
  my $self = shift;
  my $inputIndex = shift;
  
  return if (! defined $inputIndex || $inputIndex eq "");
  
  my ($begin, $end) = $inputIndex->getTextIndices;
  
  $self->getTempIndex->setBeginWithPos($begin);
  $self->getTempIndex->setEndWithPos($end);
}


=head2 reset

=head3 Overrides:

reset in class Text::AbstractText

=cut
around 'reset' => sub {
  my $next = shift;
  my $self = shift;
  
  $self->$next();
  $self->resetHeaderInfo();
  $self->resetExtraInfo();
  # only for entry text attribute
  $self->initEntryAttr();
};

=head2 initEntryPartAttr()

Initializes the attributes of this object

=cut
sub initEntryAttr {
  my $self = shift;
  #CONSIDER: sic -> corr
  # content without HTML-Tags as oringal text
  Global::Values->resetEntryAttrHash;
#  my $hash = Clone::clone(Global::Values->entryAttrHash);
  my $hash = Global::Values->entryAttrHash;
  
  $self->setAttrHash($hash);
  return 1;
}

=head2 beginIndexOfFirstReference()

Returns the beginning index of the first reference or reference lemma.

=cut
sub beginIndexOfFirstReference {
  my $self = shift;
  
  #VERWEIS_ELEMENT, VERWEIS_LEMMA_ELEMENT
  my @refArr;
  if ($self->getEntryTextAttr("hasReference") ){
    # defined or empty
    push @refArr, (@{$self->getPartTextsFromMap("VERWEIS_ELEMENT") // []} ) ;
  }
  if ($self->getEntryTextAttr("hasReferenceLemma")) {
    # defined or empty
    push @refArr, @{$self->getPartTextsFromMap("VERWEIS_LEMMA_ELEMENT") // []} ;
  }
  my $beginIndex = 9999999999;
  for my $partText (@refArr) {
    my ($beginPos, $endPos) = $partText->getTextIndices;
    if ($beginPos < $beginIndex) {$beginIndex = $beginPos;}
  }
  return if $beginIndex == 9999999999;
  return $beginIndex;
}

=head2 setMaxSeverity($severity)

Compare the given severity and set the big one to the severity level of class.

=cut
sub setMaxSeverity {
  my $self = shift;
  my $severity = shift;
  if(! defined $severity || $severity eq '' 
    || ! Scalar::Util::looks_like_number($severity)) {
    croak "There is no valid input severity level!";
  }
  my $origSeverity = $self->getSeverity;
  if ($severity > $origSeverity) {
    $origSeverity = $severity;
    $self->setSeverity($origSeverity);
  } 
}

=head2 getUnknownPart($inputTextRef)

Check the given input text and returns its non-recognizable parts.

=cut
sub getUnknownPart {
  my $self = shift;
  my $inputTextRef = shift;
  return if (! defined $inputTextRef || $inputTextRef eq "" || $$inputTextRef eq "");
  my $origText = $$inputTextRef;
  my $newText = $origText;
  my $partTextMap = $self->partTextMap;
  # !! Attention: double loop!!!
  my @partTypes = keys %$partTextMap; # keys
  foreach my $partType (@partTypes) {
    my $partTextArrRef = $partTextMap->{$partType};
    for my $partText (@$partTextArrRef) {
      my ($beginPos, $endPos) = $partText->getTextIndices;
      my $len = $endPos - $beginPos;
      if ($len <= 0) {croak "There is some thing wrong in the text indices!";}
      my $blank = " " x $len;
      substr($newText, $beginPos, $len, $blank);
    }
  }
#  while (my ($partType, $partTextArrRef) = each %$partTextMap) {
#    for my $partText (@$partTextArrRef) {
#      my ($beginPos, $endPos) = $partText->getTextIndices;
#      my $len = $endPos - $beginPos;
#      if ($len <= 0) {croak "There is some thing wrong in the text indices!";}
#      my $blank = " " x $len;
#      substr($newText, $beginPos, $len, $blank);
#    }
#  } 
  if ($newText =~ /^\s+$/) {
    return "";
  } else {
    return \$newText;
  }
}

=head2 addPartsToMap($partTextArrRef)

Add the given list of Text::EntryPartText

=cut
sub addPartsToMap {
  my $self = shift;
  my $partTextArrRef = shift;
  return if (! defined $partTextArrRef || $partTextArrRef eq "");
  
  for my $part (@$partTextArrRef) {
    $self->addPartTextToMap($part);
  }
  return 1;
}
=head2 addPartTextToMap($partTextArrRef)

Add the given Text::EntryPartText, see 'partTextMap'

=cut
sub addPartTextToMap {
  my $self = shift;
  my $partText = shift; # Text::EntryPartText
  
  if (! defined $partText || $partText eq "" 
  || ! Utils::Tools->isBlessed($partText, "Text::EntryPartText")) {
    croak "There is no valid input entry part text!";
  }
  
  my $pType = $partText->getEntryPartType;
  
  if (exists $self->partTextMap->{$pType}) {
    my $arrRef = $self->partTextMap->{$pType};
    my $sameIn = 0;
    for my $item (@$arrRef) {
      $sameIn = $partText->hasSameAttr($item);
      last if $sameIn;
    }
    # same text part in the array
    return if $sameIn; 
    push @$arrRef, $partText;
    $self->partTextMap->{$pType} = $arrRef;
  } else {
    my @arr = ();
    push @arr, $partText;
    $self->partTextMap->{$pType} = \@arr;
  }
  return 1;
}
=head2 getPartTextsFromMap($pType)

Returns the list of Text::EntryPartText by the given part text type, see 'partTextMap'

=cut
sub getPartTextsFromMap {
  my $self = shift;
  my $pType = shift;
  return $self->partTextMap->{$pType};
}

=head2 addToProblemReports($input)

Add the input problem information to 'problemReports'.

=cut
sub addToProblemReports {
  my $self = shift;
  my $input = shift;
  
  return if (! defined $input || $input eq "");
  
  my $resArrRef = $self->getProblemReports;
  if (ref ($input) eq "ARRAY") {
    push @$resArrRef, @$input;
  } elsif (ref ($input) eq "") {
    push @$resArrRef, $input;
  }
  $self->setProblemReports($resArrRef);
  return 1
}

=head2 getPartTextRefWithTextIndex($inputTextRef)

Returns the part string of original text by the indices of TextIndex.

=head3 Parameters:

=over 4

=item *

$inputTextRef: the given text string reference.

=back

=cut
sub getPartTextRefWithTextIndex {
  my $self = shift;
  my $inputTextRef = shift;
  # valid parameters
  return if (! defined $inputTextRef || $inputTextRef eq "");
  # take the indices of TextIndex
  my ($beginIdx, $endIdx) = $self->getTextIndex->getTextIndices;
    if (! defined $beginIdx || $beginIdx eq '' 
    || ! Scalar::Util::looks_like_number($beginIdx) 
    || ! defined $endIdx || $endIdx eq '' 
    || ! Scalar::Util::looks_like_number($endIdx) 
  ) {
    croak "There is no valid beginIndex or end index";
  }
  return $self->getPartTextWithIndices($inputTextRef, $beginIdx, $endIdx);
}





=head2 getPartTextWithIndices($inputTextRef, $begin, $end)

Returns the part string of original text by given beginning and ending indices.

=head3 Parameters:

=over 4

=item *

$inputTextRef: the given text string reference.

=item *

$begin: the given beginning index.

=item *

$end: the given ending index.

=back

=cut
sub getPartTextWithIndices {
  my $self = shift;
  my $inputTextRef = shift; # origText or origContentText
  my $begin = shift;
  my $end = shift;
  if ( ! defined $inputTextRef || $inputTextRef eq ""
    ||! defined $begin || $begin eq '' 
    || ! Scalar::Util::looks_like_number($begin) 
    || ! defined $end || $end eq '' 
    || ! Scalar::Util::looks_like_number($end)
    || $begin >= $end 
  ) {
    carp "There is no valid beginIndex $begin or endIndex $end.";
    return;
  }
  my $len = $end - $begin;
  my $partText = substr($$inputTextRef, $begin, $len);
  # return the text reference
  return \$partText;
}

=head2 initTextIndex()

Initializes the Coordinate::TextIndex of this object

=cut
sub initTextIndex {
  my $self = shift;
  
  if (!$self->hasOrigText) {
    carp "There is no valid original Text";
    return;
  }
  
  my $text = $self->getOrigText;
  my $textLen = length $text;
  my $beginIndex = 0;
  
  # update text index
  $self->getTextIndex->setTextIndicesWithText(\$text, $beginIndex);
  
  return 1;
}

# update the new position to the text pointer
#sub updateTextPointer {
#  my $self = shift;
#  my $newPos = shift;
#  my $tper = $self->getTextPointer;
#  $tper->moveTo($newPos);
#  $self->setTextPointer($tper);
#}

=head2 setEntryID($id)

Sets the id of this Text::EntryText.

=cut
sub setEntryID {
  my $self = shift;
  my $id = shift;
  if (! defined $id || $id eq ""){
    carp "There is no valid Entry ID!";
    return;
  }
  $self->setID($id);
}
=head2 getEntryID($id)

Returns id of this Text::EntryText.

=cut
sub getEntryID {
  my $self = shift;
  return $self->getID;
}

=head2 addContent($partText)

Add the given Text::EntryPartText to the 'content'.

=cut
sub addContent {
  my $self = shift;
  my $partText = shift;
  #my $cArr = $self->content;
  if(! defined $partText || $partText eq "" 
    || ! Utils::Tools->isBlessed($partText, "Text::EntryPartText")) {
    carp "This given $partText is a not valid entry part text";
    return;
  }
  my $arrRef = $self->getContent;
  push @$arrRef, $partText;
  
  $self->setContent($arrRef);  
  
  return 1;
}

=head2 setEntryTextAttr($attrName, $attrValue)

Sets the attribute hashmap with the given name and value of attributes.

=cut
sub setEntryTextAttr {
  my $self = shift;
  my $attrName = shift;
  my $attrValue = shift;
  
  if (exists $self->getAttrHash->{$attrName}) {
    $self->getAttrHash->{$attrName} = $attrValue;
    return 1;
  } else {
    carp "$attrName is not in the Attribute of $self!";
    return;
  }
}
=head2 getEntryTextAttr($attrName)

Returns attribute value from the attribute hashmap by the given attribute name.

=cut
sub getEntryTextAttr {
  my $self = shift;
  my $attrName = shift;
  
  if (exists $self->getAttrHash->{$attrName}) {
    return $self->getAttrHash->{$attrName};
  } else {
    carp "$attrName is not in the Attribute of $self!";
    return;
  }
}

=head2 setEntryHeaderAttr($attrName, $attrValue)

Sets the attribute hashmap with the given name and value of header information.

=cut
sub setEntryHeaderAttr {
  my $self = shift;
  my $attrName = shift;
  my $attrValue = shift;
  
  if (exists $self->getHeaderInfo->{$attrName}) {
    $self->getHeaderInfo->{$attrName} = $attrValue;
    return 1;
  } else {
    carp "$attrName is not in the Attribute of $self!";
    return;
  }
}
=head2 getEntryHeaderAttr($attrName)

Returns header information from the attribute hashmap by the given name of header information.

=cut
sub getEntryHeaderAttr {
  my $self = shift;
  my $attrName = shift;
  
  if (exists $self->getHeaderInfo->{$attrName}) {
    return $self->getHeaderInfo->{$attrName};
  } else {
    carp "$attrName is not in the Attribute of $self!";
    return;
  }
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;
