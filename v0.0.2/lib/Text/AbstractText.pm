package Text::AbstractText;
=encoding utf8

=head1 NAME

Text::AbstractText - The abstract class of entry text object.

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This is abstract superclass to store all information in the entry text. 

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose::Role;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Coordinate::TextIndex;
use Utils::Tools;
use Global::Values;

enum 'TEXT_PART', [ @{ Global::Values->textType } ];

=head1 CONSTRUCTORS

=head2 BUILDARGS

In new.

=cut
around 'BUILDARGS' => sub {
  my $orig  = shift;
  my $class = shift;

  # There is no parameter
  if ( @_ == 0 ) {
    return $class->$orig( { id => "" } );
  }
  elsif ( @_ == 1 ) {
    # There is one parameter which 'x' is.
    my ($id) = @_;
    return $class->$orig( id => $id );
  }
  else {
    # normal input
    # Point->new(@list_or hashref)
    return $class->$orig(@_);
  }
};
=head2 _build_textindex

=cut
sub _build_textindex {
  return Coordinate::TextIndex->new;
}

=head1 ATTRIBUTES

=head2 id

=cut
has 'id' => (
  is        => 'rw',
  isa       => 'Str',
  predicate => 'hasID',
  clearer   => 'resetID',
  writer    => 'setID',
  reader    => 'getID',
  default   => '',
  lazy      => 1,
);


=head2 textIndex

The position coordinate system of the given text string.

=cut
has 'textIndex' => (
  is        => 'rw',
  isa       => 'Coordinate::TextIndex',
  predicate => 'hasTextIndex',
  clearer   => 'clearTextIndex',
  builder   => '_build_textindex',
  lazy      => 1,
  writer    => 'setTextIndex',
  reader    => 'getTextIndex',
  #handles => [qw(setTextIndicesWithText setTextIndicesWithBeginLength getTextIndices)],
);

#has 'contentTextIndex' => (
#  is        => 'rw',
#  isa       => 'Coordinate::TextIndex',
#  predicate => 'hasContentTextIndex',
#  clearer   => 'clearContentTextIndex',
#  builder   => '_build_textindex',
#  lazy      => 1,
#  writer    => 'setCTextIndex',
#  reader    => 'getCTextIndex',
#  #handles => [qw(setTextIndicesWithText setTextIndicesWithBeginLength getTextIndices)],
#);

#has 'textType' => (
#  is        => 'rw',
#  isa       => 'TEXT_PART',
#  predicate => 'hasTextType',
#  clearer   => 'clearTextType',
#  writer    => 'setTextType',
#  reader    => 'getTextType',
#);

=head2 attrHash

The attribute or information of this entry text.

=cut
has 'attrHash' => (
  is        => 'rw',
  isa       => 'HashRef[Str]',
  predicate => 'hasAttrHash',
  clearer   => 'resetAttrHash',
  writer    => 'setAttrHash',
  reader    => 'getAttrHash',
  default   => sub { {} },
  lazy      => 1,
);

# attribute keys ???
# 0. indcludeIn:  (from which Entry: value is interID)
# 1. partOf:    (Entry part name)
# 2. contain:   (which parts has it)
# 3. DateOfpub: (abbr. Date of publication)
# 4. id:        (???)
# 5. textType:      (Global::Values::TEXT_TYPE)
# 5. lineInTxt: (module Tie::File)
# 6. verweisLemma:    (???)
# 7. verweis:         (???)
# 8. interID: (only for Entry, LemmaEntry, Strecke)
# 9. synonymen:       (???)
# 10. :

=head1 METHODS

=head2 isBeginEndInOnePoint

check whether the beginning and ending indices are in same point.

=cut
sub isBeginEndInOnePoint {
  my $self = shift;
  
  my $textIndex = $self->getTextIndex;
  
  return $textIndex->isInOnePoint;
  
}

=head2 setBeginPosOfTextIndex($begin)

Sets the beginning index with the given $begin.

=cut
sub setBeginPosOfTextIndex {
  my $self      = shift;
  my $begin     = shift;
  return if (! defined $begin || $begin eq "");
  my $textIndex = $self->getTextIndex;
  $textIndex->setBeginWithPos($begin);
  $self->setTextIndex($textIndex);
  return 1;
}
=head2 setEndPosOfTextIndex($end)

Sets the ending index with the given $begin.

=cut
sub setEndPosOfTextIndex {
  my $self      = shift;
  my $end       = shift;
  return if (! defined $end || $end eq "");
  my $textIndex = $self->getTextIndex;
  $textIndex->setEndWithPos($end);
  $self->setTextIndex($textIndex);
  return 1;
}

sub setLengthOfTextIndex {
  my $self      = shift;
  my $len       = shift;
  my $textIndex = $self->getTextIndex;
  $textIndex->setLength($len);
  $self->setTextIndex($textIndex);
  return 1;
}

=head2 getTextIndices()

Returns the beginning and ending indices of its TextIndex system.

=cut
sub getTextIndices {
  my $self      = shift;
  return $self->getTextIndex->getTextIndices;
}


=head2 reset

Resets the class to its initial, empty state.

=cut
sub reset {
  my $self = shift;

  #  my @arr = ();
  #$self->resetAttribute;
  $self->resetAttrHash;
  $self->clearTextType;
  $self->clearTextIndex;
  $self->clearContentTextIndex;
  $self->clearContent;
  $self->resetID;
}

# Return 0/1; # false or true
sub hasObjectInContent {
  my $self       = shift;
  my @contentArr = $self->getContent();
  my $hasObject  = 0;
  for my $item (@contentArr) {

    # only "Str" and "Object"
    if ( Utils::Tools::isObject($item) ) {
      $hasObject = 1;
      last;
    }
  }
  return $hasObject;
}

no Moose;
1;

