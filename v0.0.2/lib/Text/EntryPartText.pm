package Text::EntryPartText;
=encoding utf8

=head1 NAME

Text::EntryPartText - Part text object in the Text::EntryText.

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

use Coordinate::TextIndex;
use Utils::Tools;

use Global::Values;

with 'Text::AbstractText';

enum 'ENTRY_TEXT_PART', [ @{ Global::Values->entryTextPartType } ];

=head1 CONSTRUCTORS

=head2 BUILDARGS

In new.

=cut
around 'BUILDARGS' => sub {
  my $orig  = shift;
  my $class = shift;

  # There is no parameter
  if ( @_ == 0 ) {
    return $class->$orig( id => "", type => "UNKNOWN_PART" );
  }
  elsif ( @_ == 2 ) {
    # There is one parameter which 'x' is.
    my ( $id, $type ) = @_;
    return $class->$orig( id => $id, type => $type );
  }
  else {
    # normal input
    # Point->new(@list_or hashref)
    return $class->$orig(@_);
  }
};
=head2 reset

=head3 Overrides:

reset in class Text::AbstractText

=cut
around 'reset' => sub {
  my $next = shift;
  my $self = shift;

  $self->$next();
  $self->initEntryPartAttr();
  $self->type("UNKNOWN_PART");
};

=head1 ATTRIBUTES

=head2 entryPartType

Type of the class (ENTRY_TEXT_PART: Global::Values->entryTextPartType)

=cut
has "entryPartType" => (
  is        => "rw",
  isa       => "ENTRY_TEXT_PART",
  predicate => 'hasEntryPartType',
  clearer   => 'resetEntryPartType',
  writer => 'setEntryPartType',
  reader => 'getEntryPartType',
  default   => 'UNKNOWN_PART',
  lazy      => 1,
);
=head2 parentEntryID

the parent(Text::EntryText) id

=cut
has "parentEntryID" =>(
  is => 'rw',
  isa => 'Str',
  predicate => 'hasParentEntryID',
  clearer => 'clearParentEntryID',
  writer => 'setParentEntryID',
  reader => 'getParentEntryID',
);
=head2 textPart

string text

=cut
has "textPart" => (
  is => 'rw',
  isa => 'Str',
  predicate => 'hasTextPart',
  clearer => 'clearTextPart',
  writer => 'setTextPart',
  reader => 'getTextPart',
);
=head2 content

string

=cut
has 'content' => (
  is        => 'rw',
  isa       => 'Str',
  predicate => 'hasContent',
  clearer   => 'clearContent',
  writer    => "setContent",
  reader    => "getContent",
);

=head1 METHODS

=head2 hasSameAttr($inputTextPart)

checks whether the given Text::EntryPartText has the same attribute of current part text.

=head3 Returns:

returns true if they have the same attributes, return false otherwise.

=cut
sub hasSameAttr {
  my $self = shift;
  my $inputPartText = shift;
  
  if(! defined $inputPartText || $inputPartText eq "" 
    || ! Utils::Tools->isBlessed($inputPartText, "Text::EntryPartText")) {
    croak "This given $inputPartText is a not valid entry part text";
  }
  # 1. textIndex
  if ($self->getTextIndex->isSameBeginEnd($inputPartText->getTextIndex)) {
    
    if ($self->getTextPart eq $inputPartText->getTextPart){
        #carp "These 2 EntryPartText: $self->getEntryPartID, $inputTextPart->getEntryPartID are Same!";
        return 1;
    } else {
      carp "These 2 EntryPartText: $self->getEntryPartID, $inputPartText->getEntryPartID have same Index but different Textpart or Content!";
      return 1;
    }
  }
  return 0;
}

=head2 setEntryPartID($id)

Sets the id of this Text::EntryPartText.

=cut
sub setEntryPartID {
  my $self = shift;
  my $id = shift;
  if (! defined $id || $id eq ""){
    carp "There is no valid Entry PartText ID!";
    return;
  }
  $self->setID($id);
}

=head2 getEntryPartID()

Returns id of this Text::EntryPartText.

=cut
sub getEntryPartID {
  my $self = shift;
  return $self->getID;
}

=head2 initEntryPartAttr()

Initializes the attributes of this object

=cut
sub initEntryPartAttr {
  my $self = shift;

  #CONSIDER: sic -> corr
  # content without HTML-Tags as oringal text
  my $hash = Global::Values->entryTextAttrHash;

  $self->attrHash($hash);
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;
