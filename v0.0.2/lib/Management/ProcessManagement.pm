package Management::ProcessManagement;
=encoding utf8

=head1 NAME

Management::ProcessManagement - The main manager of system performance.

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;
use Clone;
use File::Basename;
use Scalar::Util;

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";


use Global::Values;
use Utils::IO;
use Database::MotDBProcessor;
use Database::DBProcessor;

use State::StateProcessor;
use Text::EntryText;

=head1 CONSTRUCTORS

=head2 _build_io

Sets IO system.

=cut
sub _build_io {
  my $self = shift;
  return Utils::IO->getInstance;
}

=head1 ATTRIBUTES

=head2 io

IO system.

=cut
has 'io' => (
  is => 'ro',
  isa => 'Utils::IO',
  builder => '_build_io',
  clearer => "clearIO",
  predicate => 'hasIO',
);
=head2 inputFileName

=cut
has 'inputFileName' => (
  traits => ['String'],
  is => "rw",
  isa => "Str",
  default => q{},
  handles => {
    addFileName => "append",
    chompFileName => 'chomp',
    clearFileName => 'clear',
    fileNameLength => 'length',
  },
); 

=head2 rowNumBegin

the beginning row number to be parsed

=cut
has 'rowNumBegin' => (
  traits => ['Number'],
  is => 'ro',
  isa => 'Int',
  predicate => 'hasRowNumBegin',
  clearer => 'resetRowNumBegin',
  default => 0,
  handles => {
    setRowNumBegin => 'set',
    addRowNumBegin => 'add',
    subRowNumBegin => 'sub',
  },
);
=head2 rowNumEnd

the ending row number to be parsed

=cut
has 'rowNumEnd' => (
  traits => ['Number'],
  is => 'ro',
  isa => 'Int',
  predicate => 'hasRowNumEnd',
  clearer => 'resetRowNumEnd',
  default => 0,
  handles => {
    setRowNumEnd => 'set',
    addRowNumEnd => 'add',
    subRowNumEnd => 'sub',
  },
);
=head2 preEntryText

the previous parsed Text::EntryText

=cut
has 'preEntryText' =>(
  is => 'rw',
  isa => 'Text::EntryText | Undef',
  predicate => 'hasPreEntryText',
  clearer => 'clearPreEntryText',
  reader => "getPreEntryText",
  writer => "setPreEntryText",
);
=head2 curEntryText

the current parsing Text::EntryText

=cut
has 'curEntryText' =>(
  is => 'rw',
  isa => 'Text::EntryText | Undef',
  predicate => 'hasCurEntryText',
  clearer => 'clearCurEntryText',
  reader => "getCurEntryText",
  writer => "setCurEntryText",
);

=head2 curEntrtTextDao

the current DAO for database and EntryText

=cut
has 'curEntrtTextDao' =>(
  is => 'rw',
  isa => 'Database::DBDao',
  predicate => 'hasCurEntrtTextDao',
  clearer => 'clearCurEntrtTextDao',
  reader => "getCurEntrtTextDao",
  writer => "setCurEntrtTextDao",
);



=head1 METHODS

=head2 main($bandName, $fileNameRef, $createTable, $tableName, $rowNumBegin, $rowNumEnd)

Core!

Analyse the entire file and save the result to the database.

=head3 Paramters:

=over 4

=item *

$bandName: the given band name

=item *

$fileNameRef: the given reference of file name 

=item *

$createTable: boolen value, whether create a new table or not

=item *

$tableName: the possible table name to be created

=item *

$rowNumBegin: the beginning row number will be parsed

=item *

$rowNumBegin: the ending row number will be parsed

=back

=cut
sub main {
  my $self = shift;
  my $bandName = shift;
  my $fileNameRef = shift;
  my $createTable = shift; # 1/0
  my $tableName = shift;
  my $rowNumBegin = shift;
  my $rowNumEnd = shift;
  
  #1.
  $self->prepareProcess($bandName, $fileNameRef, $createTable, $tableName, $rowNumBegin, $rowNumEnd);
  
  my $beginNum = $self->rowNumBegin;
  my $endNum = $self->rowNumEnd;
  
  # ATTENTION: Loop
  while ($endNum >= $beginNum) {
    $self->process();
    $self->databaseHandle();
    
    print "row " . $beginNum . " done!\n" ;
#    if ($beginNum == 46) {
#      print Dumper("haha");
#    }
    $beginNum++;
    $self->setRowNumBegin($beginNum);
  }
  $self->getCurEntrtTextDao->addBatchToDB($tableName);
}

##############
# preparation
##############
=head2 prepareProcess($bandName, $fileNameRef, $createTable, $tableName, $rowNumBegin, $rowNumEnd)

Prepare all the parameters to be analysed.

=head3 Paramters:

=over 4

=item *

$bandName: the given band name

=item *

$fileNameRef: the given reference of file name 

=item *

$createTable: boolen value, whether create a new table or not

=item *

$tableName: the possible table name to be created

=item *

$rowNumBegin: the beginning row number will be parsed

=item *

$rowNumBegin: the ending row number will be parsed

=back

=cut
sub prepareProcess {
  my $self = shift;
  my $bandName = shift;
  my $fileNameRef = shift;
  my $createTable = shift; # 1/0
  my $tableName = shift;
  my $rowNumBegin = shift;
  my $rowNumEnd = shift;
  
  $self->setBandName($bandName);
  $self->setInputFileName($fileNameRef);
  
  if (defined $createTable && $createTable) {
    my $dbu = Database::DatabaseUtils->getInstance;
    $dbu->createTable($tableName, Global::Values->doEntryStmts->{$tableName});
  }
  
  $self->setNewEntryTextDao;
  
  # row number
  $self->setNewRowNum($rowNumBegin, $rowNumEnd);
}

=head2 setInputFileName($fileNameRef)

Sets the input file name for input file

=head3 Paramters:

=over 4

=item *

$fileNameRef: the given reference of file name 

=back

=cut
sub setInputFileName {
  my $self = shift;
  my $fileNameRef = shift;
  if (! defined $fileNameRef || $fileNameRef eq "") {
    croak "There is no valid file name reference!";
  }
  my $fbName = File::Basename::basename($$fileNameRef);
  
  # tie the input file
  $self->io->fetchTiedFileArr($fbName);
  # get the file name for id generate
  my $dbtApp = Database::MotDBTableApp->getInstance;
  $dbtApp->updateFileName($fbName);
  
  return 1;
}
=head2 setBandName($bn)

Sets the band name

=head3 Paramters:

=over 4

=item *

$bn: the given band name 

=back

=cut
sub setBandName {
  my $self = shift;
  my $bn = shift;
  if (! defined $bn || $bn eq "") {
    croak "There is no valid file name reference!";
  }
  my $dbtApp = Database::MotDBTableApp->getInstance;
  $dbtApp->updateBandName($bn);
  return 1;
}
=head2 setNewRowNum($rowNumBegin, $rowNumEnd)

Sets the boundary of the parse area

=head3 Paramters:

=over 4

=item *

$rowNumBegin: the beginning row number will be parsed

=item *

$rowNumBegin: the ending row number will be parsed

=back

=cut
sub setNewRowNum {
  my $self = shift;
  my $rowNumBegin = shift;
  my $rowNumEnd = shift;
  
  if (! defined $rowNumBegin || $rowNumBegin eq "") {
    $rowNumBegin = 0;
  }
  if (! defined $rowNumEnd || $rowNumEnd eq "") {
    $rowNumEnd = $self->io->getFileRowCount;
    $rowNumEnd--;
  }
  
  
  if ($rowNumBegin < 0 || $rowNumEnd >= $self->io->getFileRowCount) {
    croak "There is out of text boundary";
  } 
  $self->setRowNumBegin($rowNumBegin);
  $self->setRowNumEnd($rowNumEnd);
  return 1;
}

# for input
=head2 setNewEntryTextDao()

DAO

=cut
sub setNewEntryTextDao {
  my $self = shift;
  my $dbMger = Database::DatabaseManager->getInstance;
  my $entryTextDao = $dbMger->getDaoImpl("ENTRYTEXT_DAO_IMPL");
  $self->setCurEntrtTextDao($entryTextDao);
  return 1;
}


##############
# process
##############
=head2 process()

Analysis Process.

=cut
sub process {
  my $self = shift;
  # analyse
  $self->analyseRowText;
  $self->analysePostProcess;
}

# for process
=head2 getTextRowNum()

=cut
sub getTextRowNum {
  my $self = shift;
  
  # get current row num
  my $curRowNum = $self->rowNumBegin;
  
  if ($curRowNum > $self->rowNumEnd) {
    carp "There is out of text boundary";
    return;
  }
  # update it in the motdb
  my $dbtApp = Database::MotDBTableApp->getInstance;
  $dbtApp->updateRowNum($curRowNum);
  
#  $self->addRowNumBegin(1);
  # add 1 to it
  
  return $curRowNum;
}

# for process
=head2 getRowTextByNum()

Gets the text string by row number

=cut
sub getRowTextByNum {
  my $self = shift;
  my $rowNum = shift;
  if (! defined $rowNum || $rowNum eq "" 
  || ! Scalar::Util::looks_like_number($rowNum)) {
    croak "There is no valid row num!";
  }
  if (! $self->hasIO) {
    croak "There is no valid Utils::IO to use!";
  }
  if ($rowNum >= $self->io->getFileRowCount) {
    carp "There is out of text boundary";
    return;
  }
  return $self->io->tiedFile->[$rowNum];
}

# for process
=head2 analyseRowText()

=cut
sub analyseRowText {
  my $self = shift;
  
  my $rowNum = $self->getTextRowNum;
  my $row = $self->getRowTextByNum($rowNum);
  
  if (! defined $row) {croak "There is not valid row text with row number $rowNum!";}
  
  return if ($row eq "");
  
  # set the current row text in the MotDB in order to analyse in the state machine 
  my $dbtApp = Database::MotDBTableApp->getInstance;
  $dbtApp->setCurRowText(\$row);
  
  # clear the previous entry text
  if($self->hasPreEntryText) {
    $self->setPreEntryText(undef);
    $self->clearPreEntryText;
  }
  # set the prvEntryText and clear the current entry text
  if ($self->hasCurEntryText) {
    $self->setPreEntryText($self->getCurEntryText);
    $self->setCurEntryText(undef);
    $self->clearCurEntryText;
  }
  my $stateMachine = State::StateManagerContext->getInstance;
  my $entryText = $stateMachine->runStates;
  $self->setCurEntryText($entryText);
  return 1;
}
 
=head2 analysePostProcess()

check lemma, sigel, dito parts etc. of previous entry text and compare it with current entry text.

=cut
sub analysePostProcess {
  my $self = shift;
  
  return if (! $self->hasCurEntryText);
  my $entryType = $self->getCurEntryText->getEntryType;
  if (Global::Values->doSetGroups->{SpecialEntryText}->has($entryType)
  || Global::Values->doSetGroups->{ErrorEntryText} ->has($entryType) 
  ) {
    #ATTENTION: delete the current entry text
    $self->setCurEntryText(undef);
    $self->clearCurEntryText;
    return;
  }
  
  if (defined $self->getPreEntryText && defined $self->getCurEntryText && $self->hasPreEntryText && $self->hasCurEntryText) {
    my $stateMachine = State::StateManagerContext->getInstance;
    my $compareState = $stateMachine->getDOState("ENTRIES_COMPARISON_STATE");
    
    $compareState->input($self->getPreEntryText, $self->getCurEntryText);
    $compareState->handle;
    my $curEntryText = $compareState->output;
    
    # update the current Entry text in the class
    $self->setCurEntryText($curEntryText);
  }
  return 1;
}

##############
# database
##############
=head2 databaseHandle()

=cut
sub databaseHandle {
  my $self = shift;
  
  return if ! $self->hasCurEntryText;
  my $curEntryText = $self->getCurEntryText;
  
  my $entryType = $self->getCurEntryText->getEntryType;
  if (Global::Values->doSetGroups->{SpecialEntryText}->has($entryType)
  || Global::Values->doSetGroups->{ErrorEntryText} ->has($entryType) 
  ){
    $self->setCurEntryText(undef);
    $self->clearCurEntryText;
  }
  
  $self->setEntryTextToDB($curEntryText);

}

# db handle
=head2 setEntryTextToDB()

=cut
sub setEntryTextToDB {
  my $self = shift;
  my $entryText = shift;
  
  if (! defined $entryText || $entryText eq "" 
  || ! Utils::Tools->isBlessed($entryText, "Text::EntryText")) {
    croak "There is no valid Text::EntryText to input";
  }
  
  if ( ! $self->hasCurEntrtTextDao) {
    $self->setNewEntryTextDao;
  }
  my $entryTextDao = $self->getCurEntrtTextDao;
  $entryTextDao->addEntryToBatch($entryText);
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;
