package Global::Values;
=encoding utf8

=head1 NAME

Global::Values - Static varialbe in the system for performance

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 SEE ALSO

MooseX::Singleton, Moose::Util::TypeConstraints, Encode, utf8

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut

use Data::Dumper;
use MooseX::Singleton;
use Moose::Util::TypeConstraints;
use FindBin;
use lib "$FindBin::Bin/../";

use utf8;
use Encode;

use Carp;
use Set::Scalar;

#use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use v5.20;


=head1 ATTRIBUTES

=head2 DO_XML_NS (constant)

XML namespace for dietrich online.

=cut

use constant DO_XML_NS => "do:";

=head2 MOOSE_OBJECT (constant)

the name of Moose::Object

e.g. ($point->meta->class_precedence_list)[-1]

=cut
use constant MOOSE_OBJECT => "Moose::Object";

=head2 globVal 

Global values

=cut
has 'globVal' => (
  is      => 'rw',
  isa     => 'HashRef[Str]',
  builder => '_build_globVal',
);

=head2 env 

Computer Environment

=cut
has 'env' => (
  is      => 'rw',
  isa     => 'HashRef[Str]',
  default => sub { \%ENV }
);

#print Dumper(%ENV);

=head2 isInitPaths 

check whether file paths is initialized or not

=cut
has 'isInitPaths' => (
  is  => 'rw',
  isa => 'Bool',
);

=head2 isInitPaths 

check whether Database::MotDB is initialized or not

=cut
has 'isInitMotDB' => (
  is  => 'rw',
  isa => 'Bool',
);

=head2 entryTextPartType 

the type of Text::EntryPartText

=cut
has 'entryTextPartType' => (
  is      => "ro",
  isa     => "ArrayRef[Str]",
  builder => '_build_EntryTextPartType',
);

=head2 entryTextType 

the type of Text::EntryText

=cut
has 'entryTextType' => (
  is      => "ro",
  isa     => "ArrayRef[Str]",
  builder => '_build_EntryTextType',
);

=head2 entryAttrHash 

the attribute information of a Text::EntryText

=cut
has 'entryAttrHash' => (
  is      => "ro",
  isa     => "HashRef[Str]",
  builder => '_build_EntryAttrHash',
  clearer => 'resetEntryAttrHash',
  lazy => 1,
);

=head2 analyseUnitResultHash 

the result hashmap of textual analysis

=cut
has 'analyseUnitResultHash' => (
  is      => "ro",
  isa     => "HashRef",
  builder => '_build_analyseunitresulthash',
  clearer => 'resetAnalyseUnitResultHash',
  lazy => 1,
);

=head2 analyseUnitResultHash 

the result hashmap of entry Header information

=cut
has 'entryHeaderAttrHash' => (
  is      => "ro",
  isa     => "HashRef[Str]",
  builder => '_build_EntryHeaderHash',
  clearer => 'resetHeaderAttrHash',
  lazy => 1,
);

=head2 textType 

text type of original row text in the file.

=cut
has 'textType' => (
  is      => "ro",
  isa     => "ArrayRef[Str]",
  builder => '_build_TextType',
);

=head2 htmlAttrHash 

The hashmap of Html attribute

Attention: all values are references!

=cut
has 'htmlAttrHash' => (
  is      => 'ro',
  isa     => 'HashRef[Str]',
  builder => '_build_HtmlAttrHash',
  clearer => 'resetHtmlAttrHash',
  lazy => 1,
);
=head2 doModuleResultAttrHash 

The hashmap result for attribute of module analysis

Attention: all values are references!

=cut
has 'doModuleResultAttrHash' => (
  is      => 'ro',
  isa     => 'HashRef',
  builder => 'get_doModuleResultAttrHash',
  clearer => 'resetDoModuleResultAttrHash',
  lazy => 1,
);

=head2 dotypes 

All types for dietrich online

=cut
has 'dotypes' => (
  is      => "ro",
  isa     => 'HashRef[ArrayRef[Str]]',
  builder => '_build_dotypes',
);

=head2 doEntryStmts 

the statements of database access

=cut
has 'doEntryStmts' => (
  is      => "ro",
  isa     => 'HashRef',
  builder => '_build_tableDOEntryStmts',
);

=head2 doEntryStmts 

all dietrich types in a array

=cut
has 'allDOTypes' => (
  is      => "ro",
  isa     => 'ArrayRef[Str]',
  lazy    => 1,
  builder => '_build_AllDOTypes',
);

=head2 doCharTypes 

special characters, i.e. <, >, (, ), |, ...

=cut
has 'doCharTypes' => (
  is      => 'rw',
  isa     => 'HashRef[ArrayRef[Str]]',
  builder => "_build_doCharTypeGroups",
);

=head2 doSetGroups 

Set groups 

=cut
has 'doSetGroups' => (
  traits => ['Hash'],
  is => "ro",
  isa => "HashRef[Set::Scalar]",
  builder => '_build_dosetgroups',
);
=head2 doSetGroups 

State types for state machine

=cut
has 'doStateType' => (
  is      => 'rw',
  isa     => 'ArrayRef[Str]',
  builder => '_build_dostatetype',
);

=head2 doStateCheckValueType 

The checked values for processing state machine

=cut
has 'doStateCheckValueType' => (
  is      => 'rw',
  isa     => 'ArrayRef[Str]',
  builder => '_build_dostatecheckvaluetype',
);
=head2 doIDGeneratorType 

The types of id generator

=cut
has 'doIDGeneratorType' => (
  is      => 'rw',
  isa     => 'ArrayRef[Str]',
  builder => '_build_doidgeneratortype',
);
has 'doIDAnylyzerType' => (
  is      => 'rw',
  isa     => 'ArrayRef[Str]',
  builder => '_build_doidanalyzertype',
);
=head2 doDBTableType 

The types of accessing Database::MotDB

=cut
has 'doDBTableType' => (
  is      => 'rw',
  isa     => 'ArrayRef[Str]',
  builder => '_build_dodbtabletype',
);
=head2 doDBDriverType 

database driver

=cut
has 'doDBDriverType' => (
  is      => 'ro',
  isa     => 'ArrayRef[Str]',
  builder => '_build_dodbdrivertype',
);
=head2 doDBDaoType 

the DAO types

=cut
has 'doDBDaoType' => (
  is      => 'ro',
  isa     => 'ArrayRef[Str]',
  builder => '_build_dodbdaotype',
);
=head2 doAnalyseStrategyType 

the strategy types

=cut
has 'doAnalyseStrategyType' => (
  is      => 'ro',
  isa     => 'ArrayRef[Str]',
  builder => '_build_doanalysestrategytype',
);

=head2 doModuleOutputType 

the type of output module analysis

=cut
has 'doModuleOutputType' => (
  is      => 'ro',
  isa     => "ArrayRef[Str]",
  builder => '_build_domoduleoutputtype',
);

=head2 doModOutputResType 

the output result type of module analysis

=cut
has 'doModOutputResType' => (
  is      => 'ro',
  isa     => "ArrayRef[Str]",
  builder => '_build_domoduleoutputresulttype',
);

=head2 doModuleInputType 

the input data type of module analysis

=cut
has 'doModuleInputType' => (
  is      => 'ro',
  isa     => "ArrayRef[Str]",
  builder => '_build_domoduleinputtype',
);

=head2 doModuleUnitType 

the type of module units

=cut
has 'doModuleUnitType' => (
  is      => 'ro',
  isa     => "ArrayRef[Str]",
  builder => '_build_domoduleunittype',
);

# id system, for interID
has 'globID' => (
  is      => 'rw',
  isa     => 'HashRef[HashRef[Str]]',
  builder => '_build_globid',
);

=head2 ESCAPEPATTERNS (constant)

For string escaped according to the specified rules.

=cut
use constant ESCAPEPATTERNS => {
  CHARS_PATTERN    => qr{([&<>"'])},
  ENTITIES_PATTERN => qr{(&amp;|&lt;|&gt;|&quot;|&#39;)},
};
=head2 SPEC_CHARS_MAP (constant)

See ESCAPEPATTERNS

=cut
use constant SPEC_CHARS_MAP => {
  '&'  => '&amp;',
  '<'  => '&lt;',
  '>'  => '&gt;',
  '"'  => '&quot;',
  '\'' => '&#39;',
};
=head2 SPEC_ENTITIES_MAP (constant)

See ESCAPEPATTERNS

=cut
use constant SPEC_ENTITIES_MAP => {
  '&amp;'  => '&',
  '&lt;'   => '<',
  '&gt;'   => '>',
  '&quot;' => '"',
  '&#39;'  => '\'',
};

=head2 DO_ID_GEN (constant)

the static values for id generating

=cut
use constant DO_ID_GEN => {
  ORDER_SPLIT_CHAR    => "#",
  ORDER_INNER_CHAR    => "@",
  GLUE_SPLIT_CHAR     => ":",
  GLUE_INNER_CHAR     => "=",
  ROW_NUM_PREFIX      => "r",
  LEMMA_NUM_PREFIX    => "l",
  ENTRY_NUM_PREFIX    => "e",
  TEXTPART_NUM_PREFIX => "tp",
  GRAPH_NUM_PREFIX    => "g",
  TREE_NUM_PREFIX     => "t",
  MODULE_NUM_PREFIX   => "m",
  SPECIAL_NUM_PREFIX  => "s",
  NUMBER_LENGTH       => 7,
};
=head2 DO_IDMAP_TYPE (constant)

=cut
use constant DO_IDMAP_TYPE => {
  TREE     => "tree",
  GRAPH    => "graph",
  MODULE   => "module",
  TEXTPART => "textpart",
};


use constant DOCharTypes => {
  "<" => "elementBegin",
  "(" => "authorBegin",
  ">" => "elementEnd",
  ")" => "authorEnd",
  "|" => "pipe",
};
=head2 SEPARATOR (constant)

the separators

=cut
use constant SEPARATOR => {
  "ENTRY_PIPE"    => "|",
  "IN_INDEX"      => ",",
  "GLOB_ID"       => ":",                       # only for Global::DOID to separete
  "INTER_ID_PART" => "_",
  "INNER_PART"    => "==",
  "INNER_DB_PART" => "@@@",
  "ITEM_DB_PART"  => "@@||@@",
  "ITEM_DB_ITEM"  => "@",
  "COL_PART"      => ";",
};

=head2 SEVERITY (constant)

the severity level for reporting information

=cut
use constant SEVERITY => {
  1 => "No relevant effect on reliablity or safety",
  2 =>
"Very minor, no damage, no injuries, only results in a maintenance action (only noticed by discriminating customers)",
  3 =>
"Minor, low damage, light injuries (affects very little of the system, noticed by average customer)",
  4 =>
"Moderate, moderate damage injuries possible(most customers are annoyed, mostly contentual damage)",
  5 =>
"Critical (causes a loss of primary function; Loss of all safety Margins, 1 failue away from a catastrophe, several damages, several injuries, max 1 possible death",
  6 =>
"Catastrophic (Text becomes inoperative; The failure may failure may result in complete unsafe operation and possible multiple deaths)",
  baseline => 3,
};

=head2 DO_BAND_PART (constant)

the parts of a book

=cut
use constant DO_BAND_PART => {
  MAIN_BODY       => "mb",
  REGISTER        => "reg",
  AUTHOR_REGISTER => "author_reg",
  OTHERS          => "others",
};
=head2 CHARTYPES (constant)

the type of characters

=cut
use constant CHARTYPES => {
  ERROR_STRUCT => "errorStruct",
  STRUCT       => "struct",
  SYNTACTIC    => "syntactic",
  NORMAL       => "normal",
};

=head2 GRAPH (constant)

the parameters of graph

=cut
use constant GRAPH => {
  DIRECTED    => "directed",
  UNDIRECTED  => "undirected",
  MARKED      => "marked",
  REG_MATCHED => "regMatched",
  MATCH_TYPE  => "matchType",
  MODULE      => "module",
  NAME_SEP      => ":",
  TRUE          => 1,
  FALSE         => 0,
  ROOT_ATTR     => "root_vertex",
  LEAF_ATTR     => "leaf_vertex",
  INTERIOR_ATTR => "interior_vertex",
  ISOLATED_ATTR => "isolated_vertex",
};


=head2 ENTRY_PART (constant)

The parts of a entry

=cut
use constant ENTRY_PART => {
  LEMMA     => "lemma",
  HOMONYM   => "homonym",
  ID        => "id",
  TITEL     => "titel",
  VERFASSER => "verfasser",
  ELEMENT   => "element",
  SIG       => "sigel",
  COL       => "collation",
  VOL       => "volume",
  VERWEIS   => "verweis",
  VL        => "verweis_lemma",
  PIPE      => "pipe",
  STRECKE   => "strecke",
  SEITE     => "seite",
  IGN       => "ignore",
  SIC       => "sic",
  CORR      => "corr",
  DITO      => "dito",
};
=head2 HTML_TAG (constant)

=cut
use constant HTML_TAG => {
  ENTRY_TAG   => 'entry',
  LEMMA_TAG   => "l",
  SIG_TAG     => "s",
  GLYPH_TAG   => "g",
  BR_TAG      => 'br',
  IMG_TAG     => 'img',
  ELM_TAG     => "ele",
  SUB_TAG     => "sub",
  SUP_TAG     => "sup",
  OK_TAG      => "ok",
  VL_TAG      => "vl",
  DOP_TAG     => "dop",
  REP_TAG     => "rep",
  HOM_TAG     => "hom",
  COL_TAG     => "col",
  SIC_TAG     => "sic",
  CORR_TAG    => "corr",
  VERWEIS_TAG => "verweis",
  VL_TAG      => "vl",
  IGN_TAG     => "ignore",
  DITO_TAG    => "do",
};
=head2 HTML_ATTR (constant)

the attribute name of html tags

=cut
use constant HTML_ATTR => {
  ENTRY_ATTR_ID       => "id",
  ENTRY_ATTR_BAND     => "bd",
  ENTRY_ATTR_SEITE    => "se",
  ENTRY_ATTR_SPALTE   => "sp",
  ENTRY_ATTR_ZEILE    => "ze",
  ATTR_NAME_TYPE      => "type",
  ATTR_VALUE_BOOL_YES => "1",
  ATTR_VALUE_BOOL_NO  => "0"
};
=head2 HTML_TYPE (constant)

the part names of html tags

=cut
use constant HTML_TYPE => {
  ELEMENTS         => "elements",
  ELEMENT_PART     => "element",
  CONTENT_PART     => "content",
  ATTR_PART        => "attr",
  ATTRSEQ_PART     => "attrseq",
  TARGET_TEXT_PART => "targetText",
  LINK_PART        => "link",
  TEXT_PART        => "text"
};

=head2 ANALYSE_PARAMETERS (constant)

=cut
use constant ANALYSE_PARAMETERS => {
  HTML                  => "html",
  REG                   => "regexp",
  REGEXP_REPORT_HASH    => "engine_report_hash",
  REGEXP_PATTERN_HASH    => "pat_engine_hash",
  REPORT_REFERENCE_HASH => "report_reference_hash",
  REGEXP_INFO_FILENAME  => "regexp_infos.yaml",
  DATABASE_CONFIG_FILENAME  => "dbconfig.yaml",
  DATABASE_RESULT_FILENAME => "sqlite_test1.db",
  DATABASE_RESULT_FILENAME_2 => "sqlite_test3.db",
  DATABASE_TABLE_NAME_ENTRIES => "entries",
};


use constant REPORT => { ID_PREFIX => "report_id_", };


=head2 MODULE_PATTERN (constant)

the specified pattern name for text analysis. 

see also Global::Values::ANALYSE_PARAMETERS->{REGEXP_PATTERN_HASH}

=cut
use constant MODULE_PATTERN => {
  CHECK_LINE_TYPE         => "mod_entryType_fetch_3",
  CHECK_D74_HEADER        => "mod_d74Header_fetch_1",
  CHECK_CHAR_ERRORS        => "mod_charErrors_fetch_1",
  CHECK_TITLE_CONTENT     => "mod_title_fetch_1",
  CHECK_AUTHOR_CONTENT    => "mod_author_fetch_1",
  CHECK_SIGEL_CONTENT     => "mod_sigel_fetch_1",
  CHECK_REFERENCE_CONTENT     => "mod_reference_fetch_1",
  CHECK_COLLATION_CONTENT     => "mod_collation_fetch_1",
  CHECK_COLLATION_CONTENT_WITH_AUTHOR_SIGEL     => "mod_collation_fetch_2",
  CHECK_UNKNOWNPARTS     => "mod_unknown_fetch_1",
  CHECK_DITO_DESCENDANT     => "mod_dito_desc_fetch_1",
  CHECK_PIPE_Entry     => "mod_pipe_fetch_1",
  CHECK_STRECKE_Entry     => "mod_strecke_fetch_1",
  GET_STRECKE_AREA          => "strecke_1",
  GET_START_PIPE          => "pipe_1",
  GET_DITO_DESC          => "dito_1_1",
  GET_UNKNOWNPART          => "unknown_1",
  GET_D74_HEADER          => "header_1",
  GET_TITLE               => "title_1",
  GET_REFERENCE               => "verweis_1_1",
  GET_AUTHOR              => "verfasser_1_1",
  GET_Collation              => "collation_1",
  GET_Collation_AUTHOR_SIGEL              => "collation_2",
  CHECK_LEMMA             => "mod_lemseg_check_1",
  CHECK_GLOBAL_CHARS      => "mod_global_check_1",
};



=head1 METHODS

=cut

sub _build_dostatetype {
  my @arr = qw(
    ERROR_STATE STOP_STATE UNKNOWN_STATE INITIALIZATION_STATE REPORT_ANALYSE_STATE ENTRIES_COMPARISON_STATE
    SEPARATOR_ANALYSE_STATE STRECKE_ANALYSE_STATE ENTRY_ELEMENT_ANALYSE_STATE DITO_DESCENDANT_ANALYSE_STATE
    D74_HEADER_ANALYSE_STATE PIPE_ANALYSE_STATE LEMMA_ANALYSE_STATE DITO_ANALYSE_STATE
    TITLE_ANALYSE_STATE AUTHOR_ANALYSE_STATE SIGEL_ANALYSE_STATE COLLATION_ANALYSE_STATE
    REFERENCE_ANALYSE_STATE ENDPART_ANALYSE_STATE SPECIAL_HTML_PARTS_STATE SEARCH_ERROR_CHAR_STATE
    HTML_ELEMENT_LEMMA_ANALYSE_STATE HTML_ELEMENT_ANALYSE_STATE
  );
  return \@arr;
}
sub _build_dostatecheckvaluetype {
  my @arr = qw(
    STOP_MACHINE 
    NOT_FOUND HAS_ERROR TEXT_END ANALYSE_END
    HAS_UNKNOWN_PART 
    ENTRY_TYPE_LEMMA ENTRY_TYPE_ENTRY ENTRY_TYPE_LEMMA_HTML ENTRY_TYPE_ENTRY_HTML ENTRY_TYPE_STRECKE
    ENTRY_TYPE_SEITE ENTRY_TYPE_SEPARATOR ENTRY_TYPE_UNKNOWN 
    HAS_TITLE HAS_DITO_MAIN_ELEMENT HAS_DITO_DESCENDANT HAS_LEMMA_ELEMENT HAS_ENTRY_ELEMENT 
    HAS_D74_HEADER HAS_START_PIPE HAS_PIPE HAS_AUTHOR HAS_SIGEL HAS_COLLATION 
    HAS_REFERENCE HAS_REFERENCE_LEMMA HAS_REPETITION HAS_HOMONYM HAS_TILDE HAS_SIC HAS_CORR HAS_DOP HAS_ENTRY
    HAS_OK_ELEMENT HAS_ENDPART HAS_CHECKED_SPECIAL_HTML_TAGS HAS_CHAR_ERRORS
    HAS_SPECIAL_PART HAS_STRECKE
  );
  return \@arr;
}

sub _build_doanalysestrategytype {
  my @arr = qw(
    GET_UNKNOWNPART_STRATEGY GET_HTML_ENTRY_ELEMENT_STRATEGY GET_PIPE_STRATEGY ENTRIES_COMPARISON_STRATEGY
    ROW_TYPE_ANALYSE_STRATEGY D74_HEADER_STRATEGY STRECKE_STRATEGY GET_TITLE_STRATEGY 
    GET_LEMMA_STRATEGY GET_AUTHOR_STRATEGY GET_SIGEL_STRATEGY GET_COLLATION_STRATEGY 
    GET_REFERENCE_HTML_STRATEGY GET_REFERENCE_LEMMA_HTML_STRATEGY GET_REFERENCE_REG_STRATEGY
    GET_SIC_STRATEGY GET_CORR_STRATEGY GET_DOP_STRATEGY GET_REP_STRATEGY  GET_HOM_STRATEGY
    GET_IGNORE_STRATEGY SEARCH_ERROR_CHARS_STRATEGY GET_SUB_STRATEGY GET_SUP_STRATEGY
    REPORT_STRATEGY DO_ENTRYTEXT_RULEENGINE_STRATEGY GET_DITO_HTML_STRATEGY GET_DITO_DESC_STRATEGY
    GET_ENDPART_STRATEGY GET_OK_STRATEGY GET_END_DOP_STRATEGY ENTRYTEXT_INTEGRITY_RULEENGINE_STRATEGY
  );
  return \@arr;
}
sub _build_doidgeneratortype {
  my @arr =
    qw(ROW_NUMBER BAND_NAME FILE_NAME MODULE_NAME MODULE_NUMBER TEXTPART_NUMBER SPECIAL_NUMBER STRECKE LEMMA_NUMBER GRAPH_NUMBER TREE_NUMBER ENTRY_NUMBER ENTRY_ID GRAPH_ID TREE_ID MODULE_ID TEXTPART_ID);
  return \@arr;
}
sub _build_doidanalyzertype {
  my @arr =
    qw(ANALYSE_ENTRY_ID ANALYSE_GRAPH_ID ANALYSE_TREE_ID ANALYSE_MODULE_ID ANALYSE_TEXTPART_ID);
  return \@arr;
}
sub _build_dodbtabletype {
  my @arr = qw(
    DATA_ROW_TEXT DATA_ENTRY_TEXT
    IO_PIPE FILE_ELEMENT_ARRAY
    IDMAP_TREE IDMAP_GRAPH IDMAP_MODULE IDMAP_TEXTPART IDMAP_ROW
    CUR_ENTRY_ID CUR_TEXTPART_ID CUR_MODULE_ID CUR_GRAPH_ID CUR_TREE_ID CUR_FIELD_POSITION_ID
    STRECKE GRAPH_NUM TREE_NUM MODULE_NAME MODULE_NUM SPECIAL_NUM TEXTPART_NUM TEXTPART_NAME
    BAND_NAME FILE_NAME ROW_NUMBER FIELD_POSITION_NUM
    REG_PATTERN_ENGINE_MAP REPORT_REFERENCE_MAP 
    INTERFACE_MAPS_PATTERN_REPORT
  );
  return \@arr;
}

sub _build_dodbdrivertype {
  my @arr = qw(MYSQL SQLITE XML TEI MOJO CSV);
  return \@arr;
}
sub _build_dodbdaotype {
  my @arr = qw(ENTRYTEXT_DAO_IMPL );
  return \@arr;
}
sub _build_escapePatterns {
  my $hash = {};
  $hash->{specCharEscPattern}   = qr{([&<>"'])};
  $hash->{specCharUnEscPattern} = qr(&amp;|&lt;|&gt;|&quot;|&#39;);
  return $hash;
}

sub _build_domoduleoutputtype {
  my @arr = qw(
    MODULE_OUTPUT_MATCHED_BOOL MODULE_OUTPUT_TARGET_POS 
    MODULE_OUTPUT_REG MODULE_OUTPUT_HTML
  );
  return \@arr;
}

sub _build_domoduleoutputresulttype {
  my @arr = qw(
  HTML_RESULT REGEXP_RESULT
  D74_HEADER_REGEXP_RESULT ENTRY_ELEMENT_HTML_RESULT
  );
  return \@arr;
}
#sub _build_domoduleunitoutputtabletype {
#  my @arr = qw(
#  HTML_ELEMENT HTML_CONTENT HTML_START_TAG HTML_END_TAG
#  HTML_ENTRY_ATTR_ID HTML_ENTRY_ATTR_BAND HTML_ENTRY_ATTR_SEITE HTML_ENTRY_ATTR_SPALTE
#  HTML_ENTRY_ATTR_ZEILE 
#  );
#  return \@arr;
#}


sub _build_domoduleinputtype {
  my @arr = qw(HTML REGEXP);
  return \@arr;
}
sub _build_domoduleunittype {
  my @arr = qw(
  REGEXP_UNIT HTML_UNIT REGEXP_MOD_UNIT
  GRAPH_UNIT TREE_UNIT
  );
  return \@arr;
}

sub get_doModuleResultAttrHash {
  my $self        = shift;
  my $resAttrHash = {};
  $resAttrHash->{HTML_RESULT} = {
    id => "",
    bd   => "",
    se  => "",
    sp => "",
    ze  => "",
    others   => "",
  };
  $resAttrHash->{REGEXP_RESULT} = {
    noIdea => "",
    others => "",
  };
  return $resAttrHash;
}
#
sub _build_EntryHeaderHash {
  return {
    id => "",
    bd => "",
    se => "",
    sp => "",
    ze => "",
  };
}

# type: _build_doidgeneratortype
sub getIDUnit {
  my $self = shift;
  my $type = shift;
  my $idHashUnit = {};
  if ( $type eq "ENTRY_ID" ) {
    $idHashUnit = {
      type     => $type,
      bandName => "",
      fileName => "",
      rowNum   => "",
    };
  }
  elsif ( $type eq "GRAPH_ID" ) {
    $idHashUnit = {
      type     => $type,
      entryId  => "",
      graphNum => "",
    };
  }
  elsif ( $type eq "TREE_ID" ) {
    $idHashUnit = {
      type    => $type,
      entryId => "",
      treeNum => "",
    };
  }
  elsif ( $type eq "MODULE_ID" ) {
    $idHashUnit = {
      type       => $type,
      entryId    => "",
      moduleNum  => "",
      moduleName => "",
    };
  }
  elsif ( $type eq "TEXTPART_ID" ) {
    $idHashUnit = {
      type        => $type,
      entryId     => "",
      textPartNum => "",
    };
  }
  else {
    carp "This type: $type is not valid for id analysing!";
    return;
  }

  return $idHashUnit;
}

# this is for DOModuleOutput.pm
=head2 getOutputUnit($type)

=head3 Paramters:

=over 4

=item *

$type: the given type of output module analysis , see Global::Values->doModuleOutputType

=back

=head3 Returns:

the hashmap to be output analyse result.

=head3 See Also:

Module::ModuleUnits

=cut
sub getOutputUnit {
  my $self = shift;
  my $type = shift;
  my $hash = {};
  if ( $type eq "MODULE_OUTPUT_MATCHED_BOOL" ) {
    $hash = {
      type  => $type,
      name  => "",      # reg_exp name
      value => "",      # matched or not(1/0)
    };
  }
  elsif ( $type eq "MODULE_OUTPUT_TARGET_POS" ) {
    $hash = {
      type  => $type,
      name  => "",      # part type name
      value => "",      # part of original text
      pos   => "",      # the position with begin and end i.e. 12,21
    };
  }
  elsif ( $type eq "MODULE_OUTPUT_REG" ) {
    $hash = {
      name  => "",      # part type name
      matched => "",      # part of original text
      pos   => "",      # the position with begin and end i.e. 12,21
    };
  }
  elsif ( $type eq "MODULE_OUTPUT_REG_MOD" ) {
    $hash = {
      name  => "",      # part type name
      matched => "",      # part of original text
      pos   => "",      # the position with begin and end i.e. 12,21
    };
  }
  return $hash;
}

sub _build_doCharTypeGroups {
  my @structChars      = qw{< ( | -};
  my @errorStructChars = qw{> )};
  my @synChars         = qw{- do.};
  my @normal           = ();

  my $thash = {
    "errorStruct" => \@errorStructChars,
    "syntactic"   => \@synChars,
    "struct"      => \@structChars,
    "normal"      => \@normal,
  };
  return $thash;
}



sub _build_dochartypes {
  my @arr = qw(
  );
  return \@arr;
}

sub _build_AllDOTypes {
  my $self = shift;
  my @types;
  my @typeValRefs = values %{ $self->dotypes };

  #print Dumper(@typeValRefs);
  for my $arrRef (@typeValRefs) {
    push @types, @$arrRef;
  }

  #print Dumper(@types);
  return \@types;
}

# Attention: all values are references!
sub _build_HtmlAttrHash {
  return {
    "tagName"  => "",    # tagName: the input tag name for searching.(text reference)
    "elements" => "",    # element: element text with array ref
    "restText" => "",    # restText: the input original text without elements (text reference)
    "elePos"   => ""
    , # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare! elePos: stardIdx,offset: stardIdx != -1, offset != 0 und StardIdx + offset kleiner als nächste StartIdx
    "eleContentPos"   => ""
    , # eleContentPos: the content's positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare! elePos: stardIdx,offset: stardIdx != -1, offset != 0 und StardIdx + offset kleiner als nächste StartIdx
    "eleFlags"      => "",    # eleFlags: element flags with array ref
    "content"       => "",    # content: content with array ref
    "tagFlags"      => "",    # tagFlags: tagFlags with array ref
    "attr"          => "",    # attr: attributes in einem Tag with hash ref
    "attrseq"       => "",    # attrseq: attr keys with array ref
    "targetText"    => "",    # text: target original text with text ref
    "link"          => "",    # link: url link with text ref
    "rawText"       => "",    # rawText: the input original text ref
    "hasTagName"    => "",
    "hasRawText"    => "",
    "hasElement"    => "",
    "hasRestText"   => "",
    "hasTargetText" => "",
    "hasContent"    => "",
    "hasAttr"       => "",
    "hasAttrseq"    => "",
    "hasLink"       => "",
  };
}

sub _build_analyseunitresulthash {
  return {
    "severity" => 1,
    "pos"      => [],
    "found"    => 0,
    "eleNum"  => 0,
    "elementType" => "",
    "element"  => "",
    "content"  => "",
    "result"   => "",
    "problems" => [],
    "reports"  => "",
  }
}


sub _build_EntryAttrHash {
  return {
    inheritSigel => 0,
    inheritHeaderInfo => 0,
    inheritLemma => 0,
    inheritDitoEle => 0,
    hasHeader     => 0,
    hasStrecke    => 0,
    hasStartPipe  => 0,
#    hasOtherPipe  => 0,  # error
    hasHom        => 0,
    hasRep        => 0,
    hasDop        => 0,
    hasEndDop        => 0,
#    hasDito       => 0,
    hasDitoEle        => 0,
    hasDitoDesc        => 0, # dito descendant
    hasIgnore     => 0,
    hasSic        => 0,
    hasSub        => 0,
    hasSup        => 0,
    hasCorr       => 0,
    hasTitle      => 0,
    hasTilde      => 0,
    hasLemmaTitle => 0,
    hasEntry      => 0,
    hasMoreEntry      => 0,
    hasLemma      => 0,
    hasMoreLemmata      => 0,
    hasAuthor     => 0,
    hasMoreAuthor     => 0,
    hasSigel      => 0,
    hasMoreSigel  => 0,
    hasCollation  => 0,
    hasReference    => 0,
    hasReferenceLemma         => 0,
    hasOK         => 0,
    hasException  => 0,
    hasCharErrors => 0,
    hasUnknownparts => 0,
    exception     => "",
  };
}

sub _build_dosetgroups {
  my @lemmaEntryText = qw(ENTRY_TYPE_LEMMA ENTRY_TYPE_LEMMA_HTML);
  my @entryText = qw(ENTRY_TYPE_ENTRY ENTRY_TYPE_ENTRY_HTML);
  
  my @htmlEntryText = qw(ENTRY_TYPE_LEMMA_HTML ENTRY_TYPE_ENTRY_HTML);
  my @d74EntryText = qw(ENTRY_TYPE_ENTRY ENTRY_TYPE_LEMMA ENTRY_TYPE_SEPARATOR ENTRY_TYPE_STRECKE);
  
  my @specialEntryText = qw(ENTRY_TYPE_SEPARATOR ENTRY_TYPE_STRECKE);
  my @errorEntryText = qw(ENTRY_TYPE_UNKNOWN);
  
  
  my $lemmaEntryTextSet = Set::Scalar->new(@lemmaEntryText);
  my $entryTextSet = Set::Scalar->new(@entryText);
  my $specialEntryTextSet = Set::Scalar->new(@specialEntryText);
  my $errorEntryTextSet = Set::Scalar->new(@errorEntryText);
  my $htmlEntryTextSet = Set::Scalar->new(@htmlEntryText);
  my $d74EntryTextSet = Set::Scalar->new(@d74EntryText);
  
  my $hash = {
    "LemmaEntryText" => $lemmaEntryTextSet,
    "EntryText" => $entryTextSet,
    "SpecialEntryText" => $specialEntryTextSet,
    "ErrorEntryText" => $errorEntryTextSet,
    "HtmlEntryText" => $htmlEntryTextSet,
    "D74EntryText" => $d74EntryTextSet,
  };
  return $hash;
}

sub _build_dotypes {

  my @textType =
    qw( PLAINTEXT HTML_ELEMENT HTML_START_TAG HTML_END_TAG HTML_CONTENT HTML_EMPTY_ELEMENT UNKNOWN_TEXT MIXED_TEXT);
  my @entryTextType =
    qw(ENTRY_TYPE_LEMMA ENTRY_TYPE_ENTRY ENTRY_TYPE_LEMMA_HTML ENTRY_TYPE_ENTRY_HTML ENTRY_TYPE_STRECKE ENTRY_TYPE_SEITE ENTRY_TYPE_SEPARATOR  ENTRY_TYPE_UNKNOWN);
  my @entryTextPartType = qw( UNKNOWN_PART SPECIAL_ENTRY SPECIAL_CHARACTER CHAR_ERROR_ELEMENT
    ENTRY_ELEMENT ENTRY_START_TAG ENTRY_END_TAG ENTRY_CONTENT
    LEMMA_ELEMENT LEMMA_START_TAG LEMMA_END_TAG LEMMA_CONTENT
    HOMONYM_ELEMENT HOMONYM_START_TAG HOMONYM_END_TAG HOMONYM_CONTENT
    HEADER HEADER_NUMMER HEADER_BAND HEADER_SEITE HEADER_SPALTE HEADER_ZEILE
     TITEL VERFASSER START_PIPE STRECKE SEITE TILDE
    SIGEL_ELEMENT SIGEL_START_TAG SIGEL_END_TAG SIGEL_CONTENT
    COLLATION_ELEMENT COLLATION_EMPTY_ELEMENT COLLATION_START_TAG COLLATION_END_TAG COLLATION_CONTENT
    VOLUME_ELEMENT VOLUME_EMPTY_ELEMENT VOLUME_START_TAG VOLUME_END_TAG VOLUMEN_CONTENT
    VERWEIS_ELEMENT VERWEIS_EMPTY_ELEMENT VERWEIS_START_TAG VERWEIS_END_TAG VERWEIS_CONTENT
    VERWEIS_LEMMA_ELEMENT VERWEIS_LEMMA_EMPTY_ELEMENT VERWEIS_LEMMA_START_TAG VERWEIS_LEMMA_END_TAG VERWEIS_LEMMA_CONTENT
    IGNORE_ELEMENT IGNORE_START_TAG IGNORE_END_TAG IGNORE_CONTENT
    SIC_ELEMENT SIC_START_TAG SIC_END_TAG SIC_CONTENT
    SUB_ELEMENT SUB_START_TAG SUB_END_TAG SUB_CONTENT
    SUP_ELEMENT SUP_START_TAG SUP_END_TAG SUP_CONTENT
    DOP_ELEMENT DOP_START_TAG DOP_END_TAG DOP_CONTENT
    REP_ELEMENT REP_START_TAG REP_END_TAG REP_CONTENT
    CORR_ELEMENT CORR_START_TAG CORR_END_TAG CORR_CONTENT
    OK_ELEMENT OK_EMPTY_ELEMENT OK_START_TAG OK_END_TAG OK_CONTENT
    DITO DITO_DESC_PART DITO_ELEMENT DITO_EMPTY_ELEMENT DITO_START_TAG DITO_END_TAG DITO_CONTENT);
#
  my $thash = {
    "textType"          => \@textType,
    "entryTextType"     => \@entryTextType,
    "entryTextPartType" => \@entryTextPartType,
  };
  return $thash;
}


# returns the array reference
sub _build_tableDOEntryStmts {
  my $str11 = qq(CREATE TABLE IF NOT EXISTS entries(
          eid  INTEGER PRIMARY KEY NOT NULL,
          rowNum TEXT  ,
          entryID TEXT  ,
          entryType  TEXT ,
          severity TEXT  ,
          originalText  TEXT ,
          header  TEXT,
          AttrID    TEXT ,
          AttrBand    TEXT,
          AttrSeite    TEXT ,
          AttrSpalte    TEXT ,
          AttrZeile    TEXT,
          lemma    TEXT ,
          title    TEXT,
          author    TEXT,
          sigel    TEXT ,
          collation    TEXT,
          verweis    TEXT,
          homonym    TEXT,
          repetition    TEXT,
          DitoElement    TEXT,
          DitoDescendant    TEXT,
          ignore    TEXT,
          sic    TEXT,
          corr    TEXT,
          sub    TEXT,
          sup    TEXT,
          dop    TEXT,
          ok    TEXT,
          charErrors    TEXT,
          problems    TEXT,
          unknownparts       TEXT,
          others       TEXT
  ));
  # for expansion, connect with extr_infos
  my $str21 = qq(CREATE TABLE IF NOT EXISTS entry_extra_infos(
    entry_extra_info_id INTEGER  PRIMARY KEY NOT NULL,
    extra_info_name TEXT NOT NULL
  ));
  # for expansion, connect with extr_property
  my $str31 = qq(CREATE TABLE IF NOT EXISTS entry_extra_properties(
    entry_extra_property_id INTEGER  PRIMARY KEY NOT NULL,
    entry_extra_info_id INTEGER NOT NULL,
    entryID INTEGER NOT NULL,
    property_value TEXT NOT NULL,
    property_description TEXT
  ));
  my @entryColumns = qw( 
    eid rowNum entryID entryType severity originalText header AttrID AttrBand AttrSeite AttrSpalte 
    AttrZeile lemma title author sigel collation verweis homonym repetition DitoElement DitoDescendant  
    ignore sic corr sub sup dop ok charErrors problems unknownparts others);
  my @entryColumnsNoID = qw( 
    rowNum entryID entryType severity originalText header AttrID AttrBand AttrSeite AttrSpalte 
    AttrZeile lemma title author sigel collation verweis homonym repetition DitoElement DitoDescendant  
    ignore sic corr sub sup dop ok charErrors problems unknownparts others);
  my $stmtHash = {
    entries => \$str11,
    entry_extra_info => \$str21,
    entry_extra_property => \$str31,
    entryColumns => \@entryColumns,
    entryColumnsNoID => \@entryColumnsNoID,
  };
  return $stmtHash;
}
sub tableReportStmts {
  my @res;
  my $str1 = qq(CREATE TABLE IF NOT EXISTS DO_REPORT(
    report_id INTEGER  PRIMARY KEY NOT NULL,
    report_name TEXT NOT NULL,
    report_class TEXT
  ));
  # There are not always a solution.
  my $str2 = qq(CREATE TABLE IF NOT EXISTS DO_REPORT_PROPERTY(
    report_prop_id INTEGER  PRIMARY KEY NOT NULL,
    report_id INTEGER NOT NULL,
    report_prop_name TEXT NOT NULL,
    report_prop_value TEXT NOT NULL,
    report_prop_severity TEXT NOT NULL,
    report_prop_description TEXT,
    report_prop_action TEXT
  ));

  # There are not always a solution.
  my $str3 = qq(CREATE TABLE IF NOT EXISTS DO_REPORT_REFERENCE_TABLE(
    ref_id INTEGER  PRIMARY KEY NOT NULL,
    ref_name TEXT NOT NULL,
    ref_value TEXT NOT NULL,
    ref_severity TEXT NOT NULL,
    ref_description TEXT,
    ref_action TEXT
  ));
  push @res, $str1, $str2, $str3;
  return \@res;
}

sub _build_TextType {
  my $self = shift;
  my $arr  = $self->dotypes->{textType};
  return $arr;
}
#
sub _build_EntryTextType {
  my $self = shift;
  my $arr  = $self->dotypes->{entryTextType};
  return $arr;
}

sub _build_EntryTextPartType {
  my $self = shift;
  my $arr  = $self->dotypes->{entryTextPartType};
  return $arr;
}

sub _build_globVal {
  return {
    "curRowNum" => 0,    # line number in the text file
    #    isEntry           => ",
    #    "isEntryLemma"      => "",
    #    "isEntryStrecke"    => "",
    #    "isEntrySeite"      => "",
    #    "isEntry"           => "",
    #    "isOK"              => "",
    #    "hasID"             => "",
    #    "hasPipe"           => "",
    #    "hasLemma"          => "",
    #    "hasHomonym"        => "",
    #    "hasTitel"          => "",
    #    "hasLemmaTitel"     => "",
    #    "hasTilde"          => "",
    #    "hasVerfasser"      => "",
    #    "hasSigel"          => "",
    #    "hasVolumen"        => "",
    #    "hasVolPage"        => "",
    #    "hasVerweis"        => "",
    #    "hasVerweisLemma"   => "",
    "binPath"           => "",           #die aktuelle execute path
    "dataPath"          => "",           #der Pfad der data Ordner
    "logPath"           => "",           #der Pfad der Log Ordner
    "configPath"        => "",           #der Pfad der Config Ordner
    "designPath"        => "",           #der Pfad der design Ordner
    "templatePath"      => "",           #der Pfad der data Ordner
    "webPath"           => "",           #der Pfad der web Ordner
    "testPath"          => "",           #der Pfad der test Ordner
    "dbPath"            => "",           #der Pfad der datenbank Ordner
    "inputFileName"     => "",           #der Name der eingegebenen Text-Datei
    "inputFileBaseName" => "",           #der basename der eingegebenen Text-Datei für setInterID
    "outputFileName"    => "",           #der Name der ausgegebenen Text-Datei
    "logFileName"       => "",           #der Name der Log-Datei
    "dbFileName"        => "",           #der Name von datenbank-datei
    "logTableName"      => "log",        #der Name von datenbank-datei
    "segTableName"      => "segment",    #der Name von datenbank-datei
    "patTableName"      => "pattern",    #der Name von datenbank-datei
    "logDBName"         => "Log",        #der Name von datenbank-datei
    "segDBName"         => "Segment",    #der Name von datenbank-datei
    "patDBName"         => "Pattern",    #der Name von datenbank-datei
    "dbDataSuffix"      => ".db",
    "dbSchemaName"   => "DietrichDB::Schema",
    "batchLimit"     => 500,
    "sqliteDriver"   => "SQLite",
    "mysqlDriver"    => "mysql",
    "dbTableReport"  => "Report",
    "dbTableSegment" => "Segment",
    "bandName"       => "b74",                  # as default
  };
}


# "bandNum"  => "b74",                      # band 74 und anderem
#    "bandPart" => "mb",                       # Global::Values::DO_BAND_PART
#    "origPage" => "",                         #die durch "<seite=xx>" festgestellte zusätzliche
#                                              #Informationen der Seite
#    "strecke"  => "a",                        #die durch "<strecke=x>" festgestellte zusätzliche
#                                              #Informationene der Strecke
#    "lemma"    => "",                         #lemma
#    "lemmaNum" => "l0000",                    #block 0, block 1, block 2.....
#    "entryNum" => "e0000",                    #entries in einem Block. e1, e2, e3 ....
#    "entryAttr" => "a0000",    #das Attribut des Entrys
#    "unknownLine" => "u0000",    #das ist für unerkannte Linie in der Text-Datei.
sub _build_globid {
  my $initHash = {
    "bandNum"  => "b74",      # band 74 und anderem
    "bandPart" => "mb",       # Global::Values::DO_BAND_PART
    "origPage" => "",         #die durch "<seite=xx>" festgestellte zusätzliche
                              #Informationen der Seite
    "strecke"  => "a",        #die durch "<strecke=x>" festgestellte zusätzliche
                              #Informationene der Strecke
    "lemma"    => "",         #lemma
    "lemmaNum" => "l0000",    #block 0, block 1, block 2.....
    "entryNum" => "e0000",    #entries in einem Block. e1, e2, e3 ....
  };
  my $pHash = {
    "bandNum"  => "b74",      # band 74 und anderem
    "bandPart" => "mb",       # Global::Values::DO_BAND_PART
    "origPage" => "",         #die durch "<seite=xx>" festgestellte zusätzliche
                              #Informationen der Seite
    "strecke"  => "a",        #die durch "<strecke=x>" festgestellte zusätzliche
                              #Informationene der Strecke
    "lemma"    => "",         #lemma
    "lemmaNum" => "l0000",    #block 0, block 1, block 2.....
    "entryNum" => "e0000",    #entries in einem Block. e1, e2, e3 ....
  };

  my $bidhash = {
    "initial"    => $initHash,
    "processing" => $pHash,
  };
  return $bidhash;
}

sub dbConfig {
  my $self = shift;
  my $db_connect_type = {
    "SQLITE_CONNECTION" => {
      dsn => "DBI:SQLite:dbname=../data_result/sqlite_test1.db",
      user => "",
      password => "",
      optional_attr => {
        RaiseError => 1, 
        sqlite_unicode => 1, 
        quote_char => '"', 
        name_sep => '.'
      },
    },
  };
  return $db_connect_type;
}

##################################
#CORE:
sub regExpInfos {
  my $self = shift;
  # Global::Values->dotypes
  my $pat_engine_hash = {
    "mod_strecke_fetch_1" => {
      "content"        => "",
      "successorTrue"  => [ 'strecke_1'],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "success"        => "",
      "failure"        => "",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
        "GC1: Globale Modul zum Überprüfen der eventuellen Fehler in der Literaturangaben.",
    },
    # <strecke=xx>:
    "strecke_1" => {
      "content" => qr{(?:(?:<|>)strecke=([^<>]*)(?:<|>))}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "strecke",
      "success"        => "rpt_strecke_1",
      "failure"        => "rpt_strecke_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
"In der aktuellen Literaturangabe kommt kein Strecke-Bereich vor mit der nachfolgenden Form  'strecke=*'.",
    },
    # <seite=xxx>
    "seite_1" => {
      "content"        => qr{(?:(?:<|>)seite=(\d{1,4})(?:<|>))}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "otherline",
      "success"        => "rpt_page_area_1",
      "failure"        => "rpt_page_area_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
"In der aktuellen Literaturangabe kommt kein Seitenzahl-Bereich vor mit der nachfolgenden Form 'seite=*'.",
    },
    "mod_pipe_fetch_1" => {
      "content"        => "",
      "successorTrue"  => [ 'pipe_1'],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "success"        => "",
      "failure"        => "",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
        "GC1: Globale Modul zum Überprüfen der eventuellen Fehler in der Literaturangaben.",
    },
    # Artikel-Segmentieren 1: | ... (Überprüfen den Anfang des Artikels)
#      "content"        => qr{^(?:\s*\|\s*)(.*?)$}i,
    "pipe_1" => {
      "content"        => qr{^(\s*\|)},
      "successorTrue"  => [],
      "successorFalse" => ['pipe_2'],
      "type"           => "PIPE",
      "success"        => "rpt_pipe_1",
      "failure"        => "rpt_pipe_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
        "Keine Pipe steht am Anfang der Artikel-Literaturangabe, nicht so wie '| ....' ",
    },
   # Artikel-Segmentieren 2: | ... (Überprüfen die Pipe in der Literaturangabe)
    "pipe_2" => {
      "content"        => qr{(\S+?(?:\s*\|\s*)(.*?))$}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "PIPE",
      "success"        => "rpt_pipe_3",
      "failure"        => "rpt_pipe_4",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
        "Keine Pipe steht in der Artikel-Literaturangabe, nicht so wie '... | ....' ",
    },
    "mod_dito_desc_fetch_1" => {
      "content"        => "",
      "successorTrue"  => [ 'dito_1_1'],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "success"        => "",
      "failure"        => "",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
        "GC1: Globale Modul zum Überprüfen der eventuellen Fehler in der Literaturangaben.",
    },
    # Verweisung: do. = dito (Casesensitive)
#      "content"        => qr{\bdo\b\s*\.?.*?$},
    "dito_1_1" => {
      "content"        => qr{\b(do\b\s*\.)},
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "DITO",
      "success"        => "rpt_dito_1",
      "failure"        => "rpt_dito_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" => "Die aktuelle Literaturangabe hat keine 'dito' Verweisung mit Form 'do.'.",
    },
    "mod_unknown_fetch_1" => {
      "content"        => "",
      "successorTrue"  => [ 'unknown_1'],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "success"        => "",
      "failure"        => "",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
        "GC1: Globale Modul zum Überprüfen der eventuellen Fehler in der Literaturangaben.",
    },
    # global pat 1: es gibt Sonderzeichen wie ^^ in den bestimmten Bereichen oder nicht.
    "unknown_1" => {
      "content"        => qr{(\S+)}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "unknown",
      "success"        => "rpt_unknown_part_1",
      "failure"        => "rpt_unknown_part_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Im Bereich gibt es keine ungültige Zeichen wie '^' oder '^^'.",
    },
    "mod_charErrors_fetch_1" => {
      "content"        => "",
      "successorTrue"  => [ 'char_1', 'char_2', 'pipe_2' ],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "success"        => "",
      "failure"        => "",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
        "GC1: Globale Modul zum Überprüfen der eventuellen Fehler in der Literaturangaben.",
    },
    # global pat 1: es gibt Sonderzeichen wie ^^ in den bestimmten Bereichen oder nicht.
    "char_1" => {
      "content"        => qr{([\^\#¿*]+)}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "sonderzeichen",
      "success"        => "rpt_char_6",
      "failure"        => "rpt_char_1",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Im Bereich gibt es keine ungültige Zeichen wie '^' oder '^^'.",
    },
    "char_2" => {
      "content"        => qr{([0-9]\p{L}[0-9]|\p{L}[0-9]\p{L}|\b[0-9]\p{L}+|\b\p{L}[0-9]+|\p{L}[0-9]+\b)}xi,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "sonderzeichen",
      "success"        => "rpt_char_5",
      "failure"        => "rpt_char_7",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
        "In der Bereich gibt es keine Zeichenketten wie '3chaft' ,'3Ö2' oder '30J'.",
    },
    # check verweis
    "mod_reference_fetch_1" => {
      "content"        => "",
      "successorTrue"  => ['verweis_1_1'],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "success"        => "",
      "failure"        => "",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Das Modul wird überprüfen, ob das Entry die Kollation hat.",
    },
# Verweisung: -(-) s. (a./u.)  schreibungsabhängig
# besser als verweis_1, aber die kann nicht alle Verweisungen herausfinden,
# nämlich ungefähr 300 Verweisungen kann nicht richtig gefunden, sie können
# durch andere Pattern gefunden z.B. Ohne Autor, Ohne Sigel oder Sigel liegt am Ende der Angabe,
# aber die Herausgefundenen sind meistens richtig
#      "content"        => qr{(?<![~'.])((--?((?!\d).)+?)|(--?)?) \bs\.\s*((a|u)(\.|nter|uch))?.*?$},
    "verweis_1_1" => {
      "content"        => qr{((?<![~'.])((--?((?!\d).)+?)|(--?)?) \b(s\.\s*((a|u)(\.|nter|uch))|(Unter|Ferner)\b)+((?! <ok\s*/?>).)*)}xi,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "verweis",
      "success"        => "rpt_verweis_1",
      "failure"        => "rpt_verweis_4",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
"Es gibt keine Verweisung in der aktuellen Literaturangabe mit nachfolgender Form: -(-) s. (a./u.).",
    },

    # Verweisung: -- Unter, -- Ferner
    "verweis_1_2" => {
      "content"        => qr{((?<![~'])((--?(\w|\s|~)*)|(--?))\b(Unter|Ferner)\b(((?! <ok\s*/?>).)+))}xi,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "verweis",
      "success"        => "rpt_verweis_2",
      "failure"        => "rpt_verweis_5",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
"Es gibt keine Verweisung in der aktuellen Literaturangabe mit nachfolgender Form: -(-) Ferner/Unter...",
    },
    # check collation
    "mod_collation_fetch_1" => {
      "content"        => "",
      "successorTrue"  => ['collation_1'],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "success"        => "",
      "failure"        => "",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Das Modul wird überprüfen, ob das Entry die Kollation hat.",
    },
    # check collation
    "mod_collation_fetch_2" => {
      "content"        => "",
      "successorTrue"  => ['collation_2'],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "success"        => "",
      "failure"        => "",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Das Modul wird überprüfen, ob das Entry die Kollation hat.",
    },
    # begin with <col /> till <, >, | 
#      "content"        => qr{(<col\s*/?>[^<>|]*)}xi,
    "collation_1" => {
      "content"        => qr{(<col\s*/?>[^<>|]+)}xi,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "collation",
      "success"        => "rpt_collation_1",
      "failure"        => "rpt_collation_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Es gibt keinen gültigen sigel-Bereich in der Literaturangabe!",
    },
    # after author or sigel, till <, >, | 
#      "content"        => qr{([^<>|]*)}xi,
    "collation_2" => {
      "content"        => qr{([^<>|]+)}xi,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "collation",
      "success"        => "rpt_collation_1",
      "failure"        => "rpt_collation_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Es gibt keinen gültigen sigel-Bereich in der Literaturangabe!",
    },
    # for extra handle
    "collation_3" => {
      "content"        => qr{<col\s*/?>([^<>|]*)}xi,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "collation",
      "success"        => "",
      "failure"        => "",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Es gibt keinen gültigen sigel-Bereich in der Literaturangabe!",
    },
    # check sigel
    "mod_sigel_fetch_1" => {
      "content"        => "",
      "successorTrue"  => ['sigel_1'],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "success"        => "",
      "failure"        => "",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Das Modul wird überprüfen, ob das Entry das Sigel hat.",
    },
    # Sigel-Segmentieren 1: <s>...</s> für entry
#      "content"        => qr{<s>((?!</?s>).)*</s>}i,
    "sigel_1" => {
      "content"        => qr{(<s>((?! </?s> ).)*</s>)}xi,
      "successorTrue"  => [ 'sigel_2', 'sigel_4' ],
      "successorFalse" => ['sigel_3'],
      "type"           => "sigel",
      "success"        => "rpt_sigel_8",
      "failure"        => "rpt_sigel_1",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Es gibt keinen gültigen sigel-Bereich in der Literaturangabe!",
    },
    # Sigel-Segmentieren 1: <s>...</s> für Lemma-Angabe
    "sigel_1_1" => {
      "content"        => qr{<s>((?!</?s>).)*</s>}i,
      "successorTrue"  => [ 'sigel_2', 'sigel_4' ],
      "successorFalse" => ['sigel_3'],
      "type"           => "sigel",
      "success"        => "rpt_sigel_8",
      "failure"        => "rpt_sigel_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Es gibt keinen gültigen sigel-Bereich in der Lemma-Literaturangabe!",
    },
# Sigel-Segmentieren 1: <s>...</s> für Artikel-Angabe
#  "sigseg_1_2" => { "content"    => qr{<s>((?!</?s>).)*</s>}i,
#                      "successorTrue"  => ['sigseg_2', 'sigseg_4','sigseg_6'],
#                      "successorFalse" => [],
#                      "type"   => "sigel",
#                      "engine"     => Global::Values::ANALYSE_PARAMETERS->{REG},
#                      "description"=> "Es gibt keinen gültigen sigel-Bereich in der Artikel-Literaturangabe!",
#   },
# Sigel-Segmentieren 2: <s>...</s>$ (Sigel steht am Ende, es ist eine falsche Position)
    "sigel_2" => {
      "content"        => qr{<s>((?! </?s>).)*</s>$}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "sigel",
      "success"        => "rpt_sigel_3",
      "failure"        => "rpt_sigel_10",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Sigel-Bereich steht nicht am Ende der Literaturangabe!",
    },
    # Sigel-Segmentieren 3: liegt <s>, </s> in der Literaturangabe?
    "sigel_3" => {
      "content"        => qr{<\s*/?\s*s\s*>}i,
      "successorTrue"  => ['sigel_6'],
      "successorFalse" => [],
      "type"           => "sigel",
      "success"        => "rpt_sigel_3",
      "failure"        => "rpt_sigel_4",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Kein <s>-TAG in der Literaturangabe vorhanden!",
    },
    # Sigel-Segmentieren 4: Sigel hat die Form <s>3999aaa.</s> in der Literaturangabe oder nicht
    "sigel_4" => {
      "content" =>
        qr{<s>\s*([1-3][0-9][0-9][0-9]|[1-9][0-9][0-9]|[1-9][0-9]|[1-9])\s*[a-z]{0,3}\s*\.?\s*</s>},
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "sigel",
      "success"        => "rpt_sigel_11",
      "failure"        => "rpt_sigel_7",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" => "Das Sigel in der Literaturangabe ist ungültig, wie die Form '3999aaa.'.",
    },
    # Sigel-Segmentieren 5: Der Inhalt des Sigels hat die Form 3999aaa. oder nicht
    "sigel_5" => {
      "content" => qr{([1-3][0-9][0-9][0-9]|[1-9][0-9][0-9]|[1-9][0-9]|[1-9])\s*[a-z]{0,3}\s*\.?},
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "sigel",
      "success"        => "rpt_sigel_11",
      "failure"        => "rpt_sigel_7",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Der Inhalt des Sigels ist ungültig, wie die Form '3999aaa.'.",
    },
    # Sigel-Segmentieren 6: liegt mehr mal<s>, </s> in der Literaturangabe?
    "sigel_6" => {
      "content"        => qr{(<\s*/?\s*s\s*>).*?\1}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "sigel",
      "success"        => "rpt_sigel_12",
      "failure"        => "rpt_sigel_6",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" => "<s>-Tag bzw. </s>-Tag sind nicht paarig: z.B. <s>...<s> bzw. </s>...</s> !",
    },
    # check title
    "mod_title_fetch_1" => {
      "content"        => "",
      "successorTrue"  => ['title_1'],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "success"        => "",
      "failure"        => "",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Das Modul wird überprüfen, ob das Entry title hat.",
    },
    # search title , before the punctuation as \(, <s>, <col />, <verweis>, <vl>, 
#      "content"        => qr{(\(|<s>|<col />|<verweis>|<vl>|<rep>|<do>)}i,
    "title_1" => {
      "content"        => qr{^((?!<col\s*/?>|<\s*/?\s*vl>|\(|\)|<\s*/?\s*verweis>|<\s*/?\s*s>).)+}xi,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "author",
      "success"        => "rpt_title_1",
      "failure"        => "rpt_title_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" => "Jede Literaturangabe hat normalerweise einen Titel.",
    },
    # check author
    "mod_author_fetch_1" => {
      "content"        => "",
      "successorTrue"  => ['verfasser_1_1'],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "success"        => "",
      "failure"        => "",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Das Modul wird überprüfen, ob das Entry die Verfasser hat.",
    },
    # Autor-Segmentieren 1_1: (...), die Inhalt in den Klammern: (1) ~ (2) Ey (3) XXX+
#      "content"        => qr{\(((~)|(\p{Lu}\p{Ll})|(((?![()]).){3,}))\)}x,
    "verfasser_1_1" => {
      "content"        => qr{(\(((~)|(\p{Lu}\p{Ll})|(((?![()]).){3,}))\))}x,
      "successorTrue"  => [ 'verfasser_3', 'verfasser_4', 'verfasser_5', 'verfasser_6', ],
      "successorFalse" => ['verfasser_2'],
      "type"           => "autor",
      "success"        => "rpt_verfasser_1",
      "failure"        => "rpt_verfasser_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
"Kein Verfasserbereich mit runden Klammern gibt es in der aktuellen Literaturangabe: z.B. (W. Rahe).",
    },
    #  2: es gibt Sonderzeichen wie ( oder ) in den bestimmten Bereichen oder nicht.
    "verfasser_2" => {
      "content"        => qr{[\(|\)]},
      "successorTrue"  => ['verfasser_7'],
      "successorFalse" => [],
      "type"           => "autor",
      "success"        => "rpt_verfasser_4",
      "failure"        => "rpt_verfasser_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "In der Literaturangabe gibt es keine Klammer wie '(' oder ')'.",
    },
    #  3: es gibt Sonderzeichen wie ( oder ) in den bestimmten Bereichen oder nicht.
    "verfasser_3" => {
      "content"        => qr{\(~\)},
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "autor",
      "success"        => "rpt_verfasser_5",
      "failure"        => "rpt_verfasser_10",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "In dem Verfasserbereich gibt es keine Tilde wie (~).",
    },
    #  4: die Rundklammer stehen am Ende der Literaturangaben oder nicht.
    "verfasser_4" => {
      "content"        => qr{\(((~)|(\p{Lu}\p{Ll})|(((?![()]).){3,}))\)$},
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "autor",
      "success"        => "rpt_verfasser_6",
      "failure"        => "rpt_verfasser_11",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Die Rundklammern stehen am Ende der Literaturangaben nicht.",
    },
    #  5: es gibt mehrere "Autor"-Bereiche wie (xxx) in den bestimmten Bereichen oder nicht.
    "verfasser_5" => {
      "content" => 
qr{(\(((~)|(\p{Lu}\p{Ll})|(((?![()]).){3,}))\)).*?(\(((~)|(\p{Lu}\p{Ll})|(((?![()]).){3,}))\))},
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "autor",
      "success"        => "rpt_verfasser_7",
      "failure"        => "rpt_verfasser_1",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
        "In dem Verfasserbereich gibt es nur ein Paar Rundklammern für Verfassernamen.",
    },
    #  6: Nummer oder Satzzeichen steht am Anfang in dem "Autor"-Block oder nicht.
    "verfasser_6" => {
      "content"        => qr{\(((?!~|\s)\P{L}).*?\)},
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "autor",
      "success"        => "rpt_verfasser_8",
      "failure"        => "rpt_verfasser_1",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Nummer oder Satzzeichen steht nicht am Anfang in dem Verfasserbereich.",
    },
    #  7: Nur Großbuchstaben stehen in dem "Autor"-Block oder nicht.
    "verfasser_7" => {
      "content"        => qr{\((\p{Lu}+)\)},
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "autor",
      "success"        => "rpt_verfasser_9",
      "failure"        => "rpt_verfasser_1",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Nicht nur Großbuchstaben stehen in dem Verfasserbereich. z.B. (AB)",
    },
    # check d74 header
    "mod_d74Header_fetch_1" => {
      "content"        => "",
      "successorTrue"  => ['header_1'],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "success"        => "",
      "failure"        => "",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Das Modul wird überprüfen, ob das Entry ID-Bereich has.",
    },
    # ID-Segmentieren 1: ID steht am Anfang.
    "header_1" => {
      "content"        => qr{^(<[a-zA-Z]\d{5,6}\s+\d{1,3}\s+\d{1,4}\s+(li|re)\s*>)},
      "successorTrue"  => [],
      "successorFalse" => ['header_2'],
      "type"           => "header",
      "success"        => "rpt_header_5",
      "failure"        => "rpt_header_1",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
"Es gibt keinen ID-Bereich in der Literaturangabe oder der ID-Bereich steht nicht am Anfang der Lemma-Literaturangabe",
    },
    # ID-Segmentieren 2: ID steht nicht nur am Anfang.
    "header_2" => {
      "content"        => qr{<([a-zA-Z]\d{5,6}\s+\d{1,3}\s+\d{1,4}\s+(li|re))\s*>},
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "id",
      "success"        => "rpt_header_3",
      "failure"        => "rpt_header_4",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Es gibt keinen Hearder-Bereich in der Literaturangabe!",
    },
#    "header_3" => {
#      "content"        => qr{^<([a-zA-Z]\d{5,6}\s+\d{1,3}\s+\d{1,4}\s+(li|re))\s*>},
#      "successorTrue"  => [],
#      "successorFalse" => [],
#      "type"           => "id",
#      "success"        => "rpt_header_6",
#      "failure"        => "rpt_header_2",
#      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
#      "description"    => "ID-Bereich in der Literaturangabe kann nicht richtig erkannt werden!",
#    },
    # check entry type
    "mod_entryType_fetch_3" => {
      "content"        => "",
      "successorTrue"  => ['is_entry_d74', 'is_entry_html', 'is_separator', 'is_entry_lemma_html', 'is_entry_lemma_d74' , 'is_strecke'],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "success"        => "",
      "failure"        => "",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Das Modul wird überprüfen, welcher Typ das Entry ist.",
    },
#    # [ 'is_entry_d74', 'is_separator', 'is_entry_lemma_d74', 'is_strecke' ],
#    "mod_entryType_fetch_1" => {
#      "content"        => "",
#      "successorTrue"  => ['is_entry_d74'],
#      "successorFalse" => [],
#      "type"           => Global::Values::GRAPH->{MODULE},
#      "success"        => "",
#      "failure"        => "",
#      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
#      "description"    => "Das Modul wird überprüfen, welcher Typ das Entry ist.",
#    },
    # Artikel-Segmentieren 1: | ... (Überprüfen den Anfang des Artikels)
    "is_entry_d74" => {
      "content"        => qr{^(?:\s*\|\s*)(.*?)$}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "pipe",
      "success"        => "rpt_entry_type_2",
      "failure"        => "rpt_entry_type_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
        "Keine Pipe steht am Anfang der Artikel-Literaturangabe, nicht so wie '| ....' ",
    },
    # Trennzeichen des Eintrags 1: ---- / -----
    "is_separator" => {
      "content"        => qr{\-{3,5}}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "separator",
      "success"        => "rpt_entry_type_4",
      "failure"        => "rpt_entry_type_4",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Die Trennzeichen des Eintrags '----' steht in der Literaturangabe!",
    },
    # ID-Segmentieren 1: ID steht am Anfang.
    "is_entry_lemma_d74" => {
      "content"        => qr{^<([a-zA-Z]\d{5,6}\s+\d{1,3}\s+\d{1,4}\s+(li|re))\s*>},
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "header",
      "success"        => "rpt_entry_type_1",
      "failure"        => "rpt_entry_type_1",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
"Es gibt keinen ID-Bereich in der Literaturangabe oder der ID-Bereich steht nicht am Anfang der Lemma-Literaturangabe",
    },
    "is_strecke" => {
      "content"        => qr{(?:(?:<|>)\s*strecke\s*=\s*([^<>\s]*)\s*(?:<|>))}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "strecke",
      "success"        => "rpt_entry_type_3",
      "failure"        => "rpt_entry_type_3",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
"In der aktuellen Literaturangabe kommt kein Strecke-Bereich vor mit der nachfolgenden Form  'strecke=*'.",
    },
    #for d73, d75 etc.
    # [ 'is_entry_lemma_html', 'is_entry_html'],
#    "mod_entryType_fetch_2" => {
#      "content"        => "",
#      "successorTrue"  => ['is_entry_html'],
#      "successorFalse" => [],
#      "type"           => Global::Values::GRAPH->{MODULE},
#      "success"        => "",
#      "failure"        => "",
#      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
#      "description"    => "Das Modul wird überprüfen, welcher Typ das Entry ist.",
#    },
    # Artikel-Segmentieren 1: <entry ***>| ... (Überprüfen den Anfang des Artikels)
    #!!Attention: Das ist selten
    "is_entry_html" => {
      "content"        => qr{^\s*<entry\b("[^"]*"|'[^']*'|[^'">])*>\s*(?:\s*\|\s*)(.*?)</entry>\s*$}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "artikelPipe",
      "success"        => "rpt_entry_type_6",
      "failure"        => "rpt_entry_type_6",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
        "Keine Pipe steht am Anfang der Artikel-Literaturangabe, nicht so wie '| ....' ",
    },
    # ID-Segmentieren 3: ID steht am Anfang in xml-formige Datei.
#      "content"        => qr{^(?:<entry((?!</?entry>).)*?>)\s*<l>}i,
    "is_entry_lemma_html" => {
      "content"        => qr{^\s*<entry\b("[^"]*"|'[^']*'|[^'">])*>\s*<l>}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "entry_lemma_html",
      "success"        => "rpt_entry_type_5",
      "failure"        => "rpt_entry_type_5",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
"Es gibt keinen ID-Bereich in der Literaturangabe oder der ID-Bereich steht nicht am Anfang der Lemma-Literaturangabe",
    },
    "mod_otherline_check_1" => {
      "content"        => "",
      "successorTrue"  => [ 'strecke_1', 'seite_1' ],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
"In den Literaturangaben des Typs \"Otherline\" werden die Strecke- und Seitenzahl-Bereiche überprüft!",
    },
    # Die text ist nicht erkannt!
    "otherline_1" => {
      "content"        => "",
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "otherline",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Dies Text kann nicht durch das Programm erkannt werden!",
    },
# Verweisung: -(-) s. (a./u.)  schreibungsabhängig
#  "verweis_1" => { "content"    => qr{(?<![~'])((--?(\w|\s|~)*)|(--?)?)\bs\.\s*((a|u)(\.|nter|uch))?((?![()]).)+?$}i,
#                      "successorTrue"  => [],
#                      "successorFalse" => [],
#                      "type"   => "verweis",
#                      "engine"     => Global::Values::ANALYSE_PARAMETERS->{REG},
#                      "description"=> "Es gibt keine Verweisung in der aktuellen Literaturangabe mit nachfolgender Form: -(-) s. (a./u.).",
#  },
# Verweisung: do. = dito (Casesensitive)
# dito-Angabe kann auch --s.a. Verweisung haben.
#  "dito_1" => { "content"    => qr{\bdo\b\s*\.?},
#                      "successorTrue"  => [],
#                      "successorFalse" => [],
#                      "type"   => "DITO",
#                      "engine"     => Global::Values::ANALYSE_PARAMETERS->{REG},
#                      "description"=> "Es gibt keine Verweisung in der aktuellen Literaturangabe mit nachfolgender Form: 'do.'.",
#  },
    # Artikel-Segmentieren 1: <entry ***>| ... (Überprüfen den Anfang des Artikels)
    #!!Attention: Das ist selten
    "pipe_1_1" => {
      "content"        => qr{^(?:<entry>((?!</?entry>).)*</entry>)\s*(?:\s*\|\s*)(.*?)$}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "artikelPipe",
      "success"        => "rpt_pipe_1",
      "failure"        => "rpt_pipe_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
        "Keine Pipe steht am Anfang der Artikel-Literaturangabe, nicht so wie '| ....' ",
    },
# Autor-Segmentieren 1: (...), die Inhalt in den Klammern
#  "verfasser_1" => { "content"    => qr{\(((?![()]).)*\)},
#                      "successorTrue"  => [],
#                      "successorFalse" => ['verfasser_2'],
#                      "type"   => "autor",
#                      "engine"     => Global::Values::ANALYSE_PARAMETERS->{REG},
#                      "description"=> "Kein Verfasserbereich mit den Rundklammern gibt es in der aktuellen Literaturangabe: z.B. (W. Rahe).",
#   },
    # ID-Segmentieren 3: ID steht am Anfang in xml-formige Datei.
#    "entry_lemma_1" => {
#      "content"        => qr{^(?:<entry((?!</?entry>).)*?>)\s*<l>}i,
#      "successorTrue"  => ['header_3'],
#      "successorFalse" => ['header_2'],
#      "type"           => "id",
#      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
#      "description" =>
#"Es gibt kein ID-Bereich in der Literaturangabe oder der ID-Bereich steht nicht am Anfang der Lemma-Literaturangabe",
#    },
    # ID-Überprüfungsmodule:
    "mod_lemseg_check_1" => {
      "content"        => "",
      "successorTrue"  => ["lemma_1"],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
"mod_lemseg_check_1: Überprüfen die evtl. Probleme beim Lemma-Bereich in dem eingegebenen Text",
    },
    # Autor-Check module
#    "mod_autor_check_1" => {
#      "content"        => "",
#      "successorTrue"  => ['autseg_1_1'],
#      "successorFalse" => [],
#      "type"           => Global::Values::GRAPH->{MODULE},
#      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
#      "description" =>
#"mod_autor_check_1: In der aktuellen Literatur-Angabe werden der Autor-Bereich überprüft!",
#    },
    # Lemma-Segmentieren 1: <l>...</l>
    "lemma_1" => {
      "content"        => qr{<l>((?!</?l>).)*</l>}i,
      "successorTrue"  => [ 'lemma_2', 'lemseg_4' ],
      "successorFalse" => ['lemma_3'],
      "type"           => "lemma",
      "success"        => "rpt_lemma_6",
      "failure"        => "rpt_lemma_1_1",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Es gibt keinen Lemma-Bereich in der Literaturangabe!",
    },
    # Lemma-Segmentieren 2: ob <l>...</l> am Ende liegt?
    "lemma_2" => {
      "content"        => qr{<l>((?!</?l>).)*</l>$}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "lemma",
      "success"        => "rpt_lemma_5",
      "failure"        => "rpt_lemma_6",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Lemma-Bereich steht nicht am Ende der Literaturangabe!",
    },
    # Lemma-Segmentieren 3: liegt <l>, </l> in der Literaturangabe?
    "lemma_3" => {
      "content"        => qr{<\s*/?\s*l\s*>}xi,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "lemma",
      "success"        => "rpt_lemma_3",
      "failure"        => "rpt_lemma_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Kein <l> oder </l> kommt in der Literaturangabe vor!",
    },
# Lemma-Segmentieren 4: Der Inhalt des Lemmata ist in Ordnung oder nicht, es ist insbesondere für Band 74.
    "lemma_4" => {
      "content" =>
qr{<l>\s*[a-zA-ZöäüÖÄÜßáàāâåćčČèéëêěÉîīíïñóôòřšśūúûŭýźžαβ\-\/\s\.\,\(\)\[\]&!\'\"0-9„“]+\s*</l>},
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "lemma",
      "success"        => "rpt_lemma_7",
      "failure"        => "rpt_lemma_4",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
        "Der Inhalt des Lemmas enthält ungültige Zeichen (insbesondere für Band 74.)",
    },
    # lemma-Sigel-Überprüfungsmodule:
#    "mod_sigel_check_1" => {
#      "isSeg"         => 1,
#      "successorTrue" => ["sigseg_1_1"],
#      "visited"       => "",
#      "type"          => Global::Values::GRAPH->{MODULE},
#      "engine"        => Global::Values::ANALYSE_PARAMETERS->{REG},
#      "description" =>
#"mod_sigel_check_1: Überprüfen die evtl. Probleme beim Sigel-Bereich in dem eingegebenen Lemma-Angabe!",
#    },
    # artikel-Sigel-Überprüfungsmodule:
#    "mod_sigel_check_2" => {
#      "content"        => "",
#      "successorTrue"  => ["sigseg_1_2"],
#      "successorFalse" => [],
#      "type"           => Global::Values::GRAPH->{MODULE},
#      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
#      "description" =>
#"mod_sigel_check_2: Überprüfen die evtl. Probleme beim Sigel-Bereich innerhalb der Artikel-Angabe!",
#    },
    
    # Trennzeichen des Eintrags 1: ---- / -----
    "separator_1" => {
      "content"        => qr{\-{3,5}}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "separator",
      "success"        => "rpt_separator_1",
      "failure"        => "rpt_separator_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Kein Trennzeichen des Eintrags '----' steht in der Literaturangabe!",
    },
# Die Formen der Seitenzahl: S. 123(.); 123-234; 123-34; 237*; Nr. 2; 34-56, 78;
#   "seitenzahl_1" => {"content"    => qr{}i,
#                          "successorTrue"  => [],
#                          "successorFalse" => [],
#                          "type"   => "title",
#                          "engine"     => Global::Values::ANALYSE_PARAMETERS->{REG},
#                          "description"=> "" ,
#   },
# Kollation Analyse 1: gibt es die Trennzeichen in der kollation wie: u. , ;
# Falls Trennzeichen in der Kollation vorkommen, dann wird die Kollation als ein Teil gespeichert und nicht mehr analysiert.
#   "kollation_1" => {"content"    => qr{(u\.|;)+}i,
#                          "successorTrue"  => [],
#                          "successorFalse" => [],
#                          "type"   => "kollation",
#                          "engine"     => Global::Values::ANALYSE_PARAMETERS->{REG},
#                          "description"=> "In der Kollation-Bereich steht kein Trennzeichen wie 'u.', ';'.",
#   },
#   "kollation_test" => {"content"    => qr{},
#                          "successorTrue"  => [],
#                          "successorFalse" => [],
#                          "type"   => "kollation",
#                          "engine"     => Global::Values::ANALYSE_PARAMETERS->{REG},
#                          "description"=> "In der Angabe wird die Kollation nicht richtig erkannt!",
#   },
# Sondernzeichen überprüfen 1: es gibt Sonderzeichen wie [] in den bestimmten Bereichen oder nicht.
#   "sonderz_1" => {"content"    => qr{\[.+?\]}i,
#                          "successorTrue"  => [],
#                          "successorFalse" => [],
#                          "type"   => "sonderzeichen",
#                          "engine"     => Global::Values::ANALYSE_PARAMETERS->{REG},
#                          "description"=> "Im Bereich gibt es keine eckige Klammern mit Inhalt wie '[Jura]'.",
#   },
# Sondernzeichen überprüfen 2: es gibt Sonderzeichen wie : in den bestimmten Bereichen oder nicht.
#   "sonderz_2" => {"content"    => qr{\:}i,
#                          "successorTrue"  => [],
#                          "successorFalse" => [],
#                          "type"   => "sonderzeichen",
#                          "engine"     => Global::Values::ANALYSE_PARAMETERS->{REG},
#                          "description"=> "Im Bereich gibt es kein Doppelpunkt wie ':'.",
#   },
    "tilde_1" => {
      "content"        => qr{~~?}i,
      "successorTrue"  => [],
      "successorFalse" => [],
      "type"           => "tilde",
      "success"        => "rpt_tilde_1",
      "failure"        => "rpt_tilde_2",
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description"    => "Es gibt keine Tilde in der Literaturangabe!",
    },
    #   "tilde_2" => { "content"    => qr{~~}i,
    #                      "successorTrue"  => [],
    #                      "successorFalse" => [],
    #                      "type"   => "title",
    #                      "engine"     => Global::Values::ANALYSE_PARAMETERS->{REG},
    #                      "description"=> "Es gibt keine doppelte Tilde in der Literaturangabe!",
    #   },
    # global pat 2: es gibt Sonderzeichen wie [ in den bestimmten Bereichen oder nicht.
    #   "global_2" => {"content"    => qr{[\[\]]},
    #                          "successorTrue"  => ['global_4'],
    #                          "successorFalse" => [],
    #                          "type"   => "sonderzeichen",
    #                          "engine"     => Global::Values::ANALYSE_PARAMETERS->{REG},
    #                          "description"=> "In der Bereich gibt es keine wie '[' oder ']'.",
    #   },
# global pat 4: es gibt Sonderzeichen wie ^^ in den bestimmten Bereichen oder nicht.
#   "global_4" => {"content"    => qr{\[[^\[\]]*\]},
#                          "successorTrue"  => [],
#                          "successorFalse" => [],
#                          "type"   => "sonderzeichen",
#                          "engine"     => Global::Values::ANALYSE_PARAMETERS->{REG},
#                          "description"=> "Im Bereich gibt es ein eckige Klammer z.B.: '[', oder ']', aber nicht paarig.",
#   },
# global pat 5: es gibt Sonderzeichen wie < > # ( ) in den bestimmten Bereichen oder nicht.
#   "global_5" => {"content"    => qr{[<>#\(\)\\]},
#                          "successorTrue"  => [],
#                          "successorFalse" => [],
#                          "type"   => "sonderzeichen",
#                          "engine"     => Global::Values::ANALYSE_PARAMETERS->{REG},
#                          "description"=> "Im Bereich steht keine der folgenden Zeichen: '<', '>', '(', ')' oder '#'.",
#   },
# global pat 6: es gibt Sonderzeichen wie < > # ( ) in den bestimmten Bereichen oder nicht.
# global pat 7: es gibt Sonderzeichen wie nummer in den bestimmten Bereichen oder nicht.
#   "global_7" => {"content"    => qr{\d+},
#                          "successorTrue"  => [],
#                          "successorFalse" => [],
#                          "type"   => "sonderzeichen",
#                          "engine"     => Global::Values::ANALYSE_PARAMETERS->{REG},
#                          "description"=> "Im Bereich kommt keine Zahl vor.",
#  },
# Warnung: Nothing is perfekt!
# Kollation-Band oder Heft-Module 1: mg --> Array, aber Undifinierte sind auch darin.
#  "kollation_bdh_1" => {"content"    => qr{((?:'?[0-9]+\.?\,?)?\s*(?:(?:(?!S|Nr)[a-zA-ZäüöÄÖÜß])+\.?\,?)+)+?|(Nr\.\s*[0-9]+\.?\,?)+?},
#                          "successorTrue"  => [],
#                          "successorFalse" => [],
#                          "type"   => "kollation",
#                          "engine"     => Global::Values::ANALYSE_PARAMETERS->{REG},
#                          "description"=> "Kollation: Band oder Heft 1: Keine Treffen!",
#  },
# Global Fehler überprüfen 1: Das ist ein oberer Knoten für die Fehler-Überprüfungsmodule.
# Es gilt für die alle Bereich in der Literaturangaben.
    # ID-Überprüfungsmodule:
    "mod_idseg_check_1" => {
      "content"        => "",
      "successorTrue"  => ["idseg_2"],
      "successorFalse" => [],
      "type"           => Global::Values::GRAPH->{MODULE},
      "engine"         => Global::Values::ANALYSE_PARAMETERS->{REG},
      "description" =>
        "mod_idseg_check_1: Überprüft die eventuelle Probleme im ID-Bereich im eingegebenen Text",
    },
  };
  
  #CORE:
  my $report_reference_hash = {
    "rpt_info_name" => {
      "description" => "",
      "severity"    => "",
      "type"        => "",
      "action"      => "",
    },
    "rpt_dito_html_1" => {
      "description" =>
"Es gibt mindestens ein dito HTML Element in der aktuellen Literaturangabe mit nachfolgender Form: <do>...</do>",
      "severity" => "4",
      "type"     => "DITO_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_dito_html_2" => {
      "description" =>
"Es gibt kein dito HTML Element in der aktuellen Literaturangabe mit nachfolgender Form: <do>...</do>",
      "severity" => "1",
      "type"     => "DITO_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_dito_html_3" => {
      "description" =>
"Es gibt einige Probleme mit dito HTML Element in der aktuellen Literaturangabe mit nachfolgender Form: <do>...</do>",
      "severity" => "5",
      "type"     => "DITO_ELEMENT",
      "action"   => "Überprüfen Sie bitte die dito Element(e).",
    },
    "rpt_entry_1" => {
      "description" =>
"Es gibt mindestens ein Entry Element in der aktuellen Literaturangabe mit nachfolgender Form: <entry>...</entry>",
      "severity" => "1",
      "type"     => "ENTRY_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_entry_2" => {
      "description" =>
"Es gibt kein Entry Element in der aktuellen Literaturangabe mit nachfolgender Form: <entry>...</entry>",
      "severity" => "1",
      "type"     => "ENTRY_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_entry_3" => {
      "description" =>
"Es gibt einige Probleme mit Entry Element in der aktuellen Literaturangabe mit nachfolgender Form: <entry>...</entry>",
      "severity" => "5",
      "type"     => "ENTRY_ELEMENT",
      "action"   => "Überprüfen Sie bitte die Entry Element(e).",
    },
    "rpt_moreparts_1" => {
      "description" => "Die Literaturangabe könnte mehr Lemmata, Sigle, Verfasser, Entry_Element haben!",
      "severity"    => "5",
      "type"        => "LEMMA_ELEMENT",
      "action" =>
"Überprüfen Sie das LEMMA_ELEMENT, Das steht zwei Lemma-/Sigel-/Verfasser-/Entry-Elements in der Literaturangabe . Das würde eine Fehler sein!",
    },
    "rpt_unknown_part_1" => {
      "description" => "Es gibt mindestens einen unbekannten Bereich in der Literaturangabe!",
      "severity"    => "6",
      "type"        => "UNKNOWN_PART",
      "action"      => "Überprüfen Sie das UNKNOWN_ELEMENT, oder kontaktieren Sie bitten mit dem Entwickler!",
    },
    "rpt_unknown_part_2" => {
      "description" => "Es gibt keinen unbekannten Bereich in der Literaturangabe!",
      "severity"    => "1",
      "type"        => "UNKNOWN_PART",
      "action"      => "Tun Sie nichts.",
    },
    "rpt_special_entry_1" => {
      "description" => "Das ist ein spezielle Literaturangabe, so wie '----', '<strecke=xx>'",
      "severity"    => "1",
      "type"        => "SPECIAL_ENTRY",
      "action"      => "Tun Sie nichts.",
    },
#    "rpt_tilde_1" => {
#      "description" => "Es gibt eine Tilde oder Doppeltilde in der Literaturangabe!",
#      "severity"    => "2",
#      "type"        => "TILDE",
#      "action"      => "Tun Sie nichts",
#    },
#    "rpt_tilde_2" => {
#      "description" => "Es gibt keine Tilde in der Literaturangabe!",
#      "severity"    => "2",
#      "type"        => "TILDE",
#      "action"      => "Tun Sie nichts",
#    },
    "rpt_homonym_1" => {
      "description" => "HOMONYM_ELEMENT steht in der Literaturangabe mit nachfolgender Form: <hom>...</hom>.",
      "severity"    => "4",
      "type"        => "HOMONYM_ELEMENT",
      "action"      => "Tun Sie nichts",
    },
    "rpt_homonym_2" => {
      "description" => "HOMONYM_ELEMENT steht nicht in der Literaturangabe mit nachfolgender Form: <hom>...</hom>.",
      "severity"    => "1",
      "type"        => "HOMONYM_ELEMENT",
      "action"      => "Tun Sie nichts",
    },
    "rpt_homonym_3" => {
      "description" =>
"Es gibt einige Probleme mit Homonym Element in der aktuellen Literaturangabe mit nachfolgender Form: <hom>...</hom>",
      "severity" => "5",
      "type"     => "IGNORE_ELEMENT",
      "action"   => "Überprüfen Sie bitte die Homonym Element(e).",
    },
    "rpt_ignore_1" => {
      "description" =>
"Es gibt mindestens ein Ignore-Element in der aktuellen Literaturangabe mit nachfolgender Form: <ignore>...</ignore>",
      "severity" => "4",
      "type"     => "IGNORE_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_ignore_2" => {
      "description" =>
"Es gibt kein Ignore-Element in der aktuellen Literaturangabe mit nachfolgender Form: <ignore>...</ignore>",
      "severity" => "1",
      "type"     => "IGNORE_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_ignore_3" => {
      "description" =>
"Es gibt einige Probleme mit Ignore Element in der aktuellen Literaturangabe mit nachfolgender Form: <ignore>...</ignore>",
      "severity" => "5",
      "type"     => "IGNORE_ELEMENT",
      "action"   => "Überprüfen Sie bitte die Ignore Element(e).",
    },
    "rpt_rep_1" => {
      "description" =>
"Es gibt mindestens ein Repetition Element in der aktuellen Literaturangabe mit nachfolgender Form: <rep>...</rep>",
      "severity" => "4",
      "type"     => "REP_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_rep_2" => {
      "description" =>
"Es gibt kein Repetition Element in der aktuellen Literaturangabe mit nachfolgender Form: <rep>...</rep>",
      "severity" => "1",
      "type"     => "REP_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_rep_3" => {
      "description" =>
"Es gibt einige Probleme mit Repetition Element in der aktuellen Literaturangabe mit nachfolgender Form: <rep>...</rep>",
      "severity" => "5",
      "type"     => "REP_ELEMENT",
      "action"   => "Überprüfen Sie bitte die Repetition Element(e).",
    },
    "rpt_dop_1" => {
      "description" =>
"Es gibt mindestens ein Doppeln Element in der aktuellen Literaturangabe mit nachfolgender Form: <dop>...</dop>",
      "severity" => "4",
      "type"     => "DOP_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_dop_2" => {
      "description" =>
"Es gibt kein Doppeln Element in der aktuellen Literaturangabe mit nachfolgender Form: <dop>...</dop>",
      "severity" => "1",
      "type"     => "DOP_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_dop_3" => {
      "description" =>
"Es gibt einige Probleme mit Doppeln Element in der aktuellen Literaturangabe mit nachfolgender Form: <dop>...</dop>",
      "severity" => "5",
      "type"     => "DOP_ELEMENT",
      "action"   => "Überprüfen Sie bitte die Doppeln Element(e).",
    },
    "rpt_corr_1" => {
      "description" =>
"Es gibt mindestens ein Corr Element in der aktuellen Literaturangabe mit nachfolgender Form: <corr>...</corr>",
      "severity" => "4",
      "type"     => "CORR_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_corr_2" => {
      "description" =>
"Es gibt kein Corr Element in der aktuellen Literaturangabe mit nachfolgender Form: <corr>...</corr>",
      "severity" => "1",
      "type"     => "CORR_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_corr_3" => {
      "description" =>
"Es gibt einige Probleme mit Corr Element in der aktuellen Literaturangabe mit nachfolgender Form: <corr>...</corr>",
      "severity" => "5",
      "type"     => "CORR_ELEMENT",
      "action"   => "Überprüfen Sie bitte die Corr Element(e).",
    },
    "rpt_sic_1" => {
      "description" =>
"Es gibt mindestens ein Sic Element in der aktuellen Literaturangabe mit nachfolgender Form: <sic>...</sic>",
      "severity" => "4",
      "type"     => "SIC_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_sic_2" => {
      "description" =>
"Es gibt kein Sic Element in der aktuellen Literaturangabe mit nachfolgender Form: <sic>...</sic>",
      "severity" => "1",
      "type"     => "SIC_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_sic_3" => {
      "description" =>
"Es gibt einige Probleme mit Sic Element in der aktuellen Literaturangabe mit nachfolgender Form: <sic>...</sic>",
      "severity" => "5",
      "type"     => "SIC_ELEMENT",
      "action"   => "Überprüfen Sie bitte die Sic Element(e).",
    },
    "rpt_sub_1" => {
      "description" =>
"Es gibt mindestens ein Sub Element in der aktuellen Literaturangabe mit nachfolgender Form: <sub>...</sub>",
      "severity" => "4",
      "type"     => "SUB_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_sub_2" => {
      "description" =>
"Es gibt kein Sub Element in der aktuellen Literaturangabe mit nachfolgender Form: <sub>...</sub>",
      "severity" => "1",
      "type"     => "SUB_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_sub_3" => {
      "description" =>
"Es gibt einige Probleme mit Sub Element in der aktuellen Literaturangabe mit nachfolgender Form: <sub>...</sub>",
      "severity" => "5",
      "type"     => "SUB_ELEMENT",
      "action"   => "Überprüfen Sie bitte die Sub Element(e).",
    },
    "rpt_sup_1" => {
      "description" =>
"Es gibt mindestens ein Sup Element in der aktuellen Literaturangabe mit nachfolgender Form: <sup>...</sup>",
      "severity" => "4",
      "type"     => "SUP_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_sup_2" => {
      "description" =>
"Es gibt kein Sup Element in der aktuellen Literaturangabe mit nachfolgender Form: <sup>...</sup>",
      "severity" => "1",
      "type"     => "SUP_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_sup_3" => {
      "description" =>
"Es gibt einige Probleme mit Sup Element in der aktuellen Literaturangabe mit nachfolgender Form: <sup>...</sup>",
      "severity" => "5",
      "type"     => "SUP_ELEMENT",
      "action"   => "Überprüfen Sie bitte die Sup Element(e).",
    },
    "rpt_ok_1" => {
      "description" =>
"Es gibt ein OK Element in der aktuellen Literaturangabe mit nachfolgender Form: <ok />",
      "severity" => "4",
      "type"     => "VERWEIS_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_ok_2" => {
      "description" =>
"Es gibt kein OK Element in der aktuellen Literaturangabe mit nachfolgender Form: <ok />",
      "severity" => "1",
      "type"     => "OK_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_ok_3" => {
      "description" =>
"Es gibt einige Probleme mit OK Element in der aktuellen Literaturangabe mit nachfolgender Form: <ok>...</ok>",
      "severity" => "5",
      "type"     => "OK_ELEMENT",
      "action"   => "Überprüfen Sie bitte die OK Element(e).",
    },
    "rpt_reference_1" => {
      "description" =>
"Es gibt mindestens eine Verweisung in der aktuellen Literaturangabe mit nachfolgender Form: <verweis>...</verweis>",
      "severity" => "3",
      "type"     => "VERWEIS_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_reference_2" => {
      "description" =>
"Es gibt keine Verweisung in der aktuellen Literaturangabe mit nachfolgender Form: <verweis>...</verweis>",
      "severity" => "1",
      "type"     => "VERWEIS_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_reference_3" => {
      "description" =>
"Es gibt mindestens eine Verweisung in der aktuellen Literaturangabe mit nachfolgender Form: <vl>...</vl>",
      "severity" => "3",
      "type"     => "VERWEIS_LEMMA_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_reference_4" => {
      "description" =>
"Es gibt keine Verweisung in der aktuellen Literaturangabe mit nachfolgender Form: <vl>...</vl>",
      "severity" => "1",
      "type"     => "VERWEIS_LEMMA_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_reference_5" => {
      "description" =>
"Es gibt einige Probleme mit Verweis Element in der aktuellen Literaturangabe mit nachfolgender Form: <verweis>...</verweis>",
      "severity" => "5",
      "type"     => "VERWEIS_ELEMENT",
      "action"   => "Überprüfen Sie bitte die Verweis Element(e).",
    },
    "rpt_reference_6" => {
      "description" =>
"Es gibt einige Probleme mit Verweis-Lemma Element in der aktuellen Literaturangabe mit nachfolgender Form: <vl>...</vl>",
      "severity" => "5",
      "type"     => "VERWEIS_ELEMENT",
      "action"   => "Überprüfen Sie bitte die Verweis-Lemma Element(e).",
    },
    "rpt_verweis_1" => {
      "description" =>
"Es gibt mindestens eine Verweisung in der aktuellen Literaturangabe mit nachfolgender Form: -(-) s. (a./u.).",
      "severity" => "3",
      "type"     => "VERWEIS_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_verweis_2" => {
      "description" =>
"Es gibt mindestens eine Verweisung in der aktuellen Literaturangabe mit nachfolgender Form: -(-) Ferner/Unter...",
      "severity" => "3",
      "type"     => "VERWEIS_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_verweis_3" => {
      "description" => "Es gibt VERWEIS_ELEMENT in der aktuellen Literaturangabe!",
      "severity"    => "3",
      "type"        => "VERWEIS_ELEMENT",
      "action"      => "Tun Sie nichts.",
    },
    "rpt_verweis_4" => {
      "description" =>
"Es gibt keine Verweisung in der aktuellen Literaturangabe mit nachfolgender Form: -(-) s. (a./u.).",
      "severity" => "1",
      "type"     => "VERWEIS_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_verweis_5" => {
      "description" =>
"Es gibt keine Verweisung in der aktuellen Literaturangabe mit nachfolgender Form: -(-) Ferner/Unter...",
      "severity" => "1",
      "type"     => "VERWEIS_ELEMENT",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_verweis_6" => {
      "description" => "Es gibt VERWEIS_ELEMENT in der aktuellen Literaturangabe, obwohl die einige andere Teil fehlt!",
      "severity"    => "4",
      "type"        => "VERWEIS_ELEMENT",
      "action"      => "Überprüfen Sie die Literaturangabe, ob die noch einige wichtige Teile fehlt. Das würde eine Fehler sein!",
    },
    "rpt_verweis_Lemma_1" => {
      "description" => "Es gibt VERWEIS_LEMMA_ELEMENT in der aktuellen Literaturangabe!",
      "severity"    => "2",
      "type"        => "VERWEIS_LEMMA_ELEMENT",
      "action"      => "Tun Sie nichts.",
    },
    "rpt_collation_1" => {
      "description" => "Kollation Element steht in der Literaturangabe.",
      "severity"    => "1",
      "type"        => "COLLATION_ELEMENT",
      "action"      => "Tun Sie nichts",
    },
    "rpt_collation_2" => {
      "description" => "In der Angabe wird die Kollation-Element nicht richtig erkannt!",
      "severity"    => "5",
      "type"        => "COLLATION_ELEMENT",
      "action"      => "Tun Sie nichts",
    },
    "rpt_title_1" => {
      "description" => "Die Literaturangabe hat einen Titel!",
      "severity"    => "1",
      "type"        => "TITEL",
      "action"      => "Tun Sie nichts",
    },
    "rpt_title_2" => {
      "description" => "Die Literaturangabe hat kein Titel!",
      "severity"    => "4",
      "type"        => "TITEL",
      "action"      => "Überprüfen Sie die Literaturangabe, Das liegt normalerweise einen Titel in der Literaturangabe. Das würde eine Fehler sein!",
    },
#lemma begin
    "rpt_lemma_1" => {
      "description" => "Die Lemma-Literaturangabe hat kein Lemma!",
      "severity"    => "5",
      "type"        => "LEMMA_ELEMENT",
      "action" =>
"Überprüfen Sie das LEMMA_ELEMENT, Das liegt normalerweise am Anfang nach dem HEADER_ELEMENT in der Entry_Lemma-Literaturangabe. Das würde eine Fehler sein!",
    },
    "rpt_lemma_1_1" => {
      "description" => "Die Literaturangabe hat kein Lemma!",
      "severity"    => "2",
      "type"        => "LEMMA_ELEMENT",
      "action" =>
"Überprüfen Sie das LEMMA_ELEMENT, Das liegt normalerweise am Anfang nach dem HEADER_ELEMENT in der Entry_Lemma-Literaturangabe. Das würde eine Fehler sein!",
    },
    "rpt_lemma_2" => {
      "description" => "Kein <l> oder </l> kommt in der Lemma-Literaturangabe vor!",
      "description" => "Die Entry_Lemma-Literaturangabe hat falsche Lemma-Form, z.B. <l>lemma<l> (das sollte <l>lemma</l> sein) usw.!",
      "severity" => "5",
      "type"     => "LEMMA_ELEMENT",
      "action" =>
"Überprüfen Sie das LEMMA_ELEMENT, Das liegt normalerweise am Anfang nach dem HEADER_ELEMENT in der Lemma-Literaturangabe. Die <l> und </l>-Tag sollen paarig vorkommen.",
    },
    "rpt_lemma_3" => {
      "description" =>
"Mindestens ein <l> oder </l> kommt in der Lemma-Literaturangabe vor, aber <l>- oder </l>-Tag kommt unpaarig vor!",
      "severity" => "5",
      "type"     => "LEMMA_ELEMENT",
      "action" =>
"Überprüfen Sie das LEMMA_ELEMENT, Das liegt normalerweise am Anfang nach dem HEADER_ELEMENT in der Lemma-Literaturangabe. Die <l> und </l>-Tag sollen auch paarig vorkommen.",
    },
    "rpt_lemma_4" => {
      "description" =>
        "Der Inhalt des Lemmas enthält ungültige Zeichen (insbesondere für Band 74.)",
      "severity" => "5",
      "type"     => "LEMMA_ELEMENT",
      "action" =>
"Überprüfen Sie das LEMMA_ELEMENT, Das liegt normalerweise am Anfang nach dem HEADER_ELEMENT in der Lemma-Literaturangabe. Das Lemma hat vielleicht ungültige Zeichen.",
    },

#  "rpt_lemma_xxx" => {
#    "description" => "Diese Lemmata sind nicht aufsteigend gelegen!",
#    "severity" => "2",
#    "type" => "LEMMA_ELEMENT",
#    "action" => "Überprüfen Sie die LEMMA_ELEMENTs, Normalerweise sind die Lemmata aufsteigend gelegen, Aber es gibt so oft die andere Situation.",
#  },
    "rpt_lemma_5" => {
      "description" => "Lemma-Bereich steht am Ende der Literaturangabe!",
      "severity"    => "5",
      "type"        => "LEMMA_ELEMENT",
      "action" =>
"Überprüfen Sie die LEMMA_ELEMENTs, Lemma-Bereich darf nicht am Ende der Literaturangabe stehen!",
    },
    "rpt_lemma_6" => {
      "description" => "Es gibt einen Lemma-Bereich in der Literaturangabe!",
      "severity"    => "1",
      "type"        => "LEMMA_ELEMENT",
      "action"      => "Tun Sie nichts",
    },
    "rpt_lemma_7" => {
      "description" =>
        "Der Inhalt des Lemmas enthält gültige Zeichen (insbesondere für Band 74.)",
      "severity" => "1",
      "type"     => "LEMMA_ELEMENT",
      "action"   => "Tun Sie nichts",
    },
    "rpt_lemma_8" => {
      "description" => "Die Lemma-Literaturangabe hat mehr Lemmata!",
      "severity"    => "5",
      "type"        => "LEMMA_ELEMENT",
      "action" =>
"Überprüfen Sie das LEMMA_ELEMENT, Das steht zwei Lemma-Elements in der Literaturangabe . Das würde eine Fehler sein!",
    },
    "rpt_lemma_9" => {
      "description" => "Die Literaturangabe hat etwas Problem des Lemma-Bereich!",
      "severity"    => "5",
      "type"        => "LEMMA_ELEMENT",
      "action" => "Überprüfen Sie das LEMMA_ELEMENT. Das würde eine Fehler sein!",
    },
# lemma end
    "rpt_otherline_1" => {
      "description" => "Dies Text kann nicht durch das Programm erkannt werden!",
      "severity"    => "6",
      "type"        => "UNKNOWN_TEXT",
      "action" =>
        "Das Programm kann das nicht analysieren, bitte kontaktieren Sie mit dem Mitglieder.",
    },
    "rpt_entry_type_1" => {
      "description" => "Die angegebene Literatureangabe entspricht dem Typ 'ENTRY_LEMMA'!",
      "severity"    => "1",
      "type"        => "ENTRY_TYPE_LEMMA",
      "action"      => "Tun Sie nichts",
    },
    "rpt_entry_type_2" => {
      "description" => "Die angegebene Literatureangabe entspricht dem Typ 'ENTRY'!",
      "severity"    => "1",
      "type"        => "ENTRY_TYPE_ENTRY",
      "action"      => "Tun Sie nichts",
    },
    "rpt_entry_type_3" => {
      "description" => "Die angegebene Literatureangabe entspricht dem Typ 'STRECKE'!",
      "severity"    => "1",
      "type"        => "ENTRY_TYPE_STRECKE",
      "action"      => "Tun Sie nichts",
    },
    "rpt_entry_type_4" => {
      "description" => "Die angegebene Literatureangabe entspricht dem Typ 'SEPARATOR'!",
      "severity"    => "1",
      "type"        => "ENTRY_TYPE_SEPARATOR",
      "action"      => "Tun Sie nichts",
    },
    "rpt_entry_type_5" => {
      "description" => "Die angegebene Literatureangabe entspricht dem Typ 'ENTRY_LEMMA_HTML'!",
      "severity"    => "1",
      "type"        => "ENTRY_TYPE_LEMMA_HTML",
      "action"      => "Tun Sie nichts",
    },
    "rpt_entry_type_6" => {
      "description" => "Die angegebene Literatureangabe entspricht dem Typ 'ENTRY_HTML'!",
      "severity"    => "1",
      "type"        => "ENTRY_TYPE_ENTRY_HTML",
      "action"      => "Tun Sie nichts",
    },
    "rpt_entry_type_7" => {
      "description" => "Die angegebene Literatureangabe entspricht keinen Typen!",
      "severity"    => "6",
      "type"        => "ENTRY_TYPE_UNKNOWN",
      "action"      => "Tun Sie nichts",
    },
    "rpt_strecke_1" => {
      "description" =>
"In der aktuellen Literaturangabe kommt mindestens ein Strecke-Bereich vor mit der nachfolgenden Form 'strecke=*'.",
      "severity" => "2",
      "type"     => "STRECKE",
      "action"   => "Tun Sie nichts, der Strecke-Bereich liegt zur Zeit nur in der d74.txt-Datei!",
    },
    "rpt_strecke_2" => {
      "description" =>
"In der aktuellen Literaturangabe kommt kein Strecke-Bereich vor mit der nachfolgenden Form  'strecke=*'.",
      "severity" => "1",
      "type"     => "STRECKE",
      "action"   => "Tun Sie nichts, der Strecke-Bereich liegt zur Zeit nur in der d74.txt-Datei!",
    },
    "rpt_page_area_1" => {
      "description" =>
"In der aktuellen Literaturangabe kommt mindestens ein Seitenzahl-Bereich vor mit der nachfolgenden Form 'seite=*'.",
      "severity" => "2",
      "type"     => "SEITE",
      "action" => "Tun Sie nichts, der Seitenzahl-Bereich liegt zur Zeit nur in der d74.txt-Datei!",
    },
    "rpt_page_area_2" => {
      "description" =>
"In der aktuellen Literaturangabe kommt kein Seitenzahl-Bereich vor mit der nachfolgenden Form 'seite=*'.",
      "severity" => "1",
      "type"     => "SEITE",
      "action" => "Tun Sie nichts, der Seitenzahl-Bereich liegt zur Zeit nur in der d74.txt-Datei!",
    },
        "rpt_dito_1" => {
      "description" =>
"Es gibt mindestens einen Dito-Bereich in der aktuellen Literaturangabe mit nachfolgender Form: 'do.'!",
      "severity" => "3",
      "type"     => "DITO_DESC_PART",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_dito_2" => {
      "description" =>
        "Es gibt keinen Dito-Bereich in der aktuellen Literaturangabe mit nachfolgender Form: 'do.'.",
      "severity" => "1",
      "type"     => "DITO_DESC_PART",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_dito_3" => {
      "description" => "Es gibt einige Probleme in dem Dito-Bereich in der aktuellen Literaturangabe.",
      "severity" => "5",
      "type"     => "DITO_DESC_PART",
      "action"   => "Überprüfen die Dito Bereich!",
    },
    "rpt_pipe_1" => {
      "description" =>
        "Mindestens eine Pipe steht am Anfang der Artikel-Literaturangabe, so wie '| ....'.",
      "severity" => "2",
      "type"     => "PIPE",
      "action"   => "Tun Sie nichts.",
    },
    "rpt_pipe_2" => {
      "description" =>
        "Keine Pipe steht am Anfang der Artikel-Literaturangabe, nicht so wie '| ....'. ",
      "severity" => "5",
      "type"     => "PIPE",
      "action" =>
"Überprüfen Sie das Entry, Eine Pipe soll normalerweise am Anfang der Entry-Literaturangabe liegen. Das würde eine Fehler sein!",
    },
    "rpt_pipe_3" => {
      "description" =>
        "Mindestens eine Pipe steht in der Artikel-Literaturangabe, so wie '... | ....'.",
      "severity" => "5",
      "type"     => "PIPE",
      "action" =>
"Überprüfen Sie die Pipe, Das liegt normalerweise am Anfang der Literaturangabe. Das würde eine Fehler sein!",
    },
    "rpt_pipe_4" => {
      "description" =>
        "Keine Pipe steht in der Artikel-Literaturangabe, nicht so wie '... | ....'.",
      "severity" => "1",
      "type"     => "PIPE",
      "action"   => "Tun Sie nichts",
    },
    "rpt_verfasser_1" => {
      "description" =>
"Mindestens ein Verfasserbereich mit den Rundklammern gibt es in der aktuellen Literaturangabe: z.B. (W. Rahe).",
      "severity" => "1",
      "type"     => "VERFASSER",
      "action"   => "Tun Sie nichts",
    },
    "rpt_verfasser_2" => {
      "description" =>
"Kein Verfasserbereich mit den Rundklammern gibt es in der aktuellen Literaturangabe: z.B. (W. Rahe).",
      "severity" => "2",
      "type"     => "VERFASSER",
      "action"   => "Tun Sie nichts",
    },
    "rpt_verfasser_3" => {
      "description" => "In der Literaturangabe gibt es keine Klammer wie '(' oder ')'.",
      "severity"    => "2",
      "type"        => "VERFASSER",
      "action"      => "Tun Sie nichts",
    },
    "rpt_verfasser_4" => {
      "description" => "In dem Verfasserbereich: Klammern unpaarig!",
      "severity"    => "5",
      "type"        => "VERFASSER",
      "action" =>
"Überprüfen Sie den VERFASSERBEREICH, Die Rundklammern sollen immer paarig vorkommen. Das würde eine Fehler sein!",
    },
    "rpt_verfasser_5" => {
      "description" => "In dem Verfasserbereich gibt es eine Tilde wie (~).",
      "severity"    => "2",
      "type"        => "VERFASSER",
      "action"      => "Tun Sie nichts",
    },
    "rpt_verfasser_6" => {
      "description" => "Die Rundklammern stehen am Ende der Literaturangaben.",
      "severity"    => "5",
      "type"        => "VERFASSER",
      "action" =>
"Überprüfen Sie den VERFASSERBEREICH, Die Rundklammern mit dem Verfasserbereich sind nicht erlaubt, am Ende der Literaturangaben zu stehen. Das würde eine Fehler sein!",
    },
    "rpt_verfasser_7" => {
      "description" =>
        "In dem Verfasserbereich gibt es mehrere Paaren Rundklammern für Verfassernamen.",
      "severity" => "4",
      "type"     => "VERFASSER",
      "action" =>
"Überprüfen Sie den VERFASSERBEREICH, normalerweise gibt es nur ein Verfasserbereich in der Literaturangabe. Es soll eine Fehler sein oder die Literaturangabe würde die Erweiterung(en) haben. Sie können auch unsere Wiki-Seite online lesen.",
    },
    "rpt_verfasser_8" => {
      "description" => "Nummer oder Satzzeichen stehen am Anfang in dem Verfasserbereich stehen!",
      "severity"    => "5",
      "type"        => "VERFASSER",
      "action" =>
"Überprüfen Sie den VERFASSERBEREICH, Nummer oder Satzzeichen dürfen nicht am Anfang in dem Verfasserbereich stehen! Das soll eine Fehler sein.",
    },
    "rpt_verfasser_9" => {
      "description" => "Nur Großbuchstaben stehen in dem Verfasserbereich. z.B. (AB)",
      "severity"    => "4",
      "type"        => "VERFASSER",
      "action"      => "Tun Sie nichts",
    },
    "rpt_verfasser_10" => {
      "description" => "In dem Verfasserbereich gibt es keine Tilde wie (~).",
      "severity"    => "1",
      "type"        => "VERFASSER",
      "action"      => "Tun Sie nichts",
    },
    "rpt_verfasser_11" => {
      "description" => "Die Rundklammern stehen nicht am Ende der Literaturangaben.",
      "severity"    => "1",
      "type"        => "VERFASSER",
      "action"      => "Tun Sie nicht.",
    },
    "rpt_header_1" => {
      "description" => "Es gibt keinen HEADER-Bereich in der Lemma-Literaturangabe",
      "severity"    => "5",
      "type"        => "HEADER",
      "action" =>
"Überprüfen Sie den HEADER-BEREICH, der HEADER-Bereich sollen am Anfang der Lemma-Angabe stehen.",
    },
    "rpt_header_2" => {
      "description" => "HEADER-Bereich in der Literaturangabe kann nicht richtig erkannt werden!",
      "severity"    => "5",
      "type"        => "HEADER",
      "action" =>
"Überprüfen Sie den HEADER-BEREICH, der HEADER-Bereich sollen am Anfang der Lemma-Angabe stehen. Seine Form in der d74.txt ist so wie <f00020 74 213 re>.",
    },
    "rpt_header_3" => {
      "description" =>
"Es gibt mindestens einen HEADER-Bereich in der Literaturangabe, aber dieser steht vielleicht nicht am Anfang!",
      "severity" => "5",
      "type"     => "HEADER",
      "action" =>
"Überprüfen Sie den HEADER-BEREICH, der HEADER-Bereich sollen am Anfang der Lemma-Angabe stehen. Das soll ein Fehler sein!",
    },
    "rpt_header_4" => {
      "description" => "Es gibt kein HEADER-Bereich in der Lemma-Literaturangabe",
      "severity"    => "5",
      "type"        => "HEADER",
      "action"      => "Tun Sie nichts.",
    },
    "rpt_header_5" => {
      "description" => "Der Header-Bereich steht am Anfang der Lemma-Angabe",
      "severity"    => "1",
      "type"        => "HEADER",
      "action"      => "Tun Sie nichts.",
    },
    "rpt_header_6" => {
      "description" => "Der HEADER-Bereich hat die richtige Form darzustellen!",
      "severity"    => "1",
      "type"        => "HEADER",
      "action"      => "Tun Sie nichts.",
    },
    "rpt_header_7" => {
      "description" => "Der HEADER-Bereich hat die falsche Form darzustellen!",
      "severity"    => "5",
      "type"        => "HEADER",
      "action"      => "Überprüfen Sie den HEADER Bereich!",
    },
    "rpt_sigel_1" => {
      "description" => "Es gibt keinen gültigen sigel-Bereich in der Literaturangabe!",
      "severity"    => "2",
      "type"        => "SIGEL_ELEMENT",
      "action"      => "Tun Sie nichts",
    },
    "rpt_sigel_2" => {
      "description" => "Es gibt keinen gültigen Sigel-Bereich in der Lemma-Literaturangabe!",
      "severity"    => "5",
      "type"        => "SIGEL_ELEMENT",
      "action" =>
"Überprüfen Sie das SIGEL_ELEMENT, Lemma-Literaturangabe hat normalerweise eine gültige Sigel-Bereich, aber die Angabe können auch direkt eine Weiterleitung oder Verweis oder Verweis_Lemma haben. Das ist auch gültig.",
    },
    "rpt_sigel_3" => {
      "description" => "Sigel-Bereich steht am Ende der Literaturangabe!",
      "severity"    => "5",
      "type"        => "SIGEL_ELEMENT",
      "action" =>
"Überprüfen Sie das SIGEL_ELEMENT, Sigel-Bereich darf nicht am Ende der Literaturangabe stehen!",
    },
    "rpt_sigel_4" => {
      "description" => "Kein Kein <s>-TAG oder kein </s>-Tag in der Literaturangabe vorhanden!",
      "severity"    => "2",
      "type"        => "SIGEL_ELEMENT",
      "action"      => "Tun Sie nichts!",
    },
    "rpt_sigel_5" => {
      "description" => "Kein <s>-TAG oder kein </s>-Tag in der Lemma-Literaturangabe vorhanden!",
      "severity"    => "4",
      "type"        => "SIGEL_ELEMENT",
      "action" =>
"Überprüfen Sie das SIGEL_ELEMENT, VERWEIS, VERWEIS_LEMMA, Lemma-Literaturangabe hat normalerweise eine gültige Sigel-Bereich, aber die Angabe können auch direkt eine Weiterleitung oder Verweis oder Verweis_Lemma haben. Das ist auch gültig!",
    },
    "rpt_sigel_6" => {
      "description" =>
"Mindestens ein <s> oder </s> kommt in der Literaturangabe vor, aber <s>- oder </s>-Tag kommt unpaarig vor!",
      "severity" => "5",
      "type"     => "SIGEL_ELEMENT",
      "action" =>
        "Überprüfen Sie das SIGEL_ELEMENT, Die <s> und </s>-Tag sollen paarig vorkommen.",
    },
    "rpt_sigel_7" => {
      "description" => "Der Inhalt des Sigels ist ungültig, wie die Form '3999aaa.'.",
      "severity"    => "5",
      "type"        => "SIGEL_ELEMENT",
      "action"      => "Überprüfen Sie das SIGEL_ELEMENT, der Inhalt würde nicht richtig sein.",
    },
    "rpt_sigel_8" => {
      "description" => "<s>-Tag bzw. </s>-Tag sind nicht paarig: z.B. <s>...<s> bzw. </s>...</s> !",
      "severity"    => "5",
      "type"        => "SIGEL_ELEMENT",
      "action" =>
        "Überprüfen Sie das SIGEL_ELEMENT, Die <s> und </s>-Tag sollen paarig vorkommen.",
    },
    "rpt_sigel_9" => {
      "description" => "Es gibt einen sigel-Bereich in der Literaturangabe!",
      "severity"    => "1",
      "type"        => "SIGEL_ELEMENT",
      "action"      => "Tun Sie nichts.",
    },
    "rpt_sigel_10" => {
      "description" => "Sigel-Bereich steht nicht am Ende der Literaturangabe!",
      "severity"    => "1",
      "type"        => "SIGEL_ELEMENT",
      "action"      => "Tun Sie nichts.",
    },
    "rpt_sigel_11" => {
      "description" => "Das Sigel in der Literaturangabe ist gültig.",
      "severity"    => "1",
      "type"        => "SIGEL_ELEMENT",
      "action"      => "Tun Sie nichts.",
    },
    "rpt_sigel_12" => {
      "description" => "<s>-Tag oder </s>-Tag kommt mehrfach vor!",
      "severity"    => "5",
      "type"        => "SIGEL_ELEMENT",
      "action" =>
        "Überprüfen Sie das SIGEL_ELEMENT, Die <s> und </s>-Tag sollen paarig vorkommen.",
    },
    "rpt_sigel_13" => {
      "description" => "Die <s>-Tag Paare kommen mehrfach vor!",
      "severity"    => "5",
      "type"        => "SIGEL_ELEMENT",
      "action" =>
        "Überprüfen Sie das SIGEL_ELEMENT, Die <s> und </s>-Tag soll nur einmal vorkommen.",
    },
    "rpt_sigel_14" => {
      "description" =>
"Es gibt einige Probleme mit Sigel Element in der aktuellen Literaturangabe mit nachfolgender Form: <s>...</s>",
      "severity" => "5",
      "type"     => "SIGEL_ELEMENT",
      "action"   => "Überprüfen Sie bitte die Sigel Element(e).",
    },
    "rpt_separator_1" => {
      "description" => "Trennzeichen des Eintrags '----' steht in der Literaturangabe!",
      "severity"    => "2",
      "type"        => "ENTRY_TYPE_SEPARATOR",
      "action"      => "Tun Sie nichts",
    },
    "rpt_separator_2" => {
      "description" => "Kein Trennzeichen des Eintrags '----' steht in der Literaturangabe!",
      "severity"    => "2",
      "type"        => "ENTRY_TYPE_SEPARATOR",
      "action"      => "Tun Sie nichts",
    },
    "rpt_char_1" => {
      "description" => "Im Bereich gibt es keine ungültige Zeichen wie '^' oder '^^'!",
      "severity"    => "1",
      "type"        => "SPECIAL_CHARACTER",
      "action"      => "Tun Sie nichts",
    },
    "rpt_char_2" => {
      "description" => "In der Bereich gibt es '[' oder ']'.",
      "severity"    => "4",
      "type"        => "SPECIAL_CHARACTER",
      "action" =>
"Überprüfen Sie die Literaturangabe, zwischen '[' und ']' können der sondere  Inhalt gelegen werden, das kann auch die Fehler vorkommen. Es könnte ein Tilde sein.",
    },
    "rpt_char_3" => {
      "description" =>
        "Im Bereich gibt es ein eckige Klammer z.B.: '[', oder ']', aber nicht paarig.",
      "severity" => "5",
      "type"     => "SPECIAL_CHARACTER",
      "action" =>
"Überprüfen Sie die Literaturangabe,  die eckige Klammern z.B.: '[', und ']', sollen paarig vorkommen.",
    },
    "rpt_char_4" => {
      "description" =>
        "Im Bereich steht mindestens eines der folgenden Zeichen: '<', '>', '(', ')' oder '#'.",
      "severity" => "5",
      "type"     => "SPECIAL_CHARACTER",
      "action" =>
"Überprüfen Sie die Literaturangabe, diese Zeichen: '<', '>', '(', ')' oder '#' sollen nicht in der Literaturangabe vorkommen.",
    },
    "rpt_char_5" => {
      "description" => "In der Bereich gibt es Zeichenketten wie '3chaft' ,'3Ö2' oder '30J'.",
      "severity"    => "5",
      "type"        => "SPECIAL_CHARACTER",
      "action" =>
"Überprüfen Sie die Literaturangabe, diese Zeichenketten, wie '3chaft' ,'3Ö2' oder '30J', sollten falsch sein.",
    },
    "rpt_char_6" => {
      "description" => "Im Bereich gibt es ungültige Zeichen wie '^', '^^' usw.",
      "severity"    => "5",
      "type"        => "SPECIAL_CHARACTER",
      "action" =>
"Überprüfen Sie die Literaturangabe, '^' soll nicht vorkommen. Es könnte ein Tilde sein.",
    },
    "rpt_char_7" => {
      "description" =>
        "In der Bereich gibt es keine Zeichenketten wie '3chaft' ,'3Ö2' oder '30J' usw.",
      "severity" => "1",
      "type"     => "SPECIAL_CHARACTER",
      "action"   => "Tun Sie nichts.",
    },
  };
  my $regHash = {
    Global::Values::ANALYSE_PARAMETERS->{REGEXP_PATTERN_HASH}    => $pat_engine_hash,
    Global::Values::ANALYSE_PARAMETERS->{REPORT_REFERENCE_HASH} => $report_reference_hash,
  };
  return $regHash;
}

##############################
# files struckture
#
# root
#  |
#  |--src
#  |   |
#  |   - main.pl (method initPaths)
#  |--data
#  |--data_result
#  |--design
#  |--database
#  |--design
#  |--lib
#  |--template
#  |--test
#  |--web

=head2 initPaths()

Initializes the file paths of system.

=cut
sub initPaths {
  my $self = shift;
  if ( !$self->isInitPaths ) {
    use FindBin;

    #    use Cwd;
    #    print Dumper(cwd);
    $self->globVal->{binPath} = "$FindBin::Bin/";
    $self->globVal->{dataPath}     = "$FindBin::Bin/../data";
    $self->globVal->{dataResultPath}   = "$FindBin::Bin/../data_result";
    $self->globVal->{databasePath} = "$FindBin::Bin/../database";
    $self->globVal->{designPath}   = "$FindBin::Bin/../design";
    $self->globVal->{templatePath} = "$FindBin::Bin/../template";
    $self->globVal->{testPath}     = "$FindBin::Bin/../test";
    $self->globVal->{webPath}      = "$FindBin::Bin/../web";
    $self->globVal->{configPath}   = "$FindBin::Bin/../config";

    $self->isInitPaths(1);
  }
}
=head2 initMotDB()

Initializes Database::MotDB

=cut
sub initMotDB {
  my $self = shift;
  if (! $self->isInitMotDB) {
    $self->isInitMotDB(1);
  }
}

no Moose::Util::TypeConstraints;
no Moose;
__PACKAGE__->meta->make_immutable;
1;

