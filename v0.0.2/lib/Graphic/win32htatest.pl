use Win32::HTA;
 
my $hta = Win32::HTA->new();
 
# Simple MessageBox with return value
my $chosen = $hta->show(
    TITLE => 'Important Information!',
    DLG_HTML => qq(
        <p id="msg_txt">Something happened!</p><br>
        <button id="bok"     type="button" onclick="ret_handler();">OK</button>
        <button id="bcancel" type="button" onclick="esc_handler();">CANCEL</button>
    ),
    CSS => q(
        p      { width: 250px; text-align: center; }
        button { width:  80px; margin-left: 30px;}
    ),
    ON_LOAD => q[
        bok.focus();
    ],
    RET_HANDLER => q(pipe_string('OK')),
    ESC_HANDLER => q(pipe_string('CANCEL')),
);
print $chosen, "\n";
 
# Reset to default
$hta->clear();
 
 
# AJAX communication
require JSON;
$hta->show(
    AJAX => sub {
        my($request) = @_;
        print JSON::encode_json($request);
        my $ok = int(rand()+0.5);
        return {
            succeeded => $ok,
            data      => {
                name => $request->{request},
                text => $request->{data},
            },
            $ok ? () : ( errtxt => 'Something went wrong!'),
        };
    },
    IE_MODE => 11,
    BG_COLOR => '#888800',
    DLG_HTML => qq(
        <button id="but" type="button" onclick="bpress()">Talk to Perl</button><br>
    ),
    RET_HANDLER => q(but.click();),
    CSS => 'button {width: 150px;}',
    JS_HEAD => q(
        function bpress() {
            var reply = ajax_request({request : "test", data : 'testdata'});
            if ( reply['succeeded'] ) {
                alert(reply['data']['text']);
            } else {
                alert(reply['errtxt']);
            }
            close();
        }
    ),
);