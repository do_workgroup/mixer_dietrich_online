use Tk;

#use strict; 
#use Tk; 
#my($o, $s) = (250, 20); 
#my($pi, $x, $y) = (3.1415926, 0); 
#my $mw = MainWindow->new; 
#my $c = $mw->Canvas(-width => 500, -height => 500); 
#$c->pack; 
#$c->createLine(50, 250, 450, 250); 
#$c->createText(10, 250, -fill => 'blue', -text => 'X'); 
#$c->createLine(250, 50, 250, 450); 
#$c->createText(250, 10, -fill => 'blue', -text => 'Y');
#for ($x = -(3*$pi); $x <= +(3*$pi); $x += 0.1) { 
#    $y = sin($x); 
#    $c->createText( $x*$s+$o, $y*$s+$o, -fill => 'red', -text => '.'); 
#    $y = cos($x); 
#    $c->createText( $x*$s+$o, $y*$s+$o, -fill => 'green', -text => '.'); 
#}
#
#MainLoop;



 
#my $mw = MainWindow->new;
# 
#$mw->Label(-text => 'File Name')->pack;
#my $filename = $mw->Entry(-width => 20);
#$filename->pack;
# 
#$mw->Label(-text => 'Font Name')->pack;
#my $font = $mw->Entry(-width => 10);
#$font->pack;
# 
#$mw->Button(
#    -text => 'Fax',
#    -command => sub{do_fax($filename, $font)}
#)->pack;
# 
#$mw->Button(
#    -text => 'Print',
#    -command => sub{do_print($filename, $font)}
#)->pack;
# 
#MainLoop;
# 
#sub do_fax {
#    my ($file, $font) = @_;
#    my $file_val = $file->get;
#    my $font_val = $font->get;
#    print "Now faxing $file_val in font $font_val\n";
#}
# 
#sub do_print {
#    my ($file, $font) = @_;
#    my $file_val = $file->get;
#    my $font_val = $font->get;
#    print "Sending file $file_val to printer in font $font_val\n";
#}


## Create B<MainWindow> and canvas
#my $mw = MainWindow->new;
#my $canvas = $mw->Canvas;
#$canvas->pack(-expand => 1, -fill => 'both');
# 
## Create various items
#create_item($canvas, 1, 1, 'circle', 'blue', 'Jane');
#create_item($canvas, 4, 4, 'circle', 'red', 'Peter');
#create_item($canvas, 4, 1, 'square', 'blue', 'James');
#create_item($canvas, 1, 4, 'square', 'red', 'Patricia');
# 
## Single-clicking with left on a 'circle' item invokes a procedure
#$canvas->bind('circle', '<1>' => sub {handle_circle($canvas)});
## Double-clicking with left on a 'blue' item invokes a procedure
#$canvas->bind('blue', '<Double-1>' => sub {handle_blue($canvas)});
#MainLoop;
# 
## Create an item; use parameters as tags (this is not a default!)
#sub create_item {
#    my ($can, $x, $y, $form, $color, $name) = @_;
# 
#    my $x2 = $x + 1;
#    my $y2 = $y + 1;
#    my $kind;
#    $kind = 'oval' if ($form eq 'circle');
#    $kind = 'rectangle' if ($form eq 'square');
#    $can->create(
#        ($kind, "$x" . 'c', "$y" . 'c',
#        "$x2" . 'c', "$y2" . 'c'),
#        -tags => [$form, $color, $name],
#        -fill => $color);
#}
# 
## This gets the real name (not current, blue/red, square/circle)
## Note: you'll want to return a list in realistic situations...
#sub get_name {
#    my ($can) = @_;
#    my $item = $can->find('withtag', 'current');
#    my @taglist = $can->gettags($item);
#    my $name;
#    foreach (@taglist) {
#        next if ($_ eq 'current');
#        next if ($_ eq 'red' or $_ eq 'blue');
#        next if ($_ eq 'square' or $_ eq 'circle');
#        $name = $_;
#        last;
#    }
#    return $name;
#}
# 
#sub handle_circle {
#    my ($can) = @_;
#    my $name = get_name($can);
#    print "Action on circle $name...\n";
#}
# 
#sub handle_blue {
#    my ($can) = @_;
#    my $name = get_name($can);
#    print "Action on blue item $name...\n";
#}




#use POSIX 'acos';
#
#my $mw = MainWindow->new;
#my $text = $mw->Text(qw/-width 40 -height 10/)->pack;
# 
#tie *STDOUT, ref $text, $text;
# 
#print "Hello Text World!\n";
#printf "pi ~= %1.5f", acos(-1.0);
# 
#MainLoop;










