use GD::Graph::hbars;
use GD::Text;
use Tk;
use Tk::PNG;
use MIME::Base64;

GD::Text->font_path("C:/Windows/Fonts/");


my @data = ([qw(click clack moo giggle duck)], [100,37,75,88,150]);

my $g = GD::Graph::hbars->new(800,600);

$g->set_title_font("arialbd.ttf",16);
$g->set_x_label_font("timesbd.ttf",16);
$g->set_y_label_font("timesbd.ttf",16);
$g->set_x_axis_font("timesbd.ttf",16);
$g->set_y_axis_font("timesbd.ttf",16);

$g->set(
  x_label => "x Achse", 
  y_label => "y Achse",
  title => 'Comparison',
  x_label_position => 0.5,
  bar_spacing => 10,
  values_space => 15,
  shadow_depth => 4,
  shadowclr => 'dred',
  transparent => 0,
  showvalues => $g,
);


$g->plot(\@data);


#open my $T, '>', 't.png' or die "Can't open t.png:$!\n";
#binmode $T;
#print $T $g->gd->png;
#close $T;


my $mw = MainWindow->new;
my $png = $mw->Photo(
  -data => encode_base64($g->gd->png),
  -format => 'png',
);

my $btn = $mw->Button(-text=> 'doit', -command => sub {
  $data[1] = [map{rand 5} 1 .. 5];
  undef $g;
  $g = GD::Graph::hbars->new(800,600);
  $g->set_title_font("arialbd.ttf",16);
  $g->set_x_label_font("timesbd.ttf",16);
  $g->set_y_label_font("timesbd.ttf",16);
  $g->set_x_axis_font("timesbd.ttf",16);
  $g->set_y_axis_font("timesbd.ttf",16);
  
  $g->set(
    x_label => "x Achse", 
    y_label => "y Achse",
    title => 'Comparison',
    x_label_position => 0.5,
    bar_spacing => 10,
    values_space => 15,
    shadow_depth => 4,
    shadowclr => 'dred',
    transparent => 0,
    showvalues => $g,
  );
  my $gd = $g->plot(\@data) or die $!;
  
  $png->blank;
  $png->configure(
    -data => encode_base64($gd->png),
    -format => 'png',
  );
  $mw->update;
})->pack;

$mw->Label(-image=>$png)->pack;
$mw->repeat(1000, sub{$btn->invoke; $png->update});
MainLoop;











