package Module::ModuleUnitFactory;
=encoding utf8

=head1 NAME

Module::ModuleUnitFactory 

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package can be used to create Module Unit Objects. This is based on factory pattern mode.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

no Moose;
__PACKAGE__->meta->make_immutable;
1;




#CLASS:
package Module::AbstractModuleUnitFactory;
=encoding utf8

=head1 NAME

Module::AbstractModuleUnitFactory - The abstract class of factory to create module units

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This is abstract superclass for creating module units. Standard behaviors are defined here, 

the programmer needs only to extend this class and provide implementations for this methods. 

The non-abstract methods have their descriptions with its implementation in Detail.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose::Role;
#use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

=head1 REQUIRES METHODS

=head2 getRegModUnit

Returns RegExp analyse Module unit

=cut
requires 'getRegModUnit';
=head2 getRegModuleModUnit

Returns RegExp Model Module unit

=cut
requires 'getRegModuleModUnit';
=head2 getHtmlModUnit

Returns Html Module unit

=cut
requires 'getHtmlModUnit';
=head2 getGraphModUnit

Returns Graph Module unit

=cut
requires 'getGraphModUnit';
=head2 getModUnit

Returns the maked Module unit

=cut
requires 'getModUnit';

no Moose;
1;

###########################################
# package Module::RegModuleUnitFactory
###########################################
package Module::RegModuleUnitFactory;

use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Module::ModuleUnits;

with 'Module::AbstractModuleUnitFactory';


sub getRegModUnit {
  my $self = shift;
  my $unit = Module::RegModuleUnit->new();
  return $unit;
}
sub getHtmlModUnit {
  return;
}
sub getGraphModUnit {
  return;
}

sub getRegModuleModUnit {}

sub getModUnit {
  my $self = shift;
  return $self->getRegModUnit;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

#class:
###########################################
# package Module::HtmlModuleUnitFactory
###########################################
package Module::HtmlModuleUnitFactory;

use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Module::ModuleUnits;

with 'Module::AbstractModuleUnitFactory';


sub getGraphModUnit {
  return;  
}
sub getRegModUnit {
  return;  
}
sub getHtmlModUnit {
  my $self = shift;
  my $unit = Module::HTMLModuleUnit->new();
  return $unit;
}
sub getRegModuleModUnit {}

sub getModUnit {
  my $self = shift;
  return $self->getHtmlModUnit;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

#class:
###########################################
# package Module::GraphModuleUnitFactory
###########################################
package Module::GraphModuleUnitFactory;

use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Module::ModuleUnits;

with 'Module::AbstractModuleUnitFactory';


sub getRegModUnit {
  return;  
}
sub getHtmlModUnit {
  return;
}
sub getGraphModUnit {
  my $self = shift;
  my $unit = Module::GraphModuleUnit->new();
  return $unit;
}
sub getRegModuleModUnit {}

sub getModUnit {
  my $self = shift;
  return $self->getGraphModUnit;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;
#class:
###########################################
# package Module::RegModuleModeUnitFactory
###########################################
package Module::RegModuleModeUnitFactory;

use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Module::ModuleUnits;

with 'Module::AbstractModuleUnitFactory';


sub getRegModUnit {
  return;  
}
sub getHtmlModUnit {
  return;
}
sub getGraphModUnit {}

sub getRegModuleModUnit {
my $self = shift;
  my $unit = Module::RegModModuleUnit->new();
  return $unit;
}

sub getModUnit {
  my $self = shift;
  return $self->getRegModuleModUnit;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
package Module::UnitFactoryProducer;
=encoding utf8

=head1 NAME

Module::UnitFactoryProducer - a producer of module unit

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Module::AbstractModuleUnitFactory;
use Module::HtmlModuleUnitFactory;
use Module::RegModuleUnitFactory;

enum 'DO_MODULE_UNIT_TYPE', [@{Global::Values->doModuleUnitType}];

=head2 getModUnit

Returns the maked Module unit

=cut
sub getModUnit {
  my $self = shift;
  my $unitType = shift;
  return if ! defined $unitType || ! $unitType;
  if ($unitType eq 'REGEXP_UNIT') {
#    return Module::RegModuleUnitFactory->new;
    return Module::RegModuleUnit->new();
  } 
  elsif ($unitType eq 'REGEXP_MOD_UNIT') {
#    return Module::RegModuleModeUnitFactory->new;
    return Module::RegModModuleUnit->new();;
    
  }
  elsif ($unitType eq 'HTML_UNIT') {
#    return Module::HtmlModuleUnitFactory->new;
    return Module::HTMLModuleUnit->new();
  }
  elsif ($unitType eq 'GRAPH_UNIT') {
#    return Module::GraphModuleUnitFactory->new;
    return Module::GraphModuleUnit->new();
  }
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
package Module::UnitFactoryApp;
=encoding utf8

=head1 NAME

Module::UnitFactoryApp - Application of module unit Factory

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Data::Dumper;
use Carp;

use Module::AbstractModuleUnitFactory;
use Module::HtmlModuleUnitFactory;
use Module::RegModuleUnitFactory;

use Module::UnitFactoryProducer;

use Utils::Tools;

enum 'DO_MODULE_UNIT_TYPE', [@{Global::Values->doModuleUnitType}];

=head1 CONSTRUCTORS

=head2 BUILD

After new

=cut
sub BUILD {
  my $self = shift;
  $self->factoryProducer(Module::UnitFactoryProducer->new);
  
}

=head2 _build_singleton

=cut
sub _build_singleton {
  return Module::UnitFactoryApp->new;
}
=head1 ATTRIBUTES

=head2 factoryProducer

Module::UnitFactoryProducer

=cut
has 'factoryProducer' => (
  is => 'rw',
  isa => 'Module::UnitFactoryProducer',
  handles => [qw( getFactory)],
);

=head1 METHODS

=head2 getInstance

Singleton.

=cut
my $singleton_UnitFactoryApp;
sub getInstance {
  my $self = shift;
  if (! defined $singleton_UnitFactoryApp) {
    $singleton_UnitFactoryApp = $self->new;
  }
  return $singleton_UnitFactoryApp;
}
=head2 getModUnit

Returns the maked Module unit

=cut
sub getNewUnit {
  my $self = shift;
  my $unitType = shift;
  my $newUnit = $self->factoryProducer->getModUnit($unitType);
#  my $newUnit = $factory->getModUnit;
  
  return $newUnit;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

