package Module::ModuleUnits;
=encoding utf8

=head1 NAME

Module::ModuleUnits - Module Units for analysis.

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
package Module::AbstractModuleUnit;
=encoding utf8

=head1 NAME

Module::AbstractModuleUnit - The abstract class of module units

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This is abstract superclass of module units for analysis. Standard behaviors like the 

getType, clear, execute, input and output methods are defined here, the programmer needs only to extend

this class and provide implementations for this methods. 

The non-abstract methods have their descriptions with its implementation in Detail.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose::Role;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;

use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Global::Values;

=head1 ATTRIBUTES

=head2 id

the module id

=cut
has 'id' => (
  is        => 'ro',
  isa       => 'Str',
  predicate => "hasID",
  clearer   => 'clearID',
  writer    => 'setID',
  reader    => 'getID',
  init_arg  => undef,
);

=head2 name

the module name

=cut
has 'name' => (
  is        => 'ro',
  isa       => 'Str',
  predicate => "hasName",
  clearer   => 'clearName',
  writer    => 'setName',
  reader    => 'getName',
  init_arg  => undef,
);


=head2 outputTable

The Result Map for output after analyse.

=begin text

1. for regular exp Module output
 unit tables: module unit id => outputTable ...
     unitID_1=> {
       unitType => REGEXP_UNIT,
       analyseResult => {
            name    => xxx,
            matched => 1/0,
            pos     => ['1,2', "2,3" ...],
       }
     }
     unitID_2 => {
       unitType => REGEXP_MOD_UNIT,
       analyseResult => {
            name    => xxx,
            matched => 1,
            pos     => ['0,0'],
       }
     }
     unitID_3 => {
       unitType => HTML_UNIT,
       analyseResult => {...}
     }

2. for html module output
  my %result = (
    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
    "elements"    => "",    # element: element text with array ref
    "restText"    => "",    # restText: the input original text without current elements (text reference)
    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
    "eleNum"      => 1,   # the found element amount
    "eleFlags"    => "",    # eleFlags: element flags with array ref
    "content"     => "",    # content: content with array ref
    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
    "attr"        => "",    # attr: attributes in einem Tag with hash ref
    "attrseq"     => "",    # attrseq: attr keys with array ref
    "targetText"        => "",    # text: target original text with text ref
    "link"        => "",    # link: url link with text ref
    "rawText"     => "",    # rawText: the input original text ref
    "hasTagName"  => "",
    "hasRawText"  => "",
    "hasElement"  => "",
    "hasRestText"  => "",
    "hasTargetText"  => "",
    "hasContent"  => "",
    "hasAttr"  => "",
    "hasAttrseq"  => "",
    "hasLink"  => "",
  );

=end text

=cut
has 'outputTable' => (
  is        => 'ro',
  isa       => 'HashRef',
  predicate => "hasOutputTable",
  clearer   => 'resetOutputTable',
  writer    => 'setOutputTable',
  reader    => 'getOutputTable',
  lazy      => 1,
  default   => sub { {} },
  init_arg  => undef,
);
=head1 REQUIRES METHODS

=head2 getModuleUnitType

Returns the module unit type

=cut
requires "getModuleUnitType";
=head2 getModUnitID

Returns the id of module unit

=cut
requires "getModUnitID";
=head2 setModUnitID

Sets the id of module unit

=cut
requires "setModUnitID";

# name: reg name or html check name
=head2 getModUnitName

Returns the name of module unit

=cut
requires "getModUnitName";
=head2 setModUnitName

Sets the name of module unit

=cut
requires "setModUnitName";

# output table
=head2 updateOutputTable

Update the output table of module unit, see outputTable

=cut
requires "updateOutputTable";

=head2 fetchFromOutputTable($key)

Returns the value of the given key from the output table.

=cut
sub fetchFromOutputTable {
  my $self = shift;
  my $key  = shift;
  return $self->getOutputTable($key);
}

no Moose;
1;

#CLASS:
package Module::GraphModuleUnit;
=encoding utf8

=head1 NAME

Module::GraphModuleUnit - Module unit of graph processing

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 SEE ALSO: 

Algorithms::DOGraph

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;

#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Clone;

use Global::Values;

use Algorithms::DOGraph;

with 'Module::AbstractModuleUnit';

enum 'MOD_UNIT_TYPE', [ @{ Global::Values->doModuleUnitType } ];

=head1 CONSTRUCTORS

=head2 BUILD

After new.

=cut
sub BUILD {
  my $self = shift;
}
=head2 _build_curgraph

=cut
sub _build_curgraph {
  return Algorithms::DOGraph->new;
}
=head1 ATTRIBUTES

=head2 type

Type of the class (MOD_UNIT_TYPE: Global::Values->doModuleUnitType)

=cut
has 'type' => (
  is      => 'ro',
  isa     => 'MOD_UNIT_TYPE',
  default => 'GRAPH_UNIT',
);
=head2 graph

Algorithms::DOGraph

=cut
has 'graph' => (
  is      => 'ro',
  isa     => 'Algorithms::DOGraph',
  builder => '_build_curgraph',
  lazy    => 1,
  writer  => 'setGraph',
  reader  => 'getGraph',
  handles => [qw(writeGraph showGraph setCurVertex getCurVertex isMarked markV getNoMarkedDOSuccessors getNoMarkedDOSuccessorsWithMatched getNextUnmarkedDOVertex
  setVMatchAttr getVMatchAttr addToMarkedSet)],
);

=head1 METHODS

=head2 getModuleUnitType()

=head3 Overrides:

getModuleUnitType in class Module::AbstractModuleUnit

=cut
sub getModuleUnitType {
  my $self = shift;
  return $self->type;
}
=head2 getModUnitID()

=head3 Overrides:

getModUnitID in class Module::AbstractModuleUnit

=cut
sub getModUnitID {
  my $self = shift;
  return $self->getID;
}
=head2 setModUnitID()

=head3 Overrides:

setModUnitID in class Module::AbstractModuleUnit

=cut
sub setModUnitID {
  my $self = shift;
  my $id = shift;
  return $self->setID($id);
}

=head2 getModUnitName()

=head3 Overrides:

getModUnitName in class Module::AbstractModuleUnit

=cut
sub getModUnitName {
  my $self = shift;
  return $self->getName;
}
=head2 setModUnitName()

=head3 Overrides:

setModUnitName in class Module::AbstractModuleUnit

=cut
sub setModUnitName {
  my $self = shift;
  my $name = shift;
  return $self->setName($name);
}
=head2 updateOutputTable()

=head3 Overrides:

updateOutputTable in class Module::AbstractModuleUnit

=cut
sub updateOutputTable {
  my $self = shift;
  return 1;
}

=head2 process()

Immediately performs the analysis of the task in the class.

=cut
sub process {
  my $self = shift;
  # output table
  $self->updateOutputTable;
  return 1;
}

=head2 newUnit($rootName, $graphHash)

Returns a new graph module unit.

=head3 Paramters:

=over 4

=item *

$rootName: the given vertex name, also the pattern name.
=item *
$regPatMap: the given pattern information map, see Global::Values::ANALYSE_PARAMETERS->{REGEXP_PATTERN_HASH}
=back

=cut
sub newUnit {
  my $self      = shift;
  my $rootName  = shift;
  my $regPatMapRef = shift;
  
  if (! defined $rootName || $rootName eq '') {
    carp "There is no Root!";
    return;
  }
  if (! defined $regPatMapRef || $regPatMapRef eq '') {
    carp "There is no valid reg pattern engine map!";
    return;
  }
  
  my $graph = $self->getGraph;
  # root vertex
  $graph->updateCurRoot($rootName);
  # current vertex
  $graph->setCurVertex($rootName);

  my $resRef = $graph->addDOSuccessors( $rootName, $regPatMapRef );

  while ( defined $resRef && scalar @$resRef ) {
    my @temp;
    for my $v (@$resRef) {
      my $arrRef = $graph->addDOSuccessors( $v, $regPatMapRef );
      push @temp, @$arrRef if scalar @$arrRef;
    }
    $resRef = \@temp;
  }
  return $graph;
}

=head2 getUnmarkedVertexToAnalyse()

Returns a unmarked vertex name to analyse.

=cut
sub getUnmarkedVertexToAnalyse {
  my $self = shift;
  
  my $v = $self->getCurVertex;
  
  if (! defined $v || $v eq "") {
    carp "There is no valid current vertex.";
    return;
  }
  # if the vertex is not marked, return it
  if (! $self->isMarked($v)) {
    # set it marked
    $self->addToMarkedSet($v);
    # set current vertex
    $self->setCurVertex($v);
    return $v;
  } else {
    # go searching the unmarked child
    my $matched = $self->getVMatchAttr($v);
    my $child = $self->getNoMarkedDOSuccessorsWithMatched($v);
    if (defined $child && $child ne "") {
      # found valid one
      $self->addToMarkedSet($child);
      # set current vertex
      $self->setCurVertex($child);
      return $child;
    } else {
      # not found the valid one
      my $newVertex = $self->getNextUnmarkedDOVertex($v);
      if (defined $newVertex && $newVertex ne "") {
        # found valid one
        $self->addToMarkedSet($newVertex);
        # set current vertex
        $self->setCurVertex($newVertex);
        return $newVertex;
      }
    }
  }
  return;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Module::HTMLModuleUnit
###########################################
package Module::HTMLModuleUnit;

use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;
use FindBin;
use lib "$FindBin::Bin/../";
# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;

#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Clone;

use Global::Values;
use Engine::HTMLEngine;

with 'Module::AbstractModuleUnit';
enum 'MOD_UNIT_TYPE', [ @{ Global::Values->doModuleUnitType } ];
########################
# constructor
########################
sub BUILD {
  my $self = shift;
  $self->engine( Engine::HTMLEngine->new );
}
#sub DESTROY {
#    my $self = shift;
##    print "Module::HTMLModuleUnit have been garbage-collected!\n";
#}
sub _build_attribute {
#  my $htmlAttrHashRef = Clone::clone( Global::Values->htmlAttrHash );
  my $htmlAttrHashRef =  Global::Values->htmlAttrHash ;
  return $htmlAttrHashRef;
}
########################
# Variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'MOD_UNIT_TYPE',
  default => 'HTML_UNIT',
);

# input text
has 'text' => (
  is        => 'ro',
  isa       => 'Str',
  predicate => "hasText",
  clearer   => 'clearText',
  writer    => 'setText',
  reader    => 'getText',
  init_arg  => undef,
);

# module content: the tag name for Html or pattern for Reg exp
#has 'content' => (
#  is        => 'ro',
#  isa       => 'Str',
#  predicate => "hasContent",
#  clearer   => 'clearContent',
#  writer    => 'setContent',
#  reader    => 'getContent',
#  init_arg  => undef,
#);

# is this module matched or not?
has 'machted' => (
  is        => 'ro',
  isa       => 'Bool',
  predicate => "hasMatched",
  clearer   => 'clearMatched',
  writer    => 'setMatched',
  reader    => 'getMatched',
  init_arg  => undef,
);

has 'engine' => (
  is  => 'rw',
  isa => 'Engine::HTMLEngine',
);

# an attribute hash for the elements' attribute
has 'attribute' => (
  is      => 'ro',
#  isa     => 'HashRef',
  clearer => 'resetAttribute',
  default => sub {{}},
  lazy    => 1,
  writer  => 'setAttribute',
  reader  => 'getAttribute',
);


has 'tagName' => (
  is        => 'ro',
  isa       => 'Str',
  predicate => 'hasTagName',
  clearer   => 'clearTagName',
  writer    => 'setTagName',
  reader    => 'getTagName',
);

#has 'analyseResult' => (
#  is      => 'ro',
#  isa     => 'HashRef[Str]',
#  clearer => 'resetAnalyseResult',
#  default => sub { {} },
#  lazy    => 1,
#  writer  => 'setAnalyseResult',
#  reader  => 'getAnalyseResult',
#);

########################
# methods
########################
# type: ?
sub getModuleUnitType {
  my $self = shift;
  return $self->type;
}

sub getModUnitID {
  my $self = shift;
  return $self->getID;
}
sub setModUnitID {
  my $self = shift;
  my $id = shift;
  return $self->setID($id);
}
# name: reg name or html check name
sub getModUnitName {
  my $self = shift;
  return $self->getName;
}

sub setModUnitName {
  my $self = shift;
  my $name = shift;
  return $self->setName($name);
}

# input text:
sub getInputText {
  my $self = shift;
  return $self->getText;
}

sub setInputText {
  my $self    = shift;
  my $textRef = shift;
  return $self->setText($$textRef);
}

# matched: 1 / 0
sub found {
  my $self = shift;
  return $self->getMatched;
}

# positions: Array Ref Str, i.e. ["1,8", "12,19"]
sub getFoundPos {
  my $self = shift;

  #  if ($self->found && $self->hasFoundElements) {
  #    return $self->getFoundElements;
  #  }
  return;
}

sub updateOutputTable {
  my $self = shift;
  my $resHashRef = shift;

  # set the output hashtable
  $self->setOutputTable($resHashRef);
  return 1;
}

# GET MORE ELEMENTS IN ONE TIME
# process!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
sub process {
  my $self = shift;

  my $text = $self->getInputText;
  my $tag  = $self->getTagName;

  if ( !defined $text || !$text || !defined $tag || !$tag ) {
    $self->setMatched(0);
    return;
  }

  my @resTypes = (
    Global::Values::HTML_TYPE->{ELEMENT_PART},
    Global::Values::HTML_TYPE->{CONTENT_PART},
    Global::Values::HTML_TYPE->{ATTR_PART},
  );

  my $resHashRef = $self->engine->fetchHtmlElement( $tag, \@resTypes, \$text );

  # get all useful information for this unit searching
  #$self->setAnalyseResult($resHashRef);

  if ( ! $resHashRef->{hasElement} ) {
    $self->setMatched(0);
  } else {
    $self->setMatched(1);
  }
  # attribute of element, they could be empty
  $self->setAttribute( $resHashRef->{attr} );
  
  # output table
  $self->updateOutputTable($resHashRef);
  return 1;
}

############### HTML ##################

## id="a00001" bd="75" se="077" sp="li" ze="01"
#sub getDOEntryAttrInfo {}
## id="a00001" bd="75" se="077" sp="li" ze="01"
#sub setDOEntryAttrInfo {}

# content: for html is the name of tags
#sub getModuleContent {
#  my $self = shift;
#  return $self->getContent;
#}
#
## content: for html is the name of tags
#sub setModuleContent {
#  my $self    = shift;
#  my $tagName = shift;
#  return $self->setContent($tagName);
#}
no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package Module::RegModuleUnit
###########################################
package Module::RegModuleUnit;

use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;

#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;

use Global::Values;

with 'Module::AbstractModuleUnit';

enum 'MOD_UNIT_TYPE', [ @{ Global::Values->doModuleUnitType } ];

########################
# constructor
########################
#sub DESTROY {
#    my $self = shift;
##    print "Module::RegModuleUnit have been garbage-collected!\n";
#}
########################
# Variables
########################

has 'type' => (
  is      => 'ro',
  isa     => 'MOD_UNIT_TYPE',
  default => 'REGEXP_UNIT',
);

# input text
has 'text' => (
  is        => 'ro',
  isa       => 'Str',
  predicate => "hasText",
  clearer   => 'clearText',
  writer    => 'setText',
  reader    => 'getText',
  init_arg  => undef,
);

# module content: the tag name for Html or pattern for Regexp,
has 'content' => (
  is        => 'ro',
#  isa       => 'Str',
  predicate => "hasContent",
  clearer   => 'clearContent',
  writer    => 'setContent',
  reader    => 'getContent',
  init_arg  => undef,
);

# is this module matched or not?
has 'machted' => (
  is        => 'ro',
  isa       => 'Bool',
  predicate => "hasMatched",
  clearer   => 'clearMatched',
  writer    => 'setMatched',
  reader    => 'getMatched',
  init_arg  => undef,
);
has 'foundElements' => (
  is        => 'ro',
  isa       => 'ArrayRef[Str]',
  predicate => "hasFoundElements",
  clearer   => 'resetFoundElements',
  writer    => 'setFoundElements',
  reader    => 'getFoundElements',
  lazy      => 1,
  default   => sub { [] },
  init_arg  => undef,
);
# an attribute hash for the reg exp results
has 'attribute' => (
  is      => 'ro',
  isa     => 'HashRef',
  clearer => 'resetAttribute',
  builder => '_build_attribute',
  lazy    => 1,
  writer  => 'setAttribute',
  reader  => 'getAttribute',
);
########################
# methods
########################
# type: ?
sub getModuleUnitType {
  my $self = shift;
  return $self->type;
}

sub getModUnitID {
  my $self = shift;
  return $self->getID;
}
sub setModUnitID {
  my $self = shift;
  my $id = shift;
  return $self->setID($id);
}
# name: reg name or html check name
sub getModUnitName {
  my $self = shift;
  return $self->getName;
}

sub setModUnitName {
  my $self = shift;
  my $name = shift;
  return $self->setName($name);
}
#
# input text:
sub getInputText {
  my $self = shift;
  return $self->getText;
}

sub setInputText {
  my $self    = shift;
  my $textRef = shift;
  return $self->setText($$textRef);
}

# matched: 1 / 0
sub found {
  my $self = shift;
  return $self->getMatched;
}

# positions: Array Ref Str, i.e. ["1,8", "12,19"]
sub getFoundPos {
  my $self = shift;

  if ( $self->found && $self->hasFoundElements ) {
    return $self->getFoundElements;
  }
  return;
}

# process!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
sub process {
  my $self     = shift;
  my $beginPos = shift;
  my $endPos   = shift;

  my $pattern = $self->getModuleContent;
  my $text    = $self->getInputText;
  
  # get all the matched elements in the given text
  # indicesArrRef: ["1,4", "7,9"] - matched: 1/0
  my ( $indicesArrRef, $matched ) = $self->processPattern( \$text, \$pattern, $beginPos, $endPos );

  # matched
  $self->setMatched($matched);
  # take the found indices to the found Elements
  if ( $self->found ) {
    $self->addEleArrRefToFoundElements($indicesArrRef);
  }
#  my $resHash = Clone::clone( Global::Values->getOutputUnit("MODULE_OUTPUT_REG") );
  my $resHash =  Global::Values->getOutputUnit("MODULE_OUTPUT_REG") ;

  my $regName = $self->getModUnitName;
  $resHash->{name}    = $regName;
  $resHash->{matched} = $self->found;
  $resHash->{pos}     = $self->getFoundElements;

  $self->setAttribute($resHash);

  # to output table
  $self->updateOutputTable;
  return 1;
}

# Reg Core!!!
sub processPattern {
  my $self = shift;
  my ( $textRef, $patternRef, $beginIndex, $endIndex ) = @_;
  # begin and end index
  
  if ( !defined $textRef || !defined $patternRef ) {
    carp "There is no valid input params!";
    return;
  }
  if ( !defined $beginIndex || $beginIndex eq '' ) {
    $beginIndex = 0;
  }
  if ( !defined $endIndex || $endIndex eq '' ) {
    $endIndex = -1;
  }

  my @indices;
  my $text    = ${$textRef};
  my $pattern = $$patternRef;

  # go with the begin index
  pos($text) = $beginIndex;
  my $success = 0;

  while ( not $text =~ m/\G\z/xgc) {
    if ( $text =~ m/\G$pattern/xgc ) {
      $success = 1;

      # There is a valid end index and the found part is out of the end index
      if ( $endIndex != -1 && $endIndex <= $+[0] ) {
        last;
      }
      my $indices = $self->setIndices( $-[0], $+[0] );

      # add the indices to the array
      push @indices, $indices;
    } elsif ( $text =~ m/\G(.)/xgcsi ) {
      #print $-[0] . "\n";
    }
    else {
      croak "$0: oops, this shouldn't happen!";
    }
  }
  return ( \@indices, $success );
}
#
sub setIndices {
  my $self  = shift;
  my $begin = shift;
  my $end   = shift;
  return Utils::Tools->setIndices( $begin, $end );
}

# output tables: module unit id => outputTable ...
#     unitID_1=> {
#       unitType => REGEXP_UNIT,
#       analyseResult => {
#            name    => xxx,
#            matched => 1/0,
#            pos     => ['1,2', "2,3" ...],
#       }
#     }
#     unitID_2 => {
#       unitType => HTML_UNIT,
#       analyseResult => {...}
#     }
#     unitID_3 => {
#       unitType => xxx,
#       analyseResult => {...}
#     }
sub updateOutputTable {
  my $self = shift;

  my $unitType = $self->getModuleUnitType;
  my $unitID   = $self->getModUnitID;
  my $resHash  = $self->getAttribute;

  # idhash => 1. unit Type 2. analyse result
  my $idhash = {
    unitType      => $unitType,
    analyseResult => $resHash,
  };

  # moduleUnitID => idhash
  my $hash = { $unitID => $idhash};

  # set the output hashtable
  $self->setOutputTable($hash);
  return 1;
}

## <a00005 74 77 li>
#sub getDOEntryAttrInfo {}
## <a00005 74 77 li>
#sub setDOEntryAttrInfo {}

############### Reg exp ##################

# content: For Reg Exp is the pattern
sub getModuleContent {
  my $self = shift;
  return $self->getContent;
}

sub setModuleContent {
  my $self = shift;
  my $qr   = shift;
  return $self->setContent($qr);
}


sub addEleArrRefToFoundElements {
  my $self = shift;
  my $inputArrRef = shift;
  
  if (! defined $inputArrRef ||$inputArrRef eq "") {
    croak "There is no valid input elements array ref!";
  }
  for my $item (@$inputArrRef) {
    $self->addEleToFoundElements($item);
  }
  return 1;
}
sub addEleToFoundElements {
  my $self    = shift;
  my $item = shift;
  return if ( ! defined $item || $item eq "" );
  if ( !$self->hasFoundElements ) {
    my @arr = ();
    push @arr, $item;
    $self->setFoundElements( \@arr );
    return 1;
  }
  my $arr = $self->getFoundElements;
  return if ( Utils::Tools->isInArray( $arr, \$item ) );
  push @$arr, $item;
  $self->setFoundElements($arr);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Module::RegModModuleUnit
###########################################
# especially for the 'mod_xxx' element in the regPattern map
package Module::RegModModuleUnit;

use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;

#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;

use Global::Values;

with 'Module::AbstractModuleUnit';

enum 'MOD_UNIT_TYPE', [ @{ Global::Values->doModuleUnitType } ];

########################
# constructor
########################
#sub DESTROY {
#    my $self = shift;
##    print "Module::RegModModuleUnit have been garbage-collected!\n";
#}
########################
# Variables
########################

has 'type' => (
  is      => 'ro',
  isa     => 'MOD_UNIT_TYPE',
  default => 'REGEXP_MOD_UNIT',
);

# input text
has 'text' => (
  is        => 'ro',
  isa       => 'Str',
  predicate => "hasText",
  clearer   => 'clearText',
  writer    => 'setText',
  reader    => 'getText',
  init_arg  => undef,
);

# module content: the tag name for Html or pattern for Reg exp
has 'content' => (
  is        => 'ro',
#  isa       => 'Str',
  predicate => "hasContent",
  clearer   => 'clearContent',
  writer    => 'setContent',
  reader    => 'getContent',
  default => "",
  init_arg  => undef,
);

# is this module matched or not?
has 'machted' => (
  is        => 'ro',
  isa       => 'Bool',
  predicate => "hasMatched",
  clearer   => 'clearMatched',
  default   => 1,
  writer    => 'setMatched',
  reader    => 'getMatched',
  init_arg  => undef,
);

has 'foundElements' => (
  is        => 'ro',
  isa       => 'ArrayRef[Str]',
  predicate => "hasFoundElements",
  clearer   => 'resetFoundElements',
  writer    => 'setFoundElements',
  reader    => 'getFoundElements',
  lazy      => 1,
  default   => sub { [] },
  init_arg  => undef,
);

# an attribute hash for the reg exp results
has 'attribute' => (
  is      => 'ro',
  isa     => 'HashRef',
  clearer => 'resetAttribute',
  builder => '_build_attribute',
  lazy    => 1,
  writer  => 'setAttribute',
  reader  => 'getAttribute',
);
########################
# methods
########################
# type: ?
sub getModuleUnitType {
  my $self = shift;
  return $self->type;
}

sub addEleArrRefToFoundElements {
  my $self = shift;
  my $inputArrRef = shift;
  
  if (! defined $inputArrRef ||$inputArrRef eq "") {
    croak "There is no valid input elements array ref!";
  }
  for my $item (@$inputArrRef) {
    $self->addEleToFoundElements($item);
  }
  return 1;
}
sub addEleToFoundElements {
  my $self    = shift;
  my $item = shift;
  return if ( ! defined $item || $item eq "" );
  if ( !$self->hasFoundElements ) {
    my @arr = ();
    push @arr, $item;
    $self->setFoundElements( \@arr );
    return 1;
  }
  my $arr = $self->getFoundElements;
  return if ( Utils::Tools->isInArray( $arr, \$item ) );
  push @$arr, $item;
  $self->setFoundElements($arr);
  return 1;
}

sub getModUnitID {
  my $self = shift;
  return $self->getID;
}
sub setModUnitID {
  my $self = shift;
  my $id = shift;
  return $self->setID($id);
}
# name: reg name or html check name
sub getModUnitName {
  my $self = shift;
  return $self->getName;
}

sub setModUnitName {
  my $self = shift;
  my $name = shift;
  return $self->setName($name);
}
#
# input text:
sub getInputText {
  my $self = shift;
  return $self->getText;
}

sub setInputText {
  my $self    = shift;
  my $textRef = shift;
  return $self->setText($$textRef);
}
# found is always 1 
sub found {
  my $self = shift;
  return 1;
}

# positions: Array Ref Str, i.e. ["1,8", "12,19"]
sub getFoundPos {
  my $self = shift;
  if ( $self->found && $self->hasFoundElements ) {
    return $self->getFoundElements;
  }
  return;
}
# process!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
sub process {
  my $self     = shift;
  my $beginPos = shift;
  my $endPos   = shift;

  # pattern is empty
  my $pattern = $self->getModuleContent;
  # text is not matter
  my $text    = $self->getInputText;

  # indicesArrRef: ["1,4", "7,9"] - matched: 1/0
  #my ( $indicesArrRef, $matched ) = $self->processPattern( \$text, \$pattern, $beginPos, $endPos );

  # matched is always 1
  #$self->setMatched($matched);
  
  # found is always 1 but found elements is empty
#  if ( $self->found ) {
#    $self->setFoundElements($indicesArrRef);
#  }
  
#  my $resHash = Clone::clone( Global::Values->getOutputUnit("MODULE_OUTPUT_REG") );
  my $resHash =  Global::Values->getOutputUnit("MODULE_OUTPUT_REG") ;

  my $regName = $self->getModUnitName;
  $resHash->{name}    = $regName;
  $resHash->{matched} = $self->found;
  $resHash->{pos}     = ["0,0"];
  
  $self->setAttribute($resHash);

  # to output table
  $self->updateOutputTable;
  return 1;
}

sub setIndices {
  my $self  = shift;
  my $begin = shift;
  my $end   = shift;
  return Utils::Tools->setIndices( $begin, $end );
}

sub updateOutputTable {
  my $self = shift;

  my $unitType = $self->getModuleUnitType;

  my $unitID   = $self->getModUnitID;
  my $resHash  = $self->getAttribute;

  # idhash => 1. unit Type 2. analyse result
  my $idhash = {
    unitType      => $unitType,
    analyseResult => $resHash,
  };

  # moduleUnitID => idhash
  my $hash = { $unitID => $idhash};

  # set the output hashtable
  $self->setOutputTable($hash);
  return 1;
}

## <a00005 74 77 li>
#sub getDOEntryAttrInfo {}
## <a00005 74 77 li>
#sub setDOEntryAttrInfo {}

############### Reg exp ##################
# content: For Reg Exp is the pattern
sub getModuleContent {
  my $self = shift;
  return $self->getContent;
}
sub setModuleContent {
  my $self = shift;
  my $qr   = shift;
  return $self->setContent($qr);
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

