package Utils::IO;
=encoding utf8

=head1 NAME

Utils::IO - Input and Ouput system through data streams and file system.

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package provides static methods for manipulating i/o system.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 SEE ALSO

FindBin, lib, Data::Dumper, Scalar::Util, File::Find::Ruler, Cwd, File::Spec, File::Basename,

YAML, Fcntl, Tie::File, utf8, Encode etc.

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

use Scalar::Util qw(blessed);
#use File::Find;
use File::Find::Rule;
use Cwd;
use File::Spec;
use File::Basename;

use YAML;
use Fcntl;
use Tie::File;

use utf8;
use Encode;
#use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use v5.20;

use Global::Values;
use Utils::Tools;
use Database::MotDB;

=head1 CONSTRUCTORS

=head2 BUILD

After new.

=cut
sub BUILD {
  my $self = shift;
  # make a new fsm (FSM::Simple)
  $self->setInputDir();
  $self->setOutputDir();
  $self->setConfigDir();
}


=head1 ATTRIBUTES

=head2 curFile

The current selected file name

Attention: variant operating system, Unix, windows

=cut
has 'curFile' => (
  is => 'rw', 
  isa => 'Str',
  predicate => 'hasCurFile',
  clearer => 'clearCurFile',
);
=head2 tiedFile

The tied file to be analysed, see also Tie::File.

=cut
has 'tiedFile' => (
  is => 'rw', 
  isa => 'ArrayRef[Str]',
  predicate => 'hasTiedFile',
  clearer => 'clearTiedFile',
);
=head2 fileRowCount

the count number of file rows

=cut
has 'fileRowCount' => (
  is => 'rw',
  isa => 'Int',
  predicate => 'hasFileRowCount',
  default => 0,
  clearer => 'resetFileRowCount',
  writer => 'setFileRowCount',
  reader => 'getFileRowCount',
);

=head2 inputDirectory

the directory of input files, see Global::Values->globVal->{dataPath}

=cut
has 'inputDirectory' => (
  is => 'rw',
  isa => 'Str',
  predicate => 'hasInputDirectory',
);

=head2 outputDirectory

the directory of output files, see Global::Values->globVal->{databasePath}

=cut
has 'outputDirectory' => (
  is => 'rw',
  isa => 'Str',
  predicate => 'hasOutputDirectory',
);

=head2 configDirectory

the directory of output files, see Global::Values->globVal->{configPath}

=cut
has 'configDirectory' => (
  is => 'rw',
  isa => 'Str',
  predicate => 'hasConfigDirectory',
);

=head2 inputFiles

Attention: variant operating system, Unix, windows

=cut
has 'inputFiles' => (
  is => 'rw',
  isa => 'ArrayRef[Str]',
  predicate => 'hasInputFiles',
  clearer => 'clearInputFiles',
);
=head2 outputFiles

=cut
has 'outputFiles' => (
  is => 'rw',
  isa => 'ArrayRef[Str]',
  predicate => 'hasOutputFiles',
  clearer => 'clearOutputFiles',
);
=head2 configFiles

=cut
has 'configFiles' => (
  is => 'rw',
  isa => 'ArrayRef[Str]',
  predicate => 'hasConfigFiles',
  clearer => 'clearConfigFiles',
);
=head2 curRowText

the current selected row text string

=cut
has 'curRowText' => (
  traits => ['String'],
  is => 'rw',
  isa => 'Str',
  default => q{},
  handles => {
    addCurRowText => 'append',
    chompCurRowText => 'chomp',
    clearCurRowText => 'clear',
    curRowTextLength => 'length',
  },
);

=head1 METHODS

=head2 getInstance

Singleton.

=cut
my $singleton_IO;
sub getInstance {
  my $self = shift;
  if (! defined $singleton_IO) {
    $singleton_IO = $self->new;
  }
  return $singleton_IO;
}

=head2 setCurFile($fname)

Sets the file to be analyse by the given file name.

=cut
sub setCurFile {
  my $self = shift;
  my $fname = shift;
  # find the time with absolute path and save it to $self->inputFiles
  $self->findInputFilePath($fname);
  # set the first file to $self->curFile
  my $firstFile = $self->inputFiles->[0];
  if (! defined $firstFile || $firstFile eq "") {
    croak "There is no valid input file: $fname in the " . $self->inputDirectory ; 
  }
  $self->curFile($firstFile);
  return 1; 
}
=head2 setFileNameToDB()

Sets the current analysing file name to the Database::MotDB.

=cut
sub setFileNameToDB {
  my $self = shift;
  if ($self->hasCurFile) {
    Database::MotDB->updateFileName($self->curFile);
  } else {
    carp "There is no valid input file to analyse";
  }
}

=head2 fetchTiedFileArr($fname)

Gets the text file by the given file name and sets it to the 'curFile'.

=cut
sub fetchTiedFileArr {
  my $self = shift;
  my $fname = shift;
  my @res;
  
  # find the time with absolute path and save it to $self->inputFiles
  $self->findInputFilePath($fname);
  # set the first file to $self->curFile
  $self->curFile($self->inputFiles->[0]);
  # tying the curFile to $self->tiedFile
  $self->tieFile($self->curFile);
}

=head2 tieFile($fname)

Access and storing the lines of a text file via Tie::File.

=cut
sub tieFile {
  my $self = shift;
  my $fname = shift;
  
  use utf8;
  use Encode;
  
  my @farr;
  tie @farr, 'Tie::File', $fname, discipline => ':encoding(utf8)' 
    or croak 'There is no valid file to input!';
  
  my $count = scalar @farr;
  $self->setFileRowCount($count);
  $self->tiedFile(\@farr);
  $self->hasTiedFile(1);
}

=head2 setInputDir()

Sets the input directory, see Global::Values->globVal->{dataPath}.

=cut
sub setInputDir {
  my $self = shift;
  
  my $dpath = Global::Values->globVal->{dataPath};
  
  if (! defined $dpath || $dpath eq "") {
    croak 'There is no valid Directory by getInputDir()!';
  }
  $self->inputDirectory($dpath);
  $self->hasInputDirectory(1);
}
=head2 setOutputDir()

Sets the output directory, see Global::Values->globVal->{databasePath}.

=cut
sub setOutputDir {
  my $self = shift;
  
  my $dpath = Global::Values->globVal->{databasePath};
  
  if (! defined $dpath || $dpath eq "") {
    croak 'There is no valid Directory by getOutputDir()!';
  }
  $self->outputDirectory($dpath);
  $self->hasOutputDirectory(1);
}
=head2 setConfigDir()

Sets the configuration directory, see Global::Values->globVal->{configPath}.

=cut
sub setConfigDir {
  my $self = shift;
  
  my $cpath = Global::Values->globVal->{configPath};
  
  if (! defined $cpath || $cpath eq "") {
    croak 'There is no valid Configuration Directory!';
  }
  $self->configDirectory($cpath);
  $self->hasConfigDirectory(1);
}
=head2 findInputFilePath($name)

Gets the text file with the given file name.

=cut
sub findInputFilePath {
  my $self = shift;
  my $name = shift;
  if (! defined $name || $name eq "") {
    croak "There is no valid file name!";
  }
  my @files = File::Find::Rule->file()->name($name)->in($self->inputDirectory);
  if (scalar @files == 0) {
    croak "No File could be found!";
  }
  #print Dumper(@files);
  $self->inputFiles(\@files);
  $self->hasInputFiles(1);
}

=head2 getConfigFile($name)

Gets the configuration file with the given file name.

=cut
sub getConfigFile {
  my $self = shift;
  my $name = shift;
    
  my @files = File::Find::Rule->file()->name($name)->in($self->configDirectory);
  
  croak "File $name not found." if (! @files);
  
  return \@files;
}

=head2 readConfigFile($fname)

Read the configuration file by given file name and returns a configuration hashmap.

=head3 See alse

YAML

=cut
sub readConfigFile {
  my $self = shift;
  my $fname = shift;
  # dir will be appointed, only basename will be needed.
  $fname = basename $fname;
  my $fArrRef = $self->getConfigFile($fname);
  croak "The file $fname could not be found in the config dir!" 
    if (! defined $fArrRef || ! @$fArrRef);
  $fname = $$fArrRef[0];
  
  open my $fh, '<:encoding(UTF-8)', $fname 
    or croak "Can't open file: $!";
  
  my $yml = do {local $/; <$fh>};
  
  my $config = YAML::Load($yml);
  
  close $fh;
  
  # returns the reference of config hash.
  return $config;
}

=head2 writeConfigFile($fname, $data)

Write the configuration file by given file name and the specified data map.

=head3 See alse

YAML

=cut
sub writeConfigFile {
  my $self = shift;
  my $fname = shift;
  my $data = shift;
  
  my $cpath = File::Spec->catfile($self->configDirectory, $fname);
  
  open my $fh, ">:encoding(UTF-8)", $cpath;
  print $fh YAML::Dump($data);
  close $fh;
  return 1;
}
=head2 setDBConfig()

Sets database config information.

=cut
sub setDBConfig {
  my $self = shift;
  $self->writeConfigFile(Global::Values->ANALYSE_PARAMETERS->{DATABASE_CONFIG_FILENAME}, Global::Values->dbConfig);
}
=head2 setRegExpInfos()

Sets the information of RegExp patterns.

=cut
sub setRegExpInfos {
  my $self = shift;
  $self->writeConfigFile(Global::Values->ANALYSE_PARAMETERS->{REGEXP_INFO_FILENAME}, Global::Values->regExpInfos);
}

#deprecated
sub setFSMStatesFromConfig {
  my $self = shift;
  my $fsm = shift;
  my $stateFile = shift;
  return if ! Utils::Tools->isBlessed($fsm, 'FSM::Simple');
  my $resRef = $self->readConfigFile($stateFile);
  
  if ($resRef) {
    #TODO: check the resRef form.
    my @hashKeys = keys %$resRef;
    foreach my $key (@hashKeys) {
      my $value = $resRef->{$key};
      $fsm->add_state( name => $key, sub => \&{ $value->{output_function} } );
    }
#    while ( my ( $key, $value ) = each %$resRef ) {
#      #print Dumper(ref $value->{output_function});
#      $fsm->add_state( name => $key, sub => \&{ $value->{output_function} } );
#    }
    return 1;
  }
  return;
}

#deprecated
sub setFSMTransFromConfig {
  my $self = shift;
  my $fsm = shift;
  my $transFile = shift;
  return if ! Utils::Tools->isBlessed($fsm, 'FSM::Simple');
  my $resRef = $self->readConfigFile($transFile);
  if ($resRef) {
  #TODO: check the resRef form.
    for my $item (@{$resRef}) {
      $fsm->add_trans(from => $item->{from}, to =>$item->{to}, exp_val => $item->{exp_val});
    }
    return 1;
  }
  return;  
}

#sub error            { }
#sub stop             { }
#sub init             { }
#sub getSepEntry      { }
#sub getStreckEntry   { }
#sub getHeader        { }
#sub getPipe          { }
#sub getLemmaEntryEle { }
#sub getEntryEle      { }
#sub getLemmaEle      { }
#sub getHomEle        { }
#sub getTitle         { }
##sub getAuthor       { }
#sub getCol           { }
#sub getVerweis       { }
#sub getEndSpec       { }
#sub getIntSpec       { }
#sub checkBugs        { }


no Moose;
__PACKAGE__->meta->make_immutable;
1;