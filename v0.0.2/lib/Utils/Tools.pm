package Utils::Tools;
=encoding utf8

=head1 NAME

Utils::Tools - Tools which can be invoked from the system.

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package provides static methods for tools which can be invoked from the system.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 SEE ALSO

FindBin, lib, Data::Dumper, Scalar::Util, List::MoreUtils, List::Util, DateTime, utf8, Encode etc.

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use MooseX::Singleton;
use Moose::Util::TypeConstraints;
use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

use Global::Values;

use Scalar::Util;
use List::MoreUtils;
use List::Util;
use DateTime;


use utf8;
use Encode;
#use Mojo::Util;
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use v5.20;


=head1 METHODS

=head2 trim($)

Returns the string, without leading and trailing whitespace.

=cut
sub trim($) {
  my $self = shift;
  my $string = shift;
  $string =~ s/^\s+//;
  $string =~ s/\s+$//;
  return $string;
}

=head2 chompText($textRef)

Returns the string, without return whitespace.

=cut
sub chompText {
  my $self = shift;
  my $textRef = shift;
  return if (! defined $textRef || ref($textRef) ne "SCALAR");
  my $str = $$textRef;
  $str =~ s/[\n\r]//mg;
  return \$str;
}

=head2 inList($aref, $target)

Returns true if the target string($target) in the given array reference($aref).

=cut
sub inList {
  my $self = shift;
  my ( $aref, $target ) = @_;
  #print @$aref;
  return grep /^$target$/, @$aref;
}

=head2 delListEle($aref, $target)

Returns a new array reference without the target string($target).

=cut
sub delListEle {
  my $self = shift;
  my ( $aref, $target ) = @_;
  my @newList = grep { !/^$target$/ } @$aref;
  return \@newList;
}

# type: start, end
sub timer {
  my $self = shift;
  my $type = shift;
  
  state $temp;
  
  if ($type =~ m/^start/ && ! defined $temp) {
    $temp = DateTime->now();
  } elsif ($type =~ m/^end/ && defined $temp) {
    my $new = DateTime->now();
    my $elapse = $new - $temp;
    $temp = undef;
    #print "Elapsed time : ".$elapse->in_units('minutes') . " sec\n";
    print "Elapsed time : ".$elapse->in_units('seconds') . " sec\n";
  } else {
    carp "Something Wrong with Timer! Please check it";
    return;
  }
}

=head2 getAreaIndices($textRef, $partType, $indices, $beginIndex)

=head3 Paramters:

=over 4

=item *

$textRef: the given string reference

=item *

$partType: the given part text type to fetch, i.e. front, middle, back

=item *

$indices: the given indices with form "xx,yy".

=item *

$beginIndex: the beginning index

=back

=head3 Returns:

the specified part text of the given string text. It could be the font, middle and back part.

=cut
sub getAreaIndices {
  my $self = shift;
  my $textRef = shift;
  my $partType = shift; # front, middle, back
  my $indices = shift; # "45,87"
  my $beginIndex = shift;
  
  if (! defined $partType || $partType eq "") {
    croak "There is no valid part type as front, middle, back";
  }
  if (! defined $indices || $indices eq "" || not $indices =~ m/\d+,\d*\d+/) {
    croak "There is no valid input indices!";
  }
  if (! defined $beginIndex || $beginIndex eq "") {
    $beginIndex = 0;
  }
  
  my $len = length $$textRef;
  
  my $indicesArrRef = $self->splitIndices($indices);
  
  my $begin;
  my $end;
  
  if ($partType eq "front") {
    $begin = $beginIndex;
    $end = $indicesArrRef->[0];
  } elsif($partType eq "middle") {
    return $indices;
  } elsif ($partType eq "back") {
    
    $begin = $indicesArrRef->[1];
    $end = $len;
  }
  
  if (! defined $begin || ! defined $end) {
    croak "There is no valid begin/end index";
  }
  my $newIndices = $self->setIndices($begin, $end);
  return $newIndices;
}

=head2 setIndices($begin, $end)

Returns the string form of the beginning/ending indices with form "xx,yy".

=cut
sub setIndices {
  my $self = shift;
  my $begin = shift;
  my $end = shift;
  
  if (! defined $begin || ! defined $end || $begin eq '' || $end eq '' 
      || ! Scalar::Util::looks_like_number($begin) || !Scalar::Util::looks_like_number($begin)) {
    #carp "There are no valid beginIndex $begin or endIndex $end!";
    return;
  }
  if ($begin > $end) {
    carp "Beginindex: $begin and Endindex: $end are not valid!";
    return;
  }
  my $sep = Global::Values->SEPARATOR->{"IN_INDEX"};
  my $res = join($sep, $begin, $end);
  
  return $res;
}

=head2 setIndicesArray($indicesArrRef)

Returns the string form of the beginning/ending indices with form "xx,yy".

=cut
sub setIndicesArray {
  my $self = shift;
  my $indicesArrRef = shift;
  
  return if (! defined $indicesArrRef || $indicesArrRef eq "");
  
  my @res;
  my @indicesArr = @$indicesArrRef;
  my $iterator = List::MoreUtils::natatime(2, @indicesArr);
  while (my @indices = $iterator->()) {
    my $begin = $indices[0]; 
    my $end = $indices[1]; 
    my $index = $self->setIndices($begin, $end);
    push @res, $index if (defined $index && $index ne "");
  }
  return \@res;
}



=head2 getTextPartRefByIndices($begin, $end)

Returns the text reference by the given boundary indices and target indices

=cut
sub getTextPartRefByIndices {
  my $self = shift;
  my $textRef = shift;
  my $boundaryIndices = shift;
  my $targetIndices = shift;
  my $partType = shift;  # front, middle, back
  
   if (! defined $textRef || ref($textRef) ne 'SCALAR' || $textRef eq '' || ! defined $boundaryIndices 
      || ! defined $targetIndices || $targetIndices eq "" || ! defined $partType || $partType eq "") {
    carp "There are some problems in the input parameters";
    return;
  }
  
  my $boundaryBegin;
  my $boundaryEnd;
  # split the indices to the index array reference
  if ($boundaryIndices eq "") {
    my $textLen = length $$textRef;
    $boundaryBegin = 0;
    $boundaryEnd = $boundaryBegin + $textLen;
  } else {
  # boundary
    my $boundaryIdxArrRef = $self->splitIndices($boundaryIndices);
    $boundaryBegin = $boundaryIdxArrRef->[0];
    $boundaryEnd = $boundaryIdxArrRef->[1];
  }
  my $targetIdxArrRef = $self->splitIndices($targetIndices);
  
  # target
  my $targetBegin = $targetIdxArrRef->[0];
  my $targetEnd = $targetIdxArrRef->[1];
  
  # take it
  my $resStr;
  my $len;
  if ($partType eq 'front') {
    $len = $targetBegin - $boundaryBegin;
    $resStr = substr($$textRef, $boundaryBegin, $len);
  } elsif ($partType eq 'middle') {
    $len = $targetEnd - $targetBegin;
    $resStr = substr($$textRef, $targetBegin, $len);
  } elsif ($partType eq 'back') {
    $len = $boundaryEnd - $targetEnd;
    $resStr = substr($$textRef, $targetEnd, $len);
  }
  # return the text reference
  return \$resStr;
}

=head2 compareIndices($idx1, $idx2)

Compare the given two indices with form "xx,yy"(beginning index, ending index).

=head3 Returns:

=over 4

=item *

0 : if the two indices are same

=item *

1 : if the first one greater than the second.

=item *

1 : if the second one greater than the first.

=back

=cut
sub compareIndices {
  my $self = shift;
  my $idx1 = shift;
  my $idx2 = shift;
  
  # check the valid
  if ((! defined $idx1 || $idx1 eq "") && (! defined $idx2 || $idx2 eq "")) {
    croak "Input parameters are not valid!";
  } elsif (! defined $idx1 || $idx1 eq "") {
    return -1;
  } elsif (! defined $idx2 || $idx2 eq "") {
    return 1;
  } elsif ($idx1 eq $idx2) {
    return 0;
  }
  
  my $posArr1 = $self->splitIndices($idx1);
  my $posArr2 = $self->splitIndices($idx2);
  
  if ($posArr1->[0] < $posArr2->[0]) {
    return -1;
  } else {
    return 1;
  }
}

=head2 splitIndices($indices)

Analyses the given indices string and returns the index array with beginning and ending index.

=cut
sub splitIndices {
  my $self = shift;
  my $indices = shift;
  my @res;
  if (! defined $indices || $indices eq '' ) {
    carp "There is not a valid indices!";
    return;
  }

  my $sep = Global::Values->SEPARATOR->{IN_INDEX};
  if ($indices =~ m/(\d+)$sep\s*(\d+)/x) {
    if ( $1 >= $2) {
      carp "Beginindex: $1 and Endindex: $2 are not valid!";
      return;
    }
    push @res, $1;
    push @res, $2;
  } else {
    carp "$indices haven't the correct form, i.e. \'23,32\'!";
    return;
  }
  return \@res;
}

=head2 changeIndexLenToIndices($indexLensArrRef)

Attention: utf8 encode/decode

=head3 Paramters:

=over 4

=item *

$indexLensArrRef: the given string that contains the beginning index and the specified length of substring.

=back

=head3 Returns:

the string array reference with beginning and ending indices. 

=cut
sub changeIndexLenToIndices {
  my $self = shift;
  my $indexLensArrRef = shift;
  return if (! defined $indexLensArrRef || $indexLensArrRef eq "");
  my @indicesArr = ();
  my $sep = Global::Values->SEPARATOR->{IN_INDEX};
  for my $item (@$indexLensArrRef) {
    if ($item =~ m/(\d+)$sep\s*(\d+)/x) {
      my $beginIndex = $1;
      my $len = $2;
      my $endIndex = $beginIndex + $len;
      my $index = join ($sep, $beginIndex, $endIndex);
      push @indicesArr, $index;
    }
  }
  return \@indicesArr;
}
=head2 changeIndicesToIndexLen($indicesArrRef)

Attention: utf8 encode/decode

=head3 Paramters:

=over 4

=item *

$indicesArrRef: the given index array reference with the beginning and ending index.

=back

=head3 Returns:

the string array reference with beginning index and the length of substring. 

=cut
sub changeIndicesToIndexLen {
  my $self = shift;
  my $indicesArrRef = shift;
  return if (! defined $indicesArrRef || $indicesArrRef eq "");
  my @indexLenArr = ();
  my $sep = Global::Values->SEPARATOR->{IN_INDEX};
  for my $item (@$indicesArrRef) {
    if ($item =~ m/(\d+)$sep\s*(\d+)/x) {
      my $beginIndex = $1;
      my $endIndex = $2;
      my $len = $endIndex - $beginIndex;
      my $indexLen = join ($sep, $beginIndex, $len);
      push @indexLenArr, $indexLen;
    }
  }
  return \@indexLenArr;
}

=head2 isObject($item)

Returns true if the given item is a Moose::Object, returns false otherwise.

=cut
sub isObject {
  my $self = shift;
  my $item = shift;
  my $result = 0;
  if ((ref $item ne "") && (defined $item->meta) 
    && (defined $item->meta->class_precedence_list) 
    && ($item->meta->class_precedence_list)[-1] eq Global::Values->MOOSE_OBJECT) {
    $result = 1;
  }
  return $result;
}

=head2 isBlessed($item, $objName)

Returns true if the given item is the specified object, returns false otherwise.

=head3 See also:

Scalar::Util

=cut
sub isBlessed {
  my $self = shift;
  my ($item, $objName) = @_;
  my $res = 0;
  
  if(defined $item && ref($item) ne "" && Scalar::Util::blessed($item) 
      && ref($item) eq $objName) {
    $res = 1;
  }
  return $res;
}

=head2 isBlessed($pt1, $pt2)

Compare Coordinate::Point's positions.

=head3 Returns:

Returns the value 0 if the given points are same; a value -1 if point 1 < point 2;

a value 1 if point1 > point 2

=head3 See also:

Scalar::Util, Coordinate::Point

=cut
sub comparePTsPos {
  #my $self = shift;
  my ($pt1, $pt2) = @_;
  
  if (! isBlessed($pt1, "Coordinate::Point") 
  || ! isBlessed($pt2, "Coordinate::Point")){
    carp "There are some invalid Parameters!";
    return;
  }
  
  # change for the different Coordinate::Point
  my $pos1 = $pt1->getPosition;
  my $pos2 = $pt2->getPosition;

  return ($pos1 cmp $pos2);
}

=head2 specCharEscape($str)

Replaces the unsafe characters in the given string($str), see Global::Values->ESCAPEPATTERNS->{CHARS_PATTERN}.

=cut
sub specCharEscape {
  my $self = shift;
  my $str = shift;

  #  $str =~ s/((?{ Global::Values->ESCAPEPATTERNS->{CHARS_PATTERN} }))/(?{ Globval::Values->SPEC_CHARS_MAP->{($1)} })/;
  my $pat = Global::Values->ESCAPEPATTERNS->{CHARS_PATTERN};
  my $charsMap = Global::Values->SPEC_CHARS_MAP;
  #print Dumper($charsMap);
  $str =~ s/$pat/$charsMap->{$1}/ge;
  return $str;
}

=head2 specCharUnEscape($str)

Returns a string with the specified replaced bytes, see Global::Values->ESCAPEPATTERNS->{ENTITIES_PATTERN}.

=cut
sub specCharUnEscape {
  my $self = shift; 
  my $str = shift;
  my $pat = Global::Values->ESCAPEPATTERNS->{ENTITIES_PATTERN};
  my $entitiesMap = Global::Values->SPEC_ENTITIES_MAP;
  $str =~ s/$pat/$entitiesMap->{$1}/ge;
  return $str;
}

=head2 rmPunct($text)

Remove the punctuations of the given string text($text).

=cut
sub rmPunct {
  my $self = shift;
  my $text = shift;
  $text =~ s/[[:punct:]]//g;
  return $text;
}

=head2 isInHashValue($hash, $value)

Returns ture if the given hashmap($hash) has the specified value($value), return false otherwise.

=head3 See alse:

List::MoreUtils

=cut
sub isInHashValue {
  my $self = shift;
  my $hash = shift;
  my $value = shift;
  
  my $res = List::MoreUtils {$_ eq $value} values %$hash;
  
  return $res;
}

=head2 isInArray($arrRef, $valueRef)

Returns ture if the given array ($arrRef) has the specified value($valueRef), return false otherwise.

=head3 See alse:

List::MoreUtils

=cut
sub isInArray {
  my $self = shift;
  my $arrRef = shift;
  my $valueRef = shift;
  return if (! defined $$valueRef || $$valueRef eq "");
  return if (! defined $arrRef || $arrRef eq "" || ref $arrRef ne "ARRAY");
  
  my $res = List::MoreUtils::any {$_ eq $$valueRef} @$arrRef;
  return $res;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;