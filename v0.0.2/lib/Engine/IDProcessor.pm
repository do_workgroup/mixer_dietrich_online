package Engine::IDProcessor;
=encoding utf8

=head1 NAME

Engine::IDProcessor - The ID Generators for analysis.

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
package Engine::AbstractIDGenerator;
=encoding utf8

=head1 NAME

Engine::AbstractIDGenerator - The abstract class of ID Generators

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This is abstract superclass for generating ID during the system performance. Standard behaviors like the 

getType, generate are defined here, the programmer needs only to extend

this class and provide implementations for this methods. 

The non-abstract methods have their descriptions with its implementation in Detail.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose::Role;

use FindBin;
use lib "$FindBin::Bin/../";


=head1 REQUIRES METHODS

=head2 getGeneratorType

Returns the generator type

=cut
requires 'getGeneratorType';

=head2 generate

Generates an ID.

=cut
requires 'generate';


=head2 appendPrefixChar($prefix, $numLen, $number)

=head3 Parameters:

=over 4

=item *

$prefix: the given prefix charactor, see Global::Values->DO_ID_GEN

=item *

$numLen: the given length of the entire string

=item *

$number: the given number to be concatenated

=back

=head3 Returns:

the concatenated string with the specified prefix and number.

=cut
sub appendPrefixChar {
  my $self = shift;
  my $prefix = shift;
  my $numLen = shift;
  my $number = shift;
  # the length will be flexible extended
  if ($numLen < length($number)) {
    $numLen = length($number);
  }
  my $new =  sprintf("$prefix%0*d", $numLen, $number);
  return $new;
}


no Moose;
1;

#CLASS:
package Engine::RowNumGenerator;
=encoding utf8

=head1 NAME

Engine::RowNumGenerator - ID Generator for text row number

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Engine::AbstractIDGenerator';

use Global::Values;
use Database::MotDB;

enum 'ID_GEN_TYPE', [@{Global::Values->doIDGeneratorType}];

=head1 ATTRIBUTES

=head2 type

Type of the class (ID_GEN_TYPE: Global::Values->doIDGeneratorType)

=cut
has 'type' => (
  is => 'ro',
  isa => 'ID_GEN_TYPE',
  default => 'ROW_NUMBER',
);

=head2 rowNum

the number of text row

=cut
has 'rowNum' => (
  is => 'rw',
  isa => 'Int',
  predicate => 'hasRowNum',
  clearer => 'resetRowNum',
  default => 0,
  lazy => 1,
);

=head1 METHODS

=head2 updateRowNum($num)

Update the rowNum value in the Database::MotDB by Database::MotDBProcessor

=cut
sub updateRowNum {
  my $self = shift;
  my $num = Database::MotDBTableApp->getInstance->getRowNum;
  $self->rowNum($num);
}
=head2 getGeneratorType()

=head3 Overrides:

getGeneratorType in class Engine::AbstractIDGenerator

=cut
sub getGeneratorType {
  my $self = shift;
  return $self->type;
}

=head2 generate()

=head3 Overrides:

generate in class Engine::AbstractIDGenerator

=cut
sub generate {
  my $self = shift;
  my $formatStr = shift;
  my $map = shift;
  $self->updateRowNum;
  if ($self->hasRowNum) {
    
    # i.e. r000001, r000002
    my $num = $self->rowNum;
    my $numLen = Global::Values::DO_ID_GEN->{NUMBER_LENGTH};
    my $prefix = Global::Values::DO_ID_GEN->{ROW_NUM_PREFIX};
    my $new = $self->appendPrefixChar($prefix, $numLen, $num);
    return $new;
  } else {
    carp "There is no valid row number!";
    return;
  }
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


###########################################
# package Engine::BandNameGenerator;
###########################################
package Engine::BandNameGenerator;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Engine::AbstractIDGenerator';

use Global::Values;
use Database::MotDB;

enum 'ID_GEN_TYPE', [@{Global::Values->doIDGeneratorType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ID_GEN_TYPE',
  default => 'BAND_NAME',
);

has 'band' => (
  is => 'rw',
  isa => 'Str',
  predicate => 'hasBand',
  clearer => 'resetBand',
  default => "",
  lazy => 1,
);

########################
# methods
########################
sub updateBand {
  my $self = shift;
  #my $name = Database::MotDB->getBandName;
  my $name = Database::MotDBTableApp->getInstance->getBandName;
  if (! defined $name || $name eq '') {
    carp "There is no band name! Please Check it.";
    return;
  }
  $self->band($name);
}
########################
# implements methods
########################
sub getGeneratorType {
  my $self = shift;
  return $self->type;
}
sub generate {
  my $self = shift;
  my $formatStr = shift;
  my $map = shift;
  $self->updateBand;
  return $self->band;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


###########################################
# package Engine::FileNameGenerator;
###########################################
package Engine::FileNameGenerator;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Engine::AbstractIDGenerator';

use Global::Values;
use Database::MotDB;
use Utils::Tools;

enum 'ID_GEN_TYPE', [@{Global::Values->doIDGeneratorType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ID_GEN_TYPE',
  default => 'FILE_NAME',
);

has 'fileName' => (
  is => 'rw',
  isa => 'Str',
  predicate => 'hasFileName',
  clearer => 'resetFileName',
  default => "",
  lazy => 1,
);

########################
# methods
########################
sub updateFileName {
  my $self = shift;
  my $name = Database::MotDBTableApp->getInstance->getFileName;
  
  if (! defined $name || $name eq '') {
    carp "There is no file name! Please Check it.";
    return;
  }
  $self->fileName($name);
}
########################
# implements methods
########################
sub getGeneratorType {
  my $self = shift;
  return $self->type;
}
sub generate {
  my $self = shift;
  my $formatStr = shift;
  my $map = shift;
  $self->updateFileName;
  my $name = $self->fileName;
  $name = Utils::Tools->rmPunct($name);
  return $name;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;



###########################################
# package Engine::StreckeGenerator;
###########################################
package Engine::StreckeGenerator;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Engine::AbstractIDGenerator';

use Global::Values;
use Database::MotDB;
use Utils::Tools;

enum 'ID_GEN_TYPE', [@{Global::Values->doIDGeneratorType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ID_GEN_TYPE',
  default => 'STRECKE',
);

has 'strecke' => (
  is => 'rw',
  isa => 'Str',
  predicate => 'hasStrecke',
  clearer => 'resetStrecke',
  default => "a",
  lazy => 1,
);

########################
# methods
########################
sub updateStrecke {
  my $self = shift;
#  my $stk = Database::MotDB->getStrecke;
  my $stk = Database::MotDBTableApp->getInstance->getTableProcessor("STRECKE")->query;
  if (! defined $stk || $stk eq '') {
    carp "There is no strecke! Please Check it.";
    return;
  }
  $self->strecke($stk);
}
########################
# implements methods
########################
sub getGeneratorType {
  my $self = shift;
  return $self->type;
}
sub generate {
  my $self = shift;
  my $formatStr = shift;
  my $map = shift;
  $self->updateStrecke;
  return $self->strecke;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


############################################
## package Engine::LemmaNumGenerator;
############################################
#package Engine::LemmaNumGenerator;
#use Moose;
#use Moose::Util::TypeConstraints;
#use namespace::autoclean;
#
#use FindBin;
#use lib "$FindBin::Bin/../";
#
#use Carp;
#
#with 'Engine::AbstractIDGenerator';
#
#use Global::Values;
#use Database::MotDB;
#use Utils::Tools;
#
#enum 'ID_GEN_TYPE', [@{Global::Values->doIDGeneratorType}];
#
#########################
## Variables
#########################
#
#has 'type' => (
#  is => 'ro',
#  isa => 'ID_GEN_TYPE',
#  default => 'LEMMA_NUMBER',
#);
#
##has 'lemmaNum' => (
##  is => 'rw',
##  isa => 'Str',
##  predicate => 'hasLemmaNum',
##  clearer => 'resetLemmaNum',
##  default => "0",
##  lazy => 1,
##);
#
#########################
## methods
#########################
##sub updateLemmaNum {
##  my $self = shift;
##  my $lemNum = Database::MotDB->getLemNum;
##  
##  if (! defined $lemNum || $lemNum eq '') {
##    carp "There is no lemma number! Please Check it.";
##    return;
##  }
##  $self->lemmaNum($lemNum);
##}
#########################
## implements methods
#########################
#sub getGeneratorType {
#  my $self = shift;
#  return $self->type;
#}
#sub generate {
#  my $self = shift;
#  my $formatStr = shift;
#  my $map = shift;
#  
##  $self->updateLemmaNum;
##  my $num = $self->lemmaNum;
##  my $numLen = Global::Values::DO_ID_GEN->{NUMBER_LENGTH};
##  my $new = $self->appendPrefixChar(Global::Values::DO_ID_GEN->{LEMMA_NUM_PREFIX}, $numLen, $num);
#  
#  my $num = Database::MotDB->getNumAndIncr($self->type);
#  my $num = Database::MotDBTableApp->getInstance->getTableProcessor("")->getNumAndIncr;
#  my $numLen = Global::Values::DO_ID_GEN->{NUMBER_LENGTH};
#  my $prefix = Global::Values::DO_ID_GEN->{LEMMA_NUM_PREFIX};
#  my $new = $self->appendPrefixChar($prefix, $numLen, $num);
#  
#  return $new;
#}
#
#
##sub appendPrefixChar {
##  my $self = shift;
##  my $prefix = shift;
##  my $numLen = shift;
##  my $number = shift;
##  
##  my $new =  sprintf("$prefix%0*d", $numLen, $number);
##  return $new;
##}
#
#no Moose;
#__PACKAGE__->meta->make_immutable;
#1;




############################################
## package Engine::EntryNumGenerator;
############################################
#package Engine::EntryNumGenerator;
#use Moose;
#use Moose::Util::TypeConstraints;
#use namespace::autoclean;
#
#use FindBin;
#use lib "$FindBin::Bin/../";
#
#use Carp;
#
#with 'Engine::AbstractIDGenerator';
#
#use Global::Values;
#use Database::MotDB;
#use Utils::Tools;
#
#enum 'ID_GEN_TYPE', [@{Global::Values->doIDGeneratorType}];
#
#########################
## Variables
#########################
#
#has 'type' => (
#  is => 'ro',
#  isa => 'ID_GEN_TYPE',
#  default => 'ENTRY_NUMBER',
#);
#
##has 'entryNum' => (
##  is => 'rw',
##  isa => 'Str',
##  predicate => 'hasEntryNum',
##  clearer => 'resetEntryNum',
##  default => "0",
##  lazy => 1,
##);
#
#########################
## methods
#########################
##sub updateEntryNum {
##  my $self = shift;
##  my $entryNum = Database::MotDB->getEntryNum;
##  
##  if (! defined $entryNum || $entryNum eq '') {
##    carp "There is no entry number! Please Check it.";
##    return;
##  }
##  $self->entryNum($entryNum);
##}
#########################
## implements methods
#########################
#sub getGeneratorType {
#  my $self = shift;
#  return $self->type;
#}
#sub generate {
#  my $self = shift;
#  my $formatStr = shift;
#  my $map = shift;
#  
##  $self->updateEntryNum;
##  my $num = $self->entryNum;
##  my $numLen = Global::Values::DO_ID_GEN->{NUMBER_LENGTH};
##  my $new = $self->appendPrefixChar(Global::Values::DO_ID_GEN->{ENTRY_NUM_PREFIX}, $numLen, $num);
#  
#  my $num = Database::MotDB->getNumAndIncr($self->type);
#  my $numLen = Global::Values::DO_ID_GEN->{NUMBER_LENGTH};
#  my $prefix = Global::Values::DO_ID_GEN->{ENTRY_NUM_PREFIX};
#  my $new = $self->appendPrefixChar($prefix, $numLen, $num);
#  
#  return $new;
#}
#
#
##sub appendPrefixChar {
##  my $self = shift;
##  my $prefix = shift;
##  my $numLen = shift;
##  my $number = shift;
##  
##  my $new =  sprintf("$prefix%0*d", $numLen, $number);
##  return $new;
##}
#
#no Moose;
#__PACKAGE__->meta->make_immutable;
#1;


###########################################
# package Engine::GraphNumGenerator;
###########################################
package Engine::GraphNumGenerator;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Engine::AbstractIDGenerator';

use Global::Values;
use Database::MotDB;
use Utils::Tools;

enum 'ID_GEN_TYPE', [@{Global::Values->doIDGeneratorType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ID_GEN_TYPE',
  default => 'GRAPH_NUMBER',
);

#has 'graphNum' => (
#  is => 'rw',
#  isa => 'Str',
#  predicate => 'hasGraphNum',
#  clearer => 'resetGraphNum',
#  default => "0",
#  lazy => 1,
#);

########################
# methods
########################
#sub updateGraphNum {
#  my $self = shift;
#  my $graphNum = Database::MotDB->getGraphNum;
#  
#  if (! defined $graphNum || $graphNum eq '') {
#    carp "There is no graph number! Please Check it.";
#    return;
#  }
#  $self->graphNum($graphNum);
#}
########################
# implements methods
########################
sub getGeneratorType {
  my $self = shift;
  return $self->type;
}
sub generate {
  my $self = shift;
  my $formatStr = shift;
  my $map = shift;
  
#  $self->updateGraphNum;
#  my $num = $self->graphNum;
#  my $numLen = Global::Values::DO_ID_GEN->{NUMBER_LENGTH};
#  my $new = $self->appendPrefixChar(Global::Values::DO_ID_GEN->{GRAPH_NUM_PREFIX}, $numLen, $num);
  my $num = Database::MotDBTableApp->getInstance->getTableProcessor("GRAPH_NUM")->getNumAndIncr;
  #my $num = Database::MotDB->getNumAndIncr($self->type);
  my $numLen = Global::Values::DO_ID_GEN->{NUMBER_LENGTH};
  my $prefix = Global::Values::DO_ID_GEN->{GRAPH_NUM_PREFIX};
  my $new = $self->appendPrefixChar($prefix, $numLen, $num);
  
  
  return $new;
}


#sub appendPrefixChar {
#  my $self = shift;
#  my $prefix = shift;
#  my $numLen = shift;
#  my $number = shift;
#  
#  my $new =  sprintf("$prefix%0*d", $numLen, $number);
#  return $new;
#}

no Moose;
__PACKAGE__->meta->make_immutable;
1;




###########################################
# package Engine::TreeNumGenerator;
###########################################
package Engine::TreeNumGenerator;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Engine::AbstractIDGenerator';

use Global::Values;
use Database::MotDB;
use Utils::Tools;

enum 'ID_GEN_TYPE', [@{Global::Values->doIDGeneratorType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ID_GEN_TYPE',
  default => 'TREE_NUMBER',
);

#has 'treeNum' => (
#  is => 'rw',
#  isa => 'Str',
#  predicate => 'hasTreeNum',
#  clearer => 'resetTreeNum',
#  default => "0",
#  lazy => 1,
#);

########################
# methods
########################
#sub updateTreeNum {
#  my $self = shift;
#  my $treeNum = Database::MotDB->getTreeNum;
#  
#  if (! defined $treeNum || $treeNum eq '') {
#    carp "There is no tree number! Please Check it.";
#    return;
#  }
#  $self->treeNum($treeNum);
#}
########################
# implements methods
########################
sub getGeneratorType {
  my $self = shift;
  return $self->type;
}
sub generate {
  my $self = shift;
  my $formatStr = shift;
  my $map = shift;
  
  #$self->updateTreeNum;
  
  #my $num = Database::MotDB->getNumAndIncr($self->type);
  my $num = Database::MotDBTableApp->getInstance->getTableProcessor("TREE_NUM")->getNumAndIncr;
  my $numLen = Global::Values::DO_ID_GEN->{NUMBER_LENGTH};
  my $prefix = Global::Values::DO_ID_GEN->{TREE_NUM_PREFIX};
  my $new = $self->appendPrefixChar($prefix, $numLen, $num);
  
  return $new;
}

#sub appendPrefixChar {
#  my $self = shift;
#  my $prefix = shift;
#  my $numLen = shift;
#  my $number = shift;
#  
#  my $new =  sprintf("$prefix%0*d", $numLen, $number);
#  return $new;
#}


no Moose;
__PACKAGE__->meta->make_immutable;
1;



###########################################
# package Engine::ModuleNameGenerator;
###########################################
package Engine::ModuleNameGenerator;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Engine::AbstractIDGenerator';

use Global::Values;
use Database::MotDB;
use Utils::Tools;

enum 'ID_GEN_TYPE', [@{Global::Values->doIDGeneratorType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ID_GEN_TYPE',
  default => 'MODULE_NAME',
);

has 'modName' => (
  is => 'rw',
  isa => 'Str',
  predicate => 'hasModName',
  clearer => 'resetModName',
  default => "",
  lazy => 1,
);

########################
# methods
########################
sub updateModName {
  my $self = shift;
  #my $name = Database::MotDB->getModName;
  my $name = Database::MotDBTableApp->getInstance->getTableProcessor("MODULE_NAME")->query;
  if (! defined $name || $name eq '') {
    carp "There is no module name! Please Check it.";
    return;
  }
  $self->modName($name);
}
########################
# implements methods
########################
sub getGeneratorType {
  my $self = shift;
  return $self->type;
}
sub generate {
  my $self = shift;
  my $formatStr = shift;
  my $map = shift;
  
  $self->updateModName;
  my $name = $self->modName;
  return $name;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


###########################################
# package Engine::EntryIDGenerator;
###########################################
package Engine::EntryIDGenerator;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Engine::AbstractIDGenerator';

use Global::Values;
#use Database::MotDB;
use Database::MotDBProcessor;
use Utils::Tools;

enum 'ID_GEN_TYPE', [@{Global::Values->doIDGeneratorType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ID_GEN_TYPE',
  default => 'ENTRY_ID',
);

has 'entryID' => (
  is => 'rw',
  isa => 'Str',
  predicate => 'hasEntryID',
  clearer => 'resetEntryID',
  default => "",
  lazy => 1,
);

########################
# methods
########################
sub updateEntryID {
  my $self = shift;
  my $id = Database::MotDBTableApp->getInstance->getTableProcessor("CUR_ENTRY_ID")->query;
#  my $id = Database::MotDB->getCurEntryID;
  if (! defined $id || $id eq '') {
    carp "There is no entry id! Please Check it.";
    return;
  }
  $self->entryID($id);
}
########################
# implements methods
########################
sub getGeneratorType {
  my $self = shift;
  return $self->type;
}
sub generate {
  my $self = shift;
  my $formatStr = shift;
  my $map = shift;
  
  $self->updateEntryID;
  my $id = $self->entryID;
  return $id;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

###########################################
# package Engine::ModuleNumGenerator;
###########################################
package Engine::ModuleNumGenerator;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Engine::AbstractIDGenerator';

use Global::Values;
use Database::MotDB;
use Utils::Tools;

enum 'ID_GEN_TYPE', [@{Global::Values->doIDGeneratorType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ID_GEN_TYPE',
  default => 'MODULE_NUMBER',
);

#has 'modNum' => (
#  is => 'rw',
#  isa => 'Str',
#  predicate => 'hasModNum',
#  clearer => 'resetModNum',
#  default => "0",
#  lazy => 1,
#);

########################
# methods
########################
#sub updateModNum {
#  my $self = shift;
#  my $modNum = Database::MotDB->getModNum;
#  
#  if (! defined $modNum || $modNum eq '') {
#    carp "There is no module number! Please Check it.";
#    return;
#  }
#  $self->modNum($modNum);
#}
########################
# implements methods
########################
sub getGeneratorType {
  my $self = shift;
  return $self->type;
}
sub generate {
  my $self = shift;
  my $formatStr = shift;
  my $map = shift;
  
#  $self->updateModNum;
  
#  my $num = Database::MotDB->getNumAndIncr($self->type);
  my $num = Database::MotDBTableApp->getInstance->getTableProcessor("MODULE_NUM")->getNumAndIncr;
  my $numLen = Global::Values::DO_ID_GEN->{NUMBER_LENGTH};
  my $prefix = Global::Values::DO_ID_GEN->{MODULE_NUM_PREFIX};
  my $new = $self->appendPrefixChar($prefix, $numLen, $num);
  
  return $new;
}

#sub appendPrefixChar {
#  my $self = shift;
#  my $prefix = shift;
#  my $numLen = shift;
#  my $number = shift;
#  
#  my $new =  sprintf("$prefix%0*d", $numLen, $number);
#  return $new;
#}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


###########################################
# package Engine::SpecialNumGenerator;
###########################################
package Engine::SpecialNumGenerator;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Engine::AbstractIDGenerator';

use Global::Values;
use Database::MotDB;
use Utils::Tools;

enum 'ID_GEN_TYPE', [@{Global::Values->doIDGeneratorType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ID_GEN_TYPE',
  default => 'SPECIAL_NUMBER',
);

########################
# methods
########################

########################
# implements methods
########################
sub getGeneratorType {
  my $self = shift;
  return $self->type;
}
sub generate {
  my $self = shift;
  my $formatStr = shift;
  my $map = shift;
  
  
  #my $num = Database::MotDB->getNumAndIncr($self->type);
  my $num = Database::MotDBTableApp->getInstance->getTableProcessor("SPECIAL_NUM")->getNumAndIncr;
  my $numLen = Global::Values::DO_ID_GEN->{NUMBER_LENGTH};
  my $prefix = Global::Values::DO_ID_GEN->{SPECIAL_NUM_PREFIX};
  my $new = $self->appendPrefixChar($prefix, $numLen, $num);
  
  return $new;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;




###########################################
# package Engine::TextPartNumGenerator;
###########################################
package Engine::TextPartNumGenerator;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Engine::AbstractIDGenerator';

use Global::Values;
use Database::MotDB;
use Utils::Tools;

enum 'ID_GEN_TYPE', [@{Global::Values->doIDGeneratorType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ID_GEN_TYPE',
  default => 'TEXTPART_NUMBER',
);

########################
# methods
########################

########################
# implements methods
########################
sub getGeneratorType {
  my $self = shift;
  return $self->type;
}
sub generate {
  my $self = shift;
  my $formatStr = shift;
  my $map = shift;
  
  
  #my $num = Database::MotDB->getNumAndIncr($self->type);
  my $num = Database::MotDBTableApp->getInstance->getTableProcessor("TEXTPART_NUM")->getNumAndIncr;
  my $numLen = Global::Values::DO_ID_GEN->{NUMBER_LENGTH};
  my $prefix = Global::Values::DO_ID_GEN->{TEXTPART_NUM_PREFIX};
  my $new = $self->appendPrefixChar($prefix, $numLen, $num);
  
  return $new;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;






################### Engine, App, Context#######################
#CLASS:
package Engine::IDGeneratorEngine;
=encoding utf8

=head1 NAME

Engine::IDGeneratorEngine - an engine to store and control the ID Generators

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

use Engine::AbstractIDGenerator;

use Global::Values;
use Database::MotDB;

enum 'ID_GEN_TYPE', [@{Global::Values->doIDGeneratorType}];

=head1 ATTRIBUTES

=head2 generatorMap

ID generator map to store the Generators with the their types.

key: type of generator  - value: id generator (object)

=cut
has 'generatorMap' => (
  is => 'rw',
  isa => 'HashRef[Engine::AbstractIDGenerator]',
  predicate => 'hasGeneratorMap',
  clearer => 'resetGeneratorMap',
  default => sub {{}},
  lazy => 1,
);

=head1 METHODS

=head2 addGenerator($gen)

Add a given generator ($gen) to the generatorMap.

=head3 Parameters:

=over 4

=item *

$gen: the given generator, it extends Engine::AbstractIDGenerator

=back

=cut
sub addGenerator {
  my $self = shift;
  my $gen = shift;
  
  # Engine::AbstractIDGenerator
  my $type = $gen->getGeneratorType;
  # put generator in the map
  $self->generatorMap->{$type} = $gen;
}

=head2 getGenerator($genType)

=head3 Parameters:

=over 4

=item *

$genType: the given type of generator, see also Global::Values->doIDGeneratorType

=back

=head3 Returns:

The specified ID Generator()

=cut
sub getGenerator {
  my $self = shift;
  my $genType = shift;
  return $self->generatorMap->{$genType};
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
package Engine::IDGenerateApp;
=encoding utf8

=head1 NAME

Engine::IDGenerateApp - a id generator manager
=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

use Global::Values;
use Database::MotDB;

use Engine::IDGeneratorEngine;

use Engine::AbstractIDGenerator;

enum 'ID_GEN_TYPE', [@{Global::Values->doIDGeneratorType}];


=head1 CONSTRUCTORS

=head2 BUILD

After new.

=cut
sub BUILD {
  my $self = shift;
  # add build in generators
  $self->addBuildInGenerators;
}
=head2 _build_genengine

=cut
sub _build_genengine {
  my $g = Engine::IDGeneratorEngine->new;
  return $g;
}

=head1 ATTRIBUTES

=head2 genEngine

Engine::IDGeneratorEngine

=cut
has 'genEngine' => (
  is => 'rw',
  isa => 'Engine::IDGeneratorEngine',
  builder => "_build_genengine",
  handles => [qw( addGenerator getGenerator)],
);

=head1 METHODS

=head2 getInstance

Singleton.

=cut
my $singleton_IDGenerateApp;
sub getInstance {
  my $self = shift;
  if (! defined $singleton_IDGenerateApp) {
    $singleton_IDGenerateApp = $self->new;
  }
  return $singleton_IDGenerateApp;
}
=head2 addBuildInGenerators

Add the generators to the main

=cut
sub addBuildInGenerators {
  my $self = shift;
  $self->addGenerator(Engine::RowNumGenerator->new);
  $self->addGenerator(Engine::BandNameGenerator->new);
  $self->addGenerator(Engine::FileNameGenerator->new);
  $self->addGenerator(Engine::StreckeGenerator->new);
#  $self->addGenerator(Engine::LemmaNumGenerator->new);
#  $self->addGenerator(Engine::EntryNumGenerator->new);
  $self->addGenerator(Engine::GraphNumGenerator->new);
  $self->addGenerator(Engine::TreeNumGenerator->new);
  $self->addGenerator(Engine::ModuleNameGenerator->new);
  $self->addGenerator(Engine::ModuleNumGenerator->new);
  $self->addGenerator(Engine::SpecialNumGenerator->new);
  $self->addGenerator(Engine::TextPartNumGenerator->new);
  $self->addGenerator(Engine::EntryIDGenerator->new);
}

=head2 generateEntryID()

Generates the entry id with the form 'bandName=fileName=rowNum'.

=cut
sub generateEntryID {
  my $self = shift;
#  my $type = shift;
  
  my $bandName = $self->getGenerator("BAND_NAME")->generate;
  my $fileName = $self->getGenerator("FILE_NAME")->generate;
  my $rowNum = $self->getGenerator("ROW_NUMBER")->generate;
  if (! defined $bandName || ! $bandName 
      || ! defined $fileName || ! $fileName 
      || ! defined $rowNum || ! $rowNum 
      ) {
    carp "There is no valid band name, file name or row number for generate!";
    return;
  }
  my $entryID = join(Global::Values::DO_ID_GEN->{GLUE_INNER_CHAR}, $bandName, $fileName, $rowNum);
  
  
  # it will be saved into the database after creating
  my $dbtApp = Database::MotDBTableApp->getInstance;
  $dbtApp->updateEntryID($entryID);
  # send it to the id map
  #$dbtApp->getTableProcessor("IDMAP_ROW")->insert($dbtApp->getRowNum, $entryID);
  
  return $entryID;
}

=head2 generateTextPartID()

Generates id for the Text::EntryPartText with the form 'entryID=textpartID'.

=cut
sub generateTextPartID {
  my $self = shift;
#  my $modName = shift;

  my $entryID = $self->getGenerator("ENTRY_ID")->generate;
  if (! defined $entryID || $entryID eq "") {
    carp "There is no valid entryID";
    $entryID = $self->generateEntryID;
  }

  my $tpNum = $self->getGenerator("TEXTPART_NUMBER")->generate;
  if (! defined $entryID || ! $entryID || ! defined $tpNum || ! $tpNum ) {
    carp "There is no valid entry id or text part number for generate!";
    return;
  }
  my $tpID = join(Global::Values::DO_ID_GEN->{GLUE_INNER_CHAR}, $entryID, $tpNum);
  
  # it will be saved into the database after creating
  my $dbtApp = Database::MotDBTableApp->getInstance;
  $dbtApp->getTableProcessor("CUR_TEXTPART_ID")->insert($tpID);
  
  # send it to the id map
  #$dbtApp->getTableProcessor("IDMAP_TEXTPART")->insert($entryID, $tpID);
  
  return $tpID;
}


=head2 generateModuleID()

Generates id for the processing module with the form 'entryID=moduleID'.

=cut
sub generateModuleID {
  my $self = shift;
  #my $modName = shift;
  
  my $entryID = $self->getGenerator("ENTRY_ID")->generate;
  if (! defined $entryID || $entryID eq "") {
    carp "There is no valid entryID";
    $entryID = $self->generateEntryID;
  }
  my $modNum = $self->getGenerator("MODULE_NUMBER")->generate;
  if (! defined $entryID || ! $entryID || ! defined $modNum || ! $modNum ) {
    carp "There is no valid entry id or module num for generate!";
    return;
  }
  #my $modName = $self->getGenerator("MODULE_NAME")->generate;
  
  my $modID = join(Global::Values::DO_ID_GEN->{GLUE_INNER_CHAR}, $entryID, $modNum);
  
  # it will be saved into the database after creating
  my $dbtApp = Database::MotDBTableApp->getInstance;
  $dbtApp->getTableProcessor("CUR_MODULE_ID")->insert($modID);
  # send it to the id map
  #FIXME: SAVE TIME
  #$dbtApp->getTableProcessor("IDMAP_MODULE")->insert($entryID, $modID);
  
  return $modID;
}


=head2 generateGraphID()

Generates id for the processing graph with the form 'entryID=graphID'.

=cut
sub generateGraphID {
  my $self = shift;
  #my $modName = shift;
  
  # get entry id
  my $entryID = $self->getGenerator("ENTRY_ID")->generate;
  if (! defined $entryID || $entryID eq "") {
    carp "There is no valid entryID";
    $entryID = $self->generateEntryID;
  }
  # graph num with prefix
  my $graphNum = $self->getGenerator("GRAPH_NUMBER")->generate;
  if (! defined $entryID || ! $entryID 
      || ! defined $graphNum || ! $graphNum ) {
    carp "There is no valid entry id or graph number for generate!";
    return;
  }
  my $graphID = join(Global::Values::DO_ID_GEN->{GLUE_INNER_CHAR}, $entryID, $graphNum);
  
  # it will be saved into the database after creating
  my $dbtApp = Database::MotDBTableApp->getInstance;
  $dbtApp->getTableProcessor("CUR_GRAPH_ID")->insert($graphID);
  
  # send it to the id map
  #$dbtApp->getTableProcessor("IDMAP_GRAPH")->insert($entryID, $graphID);
  
  return $graphID;
}

=head2 generateTreeID()

Generates id for the the structure of oringal entry text with the form 'entryID=TreeID'.

=cut
sub generateTreeID {
  my $self = shift;
#  my $modName = shift;
  
  my $entryID = $self->getGenerator("ENTRY_ID")->generate;
  if (! defined $entryID || $entryID eq "") {
    carp "There is no valid entryID";
    $entryID = $self->generateEntryID;
  }
  my $treeNum = $self->getGenerator("TREE_NUMBER")->generate;
  if (! defined $entryID || ! $entryID 
      || ! defined $treeNum || ! $treeNum ) {
    carp "There is no valid entry id or tree number for generate!";
    return;
  }
  my $treeID = join(Global::Values::DO_ID_GEN->{GLUE_INNER_CHAR}, $entryID, $treeNum);
  
  # it will be saved into the database after creating
  my $dbtApp = Database::MotDBTableApp->getInstance;
  $dbtApp->getTableProcessor("CUR_TREE_ID")->insert($treeID);
  
  # send it to the id map
  #$dbtApp->getTableProcessor("IDMAP_TREE")->insert($entryID, $treeID);
  
  return $treeID;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;
