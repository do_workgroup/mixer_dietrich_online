package Engine::HTMLEngine;
=encoding utf8

=head1 NAME

Engine::HTMLEngine - HTML Parser Interface for Dietrich Online

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This Class can parse standard html tags and self defined tags. But it cannot for now parse the nested 

html structure. Try to avoid nested structure in Dietrich Online.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 SEE ALSO

HTML::TokeParser::Simple

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

# HTML
use HTML::TokeParser::Simple;
use List::Util;
use List::MoreUtils;
use Scalar::Util;

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Data::Dumper;
use Clone;

use Global::Values;
use Utils::Tools;
use Coordinate::TextIndex;
use Text::EntryText; 
use Text::EntryPartText;

# Attention: Role
with 'Engine::AbstractEngine';


=head1 METHODS

=head2 reset()

=head3 Overrides:

reset in class Engine::AbstractEngine

=cut
sub reset {
  my $self = shift;
  
  $self->setTargetText("");
  $self->getOrigPos->reset;
  $self->getOutputText->reset;
}

=head3 searchString:

=head3 Parameters:

=over 4

=item *

$textRef: the given reference of string context

=item *

$targetRef: the given reference of target string that looking for in the $textRef

=item *

$idx: the index to start the search from.

=item *

$isFirst: 0/1, only take the first one or all of them.

=back

=head3 Returns:

the reference of position array.

=cut
sub searchString {
  my $self = shift;
  my ($textRef, $targetRef, $idx, $isFirst) = @_;
  if (! $$textRef || ! $$targetRef || ! Scalar::Util::looks_like_number($idx)) {
    carp "Error: Die Input Parameters ist nicht valid!";
    return;
  }
  my $newStr = "";
  my $startIdx = -1;
  my $pointer = -1;
  
  my @posArr = ();
  
  my $text = $$textRef;
  
  my $len = length $$targetRef;
  
  $startIdx = index($text, $$targetRef, $idx);
  # founded
  if ($startIdx >=0) {
    # only the first one
    if ($isFirst) {
      my $pos = $startIdx . "," . $len;
      push @posArr, $pos;
    } else {
      
      while ($startIdx >= 0) {
        # remove the search text 
        #substr($text, $startIdx, $len) = "";
        
        # get the position array
        my $pos = $startIdx . "," . $len;
        push @posArr, $pos;
        
        # one more index push
        $startIdx++;
        $startIdx = index($text, $$targetRef, $startIdx);
      }
    }
    #print Dumper(@posArr);
    return \@posArr;
  } else {
    return;
  }
}

=head3 searchLastPart:

=head3 Parameters:

=over 4

=item *

$textRef: the given reference of string context

=item *

$targetRef: the given reference of target string that looking for in the $textRef

=back

=head3 Returns:

the reference of position array that the index within this string context of the rightmost occurrence 

of the specified target string.

=cut
sub searchLastPart {
  my $self = shift;
  my ($textRef, $targetRef) = @_;
  if (! $$textRef || ! $$targetRef) {
    carp "Error: Die Input Parameters ist nicht valid!";
    return;
  }
  my $lastStartIdx = -1;
  my @posArr = ();
  
  my $text = $$textRef;
  my $len = length $$targetRef;
  
  $lastStartIdx = rindex($text, $$targetRef);
  # founded
  if ($lastStartIdx >=0) {
    my $pos = $lastStartIdx . "," . $len;
    push @posArr, $pos;
  }
  return \@posArr;
}


=head3 concatStrings:

Concatenate the input strings to in a string.

=head3 Parameters:

=over 4

=item *

$textBufferRef: the given reference of string buffer

=item *

$inputTextRef: the given reference of string to be concatenated

=back

=head3 Returns:

the reference of string

=cut
sub concatStrings {
  my $self = shift;
  my $textBufferRef = shift;
  my $inputTextRef = shift;
  my $textBuffer;
  if (! defined $textBufferRef || $textBufferRef eq "") {
    $textBuffer = "";
  } else {
    $textBuffer = $$textBufferRef;
  }
  my $inputText = $$inputTextRef;
  
  $textBuffer .= $inputText;
  
  return \$textBuffer;
}

=head3 getTextEndIndex:

=head3 Parameters:

=over 4

=item *

$textRef: the given reference of string text

=back

=head3 Returns:

the ending index of the given string text

=cut
sub getTextEndIndex {
  my $self = shift;
  my $textRef = shift;
  if (! defined $textRef || $textRef eq "") {
    return 0;
  }
  my $len = length $$textRef;
  
  my $index = $len-1;
  
  return $index;
}

#use HTML::TokeParser::Simple;
#elements: ganze Element mit start- /Endstag zusammen
#ttypes: content, attr, attrseq, text, link
#tag: einziger Tag: entry, l, s, 
#tagType: entry->id, l->lemma, s->sigel, ele->bemerkung
# Return the hashmap reference
=head3 fetchHtmlElement($tag, $ttypesRef, $textRef)

=head3 Parameters:

=over 4

=item *

$tag: the given HTML tag

=item *

$ttypesRef: the reference of html part types array. i.e. attr, attrseq, content, element, text

          see also Global::Values->HTML_TYPE

=item *

$textRef: the given reference of string text

=back

=head3 Returns:

the result hashmap

=begin text

  my %result = (
    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
    "elements"    => "",    # element: element text with array ref
    "restText"    => "",    # restText: the input original text without current elements (text reference)
    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
    "eleNum"      => 1,   # the found element amount
    "eleFlags"    => "",    # eleFlags: element flags with array ref
    "content"     => "",    # content: content with array ref
    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
    "attr"        => "",    # attr: attributes in einem Tag with hash ref
    "attrseq"     => "",    # attrseq: attr keys with array ref
    "targetText"        => "",    # text: target original text with text ref
    "link"        => "",    # link: url link with text ref
    "rawText"     => "",    # rawText: the input original text ref
    "hasTagName"  => "",
    "hasTagProblem"  => "",
    "hasRawText"  => "",
    "hasElement"  => "",
    "hasRestText"  => "",
    "hasTargetText"  => "",
    "hasContent"  => "",
    "hasAttr"  => "",
    "hasAttrseq"  => "",
    "hasLink"  => "",
  );
=end text


=cut
sub fetchHtmlElement {
  my $self = shift;
  my ($tag, $ttypesRef, $textRef) = @_;
  if ($tag ~~ undef || ! $tag ||
    $ttypesRef ~~ undef || ! $ttypesRef ||
    $textRef ~~ undef || ! $textRef
  ) {
    carp "Warnung: the input parameters are invalid!";
    return;
  }
  my @ttypes = @{ $ttypesRef };

# elePos: stardIdx,offset: stardIdx != -1, offset != 0 und StardIdx + offset kleiner als nächste StartIdx
  Global::Values->resetHtmlAttrHash;
#  my $htmlAttrHashRef = Clone::clone(Global::Values->htmlAttrHash);
  my $htmlAttrHashRef = Global::Values->htmlAttrHash;
  my %result = %{$htmlAttrHashRef};
  # 0. tagName: as Reference
  $result{tagName} = \$tag;
  $result{hasTagName} = 1;

  
  # 1. rawText: as Reference
  $result{rawText} = $textRef;
  if ($textRef) {$result{hasRawText} = 1};
  
  # 2.parameters for prepare to analyse the input text
  # temp
  my $tmpContent = "";
  my $tmpElement = "";
  
  # pointer of element
  my $elePointer = -1;
  
  #TODO: nested html structures => hashmap {name_number: {tagName, position, content, attribute}}
  my $nestedLevel = 0;
  
  # start - end
  my $contentFlag = 0; # initial
  my $elementFlag = 0; # initial
  
  # text result 
  my @contentArr = ();
  my @elementArr = ();
  my @elementPosArr = ();
  my @eleContentPosArr = ();
  
  # s, e, se
  my @tagFlags = ();
  my @eleFlags = ();
  
  # valid sequence is: ['s', 'e', 'se', 's', 'e']
  my @flagArr = ();
  
  # attribute hash array
  my @hashArr = ();
  
  #3. analyse the input text
  #Achtung: TEXT REF
  my $p = HTML::TokeParser::Simple->new($textRef);
  # only for one line
  my $visitedStrRef;
  my $curIndex = 0;
  # sax parser: Token0->TokenN
  while (my $token = $p->get_token) {
    # temporary string to be parsed
    my $temp = $token->as_is();
    # the string is visited
    $visitedStrRef = $self->concatStrings($visitedStrRef, \$temp);
    # parsed with specified types: content, attr, attrseq, text, link etc.
    foreach my $type (@ttypes) {

      given ($type) {
        when(/^(??{ Global::Values::HTML_TYPE->{ELEMENT_PART} })$/i) {
          # code sequence is important: end->flag->start
          
          if ($token->is_end_tag($tag)) {
            $elementFlag --;
            push @eleFlags, "e";
            if ($self->validTagFlags(\@eleFlags)) {
              $tmpElement .= $token->as_is();
              push @elementArr,  $tmpElement;
              #print Dumper("temp Element: " . $tmpElement);
              #temporary position array
              #my $elePosRef = $self->searchString($textRef, \$tmpElement, $curIndex, 1);
              my $elePosRef = $self->searchLastPart($visitedStrRef, \$tmpElement);
              if (!defined $elePosRef) {
                carp "Error: $tmpElement is not found!";
              } else {
                push @elementPosArr, @$elePosRef;  # It doesn't matter whether duplicate
                $result{hasElement} = 1;
              }
            }
            $tmpElement = "";
          }
          
          # get the content
          if ($elementFlag) {
            $tmpElement .= $token->as_is();
          }
          
          # start tag
          #print "is StartTag? " . $token->is_start_tag($tag);
          if ($token->is_start_tag($tag)) {
            $elementFlag ++;
            
            # EMPTY ELEMENTS, i.e. <br/>, <img .../>, <ok />
            if (exists $token->[2]{"/"} || $tag=~ m/[a-z0-9]+\//i) {
              $elementFlag --;
              push @eleFlags, "se";
              
              if ($self->validTagFlags(\@eleFlags)) {
              $tmpElement .= $token->as_is();
              # take the element
              push @elementArr,  $tmpElement;
              #temporary position array
#              my $elePosRef = $self->searchString($textRef, \$tmpElement, $curIndex, 1);
              my $elePosRef = $self->searchLastPart($visitedStrRef, \$tmpElement);
              if (!defined $elePosRef) {
                carp "Error: $tmpElement is not be found!";
              } else {
                push @elementPosArr, @$elePosRef;  # It doesn't matter whether duplicate
                $result{hasElement} = 1;
              }
            }
            $tmpElement = "";
            $elementFlag --;
            } else {
              # elements, i.e. <entry ... >, <ele ...>
              push @eleFlags, "s";
            }
            # get the start position of target element
            $elePointer = index($$textRef, $token->as_is(), $elePointer);
            $tmpElement .= $token->as_is();
          }
        }
        when(/^(??{ Global::Values::HTML_TYPE->{CONTENT_PART} })$/i) {
          # code sequence is important: end->flag->start
          
          # sequence: 1. end tag, 2. content, 3. start tag, because of $contentFlag 
          # end tag
          if ($token->is_end_tag($tag)) {
            $contentFlag --;
            push @tagFlags, "e";
            if ($self->validTagFlags(\@tagFlags)) {
              push @contentArr,  $tmpContent;
              # the ending index of content
              push @eleContentPosArr, $curIndex+1;
              $result{hasContent} = 1;
            }
            $tmpContent = "";
          }
          
          # get the content
          if ($contentFlag) {
            $tmpContent .= $token->as_is();
          }
          
          # start tag
          if ($token->is_start_tag($tag)) {
            $contentFlag ++;
            # void elements, i.e. <br/>, <img .../>
            if (exists $token->[2]{"/"}) {
              $contentFlag --;
              push @tagFlags, "se"; 
              push @eleContentPosArr, $curIndex, $curIndex;
            } else {
              # elements, i.e. <entry ... >, <ele ...>
              push @tagFlags, "s";
              my $contentBeginIdx = $self->getTextEndIndex($visitedStrRef) + 1;
              # the beginning index of content
              push @eleContentPosArr, $contentBeginIdx;
            }
          }
        }
        
        when(/^(??{ Global::Values::HTML_TYPE->{ATTR_PART} })$/i) {
          if ($token->is_start_tag($tag)) {
            #$result{attr} = $token->[2];
            #print ref($token->get_attr());
            #$result{attr} = $token->get_attr();
            my $attrHash = $token->get_attr();
            push @hashArr, $attrHash;
            #$result{hasAttr} = 1;
          }
        }
        
        when(/^(??{ Global::Values::HTML_TYPE->{ATTRSEQ_PART} })$/i) {
          if ($token->is_start_tag($tag)) {
            #print ref($token->[3]);
            #$result{attrseq} = $token->[3];
            #$result{hasAttrseq} = 1;
          }
        }
        
        when(/^(??{ Global::Values::HTML_TYPE->{TEXT_PART} })$/i) {
          if ($token->is_start_tag($tag)) {
            #print ref($token->as_is());
            $result{targetText} = $token->as_is();
            $result{hasTargetText} = 1;
          }
        }
        
        when(/^(??{ Global::Values::HTML_TYPE->{LINK_PART} })$/i) {
          #$result{hasLink} = 1;
        }
        
        default {
          carp "Warning: Die Inputtype $_ ist ungültig in der getTextPar()!";
          next;
        }
      }
    }
#    print Dumper("string: " . $$visitedStrRef);
    $curIndex = $self->getTextEndIndex($visitedStrRef);
#    print Dumper("index: " . $curIndex);
  }
  
  #4. tags sequence
  $result{tagFlags} = \@tagFlags;
  $result{eleFlags} = \@eleFlags;
  $result{content} = \@contentArr;
  
  $result{hasTagProblem} = $self->hasProblemInTagFlags($result{eleFlags});
  
  # elements' position
  my $elePosArrRef = Utils::Tools->changeIndexLenToIndices(\@elementPosArr);
  $result{elePos} = $elePosArrRef;
  
  # content's Position
  my $eleContentPosArrRef = Utils::Tools->setIndicesArray(\@eleContentPosArr);
  $result{eleContentPos} = $eleContentPosArrRef;
  
  $result{elements} = \@elementArr;
  $result{attr} = \@hashArr;
  my $eleNum = scalar @elementArr;
  $result{eleNum} = $eleNum;
  
  return \%result;
}


#######################################
# HTML check subroutine
#######################################


=head3 checkHtmlAttr:
 
The keys and value of attributes will be composed of only numbers, letters and unterstrike '_'

=head3 Parameters:

=over 4

=item *

$attrHashRef: the given reference of attribute hashmap

=back

=head3 Returns:

true or false

=cut
sub checkHtmlAttr {
  my $self = shift;
  my $attrHashRef = shift;
  if ($attrHashRef ~~ undef || ! $attrHashRef) {
    #carp "Warning: There isn't valid input Parameter!";
    return 0;
  }
  foreach my $key (keys %$attrHashRef) {
    my $value = $attrHashRef->{$key};
    if ($key =~ m/^[a-z0-9_]+$/i && $value=~ m/^[a-z0-9_]+$/i) {
      #print "key value ok\n"; 
    } else {
      carp "\nWarning: There is some unvalid character in the attrHtmlTest(), $key => $value !";
      return 0;
    }
  }
#  while (my ($key, $value) = each %$attrHashRef) {
#    if ($key =~ m/^[a-z0-9_]+$/i && $value=~ m/^[a-z0-9_]+$/i) {
#      #print "key value ok\n"; 
#    } else {
#      carp "\nWarning: There is some unvalid character in the attrHtmlTest(), $key => $value !";
#      return 0;
#    }
#  }
  return 1;
}

=head3 checkElePosArr:
 
check the positions valid or not.

pos1 + offset <= pos2, pos2 + offset <= pos3,  and so on...

=head3 Parameters:

=over 4

=item *

$posArrRef: the given reference of position array

=back

=head3 Returns:

true or false

=cut
sub checkElePosArr {
  my $self = shift;
  my $posArrRef = shift;
  if ($posArrRef ~~ undef || ! $posArrRef) {
    #carp "Warning: There isn't the valid input parameter!";
    return 0;
  }
  my $oldItem = "";
  foreach my $curItem (@$posArrRef) {
    if ($oldItem) {
      # 0: start index; 1: offset
      my @oldPosArr = split /,/ , $oldItem;
      my @curPosArr = split /,/ , $curItem;
      my $oldEnd = $oldPosArr[0] + $oldPosArr[1];
      if ($oldEnd > $curPosArr[0]) {
        #carp "Error: Some elements Position are out of range!";
        return 0;
      }
    }
    $oldItem = $curItem;
  }
  return 1;
}

# return true(1), if there are some problem by tag flags
# return false(0), if there are no problem or flags are empty
sub hasProblemInTagFlags {
  my $self = shift;
  my $flagArrRef = shift;
  if (!defined $flagArrRef) {
    #carp "Error: The inputed arrayref is empty!"; 
    return 0;
  }
  
  if(! @$flagArrRef) {
    # empty is not problem
    return 0;
  } 
  my $result = 0;
  my $startFlags = 0;
  my $endFlags = 0;
  
  my $index = 0;
  foreach my $item (@$flagArrRef) {
    # check the first flag
    if ($index == 0 && ! ($item eq 's' || $item eq 'se')) {
      #carp "Error: Start-Tag is not in the first place!"; 
      $result = 0;
    }
    # set the flags
    if ($item eq 's') { $startFlags++;}
    elsif ($item eq 'e') {$endFlags++;}
    elsif ($item eq 'se') {$startFlags++; $endFlags++;}
    $index ++;
  }
  # paarig
  if ($startFlags != $endFlags) {$result = 1;}  
  
  return $result;
}

=head3 validTagFlags($fArrRef)
 
check the flags valid or not.

For example: <s></s>:start end (true), <s><s>: start start(false), </s><s>: end start(false), </s></s>: end end(false)

The valid tag flags are like "s", "e", "se", "s", "e" etc.

=head3 Parameters:

=over 4

=item *

$fArrRef: the given reference of flag array

=back

=head3 Returns:

true or false

=cut
sub validTagFlags {
  my $self = shift;
  my ($fArrRef) = @_;
  if (!defined $fArrRef || ! @$fArrRef) {
    #carp "Error: The inputed arrayref is empty!"; 
    return 0;
  }
  my $result = 1;
  my $startFlags = 0;
  my $endFlags = 0;
  
  my $index = 0;
  foreach my $item (@$fArrRef) {
    # check the first flag
    if ($index == 0 && ! ($item eq 's' || $item eq 'se')) {
      #carp "Error: Start-Tag is not in the first place!"; 
      $result = 0;
    }
    # set the flags
    if ($item eq 's') { $startFlags++;}
    elsif ($item eq 'e') {$endFlags++;}
    elsif ($item eq 'se') {$startFlags++; $endFlags++;}
    $index ++;
  }
  # paarig
  if ($startFlags != $endFlags) {$result = 0;}  
  
  return $result;
}

=head3 validHtmlElements($analyseResultRef)
 
check the validation of the given hashmap reference of Html-Analysis.

For example: <s></s>:start end (true), <s><s>: start start(false), </s><s>: end start(false), </s></s>: end end(false)

The valid tag flags are like "s", "e", "se", "s", "e" etc.

=head3 Parameters:

=over 4

=item *

$analyseResultRef: the given reference of analysis result

=back

=head3 Returns:

true or false

=cut
sub validHtmlElements {
  my $self = shift;
  my $analyseResultRef = shift;
  
  if (! defined $analyseResultRef || ! $analyseResultRef) {
    carp "There is no valid input parameter!";
    return;
  }
  
  my $eleFlagsArrRef = ${$analyseResultRef}{eleFlags};
  my $tagFlagsArrRef = ${$analyseResultRef}{tagFlags};
  
  my $attrHashRef = ${$analyseResultRef}{attr};
  
  my $posArrRef = ${$analyseResultRef}{elePos};
  
  # result: 0 oder 1
  my %result = (
    "eleFlag" => "",
    "tagFlag" => "",
    "attrFlag" => "",
    "posFlag" => "",
    "isPairTags" => "",
  );
  
  #1. flags check
  $result{eleFlag} = $self->validTagFlags($eleFlagsArrRef);
  $result{tagFlag} = $self->validTagFlags($tagFlagsArrRef);
  $result{isPairTags} = $self->validTagFlags($tagFlagsArrRef);
  
  #2. attr check
  $result{attrFlag} = $self->checkHtmlAttr($attrHashRef);
  
  #3. element position check
  $result{posFlag} = $self->checkElePosArr($posArrRef);
  
  return \%result;
}


#TODO: to check in the Text, there is target Element or not.
# $textRef: input Text reference
# $tagName: tag name, i.e. "entry", "ele", "s", "l"
# $tagAttrKey(optional): the target Attribute name, i.e. "type", "/"
#=head3 hasTargetEle($textRef, $tagName, $tagAttrKey)
# 
#check the given text whether has the target elment or not.
#
#=head3 Parameters:
#
#=over 4
#
#=item *
#
#$textRef: the given reference of text
#
#=item *
#
#$tagName: the specified tag name
#
#=item *
#
#$tagAttrKey: the specified attribute name (optional)
#
#=back
#
#=head3 Returns:
#
#true or false
#
#=cut 
sub hasTargetEle {
  my $self = shift;
  my ($textRef, $tagName, $tagAttrKey) = @_;
  
  if (! defined $textRef || $$textRef eq "" || ! defined $tagName || $tagName eq "") {
    carp "Error: invalid input Parameters!";
    return;
  }
  
  #3. analyse the input text
  #Achtung: TEXT REF
  my $parser = HTML::TokeParser::Simple->new($textRef);
  
  my $isStart = 0;
  my $hasTag = 0;
  my $checkAttrKey = 0;
  
  # attribute key check
  if (defined $tagAttrKey && $tagAttrKey ne "") {
    $checkAttrKey = 1;
  }
  
  while(my $token = $parser->get_token()) {
    print Dumper($token);
     if ($token->is_start_tag($tagName)) {
       if ($checkAttrKey) {
         my $attrRes = $self->hasAttrKey($tagAttrKey, $token->get_attr());
       }
     }
  }
}


# z.B. (All values are reference!)
# %$analystTextHashRef = (
#          'elements' => ['<ele type="ignore:verweisverweis"> -- s. a. Gewand</ele>'],
#          'restText' => \'<entry id="p00002" bd="73" se="523" sp="re" ze="06"><l>Pacht</l>: Ueb. d. Regelg. h> ulo d. ~zinses v. landw. ~gn. (Th. Haas) <s>490c.</s>XI. 214</entry>'
#          'tagFlags' => [],
#          'tagProblem' => 0/1,
#          'eleFlags' => ['s','e'],
#          'text' => '',
#          'link' => '',
#          'elePos' => ['125,56'],
#          'attr' => '',
#          'content' => [],
#          'rawText' => \'<entry id="p00002" bd="73" se="523" sp="re" ze="06"><l>Pacht</l>: Ueb. d. Regelg. h> ulo d. ~zinses v. landw. ~gn. (Th. Haas)<ele type="ignore:verweisverweis"> -- s. a. Gewand</ele> <s>490c.</s>XI. 214</entry>',
#          'attrseq' => ''
#        );
=head3 extractElementsFromText($analystTextHashRef)

=head3 Parameters:

=over 4

=item *

$analyseResultRef: the given reference of analysis result

=back

=head3 Returns:

the hashmap with the element positions and raw texts of the elements.

=cut
sub extractElementsFromText {
  my $self = shift;
  my ($analystTextHashRef) = @_;
  
  if ($analystTextHashRef ~~ undef) {
    carp "Error: analystTextHashRef has problem!";
    return;
  }
  
  my $elePosArrRef = $$analystTextHashRef{elePos};
  # undef or empty
  if ($elePosArrRef ~~ undef || ! @$elePosArrRef) {
    carp "Error: elePosArrRef has problem!";
    return;
  }
  
  # Descending lexicographic sort
  my @elePosArr = reverse @$elePosArrRef;
  
  my $posArrRef = $self->splitElePos(\@elePosArr); 
  
  # rawText
  my $textRef = $$analystTextHashRef{rawText};
  my $text = $$textRef;
  # check $posArrRef
  if (! defined $posArrRef || ! @$posArrRef) {return;}
  my $item = List::MoreUtils::natatime(2, @$posArrRef);
  while(my @vals = $item->())  {
    #print $vals[0] . " " . $vals[1] . "\n";
    # remove the selected substring in the text.
    substr $text, $vals[0], $vals[1], "";
  }
  $$analystTextHashRef{restText} = \$text;
  
  return $analystTextHashRef;
}

# $kv: 12,3 or 34,12 => index, length
=head3 splitElePos($elePosArrRef)

Splits the position's string to a array, i.e. 12,3 or 34,12 => [index, length, index, length, ...]

=head3 Parameters:

=over 4

=item *

$elePosArrRef: the given array reference of element's positions

=back

=head3 Returns:

the array reference

=cut
sub splitElePos {
  my $self = shift;
  my ($elePosArrRef) = @_;
  
  #0. check
  if (! defined $elePosArrRef || ! @$elePosArrRef) {
    carp "Error: Invalid Iniput Parameters!";
    return;
  }
  
  for my $item (@$elePosArrRef) {
    if ( ! $item ~~ /^\d+,\d+$/) {
      carp "Error: Invalid Positionsinformation!";
      return;
    }
  }
  
  #1. process
  my @res;
  
  for my $kv (@$elePosArrRef) {
    my ($index, $len) = split /,/ , $kv;
    #print "\nindex: " . $index . " len: " . $len;
    push @res, $index, $len;
  }
  #print Dumper(@res);
  
  return \@res;
}

# 

=head3 hasAttrKey($attrName, $attrHashRef)

Check it whetherthe target key is in the hashmap of attribute

Attention: <ele type="ok"/> or <ok/>, the backslash is as attribute key by HTML::TokeParser::Simple.

=head3 Parameters:

=over 4

=item *

$attrName: the given attribute name

=item *

$attrHashRef: the given hashmap's reference of attribute

=back

=head3 Returns:

Returns true if it has attribute name, return false otherwise.

=cut
sub hasAttrKey {
  my $self = shift;
  my ($attrName, $attrHashRef) = @_;
  if (exists $$attrHashRef{$attrName}) {
    return 1;
  } else {
    return 0;
  }
}


# Returns the Result Array reference with strecke and ordinal id number.
sub getStreckeFromEntry {
  my $self = shift;
  my $analystTextHashRef = shift;
  
  if ($analystTextHashRef ~~ undef || ! $analystTextHashRef) {
    carp "Warnung: invalid input parameters!";
    return;
  }
  
  if (! exists $$analystTextHashRef{hasTagName} || ${$$analystTextHashRef{tagName}} ne Global::Values::HTML_TAG->{ENTRY_TAG}) {
    carp "Warnung: There is no valid entry_tag information!";
    return;
  }
  
  my $attrHashRef = $$analystTextHashRef{attr};
  
  return if (! defined $attrHashRef || ! $attrHashRef);
  
  my $idText = $$attrHashRef{Global::Values::HTML_ATTR->{ENTRY_ATTR_ID}};
  
  my $idArrRef = $self->getStreckeFromEntryID(\$idText);
  
  return $idArrRef;
}


# i.e. id="p00002"
# Returns the Result Array reference with strecke and ordinal id number.
sub getStreckeFromEntryID { 
  my $self = shift;
  my $idRef = shift;
  my @res;
  if ($$idRef =~ m/(\w{1,1})(\d{5,6})/i) {
    #print "1:" . $1 . ", 2: " . $2;
    push @res, $1;
    push @res, $2;
    return \@res;
  }
  return;
}


# get the elements information
# $htmlAnalyseHashRef: the hash reference from subroutine fetchHtmlElement()
# $tagName: tag name, i.e. "ele", "entry", "s", "l", "sub", "sup"
# Returns the hash reference
sub getElementsInfos  {
  my $self = shift;
  my ($htmlAnalyseHashRef) = @_;
  
  if ($htmlAnalyseHashRef ~~ undef || ! $htmlAnalyseHashRef) {
    carp "Error from analyseEleElements(): the invalid input parameters!";
    return;
  }
  # 0. initializing result array
  my @resArr;
  
  # 1. Tag Name exists
  if ($$htmlAnalyseHashRef{hasTagName}) {
    my $tagNameRef = $$htmlAnalyseHashRef{tagName};
    # 2. check Elements
    if ($$htmlAnalyseHashRef{hasElement}) {
      my $eleArrRef = $$htmlAnalyseHashRef{elements};
      
      foreach my $item (@$eleArrRef) {
        my $tag = $$tagNameRef;
        #my @type = ("element", "attr", "attrseq");
        my @type = (Global::Values::HTML_TYPE->{ELEMENT_PART}, Global::Values::HTML_TYPE->{CONTENT_PART}, Global::Values::HTML_TYPE->{ATTR_PART},);
   
        my $resHashRef = $self->fetchHtmlElement($tag, \@type, \$item);
        #print Dumper($resHashRef);
        push @resArr, $resHashRef;
      }
      return \@resArr;
    } else {
      return;
    }
  } else {
    print "Warning: There is no valid tag name in the $htmlAnalyseHashRef!";
    return;
  }
}


sub hasOKTag {
  my $self = shift;
  my $textRef = shift;
  if($$textRef =~ m/<ok\s*\/>/i) {
    #print "ja";
    return 1;
  }
  return 0;
}

no Moose;
__PACKAGE__->meta->make_immutable;

1;

##############################################################
#Test Bereich
##############################################################




#__DATA__
#<entry id="p00416" bd="73" se="541" sp="re" ze="69">| Teilchenfeinheit v. ~e>i. <s>491b.</s> e>i '33, S. 527</entry>

#<entry id="p00001" bd="73" se="523" sp="re" ze="04"><l>Paarung</l>'sspiele a. Artcharaktere. Beobachtgn. an Möven u. Seeschwalben (H. Wachs) <s>507<s> 307<ele type="ignore:verweisverweis"> -- s. a. Gewand</ele><ele type="ignore:ok"/></entry>

#print "HtmlEngine.pm loaded successfully!";








#######################################
# Anmerkung
#######################################
# The output Html-Analyse-Hashmap format
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################



