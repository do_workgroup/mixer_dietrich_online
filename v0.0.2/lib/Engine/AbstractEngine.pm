package Engine::AbstractEngine;
=encoding utf8

=head1 NAME

Engine::AbstractEngine - The abstract class of Engine

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This is abstract superclass for the text analyse engine. 

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose::Role;

use FindBin;
use lib "$FindBin::Bin/../";

=head1 ATTRIBUTES

=head2 targetText

The target text will be parsed.

=cut
has 'targetText' => (
  is => 'rw',
  isa => 'Str',
  predicate => 'hasTargetText',
  reader => "getTargetText",
  writer => "setTargetText",
);

=head1 REQUIRES METHODS

=head2 reset

Resets the class to its initial, empty state.

=cut
requires 'reset';


no Moose;
1;