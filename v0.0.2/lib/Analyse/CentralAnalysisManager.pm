package Analyse::CentralAnalysisManager;
=encoding utf8

=head1 NAME

Analyse::CentralAnalysisManager - Text and data analyse manager, process core as a central manager

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package is for manage analysis of Text data. 

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

# system
use Carp;
use Cwd;
use Data::Dumper;

# string
use Scalar::Util;

# file
use File::Find::Rule;
use File::Spec;
use File::Basename;
use Fcntl;
use Tie::File;

# unicode
use utf8;
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);       
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use v5.20;

## > Global package > ##
use Global::Values;
use Utils::Tools;
use Utils::IO;
## < Global package < ##

## > Database package > ##
use Database::MotDB; 
use Database::MotDBProcessor;
## < Database package < ##

## > Text package > ##
use Text::EntryText;
use Text::EntryPartText;
## < Text package < ##

## > Coordinate package > ##
use Coordinate::FieldPosition;
use Coordinate::TextIndex;
## < Coordinate package < ##

## > Module package > ##
use Module::ModuleUnits; 
#use Module::ModuleUnitAnalyzer;
use Module::ModuleUnitFactory;
## < Module package < ##

## > Analyse package > ##
use Analyse::AnalyseContentStrategies;
use Analyse::AnalyseUnitStrategies;
use Analyse::AnalyseRuleEngineStrategies;
use Analyse::AnalyseStructureStrategies;
## < Analyse package < ##

=head1 CONSTRUCTORS

=head2 BUILD

After new.

=cut
sub BUILD {
  my $self = shift;
#  $self->contentStrategyContext(Analyse::ContentStrategyContext->getInstance);
#  $self->contentStrategyContext(Analyse::ContentStrategyContext->getInstance);
}

=head2 _build_contentstrategycontext

Singleton

=cut
sub _build_contentstrategycontext {
  return Analyse::ContentStrategyContext->getInstance;
}
=head2 _build_unitstrategycontext

Singleton

=cut
sub _build_unitstrategycontext {
  return Analyse::UnitStrategyContext->getInstance;
}
=head2 _build_rulestrategyselector

Singleton

=cut
sub _build_rulestrategyselector {
  return Analyse::RuleEngineStrategySelector->getInstance;
}
=head2 _build_structstrategyselector

Singleton

=cut
sub _build_structstrategyselector {
  return Analyse::StructureStrategySelector->getInstance;
}

=head1 ATTRIBUTES

=head2 curContentStrategy

the current Analyse::AnalyseContentStrategyApi

=cut
has 'curContentStrategy' => (
  is => 'rw',
  isa => 'Analyse::AnalyseContentStrategyApi',
  predicate => 'hasContentStrategy',
  clearer => 'clearContentStrategy',
  writer => 'setContentStrategy',
  reader => 'getContentStrategy',
);
=head2 contentStrategyContext

the current Analyse::ContentStrategyContext

=cut
has 'contentStrategyContext' => (
  is => 'rw',
  isa => 'Analyse::ContentStrategyContext',
  predicate => 'hasContentStrategyContext',
  builder => '_build_contentstrategycontext',
  lazy => 1,
  reader => 'getContentStratContext',
);

=head2 curUnitStrategy

the current Analyse::UnitStrategyApi

=cut
has 'curUnitStrategy' => (
  is => 'rw',
  isa => 'Analyse::UnitStrategyApi',
  predicate => 'hasUnitStrategy',
  clearer => 'clearUnitStrategy',
  writer => 'setUnitStrategy',
  reader => 'getUnitStrategy',
);

=head2 unitStrategyContext

the current Analyse::UnitStrategyContext

=cut
has 'unitStrategyContext' => (
  is => 'rw',
  isa => 'Analyse::UnitStrategyContext',
  predicate => 'hasUnitStrategyContext',
  builder => '_build_unitstrategycontext',
  lazy => 1,
  reader => 'getUnitStratContext',
);

=head2 curRuleStrategy

the current Analyse::RuleEngineStrategyApi

=cut
has 'curRuleStrategy' => (
  is => 'rw',
  isa => 'Analyse::RuleEngineStrategyApi',
  predicate => 'hasCurRuleStrategy',
  clearer => 'clearCurRuleStrategy',
  writer => 'setCurRuleStrategy',
  reader => 'getCurRuleStrategy',
);

=head2 ruleStrategySelector

the current Analyse::RuleEngineStrategySelector

=cut
has 'ruleStrategySelector' => (
  is => 'rw',
  isa => 'Analyse::RuleEngineStrategySelector',
  predicate => 'hasRuleStrategySelector',
  builder => '_build_rulestrategyselector',
  lazy => 1,
  reader => 'getRuleStrategySelector',
);

=head2 curStructStrategy

the current Analyse::StructureStrategyApi

=cut
has 'curStructStrategy' => (
  is => 'rw',
  isa => 'Analyse::StructureStrategyApi',
  predicate => 'hasCurStructStrategy',
  clearer => 'clearCurStructStrategy',
  writer => 'setCurStructStrategy',
  reader => 'getCurStructStrategy',
);

=head2 structStrategySelector

the current Analyse::StructureStrategySelector

=cut
has 'structStrategySelector' => (
  is => 'rw',
  isa => 'Analyse::StructureStrategySelector',
  predicate => 'hasStructureStrategySelector',
  builder => '_build_structstrategyselector',
  lazy => 1,
  reader => 'getStructStrategySelector',
);

=head1 METHODS

=head2 getInstance

Singleton.

=cut
our $singleton_CentralAnalysisManager;
sub getInstance {
  my $self = shift;
  if ( !defined $singleton_CentralAnalysisManager ) {
    $singleton_CentralAnalysisManager = $self->new;
  }
  return $singleton_CentralAnalysisManager;
}

=head2 checkSeverityByRuleEngine($entryText)

Check the severity of the given $entryText(Text::EntryText), and returns the reference of Text::EntryText Array.

=cut
sub checkSeverityByRuleEngine {
  my $self = shift;
  my $entryText = shift;
  my $stype = "DO_ENTRYTEXT_RULEENGINE_STRATEGY";
  
  my $objArrRef = $self->analyseStatusProcess($entryText, $stype);
  return $objArrRef;
}
=head2 checkIntegrityByRuleEngine($entryText)

Check the Integrity of the given $entryText(Text::EntryText), and returns the reference of Text::EntryText Array.

=cut
sub checkIntegrityByRuleEngine {
  my $self = shift;
  my $entryText = shift;
  my $stype = "ENTRYTEXT_INTEGRITY_RULEENGINE_STRATEGY";
  
  my $objArrRef = $self->analyseStatusProcess($entryText, $stype);
  return $objArrRef;
}

=head2 getD74_Header($entryText)

Returns the reference of result hashmap of D74 Header information

=cut
sub getD74_Header {
  my $self = shift;
  my $entryText = shift;
  
  my $stype = "D74_HEADER_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}

=head2 getStrecke($entryText)

Returns the reference of result hashmap of strecke information

=cut
sub getStrecke {
  my $self = shift;
  my $entryText = shift;
  
  my $stype = "STRECKE_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}

=head2 getRowLineType($textRef)

Returns the reference of result hashmap of row line type analysis

=cut
sub getRowLineType {
  my $self = shift;
  my $textRef = shift;
  my $stype = "ROW_TYPE_ANALYSE_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($textRef, $stype);
  return $resHash;
}

=head2 getRowLineType($textRef)

Returns the reference of result hashmap of unknown parts

=cut
sub getUnknownparts {
  my $self = shift;
  my $textRef = shift;
  my $stype = "GET_UNKNOWNPART_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($textRef, $stype);
  return $resHash;
}

=head2 getRowLineType($textRef)

Returns the reference of result hashmap of startpipe

=cut
sub getStartPipe {
  my $self = shift;
  my $textRef = shift;
  my $stype = "GET_PIPE_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($textRef, $stype);
  return $resHash;
}

=head2 getEntryElement($entryText)

Returns the reference of result hashmap of entry element

=cut
sub getEntryElement {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_HTML_ENTRY_ELEMENT_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}

=head2 getSic($entryText)

Returns the reference of result hashmap of sic element

=cut
sub getSic {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_SIC_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}

=head2 getCorrection($entryText)

Returns the reference of result hashmap of corr element

=cut
sub getCorrection {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_CORR_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}

=head2 getDop($entryText)

Returns the reference of result hashmap of dop element

=cut
sub getDop {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_DOP_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}

=head2 getRep($entryText)

Returns the reference of result hashmap of repetition element

=cut
sub getRep {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_REP_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}

=head2 getDitoEle($entryText)

Returns the reference of result hashmap of dito element

=cut
sub getDitoEle {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_DITO_HTML_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}

=head2 getDitoDesc($entryText)

Returns the reference of result hashmap of dito descendants

=cut
sub getDitoDesc {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_DITO_DESC_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}

=head2 getHomonym($entryText)

Returns the reference of result hashmap of homonym element

=cut
sub getHomonym {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_HOM_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}

=head2 getIgnore($entryText)

Returns the reference of result hashmap of ignore element

=cut
sub getIgnore {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_IGNORE_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}
=head2 getCharErrors($entryText)

Returns the reference of result hashmap of character errors

=cut
sub getCharErrors {
  my $self = shift;
  my $entryText = shift;
  my $stype = "SEARCH_ERROR_CHARS_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}
=head2 getSub($entryText)

Returns the reference of result hashmap of sub element

=cut
sub getSub {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_SUB_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}

=head2 getSup($entryText)

Returns the reference of result hashmap of sup element

=cut
sub getSup {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_SUP_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}
=head2 getLemma($entryText)

Returns the reference of result hashmap of lemma element

=cut
sub getLemma {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_LEMMA_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}
=head2 getEndpart($entryText)

Deprecated

=cut
sub getEndpart {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_ENDPART_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}
=head2 getOkElement($entryText)

Returns the reference of result hashmap of ok element

=cut
sub getOkElement {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_OK_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}

=head2 getEndDopElement($entryText)

Returns the reference of result hashmap of dop element at the end of text

=cut
sub getEndDopElement {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_END_DOP_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}
=head2 getTitle($entryText)

Returns the reference of result hashmap of title area

=cut
sub getTitle {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_TITLE_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}
=head2 getAuthor($entryText)

Returns the reference of result hashmap of author area

=cut
sub getAuthor {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_AUTHOR_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}
=head2 getSigel($entryText)

Returns the reference of result hashmap of sigel element

=cut
sub getSigel {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_SIGEL_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}
=head2 getCollation($entryText)

Returns the reference of result hashmap of collation area. it is mostly at the end of text.

=cut
sub getCollation {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_COLLATION_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}

=head2 getReference($entryText)

Returns the reference of result hashmap of references area. it is mostly at the end of text.

=cut
sub getReference{
  my $self = shift;
  my $entryText = shift;
  
  my $refHtmlResHash = {};
  my $refLemHtmlResHash = {};
  my $refResHash = {};
  my $resHash = {};
  # get html 
  my $reftype = "GET_REFERENCE_HTML_STRATEGY";
  $refHtmlResHash = $self->analyseUnitsProcess($entryText, $reftype);
  # if not take reg
  my $refLemtype = "GET_REFERENCE_LEMMA_HTML_STRATEGY";
  $refLemHtmlResHash = $self->analyseUnitsProcess($entryText, $refLemtype);
  
  if ($refHtmlResHash->{found} && ! $refLemHtmlResHash->{found}) {
    $resHash = $refHtmlResHash;
  } elsif (! $refHtmlResHash->{found} && $refLemHtmlResHash->{found}) {
    $resHash = $refLemHtmlResHash;
  } elsif ($refHtmlResHash->{found} && $refLemHtmlResHash->{found}) {
    #take the first one
    my $refHtmlPos = $refHtmlResHash->{pos}->[0];
    my $refLemHtmlPos = $refLemHtmlResHash->{pos}->[0];
    my $compRes = Utils::Tools->compareIndices($refHtmlPos, $refLemHtmlPos);
    if ($compRes == -1) {
      $resHash = $refHtmlResHash;
    } else  {
      $resHash = $refLemHtmlResHash;
    }
  } elsif (! $refHtmlResHash->{found} && ! $refLemHtmlResHash->{found}) {
    my $stype = "GET_REFERENCE_REG_STRATEGY";
    $resHash = $self->analyseUnitsProcess($entryText, $stype);
  }
  return $resHash;
}

=head2 getReferenceHtml($entryText)

Returns the reference of result hashmap of references html element. 

=cut
sub getReferenceHtml {
  my $self = shift;
  my $entryText = shift;
  
  # get html 
  my $stype = "GET_REFERENCE_HTML_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
    return $resHash;
}

=head2 getReferenceLemmaHtml($entryText)

Returns the reference of result hashmap of references lemma html element. 

=cut
sub getReferenceLemmaHtml {
  my $self = shift;
  my $entryText = shift;
  my $stype = "GET_REFERENCE_LEMMA_HTML_STRATEGY";
  my $resHash = $self->analyseUnitsProcess($entryText, $stype);
  return $resHash;
}

=head2 compareEntries($preEntryText, $curEntryText)

Compare the two given Text::EntryText, and complement the current EntryText. 

=cut
sub compareEntries {
  my $self = shift;
  my $preEntryText = shift;
  my $curEntryText = shift;
  my $stype = "ENTRIES_COMPARISON_STRATEGY";
  my $inputArrRef = [$preEntryText, $curEntryText];
  my $newCurEntryText = $self->analyseStructAnalyseProcess($inputArrRef, $stype);
  return $newCurEntryText;
}

###########################################################
#CORE:
###########################################################
=head2 analyseStructAnalyseProcess($inputArrRef, $strategyType)

Main process to analyse structure of EntryTexts.

=head3 Paramters:

=over 4

=item *

$inputArrRef: the given reference of Text::EntryText array

=item *

$strategyType: the given strategy type

=back

=head3 Returns:

the reference of Text::Entry Array.

=cut
sub analyseStructAnalyseProcess {
  my $self = shift;
  my $inputArrRef = shift;
  my $strategyType = shift;
  
  if (! defined $inputArrRef || $inputArrRef eq "" 
  || ! defined $strategyType || $strategyType eq "") {
    croak "There is no valid input parameters!";
  }
  my $structStrategySelector = $self->getStructStrategySelector;
  my $curStructStrategy = $structStrategySelector->getStructStrategy($strategyType);
  
  $curStructStrategy->input($inputArrRef);
  $curStructStrategy->analyse;
  my $objArrRef = $curStructStrategy->output;
  
  $self->setCurStructStrategy($curStructStrategy);
  
  return $objArrRef;
}

=head2 analyseStatusProcess($entryText, $strategyType)

Main process to analyse $entryText(Text::EntryText) by business rule engine

=head3 Paramters:

=over 4

=item *

$entryText: the given Text::EntryText object

=item *

$strategyType: the given strategy type

=back

=head3 Returns:

the reference of Text::Entry Array.

=cut
sub analyseStatusProcess {
  my $self = shift;
  my $entryText = shift;
  my $strategyType = shift;
  
  if (! defined $entryText || $entryText eq "" || ! defined $strategyType || $strategyType eq "") {
    croak "There is no valid input parameters!";
  }
  my $ruleSelector = $self->getRuleStrategySelector;
  
  my $curRuleStrategy = $ruleSelector->getRuleEngineStrategy($strategyType);
  
  $curRuleStrategy->input($entryText);
  $curRuleStrategy->handle;
  my $objArrRef = $curRuleStrategy->output;
  
  $self->setCurRuleStrategy($curRuleStrategy);
  
  return $objArrRef;
}

=head2 analyseUnitsProcess($input, $strategyType)

Main process to analyse the given input text or Text::EntryText (main core).

=head3 Paramters:

=over 4

=item *

$input: the given string text or Text::EntryText object

=item *

$strategyType: the given strategy type

=back

=head3 Returns:

the reference of result hashmap. 

=cut
sub analyseUnitsProcess {
  my $self = shift;
  my $input = shift;
  my $strategyType = shift;
  
  if (! defined $input || $input eq "" || ! defined $strategyType || $strategyType eq "") {
    croak "There is no valid input parameters!";
  }
  
  #1. analyse content
  my $contentStratContext = $self->getContentStratContext;
  my $curContentStrategy = $contentStratContext->getContentStrategy($strategyType);
  # set current content strategy
  $curContentStrategy->input($input);
  # just do it! ^1^
  $curContentStrategy->execute;
  # output
  my $tables = $curContentStrategy->output;
  #set current unit strategy
  $self->setContentStrategy($curContentStrategy);
  
  # 2. analyse result unit hash
  my $unitStratContext = $self->getUnitStratContext;
  my $curUnitStrategy = $unitStratContext->getUnitStrategy($strategyType);
  if (Utils::Tools->isBlessed($input, "Text::EntryText")) {
    $curUnitStrategy->setCurEntryText($input);
  }
  # set input
  $curUnitStrategy->input($tables);
  $curUnitStrategy->analyse;
  #output
  my $resHash = $curUnitStrategy->output;
  # set current unit strategy
  $self->setUnitStrategy($curUnitStrategy);
  return $resHash;
  
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;
