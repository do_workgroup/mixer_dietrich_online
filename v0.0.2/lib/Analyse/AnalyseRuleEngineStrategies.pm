package Analyse::AnalyseRuleEngineStrategies;
=encoding utf8

=head1 NAME

Analyse::AnalyseRuleEngineStrategies - The strategies for business rule engine.

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package is to select business rule engines for text analysis and its object. 
It is based on strategy pattern mode and command pattern mode.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS: 
package Analyse::RuleEngineStrategyApi;
=encoding utf8

=head1 NAME

Analyse::RuleEngineStrategyApi - The abstract class of strategy to use business rule engine

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This is abstract superclass for using rule engine to analyse text and text-object. Standard behaviors like the 

getType, clear, handle, input and output methods are defined here, the programmer only needs to extend

this class and provide implementations for the methods. 

The non-abstract methods have their descriptions with its implementation in detail.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut

use Moose::Role;

use FindBin;
use lib "$FindBin::Bin/../";
use Carp;
use Global::Values;
use Database::MotDBProcessor;

=head1 ATTRIBUTES

=head2 inputObjects

The text-object array waiting for analysis by rule engine.

=cut
has 'inputObjects' => (
  traits  => ["Array"],
  is      => 'rw',
  isa     => 'ArrayRef[Object]',
  default => sub { [] },
  handles => {
    allInputs        => 'elements',
    addToInputs      => 'push',
    filterInputs     => 'grep',
    findItemInInput  => 'first',
    getInputsByIndex => 'get',
    countInput       => 'count',
    isInputEmpty     => 'is_empty',
    sortedInputByCode=> 'sort',
    resetInput        => 'clear',
  },
  lazy => 1,
);
=head2 inputObjects

The text-object array for the output.

=cut
has 'outputObjects' => (
  traits  => ["Array"],
  is      => 'rw',
  isa     => 'ArrayRef[Object]',
  default => sub { [] },
  handles => {
    allOutputs        => 'elements',
    addToOutputs      => 'push',
    filterOutputs     => 'grep',
    findItemInOutput  => 'first',
    getOutputsByIndex => 'get',
    countOutput       => 'count',
    isOutputEmpty     => 'is_empty',
    sortedOutputByCode=> 'sort',
    resetOutput        => 'clear',
  },
  lazy => 1,
);

=head1 REQUIRES METHODS

=head2 getRuleEngineStrategyType

Returns the type of the strategy

=cut
requires 'getRuleEngineStrategyType';

=head2 handle

Immediately performs the analysis of the task in the class.

=cut
requires 'handle';

=head2 input

Adds the specified values to the class 

=cut
requires 'input';

=head2 output

Returns the result for the output

=cut
requires 'output';

=head2 clear

Resets the class to its initial, empty state.

=cut
requires 'clear';

no Moose;
1;




#CLASS:

package Analyse::DOEntryTextRuleStrategy;
=encoding utf8

=head1 NAME

Analyse::DOEntryTextRuleStrategy - Strategy of rule engine to analyse the text::EntryText

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut

use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::RuleEngineStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

use Algorithms::DORuleEngineClass;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];


=head1 ATTRIBUTES

=head2 type

Type of the class (ANALYSE_STRATEGY_TYPE: Global::Values->doAnalyseStrategyType)

=cut
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'DO_ENTRYTEXT_RULEENGINE_STRATEGY',
);


=head1 METHODS

=head2 getRuleEngineStrategyType()

=head3 Overrides:

getRuleEngineStrategyType in class Analyse::RuleEngineStrategyApi

=cut
sub getRuleEngineStrategyType {
  my $self = shift;
  return $self->type;
}

=head2 clear()

=head3 Overrides:

clear in class Analyse::RuleEngineStrategyApi

=cut
sub clear {
  my $self = shift;
  $self->resetInput; 
  $self->resetOutput; 
}

=head2 input($input)

The reference of text-object array will be input

=head3 Overrides:

input in class Analyse::RuleEngineStrategyApi

=cut
sub input {
  my $self = shift;
  my $input = shift;
  if (! defined $input || $input eq "" ) {
    croak "There is nothing to input!";
  }
  if (ref($input) eq "ARRAY" && scalar @$input) {
    $self->addToInputs(@$input);
  } else {
    $self->addToInputs($input);
  }
  return 1;
}

=head2 output()

=head3 Overrides:

output in class Analyse::RuleEngineStrategyApi

=cut
sub output {
  my $self = shift;
  my @allObjs = $self->allOutputs;
  return \@allObjs unless $self->isOutputEmpty;
}

=head2 handle()

=head3 Overrides:

handle in class Analyse::RuleEngineStrategyApi

=cut
sub handle {
  my $self = shift;
  
  # 1. set the severity baseline
  my $sess = Algorithms::DORuleEngineSession->new;
  $sess->set_environment('severity', -1);
  
  my $ruleSetName = "doSeverity_Ruleset_1";
  
  my $ruleSet = $self->getSeverityRuleSet($ruleSetName);
  $ruleSet->sortRuleByRank("incr");
  # add it to the session
  $sess->add_ruleset($ruleSet->name, $ruleSet);

  # 2. all objects
  my @objArr = $self->allInputs;
  
  my $results = $sess->execute($ruleSetName, @objArr);
  
  $self->addToOutputs(@$results);
  return 1;
}

=head2 getSeverityRuleSet($ruleSetName)

=head3 Parameters:

=over 4

=item *

$ruleSetName: the given given name of this rule set

=back

=head3 Returns:

the rule set with the given name and ranks

=head3 See Also:

Algorithms::DORuleEngineRuleSet,

=cut
sub getSeverityRuleSet {
  my $self = shift;
  my $ruleSetName = shift;
  
  # make a severity_text rule set
  my $ruleset = Algorithms::DORuleEngineRuleSet->new(
    name => $ruleSetName,
    filter => Algorithms::DORuleEngineFilter->new(
      condition => sub {
        my ($self, $sess, $obj) = @_;
        #print "Filter: self: $self, sess: $sess, obj: $obj! \n";
        if ($obj->getSeverity > $sess->get_environment("severity")) {
          return 1;
        } else {
          return 0;
        }
      },
    ), 
  );
  # rule ok
  my $rule_ok = Algorithms::DORuleEngineRule->new(
    name => "hasOK",
    rank => 100,
    condition => sub {
      my ($self, $sess, $obj) = @_;
      if ($obj->getEntryTextAttr("hasOK")) {
        return 1;
      } else {
        return 0;
      }
    },
    action => sub {
      my ($self, $sess, $obj) =@_;
      #Attention: Wild cart FOR TEST
      $obj->setSeverity(4);
      $obj->addToProblemReports("rpt_ok_1");
    },
  );
  $ruleset->add_rule($rule_ok);
  
  
  # rule unknown entry
  my $rule_unknown_entry = Algorithms::DORuleEngineRule->new(
    name => "unknown_entry",
    rank => 90,
    condition => sub {
      my ($self, $sess, $obj) = @_;
      #has entry text error?
      if(Global::Values->doSetGroups->{ErrorEntryText}->has($obj->getEntryType)) {
        return 1;
      } else {
        return 0;
      }
    },
    action => sub {
      my ($self, $sess, $obj) =@_;
      $obj->setSeverity(6);
      $obj->addToProblemReports("rpt_otherline_1");
    },
  );
  $ruleset->add_rule($rule_unknown_entry);
  
  # rule special_entry
  my $rule_special_entry = Algorithms::DORuleEngineRule->new(
    name => "special_entry",
    rank => 80,
    condition => sub {
      my ($self, $sess, $obj) = @_;
      #special entry text i.e. ENTRY_TYPE_SEPARATOR ENTRY_TYPE_STRECKE
      if(Global::Values->doSetGroups->{SpecialEntryText}->has($obj->getEntryType)) {
        return 1;
      } else {
        return 0;
      }
    },
    action => sub {
      my ($self, $sess, $obj) =@_;
      $obj->setSeverity(3);
      $obj->addToProblemReports("rpt_special_entry_1");
    },
  );
  $ruleset->add_rule($rule_special_entry);
  
  # rule unknown and errors
  my $rule_unknown_errors = Algorithms::DORuleEngineRule->new(
    name => "hasUnknownpart",
    rank => 70,
    condition => sub {
      my ($self, $sess, $obj) = @_;
      if ($obj->getEntryTextAttr("hasUnknownparts")) {
        return 1;
      } else {
        return 0;
      }
    },
    action => sub {
      my ($self, $sess, $obj) =@_;
      $obj->setMaxSeverity(6);
      $obj->addToProblemReports("rpt_unknown_part_1");
    },
  );
  $ruleset->add_rule($rule_unknown_errors);
  
  # rule lemma 
  my $rule_lemma_area = Algorithms::DORuleEngineRule->new(
    name => "hasLemmaArea",
    rank => 50,
    condition => sub {
      my ($self, $sess, $obj) = @_;
      # if Lemma-entry has no lemma area
      if (Global::Values->doSetGroups->{LemmaEntryText}->has($obj->getEntryType) 
        && ! $obj->getEntryTextAttr("hasLemma")
      ) {
        return 1;
      } else {
        return 0;
      }
    },
    action => sub {
      my ($self, $sess, $obj) =@_;
      $obj->setMaxSeverity(5);
      $obj->addToProblemReports("rpt_lemma_1");
    },
  );
  $ruleset->add_rule($rule_lemma_area);
  
  #CONSIDER: rule verweis, verweis lemma
#  my $rule_verweis = Algorithms::DORuleEngineRule->new(
#    name => "entry_reference",
#    rank => 60,
#    condition => sub {
#      my ($self, $sess, $obj) = @_;
#      #special entry text
#      if(($obj->getEntryTextAttr("hasReference") || $obj->getEntryTextAttr("hasReferenceLemma")) 
#      && ($obj->getSeverity > 3)
#      ) {
#        return 1;
#      } else {
#        return 0;
#      }
#    },
#    action => sub {
#      my ($self, $sess, $obj) =@_;
#      $obj->setSeverity(4);
#      $obj->addToProblemReports("rpt_verweis_6");
#    },
#  );
#  $ruleset->add_rule($rule_verweis);
  
  # rule char errors
  my $rule_charErrors = Algorithms::DORuleEngineRule->new(
    name => "hasCharErrors",
    rank => 50,
    condition => sub {
      my ($self, $sess, $obj) = @_;
      if ($obj->getEntryTextAttr("hasCharErrors")) {
        return 1;
      } else {
        return 0;
      }
    },
    action => sub {
      my ($self, $sess, $obj) =@_;
      $obj->setMaxSeverity(6);
      # add it to the problem report
      $obj->addToProblemReports("rpt_char_6");
    },
  );
  $ruleset->add_rule($rule_charErrors);
  
  # rule collation
  my $rule_collation = Algorithms::DORuleEngineRule->new(
    name => "hasCollation",
    rank => 50,
    condition => sub {
      my ($self, $sess, $obj) = @_;
      if ($obj->getEntryTextAttr("hasCollation")) {
        return 0;
      } else {
        # no valid collation 
        return 1;
      }
    },
    action => sub {
      my ($self, $sess, $obj) =@_;
      $obj->setMaxSeverity(5);
      $obj->addToProblemReports("rpt_collation_2");
    },
  );
  $ruleset->add_rule($rule_collation);
  
  # rule sigel
  my $rule_sigel = Algorithms::DORuleEngineRule->new(
    name => "hasSigel",
    rank => 50,
    condition => sub {
      my ($self, $sess, $obj) = @_;
      #print "Rule->condition: self: $self, sess: $sess, obj: $obj! \n";
      # there is no sigel in the lemma entry
      if ($obj->getEntryTextAttr("hasSigel") eq 0 
        && (Global::Values->doSetGroups->{LemmaEntryText}->has($obj->getEntryType)) 
      ){
        return 1;
      } else {
        return 0;
      }
    },
    action => sub {
      my ($self, $sess, $obj) =@_;
      $obj->setMaxSeverity(5);
      $obj->addToProblemReports("rpt_sigel_2");
    },
  );
  $ruleset->add_rule($rule_sigel);
  
  # rule more sigel, lemma, entry, author
  my $rule_moreparts = Algorithms::DORuleEngineRule->new(
    name => "hasMoreParts",
    rank => 50,
    condition => sub {
      my ($self, $sess, $obj) = @_;
      #print "Rule->condition: self: $self, sess: $sess, obj: $obj! \n";
      
      if ($obj->getEntryTextAttr("hasMoreSigel") || $obj->getEntryTextAttr("hasMoreAuthor")
      ||  $obj->getEntryTextAttr("hasMoreEntry") ) 
      {
        return 1;
      } else {
        return 0;
      }
    },
    action => sub {
      my ($self, $sess, $obj) =@_;
      $obj->setMaxSeverity(5);
      $obj->addToProblemReports("rpt_moreparts_1");
    },
  );
  $ruleset->add_rule($rule_moreparts);
  
  return $ruleset;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::DOEntryIntegrityRuleStrategy;
###########################################
package Analyse::DOEntryIntegrityRuleStrategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::RuleEngineStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

use Algorithms::DORuleEngineClass;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'ENTRYTEXT_INTEGRITY_RULEENGINE_STRATEGY',
);


########################
# methods
########################
# implement
sub getRuleEngineStrategyType {
  my $self = shift;
  return $self->type;
}

sub clear {
  my $self = shift;
  $self->resetInput; 
  $self->resetOutput; 
}

sub input {
  my $self = shift;
  my $input = shift;
  
  if (! defined $input || $input eq "" ) {
    croak "There is nothing to input!";
  }
  if (ref($input) eq "ARRAY" && scalar @$input) {
    $self->addToInputs(@$input);
  } else {
    $self->addToInputs($input);
  }
  return 1;
}

sub output {
  my $self = shift;
  my @allObjs = $self->allOutputs;
  return \@allObjs unless $self->isOutputEmpty;
}

sub handle {
  my $self = shift;
  
  # 1. set the severity baseline
  my $sess = Algorithms::DORuleEngineSession->new;
  $sess->set_environment('severity', -1);
  
  my $ruleSetName = "integrity_Ruleset";
  
  my $ruleSet = $self->getIntegrityRuleSet($ruleSetName);
  $ruleSet->sortRuleByRank("incr");
  # add it to the session
  $sess->add_ruleset($ruleSet->name, $ruleSet);
  # 2. all objects
  my @objArr = $self->allInputs;
  
  my $results = $sess->execute($ruleSetName, @objArr);
  
  $self->addToOutputs(@$results);
  return 1;
}

sub getIntegrityRuleSet {
  my $self = shift;
  my $ruleSetName = shift;
  
  # make a severity_text rule set
  my $ruleset = Algorithms::DORuleEngineRuleSet->new(
    name => $ruleSetName,
    filter => Algorithms::DORuleEngineFilter->new(
      condition => sub {
        my ($self, $sess, $obj) = @_;
        #print "Filter: self: $self, sess: $sess, obj: $obj! \n";

        if ($obj->getSeverity > $sess->get_environment("severity")) {
          return 1;
        } else {
          return 0;
        }
      },
    ), 
  );
  # rule header form
  my $rule_header_form = Algorithms::DORuleEngineRule->new(
    name => "inheritHeaderInfo",
    rank => 50,
    condition => sub {
      my ($self, $sess, $obj) = @_;
      #print "Rule->condition: self: $self, sess: $sess, obj: $obj! \n";
      my $res = 0; # default is no problem
      # entry text
      if (Global::Values->doSetGroups->{EntryText}->intersection(
      Global::Values->doSetGroups->{D74EntryText})->has($obj->getEntryType)) {
        # it could not have a header, or it must be added the header info
        if (! $obj->getEntryTextAttr("hasHeader") && ! $obj->getEntryTextAttr("inheritHeaderInfo")) {
          $res = 1;
        }
      }
      return $res;
    },
    action => sub {
      my ($self, $sess, $obj) =@_;
      $obj->setSeverity(5);
      $obj->addToProblemReports("rpt_header_7");
    },
  );
  $ruleset->add_rule($rule_header_form);
  
  # rule entry lemma
  my $rule_entry_text_lemma = Algorithms::DORuleEngineRule->new(
    name => "inheritLemma",
    rank => 50,
    condition => sub {
      my ($self, $sess, $obj) = @_;
      my $res = 0;
      # entry text
      if (Global::Values->doSetGroups->{EntryText}->has($obj->getEntryType)) {
        # if entry text has no lemma and no inherited lemma
        if (! $obj->getEntryTextAttr("hasLemma") && ! $obj->getEntryTextAttr("inheritLemma")) {
          $res = 1;
        }
      }
      return $res;
    },
    action => sub {
      my ($self, $sess, $obj) =@_;
      
      $obj->setMaxSeverity(5);
      $obj->addToProblemReports("rpt_lemma_9");
    },
  );
  $ruleset->add_rule($rule_entry_text_lemma);
  
  # rule sigel area
  my $rule_check_sigel = Algorithms::DORuleEngineRule->new(
    name => "MustHavesigel",
    rank => 50,
    condition => sub {
      my ($self, $sess, $obj) = @_;
      my $res = 0;
      # if the entry text has no sigel and no inherited sigel
      if (! $obj->getEntryTextAttr("hasSigel") && ! $obj->getEntryTextAttr("inheritSigel")) {
        $res = 1;
      }
      return $res;
    },
    action => sub {
      my ($self, $sess, $obj) =@_;
      $obj->setMaxSeverity(5);
      # add it to the problem report
      $obj->addToProblemReports("rpt_sigel_14");
    },
  );
  $ruleset->add_rule($rule_check_sigel);
  
  # rule dito inherited
  my $rule_dito_pair = Algorithms::DORuleEngineRule->new(
    name => "dito_pair",
    rank => 50,
    condition => sub {
      my ($self, $sess, $obj) = @_;
      #print "Rule->condition: self: $self, sess: $sess, obj: $obj! \n";
      my $res = 0;
      # if the entry text has dito descendant area as 'do.', but it has no dito element inherited from the previous entry text
      if ($obj->getEntryTextAttr("hasDitoDesc") && ! $obj->getEntryTextAttr("inheritDitoEle") ) {
        $res = 1;
      }
      return $res;
    },
    action => sub {
      my ($self, $sess, $obj) =@_;
      $obj->setMaxSeverity(5);
      $obj->addToProblemReports("rpt_dito_3");
    },
  );
  $ruleset->add_rule($rule_dito_pair);
  
  return $ruleset;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


################### Engine, App, Context#######################
#class:
package Analyse::RuleEngineStrategySelectorEngine;
=encoding utf8

=head1 NAME

Analyse::RuleEngineStrategySelectorEngine - an engine to store and control the strategies of business rule engine

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;
use FindBin;
use lib "$FindBin::Bin/../";
use Carp;
use Analyse::RuleEngineStrategyApi;

use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];

=head1 ATTRIBUTES

=head2 ruleEngineStrategyMap

Strategy Map to store the strategies with the type of strategy.

key: type of strategy  - value: Strategy (object)

=cut
has 'ruleEngineStrategyMap' => (
  is        => 'rw',
  isa       => 'HashRef[Analyse::RuleEngineStrategyApi]',
  predicate => 'hasRuleEngineStrategyMap',
  clearer   => 'resetRuleEngineStrategyMap',
  default   => sub { {} },
  lazy      => 1,
);

=head1 METHODS

=head2 addRuleEngineStrategy($strat)

Adds a given strategy ($strat) to the contentStrategyMap.

=head3 Parameters:

=over 4

=item *

$strat: the given strategy, it extends from Analyse::AnalyseContentStrategyApi

=back

=cut
sub addRuleEngineStrategy {
  my $self  = shift;
  my $strat = shift;
  #
  my $type = $strat->getRuleEngineStrategyType;
  # put strategy in the map
  $self->ruleEngineStrategyMap->{$type} = $strat;
}

=head2 getRuleEngineStrategy($stratType)

=head3 Parameters:

=over 4

=item *

$stratType: the given type of strategy, see also Global::Values->doAnalyseStrategyType

=back

=head3 Returns:

The specified Object Strategy

=cut
sub getRuleEngineStrategy {
  my $self      = shift;
  my $stratType = shift;
  my $strategy  = $self->ruleEngineStrategyMap->{$stratType};
  # make sure old data is clear
  $strategy->clear;
  return $strategy;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
package Analyse::RuleEngineStrategySelector;
=encoding utf8

=head1 NAME

Analyse::RuleEngineStrategySelector - a controller of the rule engine strategies and rule engine strategy engine

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;
use FindBin;
use lib "$FindBin::Bin/../";
use Carp;
use Global::Values;
use Database::MotDB;
use Analyse::RuleEngineStrategySelectorEngine;
use Analyse::RuleEngineStrategyApi;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];

=head1 CONSTRUCTORS

=head2 BUILD

After new.

=cut
sub BUILD {
  my $self = shift;
  # add build in strategies
  $self->addBuildInStrategies;
}

=head2 _build_selectorengine

=cut
sub _build_selectorengine {
  my $g = Analyse::RuleEngineStrategySelectorEngine->new;
  return $g;
}

=head1 ATTRIBUTES

=head2 selectorEngine

Analyse::RuleEngineStrategySelectorEngine

=cut
has 'selectorEngine' => (
  is      => 'rw',
  isa     => 'Analyse::RuleEngineStrategySelectorEngine',
  builder => "_build_selectorengine",
  handles => [qw( addRuleEngineStrategy getRuleEngineStrategy)],
);

=head1 METHODS

=head2 getInstance

Singleton.

=cut
my $singleton_RuleEngineStrategySelector;
sub getInstance {
  my $self = shift;
  if ( !defined $singleton_RuleEngineStrategySelector ) {
    $singleton_RuleEngineStrategySelector = $self->new;
  }
  return $singleton_RuleEngineStrategySelector;
}

=head2 addBuildInStrategies

Adds the strategies to the main program

=cut
sub addBuildInStrategies {
  my $self = shift;
  $self->addRuleEngineStrategy(Analyse::DOEntryTextRuleStrategy->new); 
  $self->addRuleEngineStrategy(Analyse::DOEntryIntegrityRuleStrategy->new); 

}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

