package Analyse::AnalyseStructureStrategies;
=encoding utf8

=head1 NAME

Analyse::AnalyseStructureStrategies - The strategies for analysing the structure of text, text-object.

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package is for data structure analysis. It is based on strategy pattern mode and command pattern mode.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;

use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use utf8;
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);       
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use 5.020;

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS: 
package Analyse::StructureStrategyApi;
=encoding utf8

=head1 NAME

Analyse::StructureStrategyApi - The abstract class of strategy to analyse the structure of text or text-object

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This is abstract superclass for the text content analysis. Standard behaviors like the 

getType, clear, analyse, input and output methods are defined here, the programmer only needs to extend

this class and provide implementations for the methods. 

The non-abstract methods have their descriptions with its implementation in detail.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose::Role;
use FindBin;
use lib "$FindBin::Bin/../";

use utf8;
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);       
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use 5.020;

use Carp;
use Data::Dumper;

use Global::Values;
use Database::MotDBProcessor;


=head1 ATTRIBUTES

=head2 curEntryText

The current Text::EntryText

=begin text

=cut
has 'curEntryText' => (
  is => 'rw',
  isa => 'Text::EntryText | Undef',
  predicate => 'hasCurEntryText',
  clearer => 'clearCurEntryText',
  reader => "getCurEntryText",
  writer => "setCurEntryText",
  default => sub {undef},
  lazy => 1,
);

=head2 preEntryText

The previous Text::EntryText

=begin text

=cut
has 'preEntryText' => (
  is => 'rw',
  isa => 'Text::EntryText | Undef',
  predicate => 'hasPreEntryText',
  clearer => 'clearPreEntryText',
  reader => "getPreEntryText",
  writer => "setPreEntryText",
  default => sub {undef},
  lazy => 1,
);

=head1 REQUIRES METHODS

=head2 getStrategyType

Returns the subclass type

=cut
requires 'getStrategyType';
=head2 analyse

Immediately performs the analysis of the task in the class.

=cut
requires 'analyse';
=head2 input

Adds the specified values to the class

=cut
requires 'input';
=head2 output

Returns the result for the output

=cut
requires 'output';
=head2 clear

Resets the class to its initial, empty state.

=cut
requires 'clear';

no Moose;
1;





#CLASS:
package Analyse::EntriesComparisonStrategy;
=encoding utf8

=head1 NAME

Analyse::EntriesComparisonStrategy - Strategy to compare the structures of current EntryText 
and previous EntryText

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";
use Clone;
use Carp;
use Data::Dumper;

with 'Analyse::StructureStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];

=head1 ATTRIBUTES

=head2 type

Type of the class (ANALYSE_STRATEGY_TYPE: Global::Values->doAnalyseStrategyType)

=cut
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'ENTRIES_COMPARISON_STRATEGY',
);

=head1 METHODS

=head2 getStrategyType()

=head3 Overrides:

getStrategyType in class Analyse::StructureStrategyApi

=cut
sub getStrategyType {
  my $self = shift;
  return $self->type;
}

=head2 clear()

=head3 Overrides:

clear in class Analyse::StructureStrategyApi

=cut
sub clear {
  my $self = shift;
  $self->clearPreEntryText;
  $self->clearCurEntryText;
}

=head2 input($inputArr)

The Reference of Text::EntryText Array will be input

=head3 Overrides:

input in class Analyse::StructureStrategyApi

=cut
sub input {
  my $self    = shift;
  my $inputArr = shift;
  my $preEntryText = $inputArr->[0];
  my $curEntryText = $inputArr->[1];
  $self->setPreEntryText($preEntryText);
  $self->setCurEntryText($curEntryText);
}
=head2 output()

=head3 Overrides:

output in class Analyse::StructureStrategyApi

=head3 Returns:

The Reference of Text::EntryText Array

=cut
sub output {
  my $self = shift;
  my $entryText = $self->getCurEntryText;
  my $arr = [$entryText];
  return $arr;
}

=head2 analyse()

=head3 Overrides:

analyse in class Analyse::StructureStrategyApi

=cut
sub analyse {
  my $self = shift;
  # get it from input
  my $preEText = $self->getPreEntryText;
  my $curEText = $self->getCurEntryText;
  
  # run core
  my $curEntryText = $self->getMissingParts($preEText, $curEText);
  
  # set it for the current entry text
  $self->setCurEntryText($curEntryText);
}

#TODO:
sub analyseLemmaTitle {
  my $self = shift;
  
}

=head2 getMissingParts($preEntryText, $curEntryText)

Compare the $preEntryText, $curEntryText, and according to some rules complement the
possibly missing parts of current EntryText.

=head3 Paramters:

=over 4

=item *

$preEntryText: the given previous Text::EntryText

=item *

$curEntryText: the given current Text::EntryText, it will be compared with $preEntryText

=back

=head3 Returns:

The Text::EntryText

=cut
sub getMissingParts{
  my $self = shift;
  my $preEntryText = shift;
  my $curEntryText = shift;
  
  # 0. the entry type
  my $curEntryType = $curEntryText->getEntryType;
  my $checkHash = {};
  #1. headerinfo
  #2. lemma
  #3. sigel
  #4. dito_descendant -> dito_element
  
  # entry 
  if (Global::Values->doSetGroups->{EntryText}->has($curEntryType) ) {
    # for the information of d74 entry
    if (Global::Values->doSetGroups->{EntryText}->intersection(
      Global::Values->doSetGroups->{D74EntryText})->has($curEntryType)
    ) {
      #CONSIDER:
      #my $preHeaderinfo = Clone::clone($preEntryText->getHeaderInfo);
      my $preHeaderinfo = $preEntryText->getHeaderInfo;
      $curEntryText->setHeaderInfo($preHeaderinfo);
      $curEntryText->setEntryTextAttr("inheritHeaderInfo", 1);
    }
    my $lemmaPartTextArrRef = $preEntryText->getPartTextsFromMap("LEMMA_ELEMENT");
    $curEntryText->addPartsToMap($lemmaPartTextArrRef);
    $curEntryText->setEntryTextAttr("inheritLemma", 1);
  }
  
  # sigel 
  if (! $curEntryText->getEntryTextAttr("hasSigel") 
    &&  Global::Values->doSetGroups->{EntryText}->has($curEntryType)) {
    # take the first one
    my $sigelEleArrRef = $preEntryText->getPartTextsFromMap("SIGEL_ELEMENT");
    if (defined $sigelEleArrRef && $sigelEleArrRef ne "" && ref($sigelEleArrRef) eq "ARRAY") {
      my $sigelPartText = $sigelEleArrRef->[0];
      $curEntryText->addPartTextToMap($sigelPartText);
      $curEntryText->setEntryTextAttr("inheritSigel", 1);
    }
  }
  
  # dito
  if ($curEntryText->getEntryTextAttr("hasDitoDesc")) {
    # take the first one
    my $ditoEleArrRef = $preEntryText->getPartTextsFromMap("DITO_ELEMENT");
    if (defined $ditoEleArrRef && $ditoEleArrRef ne "" && ref($ditoEleArrRef) eq "ARRAY") {
      my $doElePartText = $ditoEleArrRef->[0];
      $curEntryText->addPartTextToMap($doElePartText);
      $curEntryText->setEntryTextAttr("inheritDitoEle", 1);
    }
  }
  return $curEntryText;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;






################### Engine, App, Context#######################
#class:
package Analyse::StructStrategySelectorEngine;
=encoding utf8

=head1 NAME

Analyse::StructStrategySelectorEngine - an engine to store and control the strategies for analysing text structure

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

use Analyse::StructureStrategyApi;
use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];


=head1 ATTRIBUTES

=head2 structStrategyMap

Strategy Map to store the strategies with the type of strategy.

key: type of strategy  - value: strategy (object)

=cut
has 'structStrategyMap' => (
  is => 'rw',
  isa => 'HashRef[Analyse::StructureStrategyApi]',
  predicate => 'hasStructStrategyMap',
  clearer => 'resetStructStrategyMap',
  default => sub {{}},
  lazy => 1,
);

=head1 METHODS

=head2 addStructStrategy($strat)

Adds a given strategy ($strat) to the contentStrategyMap.

=head3 Parameters:

=over 4

=item *

$strat: the given strategy, it extends from Analyse::StructureStrategyApi

=back

=cut
sub addStructStrategy {
  my $self = shift;
  my $strat = shift;
  
  # Engine::AbstractIDGenerator
  my $type = $strat->getStrategyType;
  # put generator in the map
  $self->structStrategyMap->{$type} = $strat;
}

=head2 getStructStrategy($stratType)

=head3 Parameters:

=over 4

=item *

$stratType: the given type of strategy, see also Global::Values->doAnalyseStrategyType

=back

=head3 Returns:

The specified Object Strategy

=cut
sub getStructStrategy {
  my $self = shift;
  my $stratType = shift;
  my $structStrategy = $self->structStrategyMap->{$stratType};
  # make sure old data is clear
  $structStrategy->clear;
  return $structStrategy;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
package Analyse::StructureStrategySelector;
=encoding utf8

=head1 NAME

Analyse::StructureStrategySelector - a controller of the content strategies and content strategy engine

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

use Global::Values;
use Database::MotDB;

use Analyse::StructStrategySelectorEngine;
use Analyse::StructureStrategyApi;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

=head1 CONSTRUCTORS

=head2 BUILD

After new.

=cut
sub BUILD {
  my $self = shift;
  # add build in strategies
  $self->addBuildInStrategies;
}

=head2 _build_structstrategyselectorengine

=cut
sub _build_structstrategyselectorengine {
  my $g = Analyse::StructStrategySelectorEngine->new;
  return $g;
}

=head1 ATTRIBUTES

=head2 selectorEngine

Analyse::StructStrategySelectorEngine

=cut
has 'selectorEngine' => (
  is => 'rw',
  isa => 'Analyse::StructStrategySelectorEngine',
  builder => "_build_structstrategyselectorengine",
  handles => [qw( addStructStrategy getStructStrategy)],
);

=head1 METHODS

=head2 getInstance

Singleton.

=cut
my $singleton_StructStrategySelector;
sub getInstance {
  my $self = shift;
  if (! defined $singleton_StructStrategySelector) {
    $singleton_StructStrategySelector = $self->new;
  }
  return $singleton_StructStrategySelector;
}

=head2 addBuildInStrategies

Adds the strategies to the main program

=cut
sub addBuildInStrategies {
  my $self = shift;
  $self->addStructStrategy(Analyse::EntriesComparisonStrategy->new);
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


 