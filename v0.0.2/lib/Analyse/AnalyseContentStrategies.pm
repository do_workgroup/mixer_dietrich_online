package Analyse::AnalyseContentStrategies;
=encoding utf8

=head1 NAME

Analyse::AnalyseContentStrategies - The strategies for textual analysis.

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package is for text data analysis. It is based on strategy pattern mode and command pattern mode.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use utf8;
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);       
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use 5.020;


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS: 
package Analyse::AnalyseContentStrategyApi;
=encoding utf8

=head1 NAME

Analyse::AnalyseContentStrategyApi - The abstract class of strategies to analyse text content

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This is abstract superclass for the text content analysis. Standard behaviors like the 

getType, clear, execute, input and output methods are defined here, the programmer only needs to extend

this class and provide implementations for the methods. 

The non-abstract methods have their descriptions with its implementation in detail.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut

use Moose::Role;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use utf8;
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);       
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use 5.020;

use Carp;
use Data::Dumper;

use Global::Values;
use Database::MotDBProcessor;

use Module::ModuleUnits;
use Module::ModuleUnitFactory;
use Engine::IDProcessor;

=head1 ATTRIBUTES

=head2 outputUnitTables

The result map for output after analysis.

=begin text

unit tables: module unit id => outputTable ...

     unitID_1=> {
       unitType => REGEXP_UNIT,
       analyseResult => {
            name    => xxx,
            matched => 1/0,
            pos     => ['1,2', "2,3" ...],
       }
     }
     unitID_2 => {
       unitType => REGEXP_MOD_UNIT,
       analyseResult => {
            name    => xxx,
            matched => 1,
            pos     => ['0,0'],
       }
     }
     unitID_3 => {
       unitType => HTML_UNIT,
       analyseResult => {...}
     }
     
=end text

=cut
has 'outputUnitTables' => (
  is        => 'rw',
  isa       => 'HashRef',
  predicate => 'hasOutputUnitTables',
  clearer   => 'clearOutputUnitTables',
  writer    => 'setOutputUnitTables',
  reader    => 'getOutputUnitTables',
  default => sub {{}},
  lazy => 1,
);

=head2 text

The given text string to analyse

=cut
has 'text' => (
  is => 'rw',
  isa => 'Str',
  lazy => 1,
  default => '',
  writer => 'setText',
  reader => 'getText',
  clearer => 'resetText',
);

=head2 text

The given Text::EntryText (Object) for saving the results of analysis

=cut
has 'curEntryText' => (
  is => 'rw',
  isa => 'Text::EntryText | Undef',
  predicate => 'hasCurEntryText',
  clearer => 'clearCurEntryText',
  reader => "getCurEntryText",
  writer => "setCurEntryText",
  default => sub {undef},
  lazy => 1,
);

=head1 REQUIRES METHODS

=head2 getContentStrategyType

Returns the subclass type

=cut
requires 'getContentStrategyType';

=head2 clear

Resets the class to its initial, empty state.

=cut
requires 'clear';

=head2 execute

Immediately performs the analysis of the task in the class.

=cut
requires 'execute';

=head2 input

Adds the user values into the class.

=cut
requires 'input';

=head2 output

Returns the result for the output

=cut
requires 'output';

=head1 METHODS

=head2 getNewGraphUnit($patName, $regPatMap)

=head3 Paramters:

=over 4

=item *

$patName: the given pattern name

=item *

$regPatMap: the given pattern information map, 
            see Global::Values::ANALYSE_PARAMETERS->{REGEXP_PATTERN_HASH}

=back

=head3 Returns:

the new graph Unit (Module::GraphModuleUnit).

=head3 See Also:

Module::GraphModuleUnit, Module::UnitFactoryApp

=cut
sub getNewGraphUnit {
  my $self      = shift;
  my $patName   = shift;
  my $regPatMap = shift;

  my $unitFApp = Module::UnitFactoryApp->getInstance;

  # pure graph unit
  my $graphUnit = $unitFApp->getNewUnit('GRAPH_UNIT');

  # new unit
  $graphUnit->newUnit( $patName, $regPatMap );
  return if ( !defined $graphUnit || $graphUnit eq "" );

  # set id of unit
  my $graphID = Engine::IDGenerateApp->getInstance->generateGraphID;
  $graphUnit->setModUnitID($graphID);

  # set name of unit
  $graphUnit->setModUnitName($patName);
  return $graphUnit;
}

=head2 getNewHtmlUnit($tagName)

=head3 Paramters:

=over 4

=item *

$tagName: the given tag name i.e. entry, s, l 

=back

=head3 Returns:

the new Html Unit (Module::HTMLModuleUnit).

=head3 See Also:

Module::HTMLModuleUnit, Module::UnitFactoryApp

=cut
sub getNewHtmlUnit {
  my $self = shift;
  my $tagName = shift;
  
  my $unitFApp = Module::UnitFactoryApp->getInstance;
  
  # pure graph unit
  my $htmlUnit = $unitFApp->getNewUnit('HTML_UNIT');
  
  $htmlUnit->setTagName($tagName);
  
  # set id of unit
  my $modID = Engine::IDGenerateApp->getInstance->generateModuleID;
  $htmlUnit->setModUnitID($modID);
  
  return $htmlUnit;
  
}

=head2 processWithGraph($graphUnit, $textRef, $regPatMap)

=head3 Paramters:

=over 4

=item *

$graphUnit: the given unit of Module::GraphModuleUnit

=item *

$textRef: the given reference of string text

=item *

$regPatMap: the given pattern information map,
            Global::Values::ANALYSE_PARAMETERS->{REGEXP_PATTERN_HASH}

=back

=head3 Returns:

the result map for output after analyse.

=head3 See Also:

Module::GraphModuleUnit, Database::MotDBTableApp, Module::UnitFactoryApp-

=cut
sub processWithGraph {
  my $self      = shift;
  my $graphUnit = shift;
  my $textRef   = shift;
  my $regPatMap = shift;
  
  if (! defined $graphUnit || $graphUnit eq ""
    || ! defined $regPatMap || $regPatMap eq ""
  ) {
    croak "There is no valid graph unit and reg pattern maps!";
  }
  if (! defined $textRef || $textRef eq "") {
    return {};
  }

  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $ufApp  = Module::UnitFactoryApp->getInstance;

  # 1. get current vertex from graph to analyse
  my $patName = $graphUnit->getUnmarkedVertexToAnalyse();

  # result hashtablesf
  my $outputTables = {};
  #!!! Attention: Loop!!!
  while ( defined $patName && $patName ne "" ) {

    #  mod_pattern
    my $regUnit;
    if ( $self->isModType($patName) ) {
      $regUnit = $self->getNewRegModUnit( 'REGEXP_MOD_UNIT', $textRef, $patName, $regPatMap );
    }
    else {
      $regUnit = $self->getNewRegModUnit( 'REGEXP_UNIT', $textRef, $patName, $regPatMap );
    }

    # process reg exp unit
    $regUnit->process;
    #
    my $curMUnitID = $regUnit->getModUnitID;
    # result output tables
    $outputTables->{$curMUnitID} = $regUnit->getOutputTable->{$curMUnitID};
    
    # set matched info to the graph unit
    $graphUnit->setVMatchAttr($patName, $regUnit->found);
    $regUnit = undef;
    $patName = $graphUnit->getUnmarkedVertexToAnalyse();
  }
  $graphUnit = undef;
  return $outputTables;
}

=head2 processWithGraph($regModType, $textRef, $patName, $regPatMap)

=head3 Paramters:

=over 4

=item *

$regModType: the given unit type of RegExp Analyse Module, i.e. REGEXP_UNIT, REGEXP_MOD_UNIT

=item *

$textRef: the given reference of string text

=item *

$patName: the given pattern name, see also $regPatMap

=item *

$regPatMap: the given pattern information map,
            Global::Values::ANALYSE_PARAMETERS->{REGEXP_PATTERN_HASH}

=back

=head3 Returns:

the new RegExp Unit (Module::RegModuleUnit, Module::RegModModuleUnit).

=head3 See Also:

Module::RegModuleUnit, Module::RegModModuleUnit, Database::MotDBTableApp, Module::UnitFactoryApp-

=cut
sub getNewRegModUnit {
  my $self       = shift;
  my $regModType = shift;
  my $textRef    = shift;
  my $patName    = shift;
  my $regPatMap  = shift;

  my $unitFApp = Module::UnitFactoryApp->getInstance;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;

  # pure reg mod unit
  my $regModUnit = $unitFApp->getNewUnit($regModType);

#  my $pattern = $regPatMap->{$patName}->{content};
  my $pattern = $mdbtApp->getTableProcessor("REG_PATTERN_ENGINE_MAP")->search($patName, "content");
  if ( (!defined $pattern || $pattern eq "") && $regModType ne 'REGEXP_MOD_UNIT' ) {
    carp "There is no valid pattern for reg exp!";
    return;
  }

  # set module id
  my $modID = Engine::IDGenerateApp->getInstance->generateModuleID;
  $regModUnit->setModUnitID($modID);

  # set name
  $regModUnit->setModUnitName($patName);

  # special mod module unit
  return $regModUnit if ( $self->isModType($patName) );

  #input Text (with position of begin or end?)
  croak "There is no valid input Text for reg mod unit" 
    if (! defined $textRef || (ref($textRef) eq "SCALAR" && ! defined $$textRef));
  $regModUnit->setText($$textRef);

  # pattern as content
  $regModUnit->setModuleContent($pattern);

  return $regModUnit;
}

=head2 isModType($patName)

Returns true if this given $patName has ''mod_'' substring, returns false otherwise.

=cut
sub isModType {
  my $self    = shift;
  my $patName = shift;
  my $res     = 0;
  if ( $patName =~ m/^mod_/ ) {
    $res = 1;
  }
  return $res;
}

no Moose;
1;


#CLASS:
package Analyse::AnalyseContentRowType;
=encoding utf8

=head1 NAME

Analyse::AnalyseContentRowType - Strategy to analyse row type in the text file

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut

use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';
use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];

=head1 ATTRIBUTES

=head2 type

Type of the class (ANALYSE_STRATEGY_TYPE: Global::Values->doAnalyseStrategyType)

=cut
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'ROW_TYPE_ANALYSE_STRATEGY',
);


=head1 METHODS

=head2 getContentStrategyType()

=head3 Overrides:

getContentStrategyType in class Analyse::AnalyseContentStrategyApi

=cut
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}
=head2 clear()

=head3 Overrides:

clear in class Analyse::AnalyseContentStrategyApi

=cut
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

=head2 input($textRef)

The reference of text will be input

=head3 Overrides:

input in class Analyse::AnalyseContentStrategyApi

=cut
sub input {
  my $self    = shift;
  my $textRef = shift;
  $self->setText($$textRef);
}

=head2 output()

=head3 Overrides:

output in class Analyse::AnalyseContentStrategyApi

=cut
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

=head2 execute()

=head3 Overrides:

execute in class Analyse::AnalyseContentStrategyApi

=cut
sub execute {
  my $self = shift;

  # get it from input
  my $text = $self->getText;

  # run core
  my $outputTables = $self->analyseRowLine( \$text );

  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

=head2 analyseRowLine($rowTextRef)

Returns the type of the given reference of text row.

=cut
sub analyseRowLine {
  my $self       = shift;
  my $rowTextRef = shift;

  #my $regModName = shift;
  #my $regPatMap = shift;

  #1. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;

  # 1.1 reg pattern map
  my $regPatternMap = $dbtApp->getRegPatMap;

  # 2. get row type graph unit
  my $graphUnit = $self->getRowTypeGraphUnit();
  
  return {} if (! defined $rowTextRef || $rowTextRef eq "");
  # 3. get output tables: reg module id => outputTable ...
  my $outputTables = $self->processWithGraph( $graphUnit, $rowTextRef, $regPatternMap );
  
  return $outputTables;
}

=head2 getRowTypeGraphUnit()

Returns the specified graph unit for analysing the row type

=cut
sub getRowTypeGraphUnit {
  my $self             = shift;
  my $rowCheckPatName  = Global::Values::MODULE_PATTERN->{CHECK_LINE_TYPE};
  my $regPatMap        = Database::MotDBTableApp->getInstance->getRegPatMap;
  my $rowTypeGraphUnit = $self->getNewGraphUnit( $rowCheckPatName, $regPatMap );
  return $rowTypeGraphUnit;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;



#CLASS:
package Analyse::Analyse_Sigel_Content_Strategy;
=encoding utf8

=head1 NAME

Analyse::Analyse_Sigel_Content_Strategy - Strategy to analyse the sigel area in the text string

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut

use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];

=head1 ATTRIBUTES

=head2 type

Type of the class (ANALYSE_STRATEGY_TYPE: Global::Values->doAnalyseStrategyType)

=cut
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_SIGEL_STRATEGY',
);

=head1 METHODS

=head2 getContentStrategyType()

=head3 Overrides:

getContentStrategyType in class Analyse::AnalyseContentStrategyApi

=cut
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

=head2 clear()

=head3 Overrides:

clear in class Analyse::AnalyseContentStrategyApi

=cut
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

=head2 input($entryText)

Text::EntryText will be input

=head3 Overrides:

input in class Analyse::AnalyseContentStrategyApi

=cut
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}

=head2 output()

=head3 Overrides:

output in class Analyse::AnalyseContentStrategyApi

=cut
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

=head2 execute()

=head3 Overrides:

execute in class Analyse::AnalyseContentStrategyApi

=cut
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getSigel();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

=head2 getSigel()

Returns the output maps of the result.

The specified text part will be controlled on Corrdinate::TextIndex in the Text::EntryText.

=cut 
sub getSigel {
  my $self = shift;
  
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
  
  #1. text and part text
  my $entryText = $self->getCurEntryText;
  
  my $origText = $entryText->getOrigText;
  my $partTextRef = $entryText->getPartTextRefWithTextIndex(\$origText);
    
  #2. html engine and unit
  my $sigelHtmlUnit = $self->getSigelHTMLModUnit;
  
  #3. set input text, this is a text part of the entire original text
  $sigelHtmlUnit->setInputText($partTextRef);
  
  #4. do it 
  $sigelHtmlUnit->process;
  
  #5. set the attribute of lemma in the entry text
  if ($sigelHtmlUnit->found) {
    $entryText->setEntryTextAttr("hasSigel", 1);
  }
  
  #6. output
  my $outputTable = $sigelHtmlUnit->getOutputTable;
  
  $sigelHtmlUnit = undef;
  return $outputTable;
  
}

=head2 getSigelHTMLModUnit()

Returns the specified Html unit for analysing the sigel area

=cut
sub getSigelHTMLModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{SIG_TAG};
  my $sigelHtmlUnit = $self->getNewHtmlUnit($tagName);
  return $sigelHtmlUnit;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Analyse_Lemma_Content_Strategy;
###########################################
package Analyse::Analyse_Lemma_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_LEMMA_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getLemma();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getLemma {
  my $self = shift;
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  # 0.1 reg pattern map
  #my $regPatternMap = $dbtApp->getRegPatMap;
  
  # entry Text
  my $entryText = $self->getCurEntryText;
  my $origText = $entryText->getOrigText;
  my $partTextRef = $entryText->getPartTextRefWithTextIndex(\$origText);
  
  #1. html engine and unit
  my $lemHtmlUnit = $self->getLemmaHtmlModUnit;
  
  # set input text, this is a relave part text of the entire original text
  $lemHtmlUnit->setInputText($partTextRef);
  
  # do it 
  $lemHtmlUnit->process;
  
  # set the attribute of lemma in the entry text
  if ($lemHtmlUnit->found) {
    $entryText->setEntryTextAttr("hasLemma", 1);
  }
  
  # output
  my $outputTable = $lemHtmlUnit->getOutputTable;
  $lemHtmlUnit = undef;
  return $outputTable;
}


sub getLemmaHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{LEMMA_TAG};
  my $lemmaHtmlUnit = $self->getNewHtmlUnit($tagName);
  return $lemmaHtmlUnit;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Analyse_Sic_HTML_Content_Strategy;
###########################################
package Analyse::Analyse_Sic_HTML_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_SIC_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getSic();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}
sub getSic {
  my $self = shift;
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  # 0.1 reg pattern map
  #my $regPatternMap = $dbtApp->getRegPatMap;
  
  # entry Text
  my $entryText = $self->getCurEntryText;
  
  my $origText = $entryText->getOrigText;
  #1. html engine and unit
  my $sigHtmlUnit = $self->getSigHtmlModUnit;
  
  # Attention: the entire original text!!
  $sigHtmlUnit->setInputText(\$origText);
  
  # do it 
  $sigHtmlUnit->process;
  
  # set the attribute of lemma in the entry text
#  if ($lemHtmlUnit->found) {
#    $entryText->setEntryTextAttr("hasSic", 1);
#  }
  
  # output
  my $outputTable = $sigHtmlUnit->getOutputTable;
  $sigHtmlUnit = undef;
  return $outputTable;
}


sub getSigHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{SIC_TAG};
  my $lemmaHtmlUnit = $self->getNewHtmlUnit($tagName);
  return $lemmaHtmlUnit;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package Analyse::Analyse_Corr_HTML_Content_Strategy;
###########################################
package Analyse::Analyse_Corr_HTML_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;
use FindBin;
use lib "$FindBin::Bin/../";
with 'Analyse::AnalyseContentStrategyApi';
use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################
########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_CORR_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getCorr();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getCorr {
  my $self = shift;
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  
  # entry Text
  my $entryText = $self->getCurEntryText;
  
  my $origText = $entryText->getOrigText;
  #1. html engine and unit
  my $corrHtmlUnit = $self->getCorrHtmlModUnit;
  
  # Attention: the entire original text!!
  $corrHtmlUnit->setInputText(\$origText);
  
  # do it 
  $corrHtmlUnit->process;
  
  # output
  my $outputTable = $corrHtmlUnit->getOutputTable;
  $corrHtmlUnit = undef;
  return $outputTable;
}
sub getCorrHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{CORR_TAG};
  my $lemmaHtmlUnit = $self->getNewHtmlUnit($tagName);
  return $lemmaHtmlUnit;
}
no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Analyse_Dop_HTML_Content_Strategy;
###########################################
package Analyse::Analyse_Dop_HTML_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_DOP_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getDop();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getDop {
  my $self = shift;
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  
  # entry Text
  my $entryText = $self->getCurEntryText;
  
  my $origText = $entryText->getOrigText;
  #1. html engine and unit
  my $dopHtmlUnit = $self->getDopHtmlModUnit;
  
  # Attention: the entire original text!!
  $dopHtmlUnit->setInputText(\$origText);
  
  # do it 
  $dopHtmlUnit->process;
  
  # set the attribute of lemma in the entry text
  if ($dopHtmlUnit->found) {
    $entryText->setEntryTextAttr("hasDop", 1);
  }
  
  # output
  my $outputTable = $dopHtmlUnit->getOutputTable;
  $dopHtmlUnit = undef;
  return $outputTable;
}


sub getDopHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{DOP_TAG};
  my $dopHtmlUnit = $self->getNewHtmlUnit($tagName);
  return $dopHtmlUnit;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Analyse_Rep_HTML_Content_Strategy;
###########################################
package Analyse::Analyse_Rep_HTML_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_REP_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getRep();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getRep {
  my $self = shift;
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  
  # entry Text
  my $entryText = $self->getCurEntryText;
  
  my $origText = $entryText->getOrigText;
  #1. html engine and unit
  my $repHtmlUnit = $self->getRepHtmlModUnit;
  
  # Attention: the entire original text!!
  $repHtmlUnit->setInputText(\$origText);
  
  # do it 
  $repHtmlUnit->process;
  
  # output
  my $outputTable = $repHtmlUnit->getOutputTable;
  $repHtmlUnit = undef;
  return $outputTable;
}


sub getRepHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{REP_TAG};
  my $dopHtmlUnit = $self->getNewHtmlUnit($tagName);
  return $dopHtmlUnit;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package Analyse::Analyse_DITO_HTML_Content_Strategy;
###########################################
package Analyse::Analyse_DITO_HTML_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";
with 'Analyse::AnalyseContentStrategyApi';
use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_DITO_HTML_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}
# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getDitoElement();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getDitoElement {
  my $self = shift;
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  
  # entry Text
  my $entryText = $self->getCurEntryText;
  
  my $origText = $entryText->getOrigText;
  #1. html engine and unit
  my $doHtmlUnit = $self->getDitoHtmlModUnit;
  
  # Attention: the entire original text!!
  $doHtmlUnit->setInputText(\$origText);
  
  # do it 
  $doHtmlUnit->process;

  # output
  my $outputTable = $doHtmlUnit->getOutputTable;
  $doHtmlUnit = undef;
  return $outputTable;
}


sub getDitoHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{DITO_TAG};
  my $ditoHtmlUnit = $self->getNewHtmlUnit($tagName);
  return $ditoHtmlUnit;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Analyse_Homonym_HTML_Content_Strategy;
###########################################
package Analyse::Analyse_Homonym_HTML_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_HOM_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getHom();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}
sub getHom {
  my $self = shift;
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  # entry Text
  my $entryText = $self->getCurEntryText;
  my $origText = $entryText->getOrigText;
  #1. html engine and unit
  my $homHtmlUnit = $self->getHomHtmlModUnit;
  
  # Attention: the entire original text!!
  $homHtmlUnit->setInputText(\$origText);
  # do it 
  $homHtmlUnit->process;
  # output
  my $outputTable = $homHtmlUnit->getOutputTable;
  $homHtmlUnit = undef;
  return $outputTable;
}
sub getHomHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{HOM_TAG};
  my $dopHtmlUnit = $self->getNewHtmlUnit($tagName);
  return $dopHtmlUnit;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;
#CLASS:
###########################################
# package Analyse::Analyse_Sub_HTML_Content_Strategy;
###########################################
package Analyse::Analyse_Sub_HTML_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_SUB_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getSub();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}
sub getSub {
  my $self = shift;
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  # entry Text
  my $entryText = $self->getCurEntryText;
  my $origText = $entryText->getOrigText;
  #1. html engine and unit
  my $subHtmlUnit = $self->getSubHtmlModUnit;
  
  # Attention: the entire original text!!
  $subHtmlUnit->setInputText(\$origText);
  # do it 
  $subHtmlUnit->process;
  # output
  my $outputTable = $subHtmlUnit->getOutputTable;
  $subHtmlUnit = undef;
  return $outputTable;
}
sub getSubHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{SUB_TAG};
  my $dopHtmlUnit = $self->getNewHtmlUnit($tagName);
  return $dopHtmlUnit;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Analyse_Sup_HTML_Content_Strategy;
###########################################
package Analyse::Analyse_Sup_HTML_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_SUP_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getSup();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}
sub getSup {
  my $self = shift;
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  # entry Text
  my $entryText = $self->getCurEntryText;

  my $origText = $entryText->getOrigText;
  #1. html engine and unit
  my $supHtmlUnit = $self->getSupHtmlModUnit;
  
  # Attention: the entire original text!!
  $supHtmlUnit->setInputText(\$origText);
  # do it 
  $supHtmlUnit->process;
  # output
  my $outputTable = $supHtmlUnit->getOutputTable;
  $supHtmlUnit = undef;
  return $outputTable;
}
sub getSupHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{SUP_TAG};
  my $dopHtmlUnit = $self->getNewHtmlUnit($tagName);
  return $dopHtmlUnit;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;
#CLASS:
###########################################
# package Analyse::Analyse_Ignore_HTML_Content_Strategy;
###########################################
package Analyse::Analyse_Ignore_HTML_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;
use FindBin;
use lib "$FindBin::Bin/../";
with 'Analyse::AnalyseContentStrategyApi';
use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################
########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_IGNORE_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getIgnore();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getIgnore {
  my $self = shift;
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  
  # entry Text
  my $entryText = $self->getCurEntryText;
  
  my $origText = $entryText->getOrigText;
  #1. html engine and unit
  my $ignoreHtmlUnit = $self->getIgnoreHtmlModUnit;
  
  # Attention: the entire original text!!
  $ignoreHtmlUnit->setInputText(\$origText);
  # do it 
  $ignoreHtmlUnit->process;
  # output
  my $outputTable = $ignoreHtmlUnit->getOutputTable;
  $ignoreHtmlUnit = undef;
  return $outputTable;
}
sub getIgnoreHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{IGN_TAG};
  my $dopHtmlUnit = $self->getNewHtmlUnit($tagName);
  return $dopHtmlUnit;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Analyse_Entry_HTML_Content_Strategy;
###########################################
package Analyse::Analyse_Entry_HTML_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_HTML_ENTRY_ELEMENT_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getHtmlEntryElement();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getHtmlEntryElement {
  my $self = shift;
  
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  
  # entry Text
  my $entryText = $self->getCurEntryText;
  
  my $origText = $entryText->getOrigText;
  #1. html engine and unit
  my $entryHtmlUnit = $self->getEntryHtmlModUnit;
  
  # Attention: the entire original text!!
  $entryHtmlUnit->setInputText(\$origText);
  # do it 
  $entryHtmlUnit->process;
  # output
  my $outputTable = $entryHtmlUnit->getOutputTable;
  $entryHtmlUnit = undef;
  return $outputTable;
}
sub getEntryHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{ENTRY_TAG};
  my $dopHtmlUnit = $self->getNewHtmlUnit($tagName);
  return $dopHtmlUnit;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Analyse_Endpart_Content_Strategy;
###########################################
package Analyse::Analyse_Endpart_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################
########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_ENDPART_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getEndpart();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getEndpart {
  my $self = shift;
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  # 0.1 reg pattern map
  #my $regPatternMap = $dbtApp->getRegPatMap;
  
  # entry Text
  my $entryText = $self->getCurEntryText;
  
  my $origText = $entryText->getOrigText;
  my $partTextRef = $entryText->getPartTextRefWithTextIndex(\$origText);
  
  #1. html engine and unit
  my $okHtmlUnit = $self->getOKHtmlModUnit;
  
  # set input text, this is a relave part text of the entire original text
  $okHtmlUnit->setInputText($partTextRef);
  
  # do it 
  $okHtmlUnit->process;
  
  # output
  my $outputTable = $okHtmlUnit->getOutputTable;
  $okHtmlUnit = undef;
  return $outputTable;
}


sub getOKHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{OK_TAG};
  my $lemmaHtmlUnit = $self->getNewHtmlUnit($tagName);
  return $lemmaHtmlUnit;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package Analyse::Analyse_OK_Content_Strategy;
###########################################
package Analyse::Analyse_OK_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";
with 'Analyse::AnalyseContentStrategyApi';
use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################
########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_OK_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getOKElement();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getOKElement {
  my $self = shift;
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  # 0.1 reg pattern map
  #my $regPatternMap = $dbtApp->getRegPatMap;
  
  # entry Text
  my $entryText = $self->getCurEntryText;
  
  my $origText = $entryText->getOrigText;
  #my $partTextRef = $entryText->getPartTextRefWithTextIndex(\$origText);
  
  #1. html engine and unit
  my $okHtmlUnit = $self->getOKHtmlModUnit;
  
  # set input text, this is a relave part text of the entire original text
  $okHtmlUnit->setInputText(\$origText);
  # do it 
  $okHtmlUnit->process;
  # output
  my $outputTable = $okHtmlUnit->getOutputTable;
  $okHtmlUnit = undef;
  return $outputTable;
}
sub getOKHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{OK_TAG};
  my $htmlUnit = $self->getNewHtmlUnit($tagName);
  return $htmlUnit;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package Analyse::Analyse_EndDop_Content_Strategy;
###########################################
package Analyse::Analyse_EndDop_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################
########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_END_DOP_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getEndDop();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getEndDop {
  my $self = shift;
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  # 0.1 reg pattern map
  #my $regPatternMap = $dbtApp->getRegPatMap;
  
  # entry Text
  my $entryText = $self->getCurEntryText;
  
  my $origText = $entryText->getOrigText;
  my $partTextRef = $entryText->getPartTextRefWithTextIndex(\$origText);
  
  #1. html engine and unit
  my $endDopHtmlUnit = $self->getEndDopHtmlModUnit;
  
  # set input text, this is a relave part text of the entire original text
  $endDopHtmlUnit->setInputText($partTextRef);
  
  # do it 
  $endDopHtmlUnit->process;
  
  # output
  my $outputTable = $endDopHtmlUnit->getOutputTable;
  $endDopHtmlUnit = undef;
  return $outputTable;
}


sub getEndDopHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{DOP_TAG};
  my $htmlUnit = $self->getNewHtmlUnit($tagName);
  return $htmlUnit;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Analyse_Reference_HTML_Content_Strategy;
###########################################
package Analyse::Analyse_Reference_HTML_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_REFERENCE_HTML_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getReference();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getReference {
  my $self = shift;
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  # 0.1 reg pattern map
  #my $regPatternMap = $dbtApp->getRegPatMap;
  
  # entry Text
  my $entryText = $self->getCurEntryText;
  
  my $origText = $entryText->getOrigText;
  my $partTextRef = $entryText->getPartTextRefWithTextIndex(\$origText);
  return {} if (! defined $partTextRef || $partTextRef eq ""); 
  #1. html engine and unit
  my $refHtmlUnit = $self->getReferenceHtmlModUnit;
  
  # set input text, this is a relave part text of the entire original text
  $refHtmlUnit->setInputText($partTextRef);
  
  # do it 
  $refHtmlUnit->process;
  
  # output
  my $outputTable = $refHtmlUnit->getOutputTable;
  $refHtmlUnit = undef;
  return $outputTable;
}
sub getReferenceHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{VERWEIS_TAG};
  my $refHtmlUnit = $self->getNewHtmlUnit($tagName);
  return $refHtmlUnit;
}
no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package Analyse::Analyse_Reference_Lemma_HTML_Content_Strategy;
###########################################
package Analyse::Analyse_Reference_Lemma_HTML_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";
with 'Analyse::AnalyseContentStrategyApi';
use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_REFERENCE_LEMMA_HTML_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}
# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getReferenceLemma();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}
sub getReferenceLemma {
  my $self = shift;
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  # 0.1 reg pattern map
  #my $regPatternMap = $dbtApp->getRegPatMap;
  
  # entry Text
  my $entryText = $self->getCurEntryText;
  
  my $origText = $entryText->getOrigText;
  my $partTextRef = $entryText->getPartTextRefWithTextIndex(\$origText);
  return {} if (! defined $partTextRef || $partTextRef eq ""); 
  
  #1. html engine and unit
  my $refHtmlUnit = $self->getReferenceLemmaHtmlModUnit;
  # set input text, this is a relave part text of the entire original text
  $refHtmlUnit->setInputText($partTextRef);
  # do it 
  $refHtmlUnit->process;
  # set the attribute of lemma in the entry text
  if ($refHtmlUnit->found) {
    $entryText->setEntryTextAttr("hasReferenceLemma", 1);
  }
  # output
  my $outputTable = $refHtmlUnit->getOutputTable;
  $refHtmlUnit = undef;
  return $outputTable;
}
sub getReferenceLemmaHtmlModUnit {
  my $self = shift;
  my $tagName = Global::Values::HTML_TAG->{VL_TAG};
  my $refHtmlUnit = $self->getNewHtmlUnit($tagName);
  return $refHtmlUnit;
}
no Moose;
__PACKAGE__->meta->make_immutable;
1;
#
#CLASS:
###########################################
# package Analyse::Analyse_D74_Header_Strategy;
###########################################
package Analyse::Analyse_D74_Header_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;
use FindBin;
use lib "$FindBin::Bin/../";
with 'Analyse::AnalyseContentStrategyApi';
use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'D74_HEADER_STRATEGY',
);
########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->checkd74Header();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub checkd74Header {
  my $self = shift;
  
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  # 0.1 reg pattern map
  my $regPatternMap = $dbtApp->getRegPatMap;
  
  #1. graph unit
  my $graphUnit = $self->getD74HeaderGraphUnit;
  
  #2. text
  my $entryText = $self->getCurEntryText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
#  my $beginIndex = $entryText->getTextPointer->getPosition;
  
  my $origText = $entryText->getOrigText;
  my $targetTextRef = $entryText->getPartTextWithIndices(\$origText, $begin, $end);
  
  return {} if (! defined $targetTextRef || $targetTextRef eq "");
  my $outputTables = $self->processWithGraph($graphUnit, $targetTextRef, $regPatternMap);
  
  return $outputTables;
}

sub getD74HeaderGraphUnit {
  my $self             = shift;
  my $rowCheckPatName  = Global::Values::MODULE_PATTERN->{CHECK_D74_HEADER};
  my $regPatMap        = Database::MotDBTableApp->getInstance->getRegPatMap;
  my $rowTypeGraphUnit = $self->getNewGraphUnit( $rowCheckPatName, $regPatMap );
  return $rowTypeGraphUnit;
}



no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Analyse_Strecke_Entry_Strategy;
###########################################
package Analyse::Analyse_Strecke_Entry_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'STRECKE_STRATEGY',
);
########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getStrecke();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}
sub getStrecke {
  my $self = shift;
  
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  # 0.1 reg pattern map
  my $regPatternMap = $dbtApp->getRegPatMap;
  
  #1. graph unit
  my $graphUnit = $self->getStreckeGraphUnit;
  
  #2. text
  my $entryText = $self->getCurEntryText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
#  my $beginIndex = $entryText->getTextPointer->getPosition;
  
  my $origText = $entryText->getOrigText;
  my $targetTextRef = $entryText->getPartTextWithIndices(\$origText, $begin, $end);
  
  return {} if (! defined $targetTextRef || $targetTextRef eq "");
  my $outputTables = $self->processWithGraph($graphUnit, $targetTextRef, $regPatternMap);
  
  return $outputTables;
}

sub getStreckeGraphUnit {
  my $self             = shift;
  my $rowCheckPatName  = Global::Values::MODULE_PATTERN->{CHECK_STRECKE_Entry};
  my $regPatMap        = Database::MotDBTableApp->getInstance->getRegPatMap;
  my $rowTypeGraphUnit = $self->getNewGraphUnit( $rowCheckPatName, $regPatMap );
  return $rowTypeGraphUnit;
}



no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package Analyse::Analyse_Title_Content_Strategy;
###########################################
package Analyse::Analyse_Title_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';
use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_TITLE_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getTitle();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getTitle {
  my $self = shift;
  
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
#  my $ufApp  = Module::UnitFactoryApp->getInstance;
  # 0.1 reg pattern map
  my $regPatternMap = $dbtApp->getRegPatMap;
  
  #1. graph unit
  my $graphUnit = $self->getTitleGraphUnit;
  
  #2. text
  my $entryText = $self->getCurEntryText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  
  my $origText = $entryText->getOrigText;
  my $targetTextRef = $entryText->getPartTextWithIndices(\$origText, $begin, $end);
  
  return {} if (! defined $targetTextRef || $targetTextRef eq "");
  my $outputTables = $self->processWithGraph($graphUnit, $targetTextRef, $regPatternMap);
  
  return $outputTables;
}

sub getTitleGraphUnit {
  my $self             = shift;
  my $titleCheckPatName  = Global::Values::MODULE_PATTERN->{CHECK_TITLE_CONTENT};
  my $regPatMap        = Database::MotDBTableApp->getInstance->getRegPatMap;
  my $titleGraphUnit = $self->getNewGraphUnit( $titleCheckPatName, $regPatMap );
  return $titleGraphUnit;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Analyse_Author_Content_Strategy;
###########################################
package Analyse::Analyse_Author_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_AUTHOR_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getAuthor();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getAuthor {
  my $self = shift;
  
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
  # 0.1 reg pattern map
  my $regPatternMap = $dbtApp->getRegPatMap;
  
  #1. graph unit
  my $graphUnit = $self->getAuthorGraphUnit;
  
  #2. text
  my $entryText = $self->getCurEntryText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  
  my $origText = $entryText->getOrigText;
  my $targetTextRef = $entryText->getPartTextWithIndices(\$origText, $begin, $end);
  
  return {} if (! defined $targetTextRef || $targetTextRef eq "");
  
  my $outputTables = $self->processWithGraph($graphUnit, $targetTextRef, $regPatternMap);
  
  return $outputTables;
}
sub getAuthorGraphUnit {
  my $self             = shift;
  my $titleCheckPatName  = Global::Values::MODULE_PATTERN->{CHECK_AUTHOR_CONTENT};
  my $regPatMap        = Database::MotDBTableApp->getInstance->getRegPatMap;
  my $titleGraphUnit = $self->getNewGraphUnit( $titleCheckPatName, $regPatMap );
  return $titleGraphUnit;
}
no Moose;
__PACKAGE__->meta->make_immutable;
1;



#CLASS:
###########################################
# package Analyse::Analyse_Dito_Desc_Content_Strategy;
###########################################
package Analyse::Analyse_Dito_Desc_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;
use FindBin;
use lib "$FindBin::Bin/../";
with 'Analyse::AnalyseContentStrategyApi';
use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];

########################
# constructor
########################
########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_DITO_DESC_STRATEGY',
);
#
########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}
# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}
# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getDitoDesc();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}
sub getDitoDesc {
  my $self = shift;
  
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
  # 0.1 reg pattern map
  my $regPatternMap = $dbtApp->getRegPatMap;
  
  #1. graph unit
  my $graphUnit = $self->getDitoDescGraphUnit;
  
  #2. text
  my $entryText = $self->getCurEntryText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  
  my $origText = $entryText->getOrigText;
  my $targetTextRef = $entryText->getPartTextWithIndices(\$origText, $begin, $end);
  
  return {} if (! defined $targetTextRef || $targetTextRef eq "");
  my $outputTables = $self->processWithGraph($graphUnit, $targetTextRef, $regPatternMap);
  
  return $outputTables;
}
sub getDitoDescGraphUnit {
  my $self             = shift;
  my $checkPatName  = Global::Values::MODULE_PATTERN->{CHECK_DITO_DESCENDANT};
  my $regPatMap        = Database::MotDBTableApp->getInstance->getRegPatMap;
  my $graphUnit = $self->getNewGraphUnit( $checkPatName, $regPatMap );
  return $graphUnit;
}



no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package Analyse::Analyse_Pipe_Content_Strategy;
###########################################
package Analyse::Analyse_Pipe_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_PIPE_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getStartPipe();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getStartPipe {
  my $self = shift;
  
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
  # 0.1 reg pattern map
  my $regPatternMap = $dbtApp->getRegPatMap;
  
  #1. graph unit
  my $graphUnit = $self->getStartPipeGraphUnit;
  
  #2. text
  my $entryText = $self->getCurEntryText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  
  my $origText = $entryText->getOrigText;
  my $targetTextRef = $entryText->getPartTextWithIndices(\$origText, $begin, $end);
  
  return {} if (! defined $targetTextRef || $targetTextRef eq "");
  my $outputTables = $self->processWithGraph($graphUnit, $targetTextRef, $regPatternMap);
  
  return $outputTables;
}
sub getStartPipeGraphUnit {
  my $self             = shift;
  my $checkPatName  = Global::Values::MODULE_PATTERN->{CHECK_PIPE_Entry};
  my $regPatMap        = Database::MotDBTableApp->getInstance->getRegPatMap;
  my $graphUnit = $self->getNewGraphUnit( $checkPatName, $regPatMap );
  return $graphUnit;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Analyse_Char_Errors_Strategy;
###########################################
package Analyse::Analyse_Char_Errors_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'SEARCH_ERROR_CHARS_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getCharErrors();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getCharErrors {
  my $self = shift;
  
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
  # 0.1 reg pattern map
  my $regPatternMap = $dbtApp->getRegPatMap;
  
  #1. graph unit
  my $graphUnit = $self->getCharErrorsGraphUnit;
  
  #2. text
  my $entryText = $self->getCurEntryText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  
  my $origText = $entryText->getOrigText;
#  if ($entryText->getEntryType eq "ENTRY_TYPE_LEMMA") {
#    my $partText_d74Header = $entryText->getPartTextsFromMap("HEADER")->[0];
#    my ($headerBegin, $headerEnd) = $partText_d74Header->getTextIndices;
#    $begin = $headerEnd;
#    $entryText->updateTextPointer($headerEnd);
#    $entryText->setBeginPosOfTextIndex($headerEnd);
#  }
  my $targetTextRef = $entryText->getPartTextWithIndices(\$origText, $begin, $end);
  
  return {} if (! defined $targetTextRef || $targetTextRef eq "");
  my $outputTables = $self->processWithGraph($graphUnit, $targetTextRef, $regPatternMap);
  
  return $outputTables;
}
sub getCharErrorsGraphUnit {
  my $self             = shift;
  my $charsCheckPatName  = Global::Values::MODULE_PATTERN->{CHECK_CHAR_ERRORS};
  my $regPatMap        = Database::MotDBTableApp->getInstance->getRegPatMap;
  my $charsGraphUnit = $self->getNewGraphUnit( $charsCheckPatName, $regPatMap );
  return $charsGraphUnit;
}



no Moose;
__PACKAGE__->meta->make_immutable;
1;



#CLASS:
###########################################
# package Analyse::Analyse_Reference_Reg_Content_Strategy;
###########################################
package Analyse::Analyse_Reference_Reg_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_REFERENCE_REG_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getReference();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getReference {
  my $self = shift;
  
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
  # 0.1 reg pattern map
  my $regPatternMap = $dbtApp->getRegPatMap;
  
  #1. graph unit
  my $graphUnit = $self->getReferenceGraphUnit;
  
  #2. text
  my $entryText = $self->getCurEntryText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  
    my $origText = $entryText->getOrigText;
  my $targetTextRef = $entryText->getPartTextWithIndices(\$origText, $begin, $end);
  
  return {} if (! defined $targetTextRef || $targetTextRef eq "");
  my $outputTables = $self->processWithGraph($graphUnit, $targetTextRef, $regPatternMap);
  
  return $outputTables;
}

sub getReferenceGraphUnit {
  my $self             = shift;
  my $refCheckPatName  = Global::Values::MODULE_PATTERN->{CHECK_REFERENCE_CONTENT};
  my $regPatMap        = Database::MotDBTableApp->getInstance->getRegPatMap;
  my $refGraphUnit = $self->getNewGraphUnit( $refCheckPatName, $regPatMap );
  return $refGraphUnit;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Analyse_Unknownparts_Strategy;
###########################################
package Analyse::Analyse_Unknownparts_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_UNKNOWNPART_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getUnknownparts();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getUnknownparts {
  my $self = shift;
  
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
  # 0.1 reg pattern map
  my $regPatternMap = $dbtApp->getRegPatMap;
  
  #1. graph unit
  my $graphUnit = $self->getUnknownpartsGraphUnit;
  
  #2. text
  my $entryText = $self->getCurEntryText;
  
  my $inputText = $entryText->getOrigText();
  # get unknownparts from entry text
  my $unknownTextRef = $entryText->getUnknownPart(\$inputText);
  
  if (! $unknownTextRef || $unknownTextRef eq "") {return {};};

  my $outputTables = $self->processWithGraph($graphUnit, $unknownTextRef, $regPatternMap);
  
  # this can be empty
  return $outputTables;
}
sub getUnknownpartsGraphUnit {
  my $self             = shift;
  my $unknownCheckPatName  = Global::Values::MODULE_PATTERN->{CHECK_UNKNOWNPARTS};
  my $regPatMap        = Database::MotDBTableApp->getInstance->getRegPatMap;
  my $unknownGraphUnit = $self->getNewGraphUnit( $unknownCheckPatName, $regPatMap );
  return $unknownGraphUnit;
}
no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package Analyse::Analyse_Collation_Content_Strategy;
###########################################
package Analyse::Analyse_Collation_Content_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

with 'Analyse::AnalyseContentStrategyApi';

use Global::Values;
use Database::MotDBProcessor;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];
########################
# constructor
########################

########################
# variables
########################
has 'type' => (
  is      => 'ro',
  isa     => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_COLLATION_STRATEGY',
);

########################
# methods
########################
# implement
sub getContentStrategyType {
  my $self = shift;
  return $self->type;
}

# implement
sub clear {
  my $self = shift;
  $self->clearOutputUnitTables;
  $self->resetText;
  $self->clearCurEntryText;
}

# implement
# input is entry text
sub input {
  my $self    = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputUnitTables;
}

# implement
sub execute {
  my $self = shift;
  # get it from input
  my $etext = $self->getCurEntryText;
  
  # run core
  my $outputTables = $self->getCollation();
  
  # set it for the output
  $self->setOutputUnitTables($outputTables);
}

sub getCollation {
  my $self = shift;
  
  #0. apps
  my $dbtApp = Database::MotDBTableApp->getInstance;
  # 0.1 reg pattern map
  my $regPatternMap = $dbtApp->getRegPatMap;
  
  #1. text
  my $entryText = $self->getCurEntryText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  
  my $origText = $entryText->getOrigText;
  my $targetTextRef = $entryText->getPartTextWithIndices(\$origText, $begin, $end);
  
  #2. graph unit
  my $graphUnit;
  if ($entryText->getEntryTextAttr("hasAuthor") || $entryText->getEntryTextAttr("hasSigel")) {
    $graphUnit = $self->getCollationGraphUnitWithAuthorORSigel;
  } else {
    $graphUnit = $self->getCollationGraphUnit;
  }
  
  return {} if (! defined $targetTextRef || $targetTextRef eq "");
  my $outputTables = $self->processWithGraph($graphUnit, $targetTextRef, $regPatternMap);
  
  return $outputTables;
}

sub getCollationGraphUnit {
  my $self             = shift;
  my $titleCheckPatName  = Global::Values::MODULE_PATTERN->{CHECK_COLLATION_CONTENT};
  my $regPatMap        = Database::MotDBTableApp->getInstance->getRegPatMap;
  my $titleGraphUnit = $self->getNewGraphUnit( $titleCheckPatName, $regPatMap );
  return $titleGraphUnit;
}
sub getCollationGraphUnitWithAuthorORSigel {
  my $self             = shift;
  my $titleCheckPatName  = Global::Values::MODULE_PATTERN->{CHECK_COLLATION_CONTENT_WITH_AUTHOR_SIGEL};
  my $regPatMap        = Database::MotDBTableApp->getInstance->getRegPatMap;
  my $titleGraphUnit = $self->getNewGraphUnit( $titleCheckPatName, $regPatMap );
  return $titleGraphUnit;
}



no Moose;
__PACKAGE__->meta->make_immutable;
1;


################### Engine, App, Context#######################
#class:
package Analyse::ContentStrategySelectorEngine;
=encoding utf8

=head1 NAME

Analyse::ContentStrategySelectorEngine - an engine to store and control the strategies for analysing text parts

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;
use FindBin;
use lib "$FindBin::Bin/../";

use utf8;
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);       
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use 5.020;

use Carp;
use Analyse::AnalyseContentStrategyApi;

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];

=head1 ATTRIBUTES

=head2 contentStrategyMap

Strategy map to store the strategies with the type of strategy.

key: type of strategy  - value: Strategy (object)

=cut
has 'contentStrategyMap' => (
  is        => 'rw',
  isa       => 'HashRef[Analyse::AnalyseContentStrategyApi]',
  predicate => 'hasContentStrategyMap',
  clearer   => 'resetContentStrategyMap',
  default   => sub { {} },
  lazy      => 1,
);
=head1 METHODS

=head2 addContentStrategy($strat)

Add a given strategy ($strat) to the contentStrategyMap.

=head3 Parameters:

=over 4

=item *

$strat: the given strategy, it extends from Analyse::AnalyseContentStrategyApi

=back

=cut
sub addContentStrategy {
  my $self  = shift;
  my $strat = shift;
  #
  my $type = $strat->getContentStrategyType;

  # put strategy in the map
  $self->contentStrategyMap->{$type} = $strat;
}

=head2 getContentStrategy($stratType)

=head3 Parameters:

=over 4

=item *

$stratType: the given type of strategy, see also Global::Values->doAnalyseStrategyType

=back

=head3 Returns:

The specified Object Strategy

=cut
sub getContentStrategy {
  my $self      = shift;
  my $stratType = shift;
  my $strategy  = $self->contentStrategyMap->{$stratType};
  # make sure old data is clear
  $strategy->clear;
  return $strategy;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
package Analyse::ContentStrategySelector;
=encoding utf8

=head1 NAME

Analyse::ContentStrategySelector - a controller of the content strategies and content strategy engine

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;
use FindBin;
use lib "$FindBin::Bin/../";

use utf8;
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);       
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use 5.020;

use Carp;

use Global::Values;
use Database::MotDB;
use Analyse::ContentStrategySelectorEngine;
use Analyse::AnalyseContentStrategyApi;
enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];

=head1 CONSTRUCTORS

=head2 BUILD

After new.

=cut
sub BUILD {
  my $self = shift;
  # add build in strategies
  $self->addBuildInStrategies;
}

=head2 _build_selectorengine

=cut
sub _build_selectorengine {
  my $g = Analyse::ContentStrategySelectorEngine->new;
  return $g;
}

=head1 ATTRIBUTES

=head2 selectorEngine

Analyse::ContentStrategySelectorEngine

=cut
has 'selectorEngine' => (
  is      => 'rw',
  isa     => 'Analyse::ContentStrategySelectorEngine',
  builder => "_build_selectorengine",
  handles => [qw( addContentStrategy getContentStrategy)],
);

=head1 METHODS

=head2 getInstance

Singleton.

=cut
my $singleton_ContentStrategySelector;
sub getInstance {
  my $self = shift;
  if ( !defined $singleton_ContentStrategySelector ) {
    $singleton_ContentStrategySelector = $self->new;
  }
  return $singleton_ContentStrategySelector;
}
=head2 addBuildInStrategies

Adds the strategies to the main program

=cut
sub addBuildInStrategies {
  my $self = shift;
  $self->addContentStrategy( Analyse::AnalyseContentRowType->new );
  $self->addContentStrategy( Analyse::Analyse_Sigel_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_D74_Header_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Lemma_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Sic_HTML_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Corr_HTML_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Dop_HTML_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Rep_HTML_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Homonym_HTML_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Ignore_HTML_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Title_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Author_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Collation_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Reference_Reg_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Reference_HTML_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Reference_Lemma_HTML_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Endpart_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Char_Errors_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Sup_HTML_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Sub_HTML_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_DITO_HTML_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Dito_Desc_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Pipe_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Strecke_Entry_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Entry_HTML_Content_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_Unknownparts_Strategy->new ); 
  $self->addContentStrategy( Analyse::Analyse_EndDop_Content_Strategy->new );  
  $self->addContentStrategy( Analyse::Analyse_OK_Content_Strategy->new );  

}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS: Context
package Analyse::ContentStrategyContext;
=encoding utf8

=head1 NAME

Analyse::ContentStrategyContext - Context for the strategy pattern

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use utf8;
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);       
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use 5.020;


use Analyse::AnalyseContentStrategyApi;
use Analyse::ContentStrategySelector;

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [ @{ Global::Values->doAnalyseStrategyType } ];

=head1 CONSTRUCTORS

=head2 _build_contentstrategyselector

=cut
sub _build_contentstrategyselector {
  return Analyse::ContentStrategySelector->getInstance;
}
=head1 ATTRIBUTES

=head2 curContentStrategy

The current selected content strategy

=cut
has 'curContentStrategy' => (
  is        => 'rw',
  isa       => 'Analyse::AnalyseContentStrategyApi',
  predicate => 'hasCurContentStrategy',
  writer    => 'setCurContentStrategy',
  reader    => 'getCurContentStrategy',
);

=head2 contentStrategySelector

=cut
has 'contentStrategySelector' => (
  is      => 'ro',
  isa     => 'Analyse::ContentStrategySelector',
  builder => '_build_contentstrategyselector',
  lazy    => 1,
);

=head1 METHODS

=head2 getInstance

Singleton.

=cut
our $singleton_ContentStrategyContext;
sub getInstance {
  my $self = shift;
  if ( !defined $singleton_ContentStrategyContext ) {
    $singleton_ContentStrategyContext = $self->new;
  }
  return $singleton_ContentStrategyContext;
}

=head2 getContentStrategy($strategyType)

=cut
sub getContentStrategy {
  my $self         = shift;
  my $strategyType = shift;    #ANALYSE_STRATEGY_TYPE

  my $csSelector = $self->contentStrategySelector;
  my $strategy   = $csSelector->getContentStrategy($strategyType);
  return $strategy;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

