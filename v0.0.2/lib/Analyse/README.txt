0.
  Strukturierung der Aufgabenstellung
  Analyse
  Auswertung der Analyseergebnisse
  

1. DateTime


2. Entry ID


3. Indices of textfield in the Entry
  
  3.1. ?? The list of textfields ??
  3.2. ?? Serializability or HashMap ??


4. category: entries and textfields
    -> avoid the unnecessary operations
  
  4.1 States -> Transaction

5. Report database
  5.1 Report Table
    report_id 
    report_name 
    report_class??
    
    ####################################################
    report_id report_name report_class
    001       sigel_rpt   sigel
    002       lemma_rpt   lemma
    
  
  5.2 Report property table
    report_prop_id 
    report_id 
    report_prop_name 
    report_prop_value 
    report_prop_severity
    report_prop_description 
    report_prop_action(solution)
    
    ####################################################
    report_prop_id report_id report_prop_name report_prop_value reference_id 
    rp0001          001       sigel_module     0                 r001        
    rp0002          001       sigel_tags       1                 r002        


  5.3 Report reference table
    reference_id 
    reference_name 
    reference_value 
    reference_description 
    reference_prop_action(solution)
    
    ###################################################
    reference_id reference_name reference_value report_prop_severity reference_description reference_prop_action
    r001         rpt_sigel_1    0               5                     no sigel in lemma     Wrong, check it
    r002         rpt_sigel_2    1               2                     sigel_tag is ok       Ok, do nothing
     

6. DO_ENTRY database
  6.1 entry table: 
      entry_id 
      textLine
      interID # for report, entryText, entryPartText, 
      entryType 
      originalText 
      strecke 
      headerIDNummer  
      headerIDBand    
      headerIDSeite  
      headerIDSpalte  
      headerIDLine  
      lemma  
      pipe 
      lemmaTitle  
      title    
      tilde  
      author  
      sigel 
      collation  
      titleCollation  
      volume  
      page  
      verweise
      #verweisLemma  
      others       
      ######################################################################################################
      entry_id textLine interID entryType originalText strecke HeaderID-xxx lemma pipe  lemmaTitle title tilde author sigel collation titleCollation volume page verweise others
      e001     112      xx-xx   entry_d74  ...         I       ...          idee  begin null        ...   ~     (xxx)  399a. ...       null          ...    ??   ...      ok,do
  
  6.2 entry extra info table:
    entry_extra_info_id 
    extra_info_name 
    extra_info_class
    ###############################################################
    entry_extra_info_id extra_info_name extra_info_class
    eex001                 repetition      do_common_tag
    eex002                 duplication     do_common_tag
    eex003                 superscript     do_common_tag
  
  6.3 entry extra property table:
    entry_extra_property_id 
    entry_extra_info_id  
    entry_id  
    property_value 
    property_description 
    ###############################################################
    entry_extra_property_id entry_extra_info_id entry_id property_value     property_description
    eep001                  eex003              e001     entryPartText_id   superscript
    eep002                  eex003              e001     entryPartText_id   superscript

  6.4 Header info table:
    header_id
    entry_id # interID
    headerIDNummer  
    headerIDBand    
    headerIDSeite  
    headerIDSpalte  
    headerIDLine  
    ###############################################################
    header_id entry_id headerIDNummer headerIDBand headerIDSeite headerIDSpalte headerIDLine
    h001      xx-xx    a00001         74           77            li             8

7. Counter database
  
  7.1 Counter info table
    ciid
    counter info id
    counter info name
    description

  7.2 Counter info property table
    cpid
    counter property id
    counter info id
    count
    

8.


9.


10.


