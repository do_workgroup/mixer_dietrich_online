package Analyse::AnalyseUnitStrategies;
=encoding utf8

=head1 NAME

Analyse::AnalyseUnitStrategies - The strategies for analysis of results from Analyse::AnalyseContentStrategies.

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package is for Text results analysis. This is based on strategy pattern mode and command pattern mode.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use utf8;
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);       
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use 5.020;

no Moose;
__PACKAGE__->meta->make_immutable;
1;
 

#CLASS: 
package Analyse::UnitStrategyApi;
=encoding utf8

=head1 NAME

Analyse::UnitStrategyApi - The abstract class of Strategy to analyse text result

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This is abstract superclass for the text content analysis. Standard behaviors like the 

getType, clear, analyse, input and output methods are defined here, the programmer needs only to extend

this class and provide implementations for this methods. 

The non-abstract methods have their descriptions with its implementation in Detail.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose::Role;

use FindBin;
use lib "$FindBin::Bin/../";

use utf8;
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);       
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use 5.020;

use Carp;
use Data::Dumper;

use Global::Values;
use Database::MotDBProcessor;


=head1 ATTRIBUTES

=head2 unitTables

The Result Map for output after analyse.

=begin text

unit tables: module unit id => outputTable ...

     unitID_1=> {
       unitType => REGEXP_UNIT,
       analyseResult => {
            name    => xxx,
            matched => 1/0,
            pos     => ['1,2', "2,3" ...],
       }
     }
     unitID_2 => {
       unitType => REGEXP_MOD_UNIT,
       analyseResult => {
            name    => xxx,
            matched => 1,
            pos     => ['0,0'],
       }
     }
     unitID_3 => {
       unitType => HTML_UNIT,
       analyseResult => {...}
     }
     
=end text

=cut
has 'unitTables' => (
  is => 'rw',
  isa => 'HashRef',
  predicate => 'hasUnitTables',
  clearer => 'clearUnitTables',
  writer => 'setUnitTables',
  reader => 'getUnitTables',
  default => sub{{}},
  lazy => 1,
);
=head2 outputHash

The result Map for the output

=cut
has 'outputHash' => (
  is => 'rw',
  isa => 'HashRef',
  predicate => 'hasOutputHash',
  clearer => 'clearOutputHash',
  writer => 'setOutputHash',
  reader => 'getOutputHash',
  default => sub {{}},
  lazy => 1,
);

=head2 text

The given Text::EntryText (Object) for save the results of analysis

=cut
has 'curEntryText' => (
  is => 'rw',
  isa => 'Text::EntryText | Undef',
  predicate => 'hasCurEntryText',
  clearer => 'clearCurEntryText',
  reader => "getCurEntryText",
  writer => "setCurEntryText",
  default => sub {undef},
  lazy => 1,
);

=head1 REQUIRES METHODS

=head2 getStrategyType

Returns the subclass type

=cut
requires 'getStrategyType';
=head2 analyse

Immediately performs the analysis of the task in the class.

=cut
requires 'analyse';
=head2 input

Adds the specified values to the class 

=cut 
requires 'input';
=head2 output

Returns the resut for the output

=cut
requires 'output';
=head2 clear

Resets the class to its initial, empty state.

=cut
requires 'clear';


=head1 METHODS

=head2 checkUnitTablesSeverity($unitTables)

=head3 Paramters:

=over 4

=item *

$unitTables: the given output unit tables, mostly from AnalyseContentStrategies.

=back

=head3 Returns: $severity, \@problemReports, \@reports

the new graph Unit (Module::GraphModuleUnit).

=over 4

=item *

$severity: the severity after analyse the unit table

=item *

\@problemReports: the reference of problems report array

=item *

\@reports: the reference of array of all the reports

=back

=head3 See Also:

Database::MotDBTableApp

=cut
sub checkUnitTablesSeverity {
  my $self = shift;
  my $unitTables = shift;
  
  my @problemReports = ();
  my @reports = ();

  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my @tableIDs = keys %$unitTables;
  my $severity = 1;
  for my $id (@tableIDs) {
    my $resHash = $unitTables->{$id};
    if ($resHash->{unitType} eq "REGEXP_UNIT") {
      my $patName = $resHash->{analyseResult}->{name};
      my $matched = $resHash->{analyseResult}->{matched};
      #the interface between regPatternMap and reportReferenceMap
      my ($reportKey, $curSeverity) = $mdbtApp->getValueFromRegPatToReport($patName, $matched, "severity");
      if (! defined $reportKey || $reportKey eq ''
        || ! defined $curSeverity || $curSeverity eq "") {
        carp "There are no valid report name or severity";
        return;
      }
      # get the maximal severity
      if ($severity < $curSeverity) {
        $severity = $curSeverity;
      }
      # set the array of problems
      if ($curSeverity > Global::Values::SEVERITY->{baseline}) {
        push @problemReports, $reportKey;
      }
      # add report to the report array
      push @reports, $reportKey;
    }
  }
  return ($severity, \@problemReports, \@reports);
}

=head2 changeRelIdxInOrigText($entryText, $relIndices)

=head3 Paramters:

=over 4

=item *

$entryText: the given Text::EntryText

=item *

$relIndices: the given relative position, it will be change into the absolute position in the original text.

=back

=head3 Returns:

the string result of the absolute position 

=head3 See Also:

Utils::Tools

=cut
sub changeRelIdxInOrigText {
  my $self = shift;
  my $entryText = shift; # Text::EntryText
  my $relIndices = shift;
  
  my $indicesArrRef = Utils::Tools->splitIndices($relIndices);
  
  my ($beginPos, $endPos) = $entryText->getTextIndex->getTextIndices;
  
  my $begin = $beginPos + $indicesArrRef->[0];
  my $end = $beginPos + $indicesArrRef->[1];
  
  my $indices = Utils::Tools->setIndices($begin, $end);
  
  return $indices;
  
}

=head2 getAllValuesOfOutputTables($attrName)

=head3 Paramters:

=over 4

=item *

$attrName: the given name of attribute

=back

=head3 Returns:

the reference of result array with the given attribute name.

This method is mostly to analyse OuputUnitTables from Analyse::AnalyseContentStrategyApi and

take the result from the unit result with type REGEXP_UNIT.

=head3 See Also:

Utils::Tools

=cut
sub getAllValuesOfOutputTables {
  my $self = shift;
  my $attrName = shift;
  if (! $self->hasUnitTables) {
    #carp "There is no valid output unit tables";
    return "";
  }
  my $outputTables = $self->getUnitTables;
  my @resArr;
  
  for my $id (keys %$outputTables) {
    my $valueHash = $outputTables->{$id};
    # only regexp_unit will be analyse
    if ($valueHash->{unitType} ne 'REGEXP_UNIT') {next;}
    
    my $target = $valueHash->{analyseResult}->{$attrName};
    
    if (! defined $target || $target eq "") { next;}
    if(ref($target) eq "ARRAY" && scalar @$target ne 0) {
      push  @resArr, @$target;
    } elsif(ref($target) eq "SCALAR") {
      push @resArr, $target;
    }   
  }
  
#  while(my ($id, $valueHash) = each %$outputTables) {
#    next if ($valueHash->{unitType} ne 'REGEXP_UNIT');
#    my $temp = $valueHash->{analyseResult}->{$attrName}; # matched or posArrRef
#    next if (! defined $temp || $temp eq "" || (ref($temp) eq "ARRAY" && scalar @$temp eq 0));
#    if (ref($temp) eq "ARRAY") {
#      push  @resArr, @$temp;
#    } else {
#      push @resArr, $temp;
#    }
#  }
  #print "res: " . $res . "\n";
  return \@resArr;
}

=head2 getAllValuesOfOutputTables($patName, $attrName)

=head3 Paramters:

=over 4

=item *

$patName: the given name of pattern

=item *

$attrName: the given name of attribute

=back

=head3 Returns:

the reference of result array with the given attribute name.

This method is mostly to analyse OuputUnitTables from Analyse::AnalyseContentStrategyApi and

take the result from the unit result with type REGEXP_UNIT and the specified pattern name.

=head3 See Also:

Utils::Tools

=cut
sub getValueOfOutputTablesByPattern {
  my $self = shift;
  my $patName = shift;
  my $attrName = shift;
  if (! $self->hasUnitTables) {
    carp "There is no valid output unit tables";
    return;
  }
  my $outputTables = $self->getUnitTables;
  my $res;
  my @hashKeys = keys %$outputTables;
  for my $key (@hashKeys) {
    my $valueHash = $outputTables->{$key};
    next if ($valueHash->{unitType} ne 'REGEXP_UNIT');
    if ($valueHash->{analyseResult}->{name} eq $patName) {
      $res = $valueHash->{analyseResult}->{$attrName}; # matched or posArrRef
      last;
    }
  }
  #print "res: " . $res . "\n";
  return $res;
} 

=head2 updateProblemReports($reportArrRef)

The serious severity is greater than Global::Values::SEVERITY->{baseline}.

=head3 Paramters:

=over 4

=item *

$reportArrRef: the reference of report array

=back

=head3 Returns:

the reference of result hashmap with the severity and report array reference of problems.

=head3 See Also:

Utils::Tools

=cut
sub updateProblemReports {
  my $self = shift;
  my $reportArrRef = shift;
  
  return if (! defined $reportArrRef || $reportArrRef eq "");
  
  my @problems;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $reportMap = $mdbtApp->getRepRefMap;
  
  my $severity = 1;
  
  for my $reportName (@$reportArrRef) {
    if (exists $reportMap->{$reportName}) {
      my $curSeverity = $reportMap->{$reportName}->{severity};
      # take the maximal severity
      if ($severity < $curSeverity) { $severity = $curSeverity;}
      if ($curSeverity > Global::Values::SEVERITY->{baseline}) {
        push @problems, $reportName;
      }
    }
  }
  my $resHash = {
    severity => $severity,
    problems => \@problems,
  };
  return $resHash;
}

=head2 mergeAnalyseUnitHashAndProblemsReports($analyseUnitHash, $problemReportHash)

Merge the two given result hashmap.

=head3 Returns:

the reference of the merged result hashmap.

=cut
sub mergeAnalyseUnitHashAndProblemsReports {
  my $self = shift;
  my $analyseUnitHash = shift;
  my $problemReportHash = shift;
  
  $analyseUnitHash->{severity} = $problemReportHash->{severity};
  $analyseUnitHash->{problems} = $problemReportHash->{problems};
  return $analyseUnitHash;
}

no Moose;
1;


#CLASS:
package Analyse::RowTypeOutputStrategy;
=encoding utf8

=head1 NAME

Analyse::RowTypeOutputStrategy - Strategy to analyse the row type area of text.

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

=head1 ATTRIBUTES

=head2 type

Type of the class (ANALYSE_STRATEGY_TYPE: Global::Values->doAnalyseStrategyType)

=cut
has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'ROW_TYPE_ANALYSE_STRATEGY',
);



=head1 METHODS

=head2 getStrategyType()

=head3 Overrides:

getStrategyType in class Analyse::UnitStrategyApi

=cut
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
=head2 input($tableHash)

The Reference of analyse hashmap will be inputed

=head3 Overrides:

input in class Analyse::UnitStrategyApi

=cut
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
=head2 clear()

=head3 Overrides:

clear in class Analyse::UnitStrategyApi

=cut
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}

=head2 output()

=head3 Overrides:

output in class Analyse::UnitStrategyApi

=cut
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

=head2 analyse()

=head3 Overrides:

analyse in class Analyse::UnitStrategyApi

=cut
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  
  my $repDBProc = Database::MotDBTableApp->getInstance->getTableProcessor("REPORT_REFERENCE_MAP");
  
  # traverse the tables
  # the ids
  my @tableIDs = keys %$tables;
  my $patName;
  for my $id  (@tableIDs) {
    my $resHash = $tables->{$id};
    if ($resHash->{unitType} eq "REGEXP_UNIT" 
      && $resHash->{analyseResult}->{matched} == 1) {
      $patName = $resHash->{analyseResult}->{name};
      last;
    }
  }
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  if (! defined $patName || $patName eq '') {
    #carp "There is no valid type of the row!";
    $patName = "";
  } else {
    $resHash->{found}= 1;
  }
  
  my $reportKey, my $type;
  if ($patName ne "") {
    #the interface between regPatternMap and reportReferenceMap
    ($reportKey, $type) = Database::MotDBTableApp->getInstance->getValueFromRegPatToReport($patName, 1, "type");
    #print Dumper("row Type: " . $type);
    
  } else {
    $type = "ENTRY_TYPE_UNKNOWN";
    $reportKey = "rpt_entry_type_7";
  }
  my @rptArr = ($reportKey);
  my $problemReportHash = $self->updateProblemReports(\@rptArr);
  
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $problemReportHash);
  $resHash->{elementType} = $type;
  
  $self->setOutputHash($resHash);
  return 1;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
package Analyse::D74HeaderOutputStrategy;
=encoding utf8

=head1 NAME

Analyse::D74HeaderOutputStrategy - Strategy to analyse the D74 Header information

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION


=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

=head1 ATTRIBUTES

=head2 type

Type of the class (ANALYSE_STRATEGY_TYPE: Global::Values->doAnalyseStrategyType)

=cut
has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'D74_HEADER_STRATEGY',
);

=head1 METHODS

=head2 getStrategyType()

=head3 Overrides:

getStrategyType in class Analyse::UnitStrategyApi

=cut
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
=head2 input($tableHash)

The Reference of analyse hashmap will be inputed

=head3 Overrides:

input in class Analyse::AnalyseContentStrategyApi

=cut
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
=head2 clear()

=head3 Overrides:

clear in class Analyse::UnitStrategyApi

=cut
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}

=head2 output()

=head3 Overrides:

output in class Analyse::UnitStrategyApi

=cut
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

=head2 getHeaderInfo()

Analyse the header Information area, and returns the  result hashmap.

=cut
sub getHeaderInfo {
  my $self = shift;
  my $indices = shift; # 0,12 (begin,end)
  if (! defined $indices || $indices eq "") {
    carp "There is no valid target indices!";
    return;
  }
  
  my $entryText = $self->getCurEntryText;
  my $origText = $entryText->getOrigText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  if (! defined $entryText || $entryText eq ""
    || ! defined $origText || $origText eq ""
    || ! defined $begin || $begin eq ""
    || ! defined $end || $end eq "") {
    carp "There are some problems in the parameters!";
    return;
  }
  
  # get the boundary indices
  my $boundaryIndices = Utils::Tools->setIndices($begin, $end);
  
  # get the taget text
  my $targetTextRef = Utils::Tools->getTextPartRefByIndices(\$origText, $boundaryIndices, $indices, "middle");
  # remove the punctuation of the header of d74
  my $headerInfoText = Utils::Tools->rmPunct($$targetTextRef);
  # get the muster of header info hash
  Global::Values->resetHeaderAttrHash;
#  my $headerInfoHash = Clone::clone(Global::Values->entryHeaderAttrHash);
  my $headerInfoHash = Global::Values->entryHeaderAttrHash;
  
  my @headerInfoArr = split " ", $headerInfoText;
  
  # for d74, only 4 attributes
  $headerInfoHash->{id} = $headerInfoArr[0];
  $headerInfoHash->{bd} = $headerInfoArr[1];
  $headerInfoHash->{se} = $headerInfoArr[2];
  $headerInfoHash->{sp} = $headerInfoArr[3];
  return $headerInfoHash;
}

=head2 analyse()

=head3 Overrides:

analyse in class Analyse::UnitStrategyApi

=head3 Returns:

The reference of result hashmap, its template is Global::Values->analyseUnitResultHash.

=cut
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  # traverse the tables
  # the ids
  my @tableIDs = keys %$tables;
  my $severity = 1;
  my $infoIndices;
  my @problems = ();
  my @reports = ();
  
  # result hashmap
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
  # get the header position
  my $patName_getD74Header = Global::Values::MODULE_PATTERN->{GET_D74_HEADER};
  my $posArrRef = $self->getValueOfOutputTablesByPattern($patName_getD74Header, "pos"); 
  # Attention: if there is found nothing, then return undef
  if(! defined $posArrRef || $posArrRef eq "" || ref($posArrRef) ne "ARRAY" || scalar @$posArrRef == 0) {
    #carp "There is some problem in the position array referece!";
  } else {
    $resHash->{pos} = $posArrRef;
    $resHash->{found} = 1;
    
    my $headerInfoHash = $self->getHeaderInfo($posArrRef->[0]);
    
    $resHash->{result} = $headerInfoHash;
  }
  # traverse the entire tables
  # attention the severity, header information if there are.
  for my $id  (@tableIDs) {
    my $resHash = $tables->{$id};
    if ($resHash->{unitType} eq "REGEXP_UNIT") {
      my $patName = $resHash->{analyseResult}->{name};
      my $matched = $resHash->{analyseResult}->{matched};
      # get the target position
      if ( $patName eq $patName_getD74Header &&  $matched == 1) {
        $infoIndices = $resHash->{analyseResult}->{pos};
      }
      #the interface between regPatternMap and reportReferenceMap
      my ($reportKey, $curSeverity) = $mdbtApp->getValueFromRegPatToReport($patName, $matched, "severity");
      if (! defined $reportKey || $reportKey eq ''
        || ! defined $curSeverity || $curSeverity eq "") {
        carp "There are no valid report name or severity";
        return;
      }
      # get the maximal severity
      if ($severity < $curSeverity) {
        $severity = $curSeverity;
      }
      # set the array of problems
      if ($curSeverity > Global::Values::SEVERITY->{baseline}) {
        push @problems, $reportKey;
      }
      # add report to the report array
      push @reports, $reportKey;
    }
  }
  
  $resHash->{severity} = $severity;
  $resHash->{problems} = \@problems;
  $resHash->{reports} = \@reports;

  $self->setOutputHash($resHash);
  return 1;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::StreckeOutputStrategy;
###########################################
package Analyse::StreckeOutputStrategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'STRECKE_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

## only for this module
sub getStreckeName {
  my $self = shift;
  my $indices = shift; # 0,12 (begin,end)
  if (! defined $indices || $indices eq "") {
    carp "There is no valid target indices!";
    return;
  }
  
  my $entryText = $self->getCurEntryText;
  my $origText = $entryText->getOrigText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  if (! defined $entryText || $entryText eq ""
    || ! defined $origText || $origText eq ""
    || ! defined $begin || $begin eq ""
    || ! defined $end || $end eq "") {
    carp "There are some problems in the parameters!";
    return;
  }
  
  # get the boundary indices
  my $boundaryIndices = Utils::Tools->setIndices($begin, $end);
  
  # get the taget text
  my $targetTextRef = Utils::Tools->getTextPartRefByIndices(\$origText, $boundaryIndices, $indices, "middle");
  
  my $stkPat = qr{(?:<\s*strecke=)([^<>]+)>}xi;
  
  if ($$targetTextRef =~ m/$stkPat/) {
    return $1;
  } else {
    return;
  }
  
}


# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "found"   => 1/0,
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  # traverse the tables
  # the ids
  my @tableIDs = keys %$tables;
  my $severity = 1;
  my $infoIndices;
  my @problems = ();
  my @reports = ();
  
  # result hashmap
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
  # get the header position
  my $patName_getStrecke = Global::Values::MODULE_PATTERN->{GET_STRECKE_AREA};
  my $posArrRef = $self->getValueOfOutputTablesByPattern($patName_getStrecke, "pos"); 
  # Attention: if there is found nothing, then return undef
  if(! defined $posArrRef || $posArrRef eq "" || ref($posArrRef) ne "ARRAY" || scalar @$posArrRef == 0) {
    #carp "There is some problem in the position array referece!";
  } else {
    $resHash->{pos} = $posArrRef;
    $resHash->{found} = 1;
    
    # NTODO:
    my $stkName = $self->getStreckeName($posArrRef->[0]);
    
    $resHash->{result} = $stkName;
  }
  # traverse the entire tables
  # attention the severity, header information if there are.
  for my $id  (@tableIDs) {
    my $resHash = $tables->{$id};
    if ($resHash->{unitType} eq "REGEXP_UNIT") {
      my $patName = $resHash->{analyseResult}->{name};
      my $matched = $resHash->{analyseResult}->{matched};
      # get the target position
      if ( $patName eq $patName_getStrecke &&  $matched == 1) {
        $infoIndices = $resHash->{analyseResult}->{pos};
      }
      #the interface between regPatternMap and reportReferenceMap
      my ($reportKey, $curSeverity) = $mdbtApp->getValueFromRegPatToReport($patName, $matched, "severity");
      if (! defined $reportKey || $reportKey eq ''
        || ! defined $curSeverity || $curSeverity eq "") {
        carp "There are no valid report name or severity";
        return;
      }
      # get the maximal severity
      if ($severity < $curSeverity) {
        $severity = $curSeverity;
      }
      # set the array of problems
      if ($curSeverity > Global::Values::SEVERITY->{baseline}) {
        push @problems, $reportKey;
      }
      # add report to the report array
      push @reports, $reportKey;
    }
  }
  
  $resHash->{severity} = $severity;
  $resHash->{problems} = \@problems;
  $resHash->{reports} = \@reports;

  $self->setOutputHash($resHash);
  return 1;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;



#CLASS:
###########################################
# package Analyse::CharErrorsOutputStrategy;
###########################################
package Analyse::CharErrorsOutputStrategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'SEARCH_ERROR_CHARS_STRATEGY',
);

########################
# variables
########################

########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}
sub getCharErrorsContent {
  my $self = shift;
  my $indicesRef = shift; # ["0,12", "22,32"] (begin,end)
  if (! defined $indicesRef || $indicesRef eq "" || ref($indicesRef) ne "ARRAY") {
    carp "There is no valid target indices!";
    return;
  }
  
  my $entryText = $self->getCurEntryText;
  my $origText = $entryText->getOrigText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  if (! defined $entryText || $entryText eq ""
    || ! defined $origText || $origText eq ""
    || ! defined $begin || $begin eq ""
    || ! defined $end || $end eq "") {
    carp "There are some problems in the parameters!";
    return;
  }
  
  # get the boundary indices
  my $boundaryIndices = Utils::Tools->setIndices($begin, $end);
  my @resTextArr;
  for my $indices (@$indicesRef) {
    # get the taget text
    my $targetTextRef = Utils::Tools->getTextPartRefByIndices(\$origText, $boundaryIndices, $indices, "middle");    
    if (! defined $targetTextRef || $targetTextRef eq "") {
      croak "There are something wrong with the given indices: $indices !";
    }
    push @resTextArr, $$targetTextRef;
  }
  
  return \@resTextArr;
}

# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17", "32,55"]
#  "found"   => 1/0,
#  "element" => $element;
#  "content" => $content;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  
  # output unit tables
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");

  # result hashmap
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  # get the header position
#  my $patName_getAuthor = Global::Values::MODULE_PATTERN->{GET_AUTHOR};
  my $posArrRef = $self->getAllValuesOfOutputTables("pos"); 
  if(! defined $posArrRef || $posArrRef eq "" || ref($posArrRef) ne "ARRAY" || scalar @$posArrRef == 0) {
    #carp "There is some problem in the position array referece!";
    
  } else {
    $entryText->setEntryTextAttr("hasCharErrors", 1);
    
    my $posCount = scalar @$posArrRef;
    for (my $i = 0; $i < $posCount; $i++) {
      my $tempPos = $self->changeRelIdxInOrigText($entryText, $posArrRef->[$i]);
      $posArrRef->[$i] = $tempPos;
    }
    
    $resHash->{pos} = $posArrRef;
    $resHash->{found} = 1;
    
    my $errorsTextArrRef = $self->getCharErrorsContent($resHash->{pos});
    # the reference of text array 
    $resHash->{element} = $errorsTextArrRef;
  }
  
  # traverse the tables
  my ($severity, $problemsArrRef, $reportsArrRef) = $self->checkUnitTablesSeverity($tables);
  
  $resHash->{severity} = $severity;
  $resHash->{problems} = $problemsArrRef;
  $resHash->{reports} = $reportsArrRef;

  $self->setOutputHash($resHash);
  return 1;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;



#CLASS:
###########################################
# package Analyse::UnknownpartsOutputStrategy;
###########################################
package Analyse::UnknownpartsOutputStrategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_UNKNOWNPART_STRATEGY',
);

########################
# variables
########################

########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}
sub getUnknownpartsContent {
  my $self = shift;
  my $indicesRef = shift; # ["0,12", "22,32"] (begin,end)
  if (! defined $indicesRef || $indicesRef eq "" || ref($indicesRef) ne "ARRAY") {
    carp "There is no valid target indices!";
    return;
  }
  
  my $entryText = $self->getCurEntryText;
  my $origText = $entryText->getOrigText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  if (! defined $entryText || $entryText eq ""
    || ! defined $origText || $origText eq ""
    || ! defined $begin || $begin eq ""
    || ! defined $end || $end eq "") {
    carp "There are some problems in the parameters!";
    return;
  }
  
  # get the boundary indices
  my $boundaryIndices = Utils::Tools->setIndices($begin, $end);
  my @resTextArr;
  for my $indices (@$indicesRef) {
    # get the taget text
    my $targetTextRef = Utils::Tools->getTextPartRefByIndices(\$origText, $boundaryIndices, $indices, "middle");    
    if (! defined $targetTextRef || $targetTextRef eq "") {
      croak "There are something wrong with the given indices: $indices !";
    }
    push @resTextArr, $$targetTextRef;
  }
  return \@resTextArr;
}

# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17", "32,55"]
#  "found"   => 1/0,
#  "element" => $element;
#  "content" => $content;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  
  # output unit tables
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");

  # result hashmap
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  # get the header position
#  my $patName_getUnknown = Global::Values::MODULE_PATTERN->{GET_UNKNOWNPART};
  my $posArrRef = $self->getAllValuesOfOutputTables("pos"); 
  if(! defined $posArrRef || $posArrRef eq "" || ref($posArrRef) ne "ARRAY" || scalar @$posArrRef == 0) {
    #carp "There is some problem in the position array referece!";
    
  } else {
    $entryText->setEntryTextAttr("hasUnknownparts", 1);
    
    $resHash->{pos} = $posArrRef;
    $resHash->{found} = 1;
    
    my $errorsTextArrRef = $self->getUnknownpartsContent($posArrRef);
    # the reference of text array 
    $resHash->{element} = $errorsTextArrRef;
  }
  
  # traverse the tables
  my ($severity, $problemsArrRef, $reportsArrRef) = $self->checkUnitTablesSeverity($tables);
  
  $resHash->{severity} = $severity;
  $resHash->{problems} = $problemsArrRef;
  $resHash->{reports} = $reportsArrRef;

  $self->setOutputHash($resHash);
  return 1;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;




#CLASS:
###########################################
# package Analyse::AuthorOutputStrategy;
###########################################
package Analyse::AuthorOutputStrategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_AUTHOR_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}
sub getAuthorContent {
  my $self = shift;
  my $indices = shift; # 0,12 (begin,end)
  if (! defined $indices || $indices eq "") {
    carp "There is no valid target indices!";
    return;
  }
  
  my $entryText = $self->getCurEntryText;
  my $origText = $entryText->getOrigText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  if (! defined $entryText || $entryText eq ""
    || ! defined $origText || $origText eq ""
    || ! defined $begin || $begin eq ""
    || ! defined $end || $end eq "") {
    carp "There are some problems in the parameters!";
    return;
  }
  
  # get the boundary indices
  my $boundaryIndices = Utils::Tools->setIndices($begin, $end);
  
  # get the taget text
  my $targetTextRef = Utils::Tools->getTextPartRefByIndices(\$origText, $boundaryIndices, $indices, "middle");
  return $targetTextRef;
}

# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $element;
#  "content" => $content;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  
  # output unit tables
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");

  # result hashmap
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  # get the header position
  my $patName_getAuthor = Global::Values::MODULE_PATTERN->{GET_AUTHOR};
  my $posArrRef = $self->getValueOfOutputTablesByPattern($patName_getAuthor, "pos"); 
  if(! defined $posArrRef || $posArrRef eq "" || ref($posArrRef) ne "ARRAY" || scalar @$posArrRef == 0) {
    #carp "There is some problem in the position array referece!";
    # do nothing!
  } else {
      # change relative indices into the original text
      my $authorIndices = $self->changeRelIdxInOrigText($entryText, $posArrRef->[0]);
      
      if (scalar @$posArrRef > 1) {
        $resHash->{eleNum} = scalar @$posArrRef;
      }
      
      $posArrRef->[0] = $authorIndices;
      $resHash->{pos} = $posArrRef;
      $resHash->{found} = 1;
      
      my $authorTextRef = $self->getAuthorContent($authorIndices);
      
      $resHash->{result} = $authorTextRef;
      $resHash->{element} = $authorTextRef;
      
      my $content = substr($$authorTextRef, 1, -1); # take away the bracket.
      
      $resHash->{content} = \$content;
  }
  
  # traverse the tables
  my ($severity, $problemsArrRef, $reportsArrRef) = $self->checkUnitTablesSeverity($tables);
  
  $resHash->{severity} = $severity;
  $resHash->{problems} = $problemsArrRef;
  $resHash->{reports} = $reportsArrRef;

  $self->setOutputHash($resHash);
  return 1;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package Analyse::DitoDescOutputStrategy;
###########################################
package Analyse::DitoDescOutputStrategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_DITO_DESC_STRATEGY',
);

########################
# variables
########################

########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}
sub getPartContentByIndices {
  my $self = shift;
  my $indices = shift; # 0,12 (begin,end)
  my $areaType = shift;
  if (! defined $indices || $indices eq ""
    || ! defined $areaType || $areaType eq ""
  ) {
    carp "There is no valid target indices or area type!";
    return;
  }
  
  my $entryText = $self->getCurEntryText;
  my $origText = $entryText->getOrigText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  if (! defined $entryText || $entryText eq ""
    || ! defined $origText || $origText eq ""
    || ! defined $begin || $begin eq ""
    || ! defined $end || $end eq "") {
    carp "There are some problems in the parameters!";
    return;
  }
  
  # get the boundary indices
  my $boundaryIndices = Utils::Tools->setIndices($begin, $end);
  
  # get the taget text
  my $targetTextRef = Utils::Tools->getTextPartRefByIndices(\$origText, $boundaryIndices, $indices, $areaType);
  return $targetTextRef;
}

# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $element;
#  "content" => $content;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  
  # output unit tables
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");

  # result hashmap
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  # get the header position
  my $patName_getDitoDesc= Global::Values::MODULE_PATTERN->{GET_DITO_DESC};
  my $posArrRef = $self->getValueOfOutputTablesByPattern($patName_getDitoDesc, "pos"); 
  if(! defined $posArrRef || $posArrRef eq "" || ref($posArrRef) ne "ARRAY" || scalar @$posArrRef == 0) {
    #carp "There is some problem in the position array referece!";
    # do nothing!
  } else {
      # change relative indices into the original text
      my $ditoDescIndices = $self->changeRelIdxInOrigText($entryText, $posArrRef->[0]);
      
      if (scalar @$posArrRef > 1) {
        $resHash->{eleNum} = scalar @$posArrRef;
      }
      
      $posArrRef->[0] = $ditoDescIndices;
      $resHash->{pos} = $posArrRef;
      $resHash->{found} = 1;
      
      my $ditoDescTextRef = $self->getPartContentByIndices($ditoDescIndices, "middle");
      
      $resHash->{result} = $ditoDescTextRef;
      $resHash->{element} = $ditoDescTextRef;
  }
  
  # traverse the content tables
  my ($severity, $problemsArrRef, $reportsArrRef) = $self->checkUnitTablesSeverity($tables);
  
  $resHash->{severity} = $severity;
  $resHash->{problems} = $problemsArrRef;
  $resHash->{reports} = $reportsArrRef;

  $self->setOutputHash($resHash);
  return 1;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::PipeOutputStrategy;
###########################################
package Analyse::PipeOutputStrategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_PIPE_STRATEGY',
);

########################
# variables
########################

########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}
sub getPartContentByIndices {
  my $self = shift;
  my $indices = shift; # 0,12 (begin,end)
  my $areaType = shift;
  if (! defined $indices || $indices eq ""
    || ! defined $areaType || $areaType eq ""
  ) {
    carp "There is no valid target indices or area type!";
    return;
  }
  
  my $entryText = $self->getCurEntryText;
  my $origText = $entryText->getOrigText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  if (! defined $entryText || $entryText eq ""
    || ! defined $origText || $origText eq ""
    || ! defined $begin || $begin eq ""
    || ! defined $end || $end eq "") {
    carp "There are some problems in the parameters!";
    return;
  }
  
  # get the boundary indices
  my $boundaryIndices = Utils::Tools->setIndices($begin, $end);
  
  # get the taget text
  my $targetTextRef = Utils::Tools->getTextPartRefByIndices(\$origText, $boundaryIndices, $indices, $areaType);
  return $targetTextRef;
}

# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $element;
#  "content" => $content;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  
  # output unit tables
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");

  # result hashmap
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  # get the header position
  my $patName_getPipe = Global::Values::MODULE_PATTERN->{GET_START_PIPE};
  my $posArrRef = $self->getValueOfOutputTablesByPattern($patName_getPipe, "pos"); 
  if(! defined $posArrRef || $posArrRef eq "" || ref($posArrRef) ne "ARRAY" || scalar @$posArrRef == 0) {
    #carp "There is some problem in the position array referece!";
    # do nothing!
  } else {
      # change relative indices into the original text
      my $startPipeIndices = $self->changeRelIdxInOrigText($entryText, $posArrRef->[0]);
            
      $posArrRef->[0] = $startPipeIndices;
      $resHash->{pos} = $posArrRef;
      $resHash->{found} = 1;
      
      my $startPipeTextRef = $self->getPartContentByIndices($startPipeIndices, "middle");
      
      $resHash->{result} = $startPipeTextRef;
      $resHash->{element} = $startPipeTextRef;
  }
  
  # traverse the content tables
  my ($severity, $problemsArrRef, $reportsArrRef) = $self->checkUnitTablesSeverity($tables);
  
  $resHash->{severity} = $severity;
  $resHash->{problems} = $problemsArrRef;
  $resHash->{reports} = $reportsArrRef;

  $self->setOutputHash($resHash);
  return 1;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::TitleOutputStrategy;
###########################################
package Analyse::TitleOutputStrategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_TITLE_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}
sub getTitleContent {
  my $self = shift;
  my $indices = shift; # 0,12 (begin,end)
  if (! defined $indices || $indices eq "") {
    carp "There is no valid target indices!";
    return;
  }
  
  my $entryText = $self->getCurEntryText;
  my $origText = $entryText->getOrigText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  if (! defined $entryText || $entryText eq ""
    || ! defined $origText || $origText eq ""
    || ! defined $begin || $begin eq ""
    || ! defined $end || $end eq "") {
    carp "There are some problems in the parameters!";
    return;
  }
  
  # get the boundary indices
  my $boundaryIndices = Utils::Tools->setIndices($begin, $end);
  
  # get the taget text
  my $targetTextRef = Utils::Tools->getTextPartRefByIndices(\$origText, $boundaryIndices, $indices, "middle");
  return $targetTextRef;
}

sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  
  # output unit tables
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");

# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => [$Element, ...];
#  "content" => [$Content, ...];
#  "result" => "";
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
  # result hashmap
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  # get the header position
  my $patName_getTitle = Global::Values::MODULE_PATTERN->{GET_TITLE};
  my $posArrRef = $self->getValueOfOutputTablesByPattern($patName_getTitle, "pos"); 
  # Attention: if there is found nothing, then return undef
  if(! defined $posArrRef || $posArrRef eq "" || ref($posArrRef) ne "ARRAY" || scalar @$posArrRef == 0) {
    #carp "There is some problem in the position array referece!";
    #return $resHash;
  } else {
    # change relative indices into the original text
    my $titleIndices = $self->changeRelIdxInOrigText($entryText, $posArrRef->[0]);
    
#    my $origText = $entryText->getOrigText;
#    my $tperPos = $entryText->getTextPointer->getPosition;
#    $titleIndices = Utils::Tools->getAreaIndices(\$origText, "front", $titleIndices, $tperPos);
    
    $posArrRef->[0] = $titleIndices;
    my $titleTextRef = $self->getTitleContent($titleIndices);
    
    if (Utils::Tools->trim($$titleTextRef) ne "") {
      $resHash->{pos} = $posArrRef;
      $resHash->{found} = 1;
      $resHash->{result} = $titleTextRef;
    }
    
  }
  # traverse the tables
  my ($severity, $problemsArrRef, $reportsArrRef) = $self->checkUnitTablesSeverity($tables);
  
  $resHash->{severity} = $severity;
  $resHash->{problems} = $problemsArrRef;
  $resHash->{reports} = $reportsArrRef;

  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;




#CLASS:
###########################################
# package Analyse::CollationOutputUnitStrategy;
###########################################
package Analyse::CollationOutputUnitStrategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_COLLATION_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}


sub getCollationContent {
  my $self = shift;
  my $indices = shift; # 0,12 (begin,end)
  if (! defined $indices || $indices eq "") {
    carp "There is no valid target indices!";
    return;
  }
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $entryText = $self->getCurEntryText;
  my $origText = $entryText->getOrigText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  if (! defined $entryText || $entryText eq ""
    || ! defined $origText || $origText eq ""
    || ! defined $begin || $begin eq ""
    || ! defined $end || $end eq "") {
    carp "There are some problems in the parameters!";
    return;
  }
  
  # get the boundary indices
  my $boundaryIndices = Utils::Tools->setIndices($begin, $end);
  
  # get the taget text
  my $targetTextRef = Utils::Tools->getTextPartRefByIndices(\$origText, $boundaryIndices, $indices, "middle");
  my $content = "";
  if (! $entryText->getEntryTextAttr("hasAuthor") && ! $entryText->getEntryTextAttr("hasSigel")) {
    my $pattern = $mdbtApp->getTableProcessor("REG_PATTERN_ENGINE_MAP")->search("collation_3", "content");
    if ($$targetTextRef =~ m/$pattern/) {
      $content = $1;
    }
  } else {
    $content = $$targetTextRef;
  }
  
  # returns the target text part and content
  return ($targetTextRef, \$content);
}



# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "found"   => 1/0,
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  
  # output unit tables
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");

  # result hashmap
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  # get the collation position
  my $patName_getCollation;
  # there are author or sigel in the entry
  if ($entryText->getEntryTextAttr("hasAuthor") || $entryText->getEntryTextAttr("hasSigel")) {
    $patName_getCollation = Global::Values::MODULE_PATTERN->{GET_Collation_AUTHOR_SIGEL};
  } else {
    $patName_getCollation = Global::Values::MODULE_PATTERN->{GET_Collation};
  }
  my $posArrRef = $self->getValueOfOutputTablesByPattern($patName_getCollation, "pos"); 
  # Attention: if there is found nothing, then return undef
  if(! defined $posArrRef || $posArrRef eq "" || ref($posArrRef) ne "ARRAY" 
  || scalar @$posArrRef == 0) {
    #carp "There is some problem in the position array referece!";
    # do nothing
  } else {
    # change relative indices into the original text
    my $collationIndices = $self->changeRelIdxInOrigText($entryText, $posArrRef->[0]);
    
    $posArrRef->[0] = $collationIndices;
    $resHash->{pos} = $posArrRef;
    $resHash->{found} = 1;
    
    my ($collationTextRef, $collationContent) = $self->getCollationContent($collationIndices);
    $resHash->{result} = $collationTextRef;
    $resHash->{element} = $collationTextRef;
    $resHash->{content} = $collationContent;
  }
  # traverse the tables
  my ($severity, $problemsArrRef, $reportsArrRef) = $self->checkUnitTablesSeverity($tables);
  
  $resHash->{severity} = $severity;
  $resHash->{problems} = $problemsArrRef;
  $resHash->{reports} = $reportsArrRef;

  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;




#CLASS:
###########################################
# package Analyse::SigelOutputUnitStrategy;
###########################################
package Analyse::SigelOutputUnitStrategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_SIGEL_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
  # given eine problem report with $repDBProc
  if($tables->{eleNum} > 1) {
    push @reports, "rpt_sigel_13";
    $entryText->setEntryTextAttr("hasMoreSigel", 1);
  }
  
  if ($tables->{hasTagProblem}) {
    push @reports, "rpt_sigel_14";
  }
  
  # sigel result hash analyse and severity
  if(! $tables->{hasElement}) {
    my $entryTextType = $entryText->getEntryType;
    # lemma
    if ($entryTextType eq "ENTRY_TYPE_LEMMA" || $entryTextType eq "ENTRY_TYPE_LEMMA_HTML") {
      push @reports, "rpt_sigel_2";
    }
  } else {
    
     # sigel element <s>...</s>
    my $sigelElement = $tables->{elements}->[0];
    my $sigelContent = $tables->{content}->[0];
    $resHash->{element} = \$sigelElement;
    $resHash->{content} = \$sigelContent;
    # the indices of sigel element
    my $posArrRef = $tables->{elePos};
    if (scalar @$posArrRef > 1) {
      $resHash->{eleNum} = scalar @$posArrRef;
    }
    my $indices = $self->changeRelIdxInOrigText($entryText, $posArrRef->[0]);
    
#    my $indicesArrRef = Utils::Tools->splitIndices($posArrRef->[0]);
#    my $tperPos = $entryText->getTextPointer->getPosition;
#    my $begin = $tperPos + $indicesArrRef->[0];
#    my $end = $tperPos + $indicesArrRef->[1];
#    my $indices = Utils::Tools->setIndices($begin, $end);
    
    $posArrRef->[0] = $indices;
    # get the real positions of the original text
    $resHash->{pos} = $posArrRef;
    
    $resHash->{found} = 1;
  }
  
  my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;




#CLASS:
###########################################
# package Analyse::ReferenceRegOutputUnitStrategy;
###########################################
package Analyse::ReferenceRegOutputUnitStrategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_REFERENCE_REG_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}
sub getRefContent {
  my $self = shift;
  my $indices = shift; # 0,12 (begin,end)
  if (! defined $indices || $indices eq "") {
    carp "There is no valid target indices!";
    return;
  }
  
  my $entryText = $self->getCurEntryText;
  my $origText = $entryText->getOrigText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  if (! defined $entryText || $entryText eq ""
    || ! defined $origText || $origText eq ""
    || ! defined $begin || $begin eq ""
    || ! defined $end || $end eq "") {
    carp "There are some problems in the parameters!";
    return;
  }
  
  # get the boundary indices
  my $boundaryIndices = Utils::Tools->setIndices($begin, $end);
  
  # get the taget text
  my $targetTextRef = Utils::Tools->getTextPartRefByIndices(\$origText, $boundaryIndices, $indices, "middle");
  return $targetTextRef;
}

# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $element;
#  "content" => $content;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  
  # output unit tables
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");

  # result hashmap
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  # get the header position
  my $patName_getRef = Global::Values::MODULE_PATTERN->{GET_REFERENCE};
  my $posArrRef = $self->getValueOfOutputTablesByPattern($patName_getRef, "pos"); 
  if(! defined $posArrRef || $posArrRef eq "" || ref($posArrRef) ne "ARRAY" 
  || scalar @$posArrRef == 0) {
    #carp "There is some problem in the position array referece!";
  } else {
    # set has reference attribute
    $entryText->setEntryTextAttr("hasReference", 1);
    # change relative indices into the original text
    my $refIndices = $self->changeRelIdxInOrigText($entryText, $posArrRef->[0]);
    
    $posArrRef->[0] = $refIndices;
    $resHash->{pos} = $posArrRef;
    $resHash->{found} = 1;
    $resHash->{elementType} = "VERWEIS_ELEMENT";
    
    my $refTextRef = $self->getRefContent($refIndices);
    
    $resHash->{result} = $refTextRef;
    $resHash->{element} = $refTextRef;
    $resHash->{content} = $refTextRef;
  }
  # traverse the tables
  my ($severity, $problemsArrRef, $reportsArrRef) = $self->checkUnitTablesSeverity($tables);
  
  $resHash->{severity} = $severity;
  $resHash->{problems} = $problemsArrRef;
  $resHash->{reports} = $reportsArrRef;

  $self->setOutputHash($resHash);
  return 1;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;




#CLASS:
###########################################
# package Analyse::ReferenceHTMLOutputUnitStrategy;
###########################################
package Analyse::ReferenceHTMLOutputUnitStrategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_REFERENCE_HTML_STRATEGY',
);

########################
# variables
########################
########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
  # given eine problem report with $repDBProc
#  if($tables->{eleNum} > 1) {
#    push @reports, "";
#  }
  
  # sigel result hash analyse and severity
  if(! $tables->{hasElement}) {
    push @reports, "rpt_reference_2";
  } else {
    # set has reference attribute
    $entryText->setEntryTextAttr("hasReference", 1);
    # sigel element <s>...</s>
    my $refElement = $tables->{elements}->[0];
    my $refContent = $tables->{content}->[0];
    $resHash->{element} = \$refElement;
    $resHash->{content} = \$refContent;
    $resHash->{elementType} = "VERWEIS_ELEMENT";
    # the indices of sigel element
    my $posArrRef = $tables->{elePos};
    
    my $indices = $self->changeRelIdxInOrigText($entryText, $posArrRef->[0]);
    
#    my $indicesArrRef = Utils::Tools->splitIndices($posArrRef->[0]);
#    my $tperPos = $entryText->getTextPointer->getPosition;
#    my $begin = $tperPos + $indicesArrRef->[0];
#    my $end = $tperPos + $indicesArrRef->[1];
#    my $indices = Utils::Tools->setIndices($begin, $end);
    
    $posArrRef->[0] = $indices;
    # get the real positions of the original text
    $resHash->{pos} = $posArrRef;
    
    $resHash->{found} = 1;
  }
  
  my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  
  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;




#CLASS:
###########################################
# package Analyse::ReferenceLemmaOutputUnitStrategy;
###########################################
package Analyse::ReferenceLemmaOutputUnitStrategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_REFERENCE_LEMMA_HTML_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
  # given eine problem report with $repDBProc

  
  if ($tables->{hasTagProblem}) {
    push @reports, "rpt_reference_6";
  }
  
  # sigel result hash analyse and severity
  if(! $tables->{hasElement}) {
    push @reports, "rpt_reference_4";
  } else {
    # set has reference attribute
    $entryText->setEntryTextAttr("hasReferenceLemma", 1);
     # sigel element <s>...</s>
    my $refElement = $tables->{elements}->[0];
    my $refContent = $tables->{content}->[0];
    $resHash->{element} = \$refElement;
    $resHash->{content} = \$refContent;
    $resHash->{elementType} = "VERWEIS_LEMMA_ELEMENT";
    # the indices of found element
    my $posArrRef = $tables->{elePos};
    
    my $indices = $self->changeRelIdxInOrigText($entryText, $posArrRef->[0]);
#    my $indicesArrRef = Utils::Tools->splitIndices($posArrRef->[0]);
#    my $tperPos = $entryText->getTextPointer->getPosition;
#    my $begin = $tperPos + $indicesArrRef->[0];
#    my $end = $tperPos + $indicesArrRef->[1];
#    my $indices = Utils::Tools->setIndices($begin, $end);
#    $posArrRef->[0] = $indices;
    # get the real positions of the original text
    $posArrRef->[0] = $indices;
    $resHash->{pos} = $posArrRef;
#    $resHash->{pos} = $indicesArrRef;
    
    $resHash->{found} = 1;
  }
  
  
    my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  
  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;



#CLASS:
###########################################
# package Analyse::Lemma_Output_Unit_Strategy;
###########################################
package Analyse::Lemma_Output_Unit_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_LEMMA_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}



#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
  if($tables->{eleNum} > 1) {
    #given eine problem report with $repDBProc
    push @reports, "rpt_lemma_8";
    $entryText->setEntryTextAttr("hasMoreLemmata", 1);
  }
  if ($tables->{hasTagProblem}) {
    push @reports, "rpt_lemma_9";
  }
  
  # LEMMA result hash analyse and severity
  if(! $tables->{hasElement}) {
    push @reports, "rpt_lemma_1_1";
  } else {
    # set has reference attribute
    $entryText->setEntryTextAttr("hasLemma", 1);
    
    # lemma element <l>...</l>
    my $lemElement = $tables->{elements}->[0];
    my $lemContent = $tables->{content}->[0];
    $resHash->{element} = \$lemElement;
    $resHash->{content} = \$lemContent;
    $resHash->{elementType} = "LEMMA_ELEMENT";
    # the indices of lemma element
    my $posArrRef = $tables->{elePos};
    
    
    my $indices = $self->changeRelIdxInOrigText($entryText, $posArrRef->[0]);
    
#    my $indicesArrRef = Utils::Tools->splitIndices($posArrRef->[0]);
#    my $tperPos = $entryText->getTextPointer->getPosition;
#    my $begin = $tperPos + $indicesArrRef->[0];
#    my $end = $tperPos + $indicesArrRef->[1];
#    my $indices = Utils::Tools->setIndices($begin, $end);
    
    $posArrRef->[0] = $indices;
    # get the real positions of the original text
    $resHash->{pos} = $posArrRef;
    
    $resHash->{found} = 1;
  }
  
  
  my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  
  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package Analyse::Sic_Output_Unit_Strategy;
###########################################
package Analyse::Sic_Output_Unit_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_SIC_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
#  if($tables->{eleNum} > 1) {
#    #given eine problem report with $repDBProc
#    push @reports, "rpt_lemma_8";
#  }
  if ($tables->{hasTagProblem}) {
    push @reports, "rpt_sic_3";
  }
  # LEMMA result hash analyse and severity
  if(! $tables->{hasElement}) {
    push @reports, "rpt_sic_2";
  } else {
    $entryText->setEntryTextAttr("hasSic", 1);
    #Attention: There could be more elements and contents with form <sic>...</sic>
    $resHash->{element} = $tables->{elements};
    $resHash->{content} = $tables->{content};
    $resHash->{elementType} = "SIC_ELEMENT";
    # the indices of lemma element
    my $posArrRef = $tables->{elePos};
    
    $resHash->{pos} = $posArrRef;
    
    $resHash->{found} = 1;
  }
  
  my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  
  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::EntryElement_Output_Unit_Strategy;
###########################################
package Analyse::EntryElement_Output_Unit_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_HTML_ENTRY_ELEMENT_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
#  if($tables->{eleNum} > 1) {
#    #given eine problem report with $repDBProc
#    push @reports, "rpt_lemma_8";
#  }
  if ($tables->{hasTagProblem}) {
    push @reports, "rpt_entry_3";
  }
  # LEMMA result hash analyse and severity
  if(! $tables->{hasElement}) {
    push @reports, "rpt_entry_2";
  } else {
    $entryText->setEntryTextAttr("hasEntry", 1);
    #Attention: There could be more elements and contents with form <sic>...</sic>
    $resHash->{element} = $tables->{elements};
    $resHash->{content} = $tables->{content};
    $resHash->{elementType} = "ENTRY_ELEMENT";
    # the indices of lemma element
    my $posArrRef = $tables->{elePos};
    if (scalar @$posArrRef > 1) {
      $entryText->setEntryTextAttr("hasMoreEntry", 1);
      $resHash->{eleNum} = scalar @$posArrRef;
      push @reports, "rpt_entry_3";
    }
    
    $resHash->{pos} = $posArrRef;
    
    $resHash->{found} = 1;
    
    # entry attributes
    $resHash->{result} = $tables->{attr}->[0];
  }
  
  my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  
  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Sub_Output_Unit_Strategy;
###########################################
package Analyse::Sub_Output_Unit_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_SUB_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  

  if ($tables->{hasTagProblem}) {
    push @reports, "rpt_sub_3";
  }
  # LEMMA result hash analyse and severity
  if(! $tables->{hasElement}) {
    push @reports, "rpt_sub_2";
  } else {
    $entryText->setEntryTextAttr("hasSub", 1);
    #Attention: There could be more elements and contents with form <sic>...</sic>
    $resHash->{element} = $tables->{elements};
    $resHash->{content} = $tables->{content};
    $resHash->{elementType} = "SUB_ELEMENT";
    # the indices of lemma element
    my $posArrRef = $tables->{elePos};
    $resHash->{pos} = $posArrRef;
    
    $resHash->{found} = 1;
  }
  
    my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  
  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package Analyse::Sup_Output_Unit_Strategy;
###########################################
package Analyse::Sup_Output_Unit_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_SUP_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
#  if($tables->{eleNum} > 1) {
#    #given eine problem report with $repDBProc
#    push @reports, "rpt_lemma_8";
#  }

  if ($tables->{hasTagProblem}) {
    push @reports, "rpt_sup_3";
  }
  
  # LEMMA result hash analyse and severity
  if(! $tables->{hasElement}) {
    push @reports, "rpt_sup_2";
  } else {
    $entryText->setEntryTextAttr("hasSup", 1);
    #Attention: There could be more elements and contents with form <sic>...</sic>
    $resHash->{element} = $tables->{elements};
    $resHash->{content} = $tables->{content};
    $resHash->{elementType} = "SUP_ELEMENT";
    # the indices of lemma element
    my $posArrRef = $tables->{elePos};
    $resHash->{pos} = $posArrRef;
    
    $resHash->{found} = 1;
  }
  
    my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;



#CLASS:
###########################################
# package Analyse::Corr_Output_Unit_Strategy;
###########################################
package Analyse::Corr_Output_Unit_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_CORR_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
  if ($tables->{hasTagProblem}) {
    push @reports, "rpt_corr_3";
  }
  
  # corr-element result hash analyse and severity
  if(! $tables->{hasElement}) {
    push @reports, "rpt_corr_2";
  } else {
    $entryText->setEntryTextAttr("hasCorr", 1);
    
    #Attention: There could be more elements and contents with form <sic>...</sic>
    $resHash->{element} = $tables->{elements};
    $resHash->{content} = $tables->{content};
    $resHash->{elementType} = "CORR_ELEMENT";
    # the indices of lemma element
    my $posArrRef = $tables->{elePos};
    
    $resHash->{pos} = $posArrRef;
    
    $resHash->{found} = 1;
  }
  
  my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;



#CLASS:
###########################################
# package Analyse::Dop_Output_Unit_Strategy;
###########################################
package Analyse::Dop_Output_Unit_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_DOP_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
  if ($tables->{hasTagProblem}) {
    push @reports, "rpt_dop_3";
  }
  
  # corr-element result hash analyse and severity
  if(! $tables->{hasElement}) {
    push @reports, "rpt_dop_2";
  } else {
    $entryText->setEntryTextAttr("hasDop", 1);
    #Attention: There could be more elements and contents with form <sic>...</sic>
    $resHash->{element} = $tables->{elements};
    $resHash->{content} = $tables->{content};
    $resHash->{elementType} = "DOP_ELEMENT";
    # the indices of lemma element
    my $posArrRef = $tables->{elePos};
    
    $resHash->{pos} = $posArrRef;
    
    $resHash->{found} = 1;
  }
  
    my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Rep_Output_Unit_Strategy;
###########################################
package Analyse::Rep_Output_Unit_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_REP_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
  if ($tables->{hasTagProblem}) {
    push @reports, "rpt_rep_3";
  }
  
  # corr-element result hash analyse and severity
  if(! $tables->{hasElement}) {
    push @reports, "rpt_rep_2";
  } else {
    $entryText->setEntryTextAttr("hasRep", 1);
    #Attention: There could be more elements and contents with form <sic>...</sic>
    $resHash->{element} = $tables->{elements};
    $resHash->{content} = $tables->{content};
    $resHash->{elementType} = "REP_ELEMENT";
    # the indices of lemma element
    my $posArrRef = $tables->{elePos};
    $resHash->{pos} = $posArrRef;
    
    $resHash->{found} = 1;
  }
  
  
  my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package Analyse::Dito_HTML_Output_Unit_Strategy;
###########################################
package Analyse::Dito_HTML_Output_Unit_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_DITO_HTML_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
  #  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
  if ($tables->{hasTagProblem}) {
    push @reports, "rpt_dito_html_3";
  }
  
  # corr-element result hash analyse and severity
  if(! $tables->{hasElement}) {
    push @reports, "rpt_dito_html_2";
  } else {
    $entryText->setEntryTextAttr("hasDitoEle", 1);
    #Attention: There could be more elements and contents with form <sic>...</sic>
    $resHash->{element} = $tables->{elements};
    $resHash->{content} = $tables->{content};
    $resHash->{elementType} = "DITO_ELEMENT";
    # the indices of lemma element
    my $posArrRef = $tables->{elePos};
    $resHash->{pos} = $posArrRef;
    
    $resHash->{found} = 1;
  }
  
  
  my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Homonym_Output_Unit_Strategy;
###########################################
package Analyse::Homonym_Output_Unit_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_HOM_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
  if ($tables->{hasTagProblem}) {
    push @reports, "rpt_homonym_3";
  }
  
  # element result hash analyse and severity
  if(! $tables->{hasElement}) {
    push @reports, "rpt_homonym_2";
  } else {
    $entryText->setEntryTextAttr("hasHom", 1);
    #Attention: There could be more elements and contents with form <sic>...</sic>
    $resHash->{element} = $tables->{elements};
    $resHash->{content} = $tables->{content};
    $resHash->{elementType} = "HOMONYM_ELEMENT";
    # the indices of lemma element
    my $posArrRef = $tables->{elePos};
    $resHash->{pos} = $posArrRef;
    
    $resHash->{found} = 1;
  }
  
  my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::Ignore_Output_Unit_Strategy;
###########################################
package Analyse::Ignore_Output_Unit_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_IGNORE_STRATEGY',
);

########################
# variables
########################


########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}

#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
  #  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
  if ($tables->{hasTagProblem}) {
    push @reports, "rpt_ignore_3";
  }
  
  # element result hash analyse and severity
  if(! $tables->{hasElement}) {
    push @reports, "rpt_ignore_2";
  } else {
    $entryText->setEntryTextAttr("hasIgnore", 1);
    #Attention: There could be more elements and contents with form <sic>...</sic>
    $resHash->{element} = $tables->{elements};
    $resHash->{content} = $tables->{content};
    $resHash->{elementType} = "IGNORE_ELEMENT";
    # the indices of lemma element
    my $posArrRef = $tables->{elePos};
    $resHash->{pos} = $posArrRef;
    
    $resHash->{found} = 1;
  }
  
  my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;




#CLASS:
############################################
## package Analyse::Endpart_Output_Unit_Strategy;
############################################
#package Analyse::Endpart_Output_Unit_Strategy;
#use Moose;
#use namespace::autoclean;
#use Moose::Util::TypeConstraints;
#
#use FindBin;
#use lib "$FindBin::Bin/../";
#
#use Carp;
#use Data::Dumper;
#
#with 'Analyse::UnitStrategyApi';
#
#use Global::Values;
#use Database::MotDBProcessor;
#
#enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];
#
#########################
## constructor
#########################
#
#########################
## Variables
#########################
#
#has 'type' => (
#  is => 'ro',
#  isa => 'ANALYSE_STRATEGY_TYPE',
#  default => 'GET_ENDPART_STRATEGY',
#);
#
#########################
## methods
#########################
## implement
#sub getStrategyType {
#  my $self = shift;
#  return $self->type;
#}
## implement
## update the input unit tables
#sub input {
#  my $self = shift;
#  my $tableHash = shift;
#  $self->setUnitTables($tableHash);
#}
## implement
#sub clear {
#  my $self = shift;
#  $self->clearUnitTables;
#  $self->clearOutputHash;
#$self->clearCurEntryText;
#}
## implement
#sub output {
#  my $self = shift;
#  return $self->getOutputHash;
#}
######################################################
## input html attr
##  my %result = (
##    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
##    "elements"    => "",    # element: element text with array ref
##    "restText"    => "",    # restText: the input original text without current elements (text reference)
##    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
##    "eleNum"      => 1,   # the found element amount
##    "eleFlags"    => "",    # eleFlags: element flags with array ref
##    "content"     => "",    # content: content with array ref
##    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
##    "attr"        => "",    # attr: attributes in einem Tag with hash ref
##    "attrseq"     => "",    # attrseq: attr keys with array ref
##    "targetText"        => "",    # text: target original text with text ref
##    "link"        => "",    # link: url link with text ref
##    "rawText"     => "",    # rawText: the input original text ref
#####################################################################
## 0 / 1: false / true
##    "hasTagName"  => "",
##    "hasTagProblem"  => "",
##    "hasRawText"  => "",
##    "hasElement"  => "",
##    "hasRestText"  => "",
##    "hasTargetText"  => "",
##    "hasContent"  => "",
##    "hasAttr"  => "",
##    "hasAttrseq"  => "",
##    "hasLink"  => "",
##  );
#####################################################################
## implement
## returns the result hash reference
##  "severity" => 1,2,3,4,5,6,
##  "pos"     => ["0,17"]
##  "found"   => 1/0,
##  "element" => $lemElement;
##  "content" => $lemContent;
##  "result" => $headerInfoHash;
##  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
##  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
#sub analyse {
#  my $self = shift;
#  
#  if (! $self->hasUnitTables) {
#    carp "There are no valid unit tables.";
#    return;
#  }
#  my $tables = $self->getUnitTables;
#  my $entryText = $self->getCurEntryText;
#  
#  my $mdbtApp = Database::MotDBTableApp->getInstance;
#  
#  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
#  
#  
#  # traverse the tables
#  my $severity = 1;
#  my $infoIndices;
#  
#  my @problems = ();
#  my @reports = ();
#  Global::Values->resetAnalyseUnitResultHash;
#  my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
#  my $resHash = Global::Values->analyseUnitResultHash;
#  
##  if($tables->{eleNum} > 1) {
##    #given eine problem report with $repDBProc
##    push @reports, "rpt_lemma_8";
##  }
#  
#  if ($tables->{hasTagProblem}) {
#    push @reports, "rpt_ok_3";
#  }
#  
#  # LEMMA result hash analyse and severity
#  if(! $tables->{hasElement}) {
#    push @reports, "rpt_ok_2";
#  } else {
#    $entryText->setEntryTextAttr("hasOK", 1);
#    
#    my @okEleArr = ($tables->{elements}->[0]);
#    my @okContentArr = ($tables->{content}->[0]);
#    
#    # ok element <ok />
##    my $okElement = $tables->{elements};
##    my $okContent = $tables->{content};
#    $resHash->{element} = \@okEleArr;
#    $resHash->{content} = \@okContentArr;
#    $resHash->{elementType} = "OK_ELEMENT";
#    # the indices of lemma element
#    my $posArrRef = $tables->{elePos};
#    
#    my $indicesArrRef = Utils::Tools->splitIndices($posArrRef->[0]);
#    
#    my $tperPos = $entryText->getTextPointer->getPosition;
#    
#    my $begin = $tperPos + $indicesArrRef->[0];
#    my $end = $tperPos + $indicesArrRef->[1];
#    
#    my $indices = Utils::Tools->setIndices($begin, $end);
#    #$posArrRef->[0] = $indices;
#    # get the real positions of the original text
#    my @indicesPosArr = ($indices);
#    $resHash->{pos} = \@indicesPosArr;
#    
#    $resHash->{found} = 1;
#  }
#  
#  my $reportHash = $self->updateProblemReports(\@reports);
#  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
#  
#  $self->setOutputHash($resHash);
#  return 1;
#}
#
#
#no Moose;
#__PACKAGE__->meta->make_immutable;
#1;

#CLASS:
###########################################
# package Analyse::OK_Output_Unit_Strategy;
###########################################
package Analyse::OK_Output_Unit_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_OK_STRATEGY',
);

########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}
#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
  #my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
#  if($tables->{eleNum} > 1) {
#    #given eine problem report with $repDBProc
#    push @reports, "rpt_lemma_8";
#  }
  
  if ($tables->{hasTagProblem}) {
    push @reports, "rpt_ok_3";
  }
  
  # LEMMA result hash analyse and severity
  if(! $tables->{hasElement}) {
    push @reports, "rpt_ok_2";
  } else {
    $entryText->setEntryTextAttr("hasOK", 1);
    
    # ok element <ok />
    my @okElementArr = ($tables->{elements}->[0]);
    my @okContentArr = ($tables->{content}->[0]);
    $resHash->{element} = \@okElementArr;
    $resHash->{content} = \@okContentArr;
    $resHash->{elementType} = "OK_ELEMENT";
    # the indices of lemma element
    my $posArrRef = $tables->{elePos};
#    my $indices = $self->changeRelIdxInOrigText($entryText, $posArrRef->[0]);
#    my $indicesArrRef = Utils::Tools->splitIndices($posArrRef->[0]);
#    my $tperPos = $entryText->getTextPointer->getPosition;
#    my $begin = $tperPos + $indicesArrRef->[0];
#    my $end = $tperPos + $indicesArrRef->[1];
#    my $indices = Utils::Tools->setIndices($begin, $end);
    #$posArrRef->[0] = $indices;
    # get the real positions of the original text
#    my @indicesArr = ($indices);
    $resHash->{pos} = $posArrRef;
    
    $resHash->{found} = 1;
  }
  
  my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  $self->setOutputHash($resHash);
  return 1;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package Analyse::EndDop_Output_Unit_Strategy;
###########################################
package Analyse::EndDop_Output_Unit_Strategy;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

with 'Analyse::UnitStrategyApi';

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

########################
# constructor
########################

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'ANALYSE_STRATEGY_TYPE',
  default => 'GET_END_DOP_STRATEGY',
);

########################
# methods
########################
# implement
sub getStrategyType {
  my $self = shift;
  return $self->type;
}
# implement
# update the input unit tables
sub input {
  my $self = shift;
  my $tableHash = shift;
  $self->setUnitTables($tableHash);
}
# implement
sub clear {
  my $self = shift;
  $self->clearUnitTables;
  $self->clearOutputHash;
  $self->clearCurEntryText;
}
# implement
sub output {
  my $self = shift;
  return $self->getOutputHash;
}
#####################################################
# input html attr
#  my %result = (
#    "tagName"     => "",    # tagName: the input tag name for searching.(text reference)
#    "elements"    => "",    # element: element text with array ref
#    "restText"    => "",    # restText: the input original text without current elements (text reference)
#    "elePos"      => "",    # elePos: the positions of elements with array ref, i.e. ("0,12", "14, 19"), Achtung: pos compare
#    "eleNum"      => 1,   # the found element amount
#    "eleFlags"    => "",    # eleFlags: element flags with array ref
#    "content"     => "",    # content: content with array ref
#    "tagFlags"    => "",    # tagFlags: tagFlags with array ref
#    "attr"        => "",    # attr: attributes in einem Tag with hash ref
#    "attrseq"     => "",    # attrseq: attr keys with array ref
#    "targetText"        => "",    # text: target original text with text ref
#    "link"        => "",    # link: url link with text ref
#    "rawText"     => "",    # rawText: the input original text ref
####################################################################
# 0 / 1: false / true
#    "hasTagName"  => "",
#    "hasTagProblem"  => "",
#    "hasRawText"  => "",
#    "hasElement"  => "",
#    "hasRestText"  => "",
#    "hasTargetText"  => "",
#    "hasContent"  => "",
#    "hasAttr"  => "",
#    "hasAttrseq"  => "",
#    "hasLink"  => "",
#  );
####################################################################
# implement
# returns the result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
sub analyse {
  my $self = shift;
  
  if (! $self->hasUnitTables) {
    carp "There are no valid unit tables.";
    return;
  }
  my $tables = $self->getUnitTables;
  my $entryText = $self->getCurEntryText;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  my $repDBProc = $mdbtApp->getTableProcessor("REPORT_REFERENCE_MAP");
  
  
  # traverse the tables
  my $severity = 1;
  my $infoIndices;
  
  my @problems = ();
  my @reports = ();
  Global::Values->resetAnalyseUnitResultHash;
  #my $resHash = Clone::clone(Global::Values->analyseUnitResultHash);
  my $resHash = Global::Values->analyseUnitResultHash;
  
#  if($tables->{eleNum} > 1) {
#    #given eine problem report with $repDBProc
#    push @reports, "rpt_lemma_8";
#  }
  
  if ($tables->{hasTagProblem}) {
    push @reports, "rpt_dop_3";
  }
  
  # LEMMA result hash analyse and severity
  if(! $tables->{hasElement}) {
    push @reports, "rpt_dop_2";
  } else {
    $entryText->setEntryTextAttr("hasEndDop", 1);
    
    # ok element <ok />
    my @endDopEleArr = ($tables->{elements}->[0]);
    my @endDopContentArr = ($tables->{content}->[0]);
    $resHash->{element} = \@endDopEleArr;
    $resHash->{content} = \@endDopContentArr;
    $resHash->{elementType} = "DOP_ELEMENT";
    # the indices of lemma element
    my $posArrRef = $tables->{elePos};
    
    my $indices = $self->changeRelIdxInOrigText($entryText, $posArrRef->[0]);
#    my $indicesArrRef = Utils::Tools->splitIndices($posArrRef->[0]);
#    my $tperPos = $entryText->getTextPointer->getPosition;
#    my $begin = $tperPos + $indicesArrRef->[0];
#    my $end = $tperPos + $indicesArrRef->[1];
#    my $indices = Utils::Tools->setIndices($begin, $end);

    my @indicesArr = ($indices);
#    $posArrRef->[0] = $indices;
    # get the real positions of the original text
    $resHash->{pos} = \@indicesArr;
    
    $resHash->{found} = 1;
  }
  
  my $reportHash = $self->updateProblemReports(\@reports);
  $resHash = $self->mergeAnalyseUnitHashAndProblemsReports($resHash, $reportHash);
  
  $self->setOutputHash($resHash);
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

################### Engine, App, Context#######################
#class:
package Analyse::UnitStrategySelectorEngine;
=encoding utf8

=head1 NAME

Analyse::UnitStrategySelectorEngine - an engine to store and control the Strategies for analysing text data

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

use Analyse::UnitStrategyApi;

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];


=head1 ATTRIBUTES

=head2 contentStrategyMap

Strategy map to store the strategies with the type of strategy.

key: type of strategy  - value: Strategy (object)

=cut
has 'unitStrategyMap' => (
  is => 'rw',
  isa => 'HashRef[Analyse::UnitStrategyApi]',
  predicate => 'hasUnitStrategyMap',
  clearer => 'resetUnitStrategyMap',
  default => sub {{}},
  lazy => 1,
);


=head1 METHODS

=head2 addUnitStrategy($strat)

Add a given strategy ($strat) to the unitStrategyMap.

=head3 Parameters:

=over 4

=item *

$strat: the given strategy, it extends from Analyse::UnitStrategyApi

=back

=cut
sub addUnitStrategy {
  my $self = shift;
  my $strat = shift;
  
  # Engine::AbstractIDGenerator
  my $type = $strat->getStrategyType;
  # put generator in the map
  $self->unitStrategyMap->{$type} = $strat;
}

=head2 getUnitStrategy($stratType)

=head3 Parameters:

=over 4

=item *

$stratType: the given type of strategy, see also Global::Values->doAnalyseStrategyType

=back

=head3 Returns:

The specified Object Strategy

=cut
sub getUnitStrategy {
  my $self = shift;
  my $stratType = shift;
  my $unitStrategy = $self->unitStrategyMap->{$stratType};
  # make sure old data is clear
  $unitStrategy->clear;
  return $unitStrategy;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
package Analyse::UnitStrategySelector;
=encoding utf8

=head1 NAME

Analyse::UnitStrategySelector - a controller of the unit strategies and unit strategy engine

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

use Global::Values;
use Database::MotDB;

use Analyse::UnitStrategySelectorEngine;

use Analyse::UnitStrategyApi;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

=head1 CONSTRUCTORS

=head2 BUILD

After new.

=cut
sub BUILD {
  my $self = shift;
  # add build in strategies
  $self->addBuildInUnitStrategies;
}

=head2 _build_unistrategyselectorengine

=cut
sub _build_unistrategyselectorengine {
  my $g = Analyse::UnitStrategySelectorEngine->new;
  return $g;
}

=head1 ATTRIBUTES

=head2 selectorEngine

Analyse::UnitStrategySelectorEngine

=cut
has 'selectorEngine' => (
  is => 'rw',
  isa => 'Analyse::UnitStrategySelectorEngine',
  builder => "_build_unistrategyselectorengine",
  handles => [qw( addUnitStrategy getUnitStrategy)],
);

=head1 METHODS

=head2 getInstance

Singleton.

=cut
my $singleton_UnitStrategySelector;
sub getInstance {
  my $self = shift;
  if (! defined $singleton_UnitStrategySelector) {
    $singleton_UnitStrategySelector = $self->new;
  }
  return $singleton_UnitStrategySelector;
}
=head2 addBuildInStrategies

Add the strategies to the main

=cut
sub addBuildInUnitStrategies {
  my $self = shift;
  $self->addUnitStrategy(Analyse::RowTypeOutputStrategy->new);
  $self->addUnitStrategy(Analyse::D74HeaderOutputStrategy->new);
  $self->addUnitStrategy(Analyse::Lemma_Output_Unit_Strategy->new);
  $self->addUnitStrategy(Analyse::TitleOutputStrategy->new);
  $self->addUnitStrategy(Analyse::AuthorOutputStrategy->new);
  $self->addUnitStrategy(Analyse::SigelOutputUnitStrategy->new);
  $self->addUnitStrategy(Analyse::CollationOutputUnitStrategy->new);
  $self->addUnitStrategy(Analyse::ReferenceRegOutputUnitStrategy->new);
  $self->addUnitStrategy(Analyse::ReferenceLemmaOutputUnitStrategy->new);
  $self->addUnitStrategy(Analyse::ReferenceHTMLOutputUnitStrategy->new);
  $self->addUnitStrategy(Analyse::ReferenceRegOutputUnitStrategy->new);
#  $self->addUnitStrategy(Analyse::Endpart_Output_Unit_Strategy->new);
  $self->addUnitStrategy(Analyse::Sic_Output_Unit_Strategy->new);
  $self->addUnitStrategy(Analyse::Corr_Output_Unit_Strategy->new);
  $self->addUnitStrategy(Analyse::Dop_Output_Unit_Strategy->new);
  $self->addUnitStrategy(Analyse::Rep_Output_Unit_Strategy->new);
  $self->addUnitStrategy(Analyse::Homonym_Output_Unit_Strategy->new);
  $self->addUnitStrategy(Analyse::Ignore_Output_Unit_Strategy->new);
  $self->addUnitStrategy(Analyse::CharErrorsOutputStrategy->new);
  $self->addUnitStrategy(Analyse::Sub_Output_Unit_Strategy->new);
  $self->addUnitStrategy(Analyse::Sup_Output_Unit_Strategy->new);
  $self->addUnitStrategy(Analyse::Dito_HTML_Output_Unit_Strategy->new);
  $self->addUnitStrategy(Analyse::DitoDescOutputStrategy->new);
  $self->addUnitStrategy(Analyse::PipeOutputStrategy->new);
  $self->addUnitStrategy(Analyse::EntryElement_Output_Unit_Strategy->new);
  $self->addUnitStrategy(Analyse::UnknownpartsOutputStrategy->new);
  $self->addUnitStrategy(Analyse::EndDop_Output_Unit_Strategy->new);
  $self->addUnitStrategy(Analyse::OK_Output_Unit_Strategy->new);
  $self->addUnitStrategy(Analyse::StreckeOutputStrategy->new);
 
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS: Context
package Analyse::UnitStrategyContext;
=encoding utf8

=head1 NAME

Analyse::UnitStrategyContext - Context for the Strategy Pattern

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use Analyse::UnitStrategyApi;
use Analyse::UnitStrategySelector;

use Global::Values;
use Database::MotDBProcessor;

enum 'ANALYSE_STRATEGY_TYPE', [@{Global::Values->doAnalyseStrategyType}];

=head1 CONSTRUCTORS

=head2 _build_unitstrategyselector

=cut
sub _build_unitstrategyselector {
  return Analyse::UnitStrategySelector->getInstance;
}
=head1 ATTRIBUTES

=head2 curUnitStrategy

The current selected unit strategy

=cut
has 'curUnitStrategy' => (
  is => 'rw',
  isa => 'Analyse::UnitStrategyApi',
  predicate => 'hasCurUnitStrategy',
  writer => 'setCurUnitStrategy',
  reader => 'getCurUnitStrategy',
);

=head2 contentStrategySelector

=cut
has 'unitStrategySelector' => (
  is => 'ro',
  isa => 'Analyse::UnitStrategySelector',
  builder => '_build_unitstrategyselector',
  lazy => 1,
);

=head1 METHODS

=head2 getInstance

Singleton.

=cut
our $singleton_UnitStrategyContext;
sub getInstance {
  my $self = shift;
  if (! defined $singleton_UnitStrategyContext) {
    $singleton_UnitStrategyContext = $self->new;
  }
  return $singleton_UnitStrategyContext;
}

=head2 getContentStrategy($strategyType)

=cut
sub getUnitStrategy {
  my $self = shift;
  my $strategyType = shift; #ANALYSE_STRATEGY_TYPE
  my $strategy = $self->unitStrategySelector->getUnitStrategy($strategyType);
  return $strategy;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;
