package Algorithms::DORuleEngineClass;

=encoding utf8

=head1 NAME

Algorithms::DORuleEngineClass - Making the Business Rule Engine Environment for Dietrich Online

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package extends the Rule::Engine module C<https://metacpan.org/release/Rule-Engine> (Author: GPHAT).

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut

use Moose;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";
use Carp;
use Data::Dumper;

use Rule::Engine;
use Rule::Engine::Filter;
use Rule::Engine::Rule;
use Rule::Engine::RuleSet;
use Rule::Engine::Session;

use Set::Scalar;
use Global::Values;



no Moose;
__PACKAGE__->meta->make_immutable;
1;




#CLASS:
package Algorithms::DORuleEngine;

=encoding utf8

=head1 NAME

Algorithms::DORuleEngine - Making the Business Rule Engine for Dietrich Online

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package extends the Rule::Engine module C<https://metacpan.org/pod/Rule::Engine> (Author: GPHAT).

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut

use Moose;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";
use Carp;
use Data::Dumper;

use Rule::Engine;

# extends rule engine, ready for developping
extends 'Rule::Engine';


no Moose;
__PACKAGE__->meta->make_immutable;
1;






#CLASS:
package Algorithms::DORuleEngineFilter;

=encoding utf8

=head1 NAME

Algorithms::DORuleEngineFilter - the condition filter of Business Rule Engine for Dietrich Online

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package extends the Rule::Engine module C<https://metacpan.org/pod/Rule::Engine::Filter> (Author: GPHAT).

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut

use Moose;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";
use Carp;
use Data::Dumper;

use Rule::Engine;
use Rule::Engine::Filter;
# extends rule engine, ready for developping
extends 'Rule::Engine::Filter';



no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
package Algorithms::DORuleEngineRule;

=encoding utf8

=head1 NAME

Algorithms::DORuleEngineRule - the rule of Business Rule Engine with ranking system for Dietrich Online

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package extends the Rule::Engine module C<https://metacpan.org/pod/Rule::Engine::Rule> (Author: GPHAT).

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut

use Moose;
use namespace::autoclean;


use Moose;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";
use Carp;
use Data::Dumper;

use Rule::Engine;
use Rule::Engine::Filter;
# extends rule engine, ready for developping
extends 'Rule::Engine::Rule';

=head1 ATTRIBUTES

=head2 rank

Ranking System for the Rules.

=cut

has 'rank' => (
  traits => ['Number'],
  is => 'rw',
  isa => 'Num',
  predicate => 'hasRank',
  clearer => 'resetRank',
  default => 0,
  handles => {
    setRank => 'set',
    addRank => 'add',
    subRank => 'sub',
  },
);


no Moose;

#print "\nDOFSM.pm loaded successfully!\n";
__PACKAGE__->meta->make_immutable;
1;




#CLASS:
package Algorithms::DORuleEngineRuleSet;
=encoding utf8

=head1 NAME

Algorithms::DORuleEngineRuleSet - the set of rules of Business Rule Engine with ranking for Dietrich Online

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package extends the Rule::Engine module C<https://metacpan.org/pod/Rule::Engine::RuleSet> (Author: GPHAT).

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";
use Carp;
use Data::Dumper;

use Rule::Engine;
use Rule::Engine::RuleSet;
# extends rule engine, ready for developping
extends 'Rule::Engine::RuleSet';

=head1 METHODS

=head2 sortRuleByRank($sortType)

Sorts the Rule LIST with the ranking and sort type (incr | decr).

=cut
sub sortRuleByRank {
  my $self = shift;
  my $sortType = shift;
  
  my $rules = $self->rules;
  my @resArr;
  if ($sortType =~ m/^incr/i ) {
    @resArr = sort {$a->rank <=> $b->rank} @$rules;
  } elsif($sortType =~ m/^decr/i) {
    @resArr = reverse sort {$a->rank <=> $b->rank} @$rules;
  }
  
  $self->rules(\@resArr);
  return 1;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
package Algorithms::DORuleEngineSession;
=encoding utf8

=head1 NAME

Algorithms::DORuleEngineSession - a rules session of Business Rule Engine for Dietrich Online

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package extends the Rule::Engine module C<https://metacpan.org/pod/Rule::Engine::Session> (Author: GPHAT).

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";
use Carp;
use Data::Dumper;

use Rule::Engine;
use Rule::Engine::Session;
# extends rule engine, ready for developping
extends 'Rule::Engine::Session';


no Moose;
__PACKAGE__->meta->make_immutable;
1;