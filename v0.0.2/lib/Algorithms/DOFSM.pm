package Algorithms::DOFSM;

=encoding utf8

=head1 NAME

Algorithms::DOFSM - Making a Finite State Machine for Dietrich Online

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package extends the FSM::Simple module C<https://metacpan.org/pod/FSM::Simple> (Author: KOSCIELNY).

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut

use Moose;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use FSM::Simple;
use Data::Dumper;

use Text::AbstractText;

has 'isConst' => (
  is      => 'rw',
  isa     => 'Bool',
  default => 0,

  #  writer => 'setConst',
  #  reader => 'getConst',
);

has 'fsm' => (
  is  => 'rw',
  isa => 'FSM::Simple',
);


############################
# Constructor
############################

sub BUILD {
  my $self = shift;

  # make a new fsm (FSM::Simple)
  $self->const();
}

sub const {
  my $self = shift;

  # singleton
  if ( !$self->isConst ) {
    $self->fsm( FSM::Simple->new() );
    $self->isConst(1);
  }
  return $self->fsm;
}

sub run {
  my $self = shift;
  $self->fsm->run();
}

#sub setRules {
#  my $self = shift;
#  $self->fsm->add_state();
#}

my $fsm_hash = {
  "state_name" => {
    "description"     => "",
    "type"            => "",                # initial_state, final_state, accept_state
    "output_function" => "sub_reference",
    "input"           => [ "", "" ],
    "output"          => [ "", "" ],
  },
  "error_state" => {
    "description"     => "the error state",
    "type"            => "accept_state",
    "output_function" => "error_state",
    "input"           => ["error"],
    "output"          => [""],
  },
  "final_state" => {
    "description"     => "the final state",
    "type"            => "final_state",
    "output_function" => "stop_state",
    "input"           => ["done"],
    "output"          => [""],
  },
  "init_state" => {
    "description"     => "initialing the whole machine",
    "type"            => "initial_state",
    "output_function" => "init_state",
    "input"           => [""],
    "output"          => [
      "d74_lemma_entry", "d74_entry",     "html_entry", "html_lemma_entry",
      "separator_entry", "strecke_entry", "unknown_entry"
    ],
  },
  "sepEntryState" => {
    "description"     => "Separator Entry.",
    "type"            => "accept_state",
    "output_function" => "getSepEntry",
    "input"           => ["separator_entry"],
    "output"          => ["done"],
  },
  "streckeState" => {
    "description"     => "Analyse des Strecke-Bereiches",
    "type"            => "accept_state",
    "output_function" => "getStreckEntry",
    "input"           => ["strecke_entry"],
    "output"          => ["done"],
  },
  "headerState" => {
    "description"     => "Analyse der HEADER_Bereich des 74 Texts mit der Reg_Engine.",
    "type"            => "accept_state",
    "output_function" => "getHeader",
    "input"           => ["d74_lemma_entry"],
    "output"          => ["get_lemma"],
  },
  "pipeState" => {
    "description"     => "Analyse des Pipe, die am Anfang der Entry steht.",
    "type"            => "accept_state",
    "output_function" => "getPipe",
    "input"           => [ "d74_entry", "get_pipe" ],
    "output"          => ["get_title"],
  },
  "lemmaEntryEleState" => {
    "description"     => "Analyse des Lemma-Entry-Elements",
    "type"            => "accept_state",
    "output_function" => "getLemmaEntryEle",
    "input"           => ["html_lemma_entry"],
    "output"          => ["get_lemma"],
  },
  "entryEleState" => {
    "description"     => "Analyse des Entry-Elements",
    "type"            => "accept_state",
    "output_function" => "getEntryEle",
    "input"           => ["html_entry"],
    "output"          => ["get_pipe"],
  },

  "lemmaState" => {
    "description"     => "Analyse des Lemma-Bereiches.",
    "type"            => "accept_state",
    "output_function" => "getLemmaEle",
    "input"           => ["get_lemma"],
    "output"          => ["get_hom"],
  },
  "homonymState" => {
    "description"     => "Analyse des Homonym-Bereich von Lemmata.",
    "type"            => "accept_state",
    "output_function" => "getHomEle",
    "input"           => ["get_hom"],
    "output"          => ["get_title"],
  },
  "titelState" => {
    "description"     => "Analyse des Titel-Bereiches.",
    "type"            => "accept_state",
    "output_function" => "getTitle",
    "input"           => ["get_title"],
    "output"          => ["get_author"],
  },

  "authorState" => {
    "description"     => "Analyse des Verfasser-Bereiches.",
    "type"            => "accept_state",
    "output_function" => "getAuthor",
    "input"           => ["get_author"],
    "output"          => ["get_sigel"],
  },
  "sigelState" => {
    "description"     => "Analyse des Sigel-Bereiches.",
    "type"            => "accept_state",
    "output_function" => "getSigel",
    "input"           => ["get_sigel"],
    "output"          => ["get_collation"],
  },
  "collationState" => {
    "description"     => "Analyse des Kollation-Bereiches.",
    "type"            => "accept_state",
    "output_function" => "getCol",
    "input"           => ["get_collation"],
    "output"          => ["get_verweis"],
  },
  "verweisState" => {
    "description"     => "Analyse des Verweis-Bereiches.",
    "type"            => "accept_state",
    "output_function" => "getVerweis",
    "input"           => [ "get_verweis", "has_verweis" ],
    "output"          => [ "has_verweis", "has_no_verweis" ],
  },
  "endSpecState" => {
    "description" =>
"Anaylse der eventuellen Speziellen Elemente am Ende der Literaturangabe, so wie <ok />, <do />.",
    "type"            => "accept_state",
    "output_function" => "getEndSpec",
    "input"           => ["has_no_verweis"],
    "output"          => ["get_interSpecParts"],
  },
  "intSpecState" => {
    "description" =>
"Anaylse der eventuellen Speziellen Elemente in der Literaturangabe, so wie '~', <seite=XX>, <rep>, <dop>, <ignore>, <sub>, <sup>, <sic>, <corr>.",
    "type"            => "accept_state",
    "output_function" => "getIntSpec",
    "input"           => ["get_interSpecParts"],
    "output"          => ["check_entry_valid"],
  },
  "checkState" => {
    "description" => "check the entry valid, there are maybe some bugs in the whole text entry.",
    "type"        => "accept_state",
    "output_function" => "checkBugs",
    "input"           => ["check_entry_valid"],
    "output"          => ["done"],
  },
};

sub addStates {
  my $self       = shift;
  my $fsmHashRef = shift;
  
  foreach my $key (keys %$fsmHashRef) {
    my $value = $fsmHashRef->{$key};
    $self->fsm->add_state( name => $key, sub => \&{ $value->{output_function} } );
  }
#  while ( my ( $key, $value ) = each %$fsmHashRef ) {
#
#    $self->fsm->add_state( name => $key, sub => \&{ $value->{output_function} } );
#    #print Dumper(ref $value->{output_function});
#  }

  #return $fsm_hash;
}

sub addtrans {
  my $self = shift;

  # initialize
  $self->fsm->add_trans( from => 'init_state', to => 'headerState', exp_val => 'd74_lemma_entry' );
  $self->fsm->add_trans(
    from    => 'init_state',
    to      => 'lemmaEntryEleState',
    exp_val => 'html_lemma_entry'
  );
  $self->fsm->add_trans( from => 'init_state', to => 'pipeState',     exp_val => 'd74_entry' );
  $self->fsm->add_trans( from => 'init_state', to => 'entryEleState', exp_val => 'html_entry' );
  $self->fsm->add_trans(
    from    => 'init_state',
    to      => 'sepEntryState',
    exp_val => 'separator_entry'
  );
  $self->fsm->add_trans( from => 'init_state', to => 'streckeState', exp_val => 'strecke_entry' );
  $self->fsm->add_trans( from => 'init_state', to => 'final_state',  exp_val => 'unknown_entry' );

  # lemma entry
  $self->fsm->add_trans( from => 'headerState',        to => 'lemmaState', exp_val => 'get_lemma' );
  $self->fsm->add_trans( from => 'lemmaEntryEleState', to => 'lemmaState', exp_val => 'get_lemma' );
  $self->fsm->add_trans( from => 'lemmaState',   to => 'homonymState', exp_val => 'get_hom' );
  $self->fsm->add_trans( from => 'homonymState', to => 'titelState',   exp_val => 'get_title' );
  $self->fsm->add_trans( from => 'titelState',   to => 'authorState',  exp_val => 'get_author' );
  $self->fsm->add_trans( from => 'authorState',  to => 'sigelState',   exp_val => 'get_sigel' );
  $self->fsm->add_trans( from => 'sigelState', to => 'collationState', exp_val => 'get_collation' );
  $self->fsm->add_trans( from => 'collationState', to => 'verweisState', exp_val => 'get_verweis' );
  $self->fsm->add_trans( from => 'verweisState',   to => 'verweisState', exp_val => 'has_verweis' );
  $self->fsm->add_trans(
    from    => 'verweisState',
    to      => 'endSpecState',
    exp_val => 'has_no_verweis'
  );
  $self->fsm->add_trans(
    from    => 'endSpecState',
    to      => 'intSpecState',
    exp_val => 'get_interSpecParts'
  );
  $self->fsm->add_trans(
    from    => 'intSpecState',
    to      => 'checkState',
    exp_val => 'check_entry_valid'
  );
  $self->fsm->add_trans( from => 'checkState', to => 'final_state', exp_val => 'done' );

  # entry
  $self->fsm->add_trans( from => 'entryEleState', to => 'pipeState', exp_val => 'get_pipe' );

  # pipe
  $self->fsm->add_trans( from => 'pipeState', to => 'titelState', exp_val => 'get_title' );

  # separator entry
  $self->fsm->add_trans( from => 'sepEntryState', to => 'final_state', exp_val => 'done' );

  # strecke entry
  $self->fsm->add_trans( from => 'streckeState', to => 'final_state', exp_val => 'done' );

  #$self->fsm->add_trans(from=>'', to => '', exp_val => '');

}

my @fsm_trans_arr = (
  {
    "from"    => "init_state",
    "to"      => "headerState",
    "exp_val" => "d74_lemma_entry",
  },
  {
    "from"    => "init_state",
    "to"      => "lemmaEntryEleState",
    "exp_val" => "html_lemma_entry",
  },
  {
    "from"    => "init_state",
    "to"      => "pipeState",
    "exp_val" => "d74_entry",
  },

  {
    "from"    => "init_state",
    "to"      => "entryEleState",
    "exp_val" => "html_entry",
  },

  {
    "from"    => "init_state",
    "to"      => "sepEntryState",
    "exp_val" => "separator_entry",
  },

  {
    "from"    => "init_state",
    "to"      => "streckeState",
    "exp_val" => "strecke_entry",
  },
  {
    "from"    => "init_state",
    "to"      => "final_state",
    "exp_val" => "unknown_entry",
  },
  {
    "from"    => "headerState",
    "to"      => "lemmaState",
    "exp_val" => "get_lemma",
  },
  {
    "from"    => "lemmaEntryEleState",
    "to"      => "lemmaState",
    "exp_val" => "get_lemma",
  },

  {
    "from"    => "lemmaState",
    "to"      => "homonymState",
    "exp_val" => "get_hom",
  },

  {
    "from"    => "homonymState",
    "to"      => "titelState",
    "exp_val" => "get_title",
  },

  {
    "from"    => "titelState",
    "to"      => "authorState",
    "exp_val" => "get_author",
  },

  {
    "from"    => "authorState",
    "to"      => "sigelState",
    "exp_val" => "get_sigel",
  },

  {
    "from"    => "sigelState",
    "to"      => "collationState",
    "exp_val" => "get_collation",
  },

  {
    "from"    => "collationState",
    "to"      => "verweisState",
    "exp_val" => "get_verweis",
  },

  {
    "from"    => "verweisState",
    "to"      => "verweisState",
    "exp_val" => "has_verweis",
  },

  {
    "from"    => "verweisState",
    "to"      => "endSpecState",
    "exp_val" => "has_no_verweis",
  },

  {
    "from"    => "endSpecState",
    "to"      => "intSpecState",
    "exp_val" => "get_interSpecParts",
  },

  {
    "from"    => "intSpecState",
    "to"      => "checkState",
    "exp_val" => "check_entry_valid",
  },

  {
    "from"    => "checkState",
    "to"      => "final_state",
    "exp_val" => "done",
  },

  {
    "from"    => "entryEleState",
    "to"      => "pipeState",
    "exp_val" => "get_pipe",
  },

  {
    "from"    => "pipeState",
    "to"      => "titelState",
    "exp_val" => "get_title",
  },
  {
    "from"    => "sepEntryState",
    "to"      => "final_state",
    "exp_val" => "done",
  },
  {
    "from"    => "streckeState",
    "to"      => "final_state",
    "exp_val" => "done",
  },
);

no Moose;
#print "\nDOFSM.pm loaded successfully!\n";
__PACKAGE__->meta->make_immutable;
1;
