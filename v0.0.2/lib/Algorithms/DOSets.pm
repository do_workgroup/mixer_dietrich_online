package Algorithms::DOSets;
=encoding utf8

=head1 NAME

Algorithms::DOSets - Sets and its operations for Dietrich Online

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package is extends the Set::Scalar module C<https://metacpan.org/pod/Set::Scalar> (Author: DAVIDO).

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut

use Moose;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";
use Carp;
use Data::Dumper;

use Set::Scalar;

use Global::Values;


=head1 ATTRIBUTES

=head2 groups

The map of different Sets

=cut
# errorStruct syntactic struct
has 'groups' => (
  is => 'rw',
  isa => 'HashRef[Set::Scalar]',
  builder => '_build_groups',
);

# e.g. $self->groups->{errorStruct}->elements


=head1 METHODS

=head2 insection
TODO
=cut

sub insection {}
=head2 union
TODO
=cut
sub union {}
=head2 difference
TODO
=cut
sub difference {}
#sub symmetricdifference {}
=head2 relations
TODO
=cut
sub relations {}

=head2 insert ($gname, $char)
Add a character into the given set group.
=cut
sub insert {
  my $self = shift;
  my ($gname, $char) = @_;
  return if (!exists $self->groups->{$gname}); 
  $self->groups->{$gname}->insert($char);
  return 1;
}

=head2 inGroups($char)
check the input character type.
return the reference of the group names array
=cut
sub inGroups {
  my $self = shift;
  my $char = shift;
  my @tArr;
  for my $gname (keys %{$self->groups}) {
     push @tArr, $gname if $self->groups->{$gname}->has($char);
  }
  if (!@tArr) {
    $self->groups->{normal}->insert($char);
    push @tArr, Global::Values::CHARTYPES->{NORMAL};
  }
  return \@tArr;
}
=head2 inGroups
Private Build in for the set groups. 
Connect to the Global::Values->doCharTypes
=cut
sub _build_groups {
  my $self = shift;
  my $rhash = {};
  for my $key (keys %{Global::Values->doCharTypes}) {
    my $cArr = Global::Values->doCharTypes->{$key};
    my $set = Set::Scalar->new();
    $set->insert(@$cArr);
    
    $rhash->{$key} = $set;
  }
  # common, structure, graph
#  for my $key (keys %{Global::Values->}) {
#  }
  return $rhash;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;
