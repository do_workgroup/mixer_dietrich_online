package Algorithms::DOGraph;
=encoding utf8

=head1 NAME

Algorithms::DOGraph - Making a graph structure to call the engine and analyse text

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package extends the Graph module C<https://metacpan.org/pod/distribution/Graph/lib/Graph.pod> (Author: JHI).

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 SEE ALSO

Graph, Graph::Writer

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";
use Carp;
use List::Util;
use Data::Dumper;

use Set::Scalar;

use Graph;
use Graph::Directed;
use Graph::Undirected;

use Graph::Writer;
use Graph::Writer::Dot;
use Graph::Writer::XML;
use Graph::Writer::VCG;
use Graph::Reader;

use Global::Values;
use Utils::Tools;

enum 'DO_TYPES', [ @{ Global::Values->allDOTypes } ];


=head1 CONSTRUCTORS

=head2 BUILDARGS

In new.

=cut
around 'BUILDARGS' => sub {
  my $orig  = shift;
  my $class = shift;
  if ( @_ == 0 ) {

    #my $G = Graph::Directed->new(hyperedged => 1, hypervertexed => 1);
    my $G = Graph::Directed->new();
    return $class->$orig( graph => $G );
  }
  elsif ( @_ == 1 ) {
    my $type = shift;
    my $G;
    if ( !defined $type
      || $type eq ""
      || $type eq Global::Values::GRAPH->{DIRECTED} )
    {
      $G = Graph::Directed->new();
    }
    elsif ( $type eq Global::Values::GRAPH->{UNDIRECTED} ) {
      $G = Graph::Undirected->new();
    }
    return $class->$orig( graph => $G );
  }
  else {
    # normal input
    # (@list_or hashref)
    return $class->$orig(@_);
  }

};

=head2 BUILD

After new.

=cut
sub BUILD {
  my $self = shift;

  #my @arr = [];
  #$self->markedVertices(\@arr);
  # make a new Graph (Graph)
  #$self->const();
}

=head2 _build_markedvertices

=cut
sub _build_markedvertices {
  return Set::Scalar->new;
}

=head1 ATTRIBUTES

=head2 graph

Graph core.

=cut
has 'graph' => (
  is        => 'rw',
  isa       => 'Graph',
  predicate => 'hasGraph',
  clearer   => 'clearGraph',
);
=head2 curVertex

current vertex.

=cut
has 'curVertex' => (
  is        => 'rw',
  isa       => 'Str',
  predicate => 'hasCurVertex',
  clearer   => 'resetCurVertex',
  writer   => 'setCurVertex',
  reader   => 'getCurVertex',
);

=head2 curRoot

current root of graph

=cut
has 'curRoot' => (
  is        => 'rw',
  isa       => 'Str',
  predicate => 'hasCurRoot',
  clearer   => 'clearCurRoot',
);

=head2 roots

Roots are the start of the potential subgraphs

=cut
has 'roots' => (
  is        => 'rw',
  isa       => 'ArrayRef[Str]',
  predicate => 'hasRoots',
  clearer   => 'resetRoots',
  default   => sub { [] },
  lazy      => 1,
);

=head2 markedVertices

Marked vertices, they are stored in a Set::Scalar.

=head3 See also:

Set::Scalar

=cut
has 'markedVertices' => (
  is        => 'rw',
  isa       => 'Set::Scalar',
  predicate => 'hasMarkedVertices',
  clearer   => 'resetMarkedVertices',
  builder   => '_build_markedvertices',
  lazy      => 1,
);

=head2 gid

graph id

=cut
has 'gid' => (
  is        => 'rw',
  isa       => 'Str',
  predicate => 'hasGID',
  clearer   => 'resetGID',
  default   => "",
  lazy      => 1,
);

=head1 METHODS

=head2 clean

Resets the class to its initial, empty state.

=cut
sub clean {
  my $self = shift;
  $self->clearGraph;
  $self->resetCurVertex;
  $self->resetCurRoot;
  $self->resetCurVPref;
  $self->resetMarkedVertices;
  $self->resetRoots;
  $self->resetGID;
}



#for test: dot
sub writeGraph {
  my $self   = shift;
  my $fname  = shift;
  my $writer = Graph::Writer::Dot->new();
  $writer->write_graph( $self->graph, $fname );
  return 1;
}

#for test: png
sub showGraph {
  my $self    = shift;
  my $dotName = shift;
  my $pngName = shift;

  my @args = ( dot => '-Tpng', $dotName, "-o$pngName" );
  system @args;
  return 1;
}

=head2 addToMarkedSet($v)

Add the specifed vertex($v) into the Marked vertex set, see 'markedVertices'.

=cut
sub addToMarkedSet {
  my $self = shift;
  my $v    = shift;
  return if !$self->hasVertex($v);
  # the given vertex is already in the set
  if ($self->isInMarkedSet($v)) {
    carp "There is a same vertex already in the marked set";
    return;
  }
  # set it to marked
  $self->markV($v, 1) if (! $self->isMarked($v));
  # add it to the set
  $self->markedVertices->insert($v);
  return 1;
}

=head2 isInMarkedSet($v)

Returns true if the given vertex($v) in the set of marked vertices, see 'markedVertices'.

=cut
sub isInMarkedSet {
  my $self = shift;
  my $v = shift;
  return $self->markedVertices->has($v);
}

# Wrong:
#sub hasDOUnmarkedChildren {
#  my $self = shift;
#  my $v = shift;
#  my $matched = $self->getVMatchAttr;
#  my $arr = $self->getDOSuccessors($v, $matched);
#  return $arr ;
#}
  
=head2 findPredecessor($v)

Finds predecessor of the given vertex

=cut
sub findPredecessor {
  my $self = shift;
  my $v    = shift;
  return if ( !$self->hasVertex($v)
    || $self->isSourceV($v) );
  my $pArrRef = $self->getDirectPredecessors($v);
  carp "There are too many Predecessors of the current Vertex: $v"
    if ( scalar @$pArrRef > 1 );
  return if ! scalar @$pArrRef;
  return $$pArrRef[0];
}

=head2 findPredecessor($v)

Returns a unmarked immediate successor of this given vertex($v).

=cut
sub unmarkedSuccessor {
  my $self = shift;
  my $v    = shift;
  return if ( !$self->hasVertex($v)
    || $self->isSinkV($v) );
  my $sArrRef = $self->getDirectSuccessors($v);

  my $res;
  foreach my $temp (@$sArrRef) {
    next if ( $self->isMarked($temp) );
    $res = $temp;
    last;
  }
  return $res;
}

=head2 getNoMarkedDOSuccessorsWithMatched($vertex)

Returns a unmarked immediate successor of this given vertex($vertex) with matched information.

=cut
sub getNoMarkedDOSuccessorsWithMatched {
  my $self = shift;
  my $vertex = shift;
  return if (! defined $vertex || $vertex eq "");
  return $vertex if ! $self->isMarked($vertex);
  
  # matched value: module is always 1
  my $matched = $self->getVMatchAttr($vertex);
  # get all immediate successors
  my $succrArrRef = $self->getDOSuccessors($vertex, $matched);
  # get one unmarked vertex (only one if it has)
  my $unmarkedV = $self->unmarkedVertex($succrArrRef);
  
  return $unmarkedV;
}

=head2 getNextUnmarkedDOVertex($curV)

Returns a unmarked vertex of this given vertex($curV). It could be a successor, a sibling 

or a vertex in other subgraph.

=cut
sub getNextUnmarkedDOVertex {
  my $self = shift;
  my $curV = shift;
  
  if (!$self->isMarked($curV)) {
    carp "This vertex is not marked yet!";
    return $curV;
  }
  
  my $matched = $self->getVMatchAttr($curV);
  
  if (! defined $matched || $matched eq "") {
    croak "Error: This vertex $curV has no matched information!";
  }
  
  my $unmarkedSuccessor = $self->getNoMarkedDOSuccessorsWithMatched($curV);
  
  #FIXME:
  # there is no unmarked successor of this current vertex
  if (defined $unmarkedSuccessor && $unmarkedSuccessor ne "") {
    return $unmarkedSuccessor;
  }
  # search unmarked successor forward
  my $isFound = 0;
  while (! defined $unmarkedSuccessor || $unmarkedSuccessor eq "") {
    # if the current vertex is undefined or empty
    last if (! defined $curV || $curV eq "");
    # if this current vertex is the root of graph, then go out of the loop
    last if ($self->getVAttr( $curV, Global::Values::GRAPH->{ROOT_ATTR}));
    #!!! Attention: get immediate predecessor of current vertex, and change the current vertex to it
    $curV= $self->findPredecessor($curV);
    # get matched infor
    $matched = $self->getVMatchAttr($curV);
    if (! defined $matched || $matched eq "") {
      croak "Error: This vertex $curV has no matched information!";
      last;
    }
    #Attention: inner 2 Loops!
    $unmarkedSuccessor = $self->getNoMarkedDOSuccessorsWithMatched($curV);
#    print Dumper("In While Loop: $unmarkedSuccessor") if (defined $unmarkedSuccessor);
  }
#  print Dumper("getNextUnmarkedDOVertex(): while done! curV-> $curV : unmarkedSucc-> $unmarkedSuccessor");
  #FIXME:
  # return if found some vertex
  if (defined $unmarkedSuccessor && $unmarkedSuccessor ne "") {
    return $unmarkedSuccessor;
  }
  # found nothing;
  return;
}
#sub hasUnmarkedSuccessor {
#  my $self = shift;
#  my $curV = shift;
#  my $matched = shift;
#  my $unmarkedSuccessor = $self->getNoMarkedDOSuccessorsWithMatched($curV, $matched);
#  if (! defined $unmarkedSuccessor || $unmarkedSuccessor eq "") {
#    return 0;
#  } else {
#    return 1;
#  }
#}
sub getUnmarkedVertexFromPredecessor {
  my $self = shift;
  my $curV = shift;
}

=head2 unmarkedVertex($vArrRef)

Returns a unmarked vertex from the given vertices array reference ($vArrRef).

=cut
sub unmarkedVertex {
  my $self    = shift;
  my $vArrRef = shift;
  return if ( !defined $vArrRef || $vArrRef eq "" );
  my $res;
  #key: one loop
  foreach my $v (@$vArrRef) {
    next if ( $self->isMarked($v) );
    $res = $v;
    last;
  }
  return $res;
}

=head2 getDOSuccessors($vertex, $matched)

Returns array reference of the immediate successors from the given vertex with the same matched value ($matched).

=cut
sub getDOSuccessors {
  my $self    = shift;
  my $vertex  = shift;
  my $matched = shift;
  
  return if (! defined $vertex || $vertex eq "");
  return if !$self->hasVertex($vertex);

  my $sArrRef = $self->getDirectSuccessors($vertex);

  my @resArr;

  # empty
  return if ( !scalar @$sArrRef );

  # check all
  for my $v (@$sArrRef) {
    if ( $self->getEMatchAttr( $vertex, $v ) eq $matched ) {
      push @resArr, $v;
    }
  }
  return \@resArr;
}

=head2 addDOSuccessors($vertex, $engineHashRef)

Adds the given vertex to the graph, and its successors will be added into the graph 

with its matched information and the RegExp Pattern Maps. 

=head3 Paramters:

=over 4

=item *

$vertex: the given vertex

=item *

$engineHashRef: the given reference pattern map, 
            see Global::Values::ANALYSE_PARAMETERS->{REGEXP_PATTERN_HASH}

=back

=head3 Returns:

the array reference of added successors of current vertex.

=cut
sub addDOSuccessors {
  my $self          = shift;
  my $vertex        = shift;
  my $engineHashRef = shift;

  # this form is only for module in the $pat_engine_hash

  my $procTrueArrRef  = $engineHashRef->{$vertex}->{successorTrue};
  my $procFalseArrRef = $engineHashRef->{$vertex}->{successorFalse};
  my @res;
  push @res, @$procTrueArrRef  if defined $procTrueArrRef  && scalar @$procTrueArrRef;
  push @res, @$procFalseArrRef if defined $procFalseArrRef && scalar @$procFalseArrRef;

  #successors with "true"
  if ( defined $procTrueArrRef && scalar @$procTrueArrRef ) {
    for my $v (@$procTrueArrRef) {

      # empty item
      next if chomp($v);

      #my $vname = $self->setDOVName($v);
      my $vname = $v;
      $self->addNewConn( $vertex, $vname );

      # Attention: Only Edges!!
      # Edge: set the matched Attribute to show the result of regexp process
      $self->setEMatchAttr( $vertex, $vname, 1 );
    }
  }

  #successors with "false"
  if ( defined $procFalseArrRef && scalar @$procFalseArrRef ) {
    for my $v (@$procFalseArrRef) {

      # empty item
      next if chomp($v);

      #      my $vname = $self->setDOVName($v);
      my $vname = $v;
      $self->addNewConn( $vertex, $vname );

      # Attention: Only Edges!!
      # Edge: set the matched Attribute to show the result of regexp process
      $self->setEMatchAttr( $vertex, $vname, 0 );
    }
  }

  # whether this graph has a cycle or not?
  if ( $self->hasCycle ) {
    croak "Error: There is a cycle in the graph wie current Root! " . $self->curRoot;
    return 0;
  } else {
    # mark the current vertex to build the part of the graph
    #FIXME: WHY MARKED THEM??
    #$self->markV( $vertex, 1 );
    return \@res;
  }
}

=head2 addNewConn($from, $to)

Adds a connection between the start vertex ($from) and end vertex ($to).

=cut
sub addNewConn {
  my $self = shift;
  my ( $from, $to ) = @_;
  return if ( !defined $from || !defined $to || chomp($from) || chomp($to) );
  $self->addVertex($from) if !$self->hasVertex($from);
  $self->addVertex($to)   if !$self->hasVertex($to);

  if ( $self->hasEdge( $from, $to ) ) {

    # no multivertexed or countedged
    $self->delEdge( $from, $to );
  }
  $self->addEdge( $from, $to );
}

=head2 updateCurRoot($rootName)

Updates the current root with the given root name.

=cut
sub updateCurRoot {
  my $self     = shift;
  my $rootName = shift;

  # valid
  if ( !defined $rootName ) {
    carp "There is no valid input Root!";
    return;
  }
  # add it to the graph
  $self->addVertex($rootName) if !$self->hasVertex($rootName);

  # set the root's attribute.
  $self->setVAttr( $rootName, Global::Values::GRAPH->{ROOT_ATTR}, Global::Values::GRAPH->{TRUE} );

  # set curRoot of graph
  $self->curRoot($rootName);

  # add it to the root list
  $self->addRoot($rootName);

  #$self->updateCurVPref($rootName);
  return 1;
}

=head2 updateCurRoot($v)

Adds a new root ($v) to the array of roots

=cut
sub addRoot {
  my $self = shift;
  my $v    = shift;
  return if !$self->hasVertex($v);
  my $resArr;
  if ( !$self->hasRoots ) {
    push @$resArr, $v;
  }
  elsif ( grep ( /^$v$/, @{ $self->roots } ) ) {
    carp "There is a same root \'$v\' in the graph!";
    return;
  }
  else {
    $resArr = $self->roots;
    push @$resArr, $v;
  }
  $self->roots($resArr);
  return 1;
}

=head2 getEMatchAttr($from, $to)

Returns the matched information of the given edge between the vertices ($from, $to).

=cut
sub getEMatchAttr {
  my $self = shift;
  my ( $from, $to ) = @_;
  return $self->getEAttr( $from, $to, Global::Values::GRAPH->{MATCH_TYPE} );
}

=head2 getEAttr($from, $to, $attrName)

Returns the attribute value of the edge between the vertices ($from, $to) with the given attribute name ($attrName).

=cut
sub getEAttr {
  my $self = shift;
  my ( $from, $to, $attrName ) = @_;
  return if ( !$self->hasVertex($from)
    || !$self->hasVertex($to)
    || !$self->hasEdge( $from, $to ) );
  return $self->graph->get_edge_attribute( $from, $to, $attrName );
}

=head2 setEMatchAttr($from, $to, $value)

Sets the matched value ($value: 1/0) of the given edge between the vertices ($from, $to).

=cut
sub setEMatchAttr {
  my $self = shift;
  my ( $from, $to, $value ) = @_;
  $self->setEAttr( $from, $to, Global::Values::GRAPH->{MATCH_TYPE}, $value );
  return 1;
}

=head2 setVMatchAttr($v, $rs)

Sets the matched value ($rs: 1/0) of the given vertex ($v).

=cut
sub setVMatchAttr {
  my $self = shift;
  my $v    = shift;
  my $rs   = shift;
  $self->setVAttr( $v, Global::Values::GRAPH->{REG_MATCHED}, $rs );
  return 1;
}

=head2 setEAttr($from, $to, $attrName, $attrValue)

Sets the attribute name and value of the given edge between the given two vertices ($from, $to).

=cut
sub setEAttr {
  my $self = shift;
  my ( $from, $to, $attrName, $attrValue ) = @_;
  return if ( !$self->hasVertex($from)
    || !$self->hasVertex($to)
    || !$self->hasEdge( $from, $to ) );
  $self->graph->set_edge_attribute( $from, $to, $attrName, $attrValue );
  return 1;
}

=head2 getVMatchAttr($v)

Returns the matched information of the given vertex($v)

=cut
sub getVMatchAttr {
  my $self = shift;
  my $v    = shift;
  return $self->getVAttr( $v, Global::Values::GRAPH->{REG_MATCHED} );
}

# set all the given vertices' marked
sub setAllmark {
  my $self    = shift;
  my $vArrRef = shift;
  my $marked  = shift;    # 1 or 0

  for my $v (@$vArrRef) {
    $self->markV( $v, $marked );
  }
  return 1;
}

=head2 markV($v, $marked)

Marks the given vertex with the given marked value($marked).

=cut
sub markV {
  my $self   = shift;
  my $v      = shift;
  my $marked = shift;
  $self->setVAttr( $v, Global::Values::GRAPH->{MARKED}, $marked );
  return 1;
}

=head2 isMarked($v)

Returns 1 if the given vertex is marked, returns false otherwise.

=cut
sub isMarked {
  my $self = shift;
  my $v    = shift;
  return $self->getVAttr( $v, Global::Values::GRAPH->{MARKED} );
}

=head2 getVAttr($v, $attrName)

Returns the attribute value of the given vertex($v) with the attribute name ($attrName).

=cut
sub getVAttr {
  my $self     = shift;
  my $v        = shift;
  my $attrName = shift;
  return if ( !$self->hasVertex($v) );
  return $self->graph->get_vertex_attribute( $v, $attrName );
}

=head2 setVAttr($v, $attrName, $attrValue)

Sets the attribute of the given vertex ($v).

=cut
sub setVAttr {
  my $self      = shift;
  my $v         = shift;
  my $attrName  = shift;
  my $attrValue = shift;

  return if !$self->hasVertex($v);
  $self->graph->set_vertex_attribute( $v, $attrName, $attrValue );
  return 1;
}

=head2 getDirectSuccessors($v)

Returns the array reference of immediate successors of the given vertex ($v).

=cut
sub getDirectSuccessors {
  my $self = shift;
  my $v    = shift;
  return if !$self->hasVertex($v);
  my @arr = $self->graph->successors($v);
  return \@arr;
}

=head2 getDirectPredecessors($v)

Returns the array reference of immediate predecessors of the given vertex ($v).

=cut
sub getDirectPredecessors {
  my $self = shift;
  my $v    = shift;
  return if !$self->hasVertex($v);
  my @arr = $self->graph->predecessors($v);
  return \@arr;
}
=head2 getAllSuccessors($v)

Returns the array reference of all predecessors of the given vertex ($v).

=cut
sub getAllSuccessors {
  my $self = shift;
  my $v    = shift;
  return if !$self->hasVertex($v);
  my @arr = $self->graph->all_successors($v);
  return \@arr;
}

=head2 hasVertex($v)

Returns true if this graph has the given vertex ($v), return false otherwise.

=cut
sub hasVertex {
  my $self = shift;
  my $v    = shift;
  return $self->graph->has_vertex($v);
}


=head2 isIsolated($v)

Returns true if this given vertex ($v) is isolated.

=cut
sub isIsolated {
  my $self = shift;
  my $v    = shift;

  #return if ! $self->hasVertex($v);
  return $self->graph->is_isolated_vertex($v);
}

=head2 isSourceV($v)

Returns true if the vertex ($v) is a source vertex, returns false otherwise.

A source vertex is defined as a vertex with successors but no predecessors.

=cut
sub isSourceV {
  my $self = shift;
  my $v    = shift;
  return if !$self->hasVertex($v);
  return $self->graph->is_source_vertex($v);
}

=head2 isSinkV($v)

Returns true if the vertex ($v) is a sink vertex, returns false otherwise.

A sink vertex as leaf is defined as a vertex with predecessors but no successors.

=cut
sub isSinkV {
  my $self = shift;
  my $v    = shift;
  return $self->graph->is_sink_vertex($v);
}

# Attention: returns the reference of edges_array
# @edges_array = result_array [ edge_array[from, to], edge_array[from, to] ...  ]
sub edgesTo {
  my $self = shift;
  my $v    = shift;
  return if !$self->hasVertex($v);
  my @arr = $self->graph->edges_to($v);
  return \@arr;
}

# Attention: returns the reference of edges_array
# @edges_array = result_array [ edge_array[from, to], edge_array[from, to] ...  ]
sub edgesFrom {
  my $self = shift;
  my $v    = shift;
  return if !$self->hasVertex($v);
  my @arr = $self->graph->edges_from($v);
  return \@arr;
}

=head2 addVertex($@)

Adds the given vertex into the graph

=cut
sub addVertex {
  my $self = shift;
  for my $v (@_) {
    if ( $self->hasVertex($v) ) {
      carp "There is already a same vertex $v in the graph!";
      next;
    }
    $self->graph->add_vertex($v);
  }
  return 1;
}

=head2 addEdge(@)

Adds the given vertices and the corresponding edges into the graph.

=cut
sub addEdge {
  my $self = shift;
  foreach my $vpair ( List::Util::pairs @_ ) {
    my $from = $vpair->key;
    my $to   = $vpair->value;
    next if ( !$self->hasVertex($from)
      || !$self->hasVertex($to)
      || $self->hasEdge( $from, $to ) );
    $self->graph->add_edge( $from, $to );
  }
  return 1;
}

=head2 hasEdge($from, $to)

Returns true if the graph has the given edge from beginning and ending vertices($from, $to).

=cut
sub hasEdge {
  my $self = shift;
  my $from = shift;
  my $to   = shift;
  return $self->graph->has_edge( $from, $to );
}

=head2 delEdge($from, $to)

Removes the given edge from beginning and ending vertices($from, $to).

=cut
sub delEdge {
  my $self = shift;
  my $from = shift;
  my $to   = shift;
  $self->graph->delete_edge( $from, $to );
}

=head2 hasCycle($from, $to)

Returns TRUE if the graph has a cycle, returns false otherwise.

=cut
sub hasCycle {
  my $self = shift;
  if ($self->graph->has_a_cycle) {
    return 1;
  } else {
    return 0;
  }
}

# directed acyclic Graph
sub isDag {
  my $self = shift;
  return $self->graph->is_dag;
}
#
sub isWeaklyConn {
  my $self = shift;
  return $self->graph->is_weakly_connected;
}

sub toString {
  my $self = shift;
  return $self->graph;
}

no Moose;
no Moose::Util::TypeConstraints;
__PACKAGE__->meta->make_immutable;
1;
