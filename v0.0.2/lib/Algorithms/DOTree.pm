package Algorithms::DOTree;
=encoding utf8

=head1 NAME

Algorithms::DOTree - A Tree object for inner data structure of Dietrich Online

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package is extends the Tree::Simple module C<https://metacpan.org/pod/Tree::Simple> (Author: DAVIDO).

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut


use Moose;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";
use Carp;

use Tree::Simple;
use Data::Dumper;
use Tree::Simple::VisitorFactory;
use Tree::Simple::Visitor;

use Utils::Tools;



=head1 ATTRIBUTES

=head2 treeName

=cut

has 'treeName' => (
  is => "rw",
  isa => "Str",
  required => 1,
#  writer => "setTreeName",
#  reader => "getTreeName",
);

=head2 tree
Extends the Tree::Simple
=cut
has 'tree' => (
  is => "rw",
  isa => "Tree::Simple",
  predicate => "hasTree",
#  writer => "setTree",
#  reader => "getTree",
);
=head2 posNameHash
The Map of each part of text in the Tree
=cut
has 'posNameHash' => (
  is => "rw",
  isa => "HashRef[Str]",
  predicate => "hasPosNameHash",
#  writer => "setPosNameHash",
#  reader => "getPosNameHash",
);
=head2 curNode

=cut
has 'curNode' => (
  is => 'rw',
  isa => 'Tree::Simple',
#  writer => "setCurNode",
#  reader => "getCurNode",
);


=head1 METHODS

=head2 addNewNode($nodeName, $parent)

Add new node to the parent Tree

=cut 
sub addNewNode {
  my $self = shift;
  my $nodeName = shift;
  my $parent = shift;
  my $new = Tree::Simple->new($nodeName, $parent);
  return $new;
}

=head2 getChildByNodeValue($nv)

Get the Child by the given node value.

=cut 
sub getChildByNodeValue {
  my $self = shift;
  my $nv = shift;
  my $tree = $self->tree;
  
  my $node;
  
  $tree->traverse(sub {
    my ($_tree) = @_;
    if ($_tree->getNodeValue() eq $nv) {
      $node = $_tree;
    }
  });
  
  if (! defined $node && $tree->getNodeValue eq $nv) {
    $node = $tree;
  }
  return $node;
}

=head2 removeNodeByNodeValue($nv)

Remove the Node by the given node value.

=cut 
sub removeNodeByNodeValue {
  my $self = shift;
  my $nv = shift;
  my $tree = $self->tree;
  
  my $node;
  
  #my $tempTree = $tree->clone();
  
  $node = getChildByNodeValue($tree, $nv);
  
  if (defined $node) {
    if ($node->isRoot) {
      $tree = undef;
    } else {
      my $parent = $node->getParent;
      $parent->removeChild($node);
    }
  }
  return $tree;
}


=head2 updateNodeByNodeValue($nv, $newNv)

Update the node value(name) with the given new Node value in the Tree

=cut 
sub updateNodeByNodeValue {
  my $self = shift;
  my $nv = shift;
  my $newNv = shift;
  my $tree = $self->tree;
  my $node;
  $node = getChildByNodeValue($tree, $nv);
  
  if (defined $node ) {
    $node->setNodeValue($newNv);
  }
  return $tree;
}

=head2 getLeftSiblingsByNodeValue($nv)

=cut 
sub getLeftSiblingsByNodeValue {
  my $self = shift;
  my $nv = shift;
  my $tree = $self->tree;
  
  my $node = getChildByNodeValue($tree, $nv);
  
  return undef if ($node->isRoot);
  
  my $curIndex = $node->getIndex();
  #print $curIndex;
  
  return undef if ($curIndex == 0);
  my $res = --$curIndex;
  my $parent = $node->getParent();
  
  $node = $parent->getChild($res);
  #print "left value: " . $node->getNodeValue . " \n";
  return $node;
}

=head2 getRightSiblingsByNodeValue($nv)

=cut 
sub getRightSiblingsByNodeValue {
  my $self = shift;
  my $nv = shift;
  my $tree = $self->tree;
  
  my $node = getChildByNodeValue($tree, $nv);
  
  return undef if ($node->isRoot);
  
  my $curIndex = $node->getIndex();
  my $res = ++$curIndex;
  
  my $parent = $node->getParent();
  
  $node = $parent->getChild($res);
  #print $curIndex;
  return undef if ! defined $node;
  #print "value: " . $node->getNodeValue . " \n";
#  my $parent = $node->getParent();
#  print "width: " . $parent->getWidth . " \n";
  
  return $node;
}

=head2 getAllChildren($nv)

Get all Children of the given node value in the tree.

=cut 
sub getAllChildren {
  my $self = shift;
  my $nv = shift;
  
  my $tree = $self->tree;
  
  my $node = getChildByNodeValue($tree, $nv);
  return "" if ! defined $node;
  
  my $children = $node->getAllChilren();
  return $children;
}


=head2 toString()

Show the Tree.

=cut 
sub toString {
  my $self = shift;
  if ($self->hasTree) {
    my $tree = $self->tree;
    print "###### show the tree #######\n" ;
    print ($tree->getNodeValue(), "\n");
    $tree->traverse(sub {
      my ($_tree) = @_;
      print (("  " x ($_tree->getDepth()+ 1)), $_tree->getNodeValue(), "\n");
    });
  } else {
    carp "Tree->toString(): There is no valid Tree!" ;
  }
}

# need test
#sub getRoot {
#  my $self = shift;
#  my $tree = $self->tree;
#  # and now bubble up to the parent (unless we are the root)
#  return $tree->getParent()->_setHeight($tree) unless $tree->isRoot();
#}


############################
# Constructor
############################
around 'BUILDARGS' => sub {
  my $orig = shift;
  my $class = shift;
  
  if (@_ == 2) {
    my ($treeName, $tree) = @_;
    return $class->$orig(treeName => $treeName, tree => $tree);
  } elsif (@_ == 1) {
    my ($treeName) = @_;
    return $class->$orig(treeName => $treeName);
  } else {
    return $class->$orig(@_);
  }
};

sub BUILD {
  my $self = shift;
  # set a new tree if there is not a tree
  if (! $self->hasTree) {
    my $tree = Tree::Simple->new($self->treeName, Tree::Simple->ROOT);
    $self->tree($tree);
    $self->hasTree(1);
  }
}


























no Moose;

#print "\nDOFSM.pm loaded successfully!\n";
__PACKAGE__->meta->make_immutable;
1;

