package Person;
use Moose;

has 'name' => (
  is => 'rw',
  isa => 'Str',
);
has 'happy' => (
  is => 'rw',
  isa => 'Bool',
);

no Moose;

package main;
use Moose;

use FSM::Simple;

my $m = FSM::Simple->new();

my $p = Person->new(name=>"xin");
my $initRef = sub  {
  my $rh_args = shift;
  my $person = shift;
  print "Let's make a cake\n";
  
  # Prepare ingredients
  
  $rh_args->{returned_value} = "makeCake";
  return $rh_args;
};

$m->add_state(name=> 'init', sub => $initRef);
$m->add_state(name=> 'make_cake', sub => \&makeCake);
#$m->add_state(name=> 'eat_cake', sub => \&eatCake);
#$m->add_state(name=> 'clean', sub => \&clean);
$m->add_state(name=> 'stop', sub => \&stop);

$m->add_trans(from => 'init',  to => 'make_cake', exp_val => 'makeCake');

$m->run();

#sub init {
#  my $rh_args = shift;
#  my $person = shift;
#  print "Let's make a cake\n";
#  
#  # Prepare ingredients
#  
#  $rh_args->{returned_value} = "makeCake";
#  return $rh_args;
#}

sub makeCake {
  my $rh_args = shift;
  my $hallo = shift;
  print "I'm making a cake\n";
  
  $rh_args->{returned_value} = "stop";
}


sub stop {
  my $rh_args = shift;
  print "Stop machine!\n";
  
  $rh_args->{returned_value} = undef;
  return $rh_args;
}
































1;