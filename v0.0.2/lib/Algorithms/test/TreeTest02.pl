use Tree::Simple;
 
# make a tree root
my $tree = Tree::Simple->new("0", Tree::Simple->ROOT);
 
# explicity add a child to it
$tree->addChild(Tree::Simple->new("1"));
 
# specify the parent when creating
# an instance and it adds the child implicity
my $sub_tree = Tree::Simple->new("2", $tree);
 
# chain method calls
$tree->getChild(0)->addChild(Tree::Simple->new("1.1"));
 
# add more than one child at a time
$sub_tree->addChildren(
          Tree::Simple->new("2.1"),
          Tree::Simple->new("2.2")
          );
 
# add siblings
$sub_tree->addSibling(Tree::Simple->new("3"));
 
# insert children a specified index
$sub_tree->insertChild(1, Tree::Simple->new("2.1a"));

print $tree->getChild("2.1a")->getNodeValue;
 
# clean up circular references
$tree->DESTROY();