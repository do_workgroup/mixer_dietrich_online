use Tree::Simple;
use Data::Dumper;
use Tree::Simple::VisitorFactory;
use Tree::Simple::Visitor;

my $tree = Tree::Simple->new("entry", Tree::Simple->ROOT);


#$tree->addChild(Tree::Simple->new("id"));
#$tree->addChild(Tree::Simple->new("lemma"));
#$tree->addChild(Tree::Simple->new("sigel"));

my $id_tree = Tree::Simple->new("id", $tree);
my $lemma_tree = Tree::Simple->new("lemma", $tree);
my $titel_tree = Tree::Simple->new("titel", $tree);
my $verfasser_tree = Tree::Simple->new("verfasser", $tree);
my $sigel_tree = Tree::Simple->new("sigel", $tree);
my $collation_tree = Tree::Simple->new("collation", $tree);
#
#$tree->getChild("sigel")->addChild(Tree::Simple->new("sigel_start"));
#$tree->getChild("sigel")->addChild(Tree::Simple->new("sigel_content"));
#$tree->getChild("sigel")->addChild(Tree::Simple->new("sigel_end"));


my $sig_begin = Tree::Simple->new("sig_start", $sigel_tree);
my $sig_content = Tree::Simple->new("sig_content", $sigel_tree);
my $sig_end = Tree::Simple->new("sig_end", $sigel_tree);


my $sub_sig_1 = Tree::Simple->new("sub_sig_1", $sig_content);
my $sub_sig_2 = Tree::Simple->new("sub_sig_2", $sig_content);
my $sub_sig_3 = Tree::Simple->new("sub_sig_3", $sig_content);


#my $tf = Tree::Simple::VisitorFactory->new();
#my $visitor = $tf->get("PathToRoot");

#my $visitor = Tree::Simple::VisitorFactory->getVisitor("FindByNodeValue");
#$sigel_tree->removeChild($sig_end);






sub getChildByNodeValue {
  my $tree = shift;
  my $nv = shift;
  
  my $node;
  
  $tree->traverse(sub {
    my ($_tree) = @_;
    if ($_tree->getNodeValue() eq $nv) {
      $node = $_tree;
    }
  });
  
  if (! defined $node && $tree->getNodeValue eq $nv) {
    $node = $tree;
  }
  
  return $node;
}


sub removeNodeByNodeValue {
  my $tree = shift;
  my $nv = shift;
  
  my $node;
  
  #my $tempTree = $tree->clone();
  
  $node = getChildByNodeValue($tree, $nv);
  
  if (defined $node) {
    if ($node->isRoot) {
      $tree = undef;
    } else {
      my $parent = $node->getParent;
      $parent->removeChild($node);
    }
  }
  return $tree;
}

sub updateNodeByNodeValue {
  my $tree = shift;
  my $nv = shift;
  my $newNv = shift;
  my $node;
  $node = getChildByNodeValue($tree, $nv);
  
  if (defined $node ) {
    $node->setNodeValue($newNv);
  }
  
  return $tree;
}

sub getLeftSisterByNodeValue {
  my $tree = shift;
  my $nv = shift;
  
  my $node = getChildByNodeValue($tree, $nv);
  
  return undef if ($node->isRoot);
  
  my $curIndex = $node->getIndex();
  #print $curIndex;
  
  return undef if ($curIndex == 0);
  my $res = --$curIndex;
  my $parent = $node->getParent();
  
  $node = $parent->getChild($res);
  #print "left value: " . $node->getNodeValue . " \n";
  return $node;
}

sub getRightSisterByNodeValue {
  my $tree = shift;
  my $nv = shift;
  
  my $node = getChildByNodeValue($tree, $nv);
  
  return undef if ($node->isRoot);
  
  my $curIndex = $node->getIndex();
  my $res = ++$curIndex;
  
  my $parent = $node->getParent();
  
  $node = $parent->getChild($res);
  #print $curIndex;
  return undef if ! defined $node;
  #print "value: " . $node->getNodeValue . " \n";
#  my $parent = $node->getParent();
#  print "width: " . $parent->getWidth . " \n";
  
  return $node;
}


my $str = "sig_content";
$str = "verfasser";
$str = "sigel";
#$str = "sig_end";
#$str = "sig_start";
#$str = "collation";
#$str = "entry";
#$str1 = "verweis";


#print $node->getDepth;

my $visitor = Tree::Simple::Visitor->new();
$tree->accept($visitor);
my $hallo = join ", ",  $visitor->getResults();
print $hallo . "\n";
print "###### show the tree #######\n" ;
print ($tree->getNodeValue(), "\n");
$tree->traverse(sub {
    my ($_tree) = @_;
    print (("  " x ($_tree->getDepth()+ 1)), $_tree->getNodeValue(), "\n");
});
print "###### show the tree ende! #######\n" ;

my $node = getChildByNodeValue($tree, $str); 

print Dumper($node->getChild(0)->getNodeValue);

#my $nodes = $node->getAllChildren();
#for my $item (@$nodes) {
#  #print $item->getDepth . "\n";
#  print Dumper($item->getNodeValue());
#}

#print "############ Left and Right ##########\n";
#my $lNode = getLeftSisterByNodeValue($tree, $str);
#print "lNode: ". $lNode->getNodeValue ."\n" if defined $lNode;
#my $rNode = getRightSisterByNodeValue($tree, $str);
#print "rNode: ". $rNode->getNodeValue ."\n" if defined $rNode;
#print Dumper( join ", " , $tf->getResults());
#
#$tf->setNodeFilter(sub {
#              my ($t) = @_;
#              return $t->getNodeValue()->description();
#              });




#$id_tree->addChildren(
#  Tree::Simple->new("3.1"),
#  Tree::Simple->new("3.2"),
#);

#$sub_tree->addSibling(Tree::Simple->new("4"));
#$sub_tree->insertChild(1, Tree::Simple->new("3.1a"));

#my $node = $tree->getUID("3.1");
#my $nodes = $sigel_tree->getSibling("lemma");
#my $count = $sigel_tree->getHeight;
#print $count. "\n";
#print $nodes->getNodeValue() . "\n";
#for my $item (@$nodes) {
#  #print $item->getDepth . "\n";
#  print Dumper($item->getNodeValue());
#}


#print "###### show the tree #######\n" ;
#print ($tree->getNodeValue(), "\n");
#$tree->traverse(sub {
#    my ($_tree) = @_;
#    print (("  " x ($_tree->getDepth()+ 1)), $_tree->getNodeValue(), "\n");
#});

#$tree->traverse(sub {
#    my ($_tree) = @_;
#    print ((' ' x $_tree->getDepth()), '<', $_tree->getNodeValue(),'>',"\n");
#},
#sub {
#    my ($_tree) = @_;
#    print ((' ' x $_tree->getDepth()), '</', $_tree->getNodeValue(),'>',"\n");
#});


$tree->DESTROY();





