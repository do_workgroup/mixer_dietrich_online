use JSON;
use Data::Dumper;

use JSON::XS;


use Cwd;


use 5.016;

use utf8;
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);       
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";

binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");



my $encoding = "UTF-8";







my %hash = ("a" => 1, "b" => 2, "c"=>3,);
%hash = (
  # otherline module  
  "pat_mod_otherline_check_1" => { 
                      "content"    => "",
                      "rank"       => 0,
                      "isSeg"      => 1,
                      "successorTrue"  => ['pat_strecke_1', 'pat_seite_1'],
                      "successorFalse" => [],
                      "ancestor"  => [],
                      "success"    => "1",
                      "trigger"    => ["info_1", "info_1"],
                      "visited"    => "",
                      "category"   => "otherline_module",
                      "engine"     => "re",
                      "description"=> ["In den Literaturangaben des Typs \"Otherline\" werden die Strecke- und Seitenzahl-Bereiche überprüft!", 
                                       "In den Literaturangaben des Typs \"Otherline\" werden die Strecke- und Seitenzahl-Bereiche überprüft!"],
  },
  # global pat 6: es gibt Sonderzeichen wie < > # ( ) in den bestimmten Bereichen oder nicht.
   "pat_global_pat_6" => {"content"    => qr{(\p{L}\p{N}\p{L})|(\p{N}\p{L}\p{N})},
                          "rank"       => 0,
                          "isSeg"       =>0,
                          "successorTrue"  => [],
                          "successorFalse" => [],
                          "ancestor"  => [],
                          "success"    => "",
                          "trigger"    => ["info_1", "warnung_4"],
                          "visited"    => "",
                          "category"   => "sonderzeichen",
                          "engine"     => "re",
                          "description"=> ["In der Bereich gibt es keine Zeichenketten wie '3chaft' ,'3Ö2' oder '30J'.", 
                                           "In der Bereich gibt es Zeichenketten wie '3chaft' ,'3Ö2' oder '30J'."],
   },
);



#my $json = JSON->new->utf8->pretty->space_after->encode(\%hash);



#my $json = JSON->new->convert_blessed->allow_nonref;
#print $json->encode(qr{(\p{L}\p{N}\p{L})|(\p{N}\p{L}\p{N})});
#my $encoded = $json->encode(\%hash);
#$encoded = Encode::decode("utf-8",$encoded);
#open (my $fh2, ">", "utf8-test.json");
#binmode ($fh2, ":utf8");
#print $fh2 $encoded;
#close($fh2);



#print Dumper($json);
#print Dumper($/);
#open (my $fh, ">:encoding($encoding)", "json_data.json");
#print $fh $json;


#for my $key (sort keys %hash) {
#  print "$hash{$key}\n";
#}

#my $reg = qr/[^a-z]/i;
#my %var = (
#  regex => $reg,
#);
#my $json = JSON::XS->new->utf8->pretty;
#my $pretty = $json->encode(\%var);
#print Dumper($pretty);



#########################
# YAML
#########################
use YAML;
#print YAML::Dump %hash;
#print "\n===================\n#";
#my $hashRef = \%hash;

#Bless($hashRef);

#my $ynode = ynode(Blessed($hashRef));
#print $ynode;


#open my $fh3, ">:encoding($encoding)", "yaml_test.yml";
#print $fh3 YAML::Dump(\%hash);
#close($fh3);

use File::Spec;
use File::Basename;

#my $dir = cwd;
#print $dir;
#my $file = "test.txt";
#my $path = File::Spec->catfile($dir, $file);
#print $path;

#my $file = "C:/GIT/dietrichonline/v0.0.2/lib/Algorithms/test/yaml_test.yml";
#$file = "yaml_test.yml";
#my $t = basename($file);
#print $t;



#open my $fh4, '<:encoding(UTF-8)', "yaml_test.yml" or die "can't open file: $!";
#my $yml = do {local $/; <$fh4>};
#my $config = YAML::Load($yml);
#print Dumper(ref %$config);

#########################
# XML
#########################


my $h = {
    "pat_mod_otherline_check_1" => { 
                      "content"    => "",
                      "rank"       => 0,
                      "isSeg"      => 1,
                      "successorTrue"  => ['pat_strecke_1', 'pat_seite_1'],
                      "successorFalse" => [],
                      "ancestor"  => [],
                      "success"    => "1",
                      "trigger"    => ["info_1", "info_1"],
                      "visited"    => "",
                      "category"   => "otherline_module",
                      "engine"     => "re",
                      "description"=> ["In den Literaturangaben des Typs \"Otherline\" werden die Strecke- und Seitenzahl-Bereiche überprüft!", 
                                       "In den Literaturangaben des Typs \"Otherline\" werden die Strecke- und Seitenzahl-Bereiche überprüft!"],
  },
};

#use XML::Hash;
#my $xml_converter = XML::Hash->new();
#my $xml_str = $xml_converter->fromHashtoXMLString($h);
#print $xml_str;













