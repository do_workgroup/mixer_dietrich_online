use FSM::Simple;
    
    my $machine = FSM::Simple->new();
    
    $machine->add_state(name => 'init',      sub => \&init);
    $machine->add_state(name => 'make_cake', sub => \&make_cake);
    $machine->add_state(name => 'eat_cake',  sub => \&eat_cake);
    $machine->add_state(name => 'clean',     sub => \&clean);
    $machine->add_state(name => 'stop',      sub => \&stop);
    
    $machine->add_trans(from => 'init',      to => 'make_cake', exp_val => 'makeCake');
    $machine->add_trans(from => 'make_cake', to => 'eat_cake',  exp_val => 1);
    $machine->add_trans(from => 'eat_cake',  to => 'clean',     exp_val => 'good');
    $machine->add_trans(from => 'eat_cake',  to => 'make_cake', exp_val => 'bad');
    $machine->add_trans(from => 'clean',     to => 'stop',      exp_val => 'foo');
    $machine->add_trans(from => 'clean',     to => 'stop',      exp_val => 'done');
    
    $machine->run();
    
    sub init {
        my $rh_args = shift;
        print "Let's make a cake\n";
        
        # Prepare ingredients.
        $rh_args->{flour}  = 2;    # kg
        $rh_args->{water}  = 0.5;  # liter
        $rh_args->{leaven} = 0.1;  # kg
        
        $rh_args->{returned_value} = 'makeCake';
        return $rh_args;
    }
    
    sub make_cake {
        my $rh_args = shift;
        print "I am making a cake\n";
    
        # Do somethink with ingredients
        # from $rh_arhs
        # and put the cake into $rh_args
        $rh_args->{cake} = '%%%';
    
        $rh_args->{returned_value} = 1;
        return $rh_args;
    }
    
    sub eat_cake {
        my $rh_args = shift;
        print "I am eating a cake\n";
        
        # Eat the cake from $rh_args->{cake}
        
        # If the cake is tasty then return 'good' otherwise 'bad'.
        srand;
        if (rand(1000) < 400) {
            $rh_args->{returned_value} = 'good';
        }
        else {
            $rh_args->{returned_value} = 'bad';
        }
        
        return $rh_args;
    }
    
    sub clean {
        my $rh_args = shift;
        print "I am cleaning the kitchen\n";
    
        $rh_args->{returned_value} = 'done';
        return $rh_args;
    }
    
    sub stop {
        my $rh_args = shift;
        print "Stop machine\n";
    
        $rh_args->{returned_value} = undef; # stop condition!!!
        return $rh_args;
    }