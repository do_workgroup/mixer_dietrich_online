package Person;
use Moose;
use namespace::autoclean;

use v5.16;
use FindBin;
use lib "$FindBin::Bin/../";

has 'name' => (
  is => 'rw',
  isa => 'Str',
);

has 'age' => (
  is => 'rw',
  isa => 'Int',
);

has 'happy' => (
  is => 'rw',
  isa => 'Bool',
  default => 0,
);


package main;
use Moose;
use Data::Dumper;

my $p1 = Person->new({name=>"a1", age=>34});
my $p2 = Person->new({name=>"b2", age=>24});

print $p1->happy;

use Rule::Engine::Filter;
use Rule::Engine::Rule;
use Rule::Engine::RuleSet;
use Rule::Engine::Session;

my $sess = Rule::Engine::Session->new;
$sess->set_environment('age', 30);

# Make a ruleset
my $rs = Rule::Engine::RuleSet->new(
  name => "first-RuleSet",
  filter => Rule::Engine::Filter->new(
    condition => sub {
      my ($self, $sess, $obj) = @_;
      #print "Filter: self: $self, sess: $sess, obj: $obj! \n";
      $obj->happy ? 1 : 0;
    },
  ), 
);

my $rule = Rule::Engine::Rule->new(
  name => "old",
  condition => sub {
    my ($self, $sess, $obj) = @_;
    #print "Rule->condition: self: $self, sess: $sess, obj: $obj! \n";
    return $obj->age <= $sess->get_environment('age');
  },
  action => sub {
    my ($self, $sess, $obj) =@_;
    #print "Rule->action: self: $self, sess: $sess, obj: $obj! \n";
    $obj->happy(1);
  },
);


# Add the rule
$rs->add_rule($rule);

# Add the ruleset to the session
$sess->add_ruleset($rs->name, $rs);


my @arrObj = ($p1, $p2);

my $results = $sess->execute('first-RuleSet', \@arrObj);

print Dumper($results);


































