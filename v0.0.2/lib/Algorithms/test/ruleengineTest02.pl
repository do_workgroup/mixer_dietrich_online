package Person;
use Moose;
use namespace::autoclean;

use v5.16;
use FindBin;
use lib "$FindBin::Bin/../";

has 'name' => (
  is => 'rw',
  isa => 'Str',
);

has 'age' => (
  is => 'rw',
  isa => 'Int',
);

has 'happy' => (
  is => 'rw',
  isa => 'Bool',
  default => 0,
);


package main;
use Moose;
use Data::Dumper;

my $p1 = Person->new({name=>"a1", age=>34});
my $p2 = Person->new({name=>"b2", age=>24});

#print $p1->happy;

use Rule::Engine::Filter;
use Rule::Engine::Rule;
use Rule::Engine::RuleSet;
use Rule::Engine::Session;

my $sess = Rule::Engine::Session->new;

# i.e. {age => 30, weight => 130}
my $env_hash = {};

my $filter_hash = {
  filter1 => { condition => sub {my ($self, $sess, $obj) = @_; $obj->happy ? 1 : 0;}},
  filter2 => { },
};

my $rule_hash = {
  rule1 => {name => "old",condition => \&condition1, action => \&hallo,},
};


#TODO: set a new ranking parameter





# need session processor
$sess->environment({age => 30, weight => 130});
#$sess->set_environment('weight', 130);

#print Dumper($sess->);
#exit;

my $filter1 = {
  condition => sub {
      my ($self, $sess, $obj) = @_;
      #print "Filter: self: $self, sess: $sess, obj: $obj! \n";
      $obj->happy ? 1 : 0;
    },
};

# Make a ruleset
my $rs = Rule::Engine::RuleSet->new(
  name => "first-RuleSet",
  filter => Rule::Engine::Filter->new($filter1), 
);



#print Dumper($rs->execute());
#exit;

sub hallo {
  my ($self, $sess, $obj) =@_;
  #print "Rule->action: self: $self, sess: $sess, obj: $obj! \n";
  $obj->happy(1);
}

sub condition1 {
    my ($self, $sess, $obj) = @_;
    #print "Rule->condition: self: $self, sess: $sess, obj: $obj! \n";
    return $obj->age <= $sess->get_environment('age');
  }

my $rule = Rule::Engine::Rule->new(
  name => "old",
  condition => \&condition1,
#  condition => sub {
#    my ($self, $sess, $obj) = @_;
#    #print "Rule->condition: self: $self, sess: $sess, obj: $obj! \n";
#    return $obj->age <= $sess->get_environment('age');
#  },
  action => \&hallo,
#  action => sub {
#    my ($self, $sess, $obj) =@_;
#    #print "Rule->action: self: $self, sess: $sess, obj: $obj! \n";
#    $obj->happy(1);
#  },
);



# Add the rule
$rs->add_rule($rule);

# Add the ruleset to the session
$sess->add_ruleset($rs->name, $rs);


#print Dumper($rule);
#exit;

my @arrObj = ($p1, $p2);

my $results = $sess->execute('first-RuleSet', \@arrObj);

print Dumper($results);




























