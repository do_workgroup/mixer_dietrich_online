package Database::MotDBProcessor;
=encoding utf8

=head1 NAME

Database::MotDBProcessor - Access of Database::MotDB.

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
package Database::AbstractMotDBTable;
=encoding utf8

=head1 NAME

Database::AbstractMotDBTable 

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

The abstract class for access the variables of Database::MotDB. Standard behaviors like the 

getType, insert, search, clear, update and remove methods are defined here, the programmer needs

only to extend this class and provide implementations for this methods. 

The non-abstract methods have their descriptions with its implementation in Detail.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose::Role;

use FindBin;
use lib "$FindBin::Bin/../";


=head1 REQUIRES METHODS

=head2 getMotDBTableType

Returns the subclass type

=cut
requires 'getMotDBTableType';

=head2 insert

Inserts an item into the specified table or variable of MotDB

=cut
requires 'insert';
=head2 search

=cut
requires 'search';
=head2 update

=cut
requires 'update';
=head2 remove

=cut
requires 'remove'; # delete
=head2 clear

=cut
requires 'clear';  # reset
=head2 query

=cut
requires 'query'; 
=head2 getNumAndIncr

Gets the number of a value and increase the value

=cut
requires 'getNumAndIncr';

=head2 incrNum($target)

Increments target value 

=cut
sub incrNum {
  my $self = shift;
  my $target = shift;
  $target = 0 if ! defined $target || $target eq "";
  $target++;
  return $target;
}
# decrease target value
=head2 decrNum($target)

decrements target value 

=cut
sub decrNum {
  my $self = shift;
  my $target = shift;
  $target = 0 if ! defined $target || $target eq "";
  
  $target--;
  return $target;  
}


no Moose;
1;


#CLASS:
package Database::MotDB_ElementArrayOFFile_Processor;
=encoding utf8

=head1 NAME

Database::MotDB_ElementArrayOFFile_Processor 

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This class is for access the value 'tiedFileArray' in the Database::MotDB.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

=head1 ATTRIBUTES

=head2 type

Type of the class (ANALYSE_STRATEGY_TYPE: Global::Values->doAnalyseStrategyType)

=cut
has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'FILE_ELEMENT_ARRAY',
);

=head1 METHODS

=head2 copyAll($arrRef)

Copy the given reference array of file to the Database::MotDB->tiedFileArray

=cut
sub copyAll {
  my $self = shift;
  my $arrRef = shift;
  if (! defined $arrRef || ref($arrRef) ne "ARRAY") {
    carp "This given Element Array of File is not valid!";
    return;
  }
  Database::MotDB->setTiedFileArray($arrRef);
  return 1;
}

=head2 getMotDBTableType()

=head3 Overrides:

getMotDBTableType in class Database::AbstractMotDBTable

=cut
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}
=head2 insert()

=head3 Overrides:

insert in class Database::AbstractMotDBTable

=cut
sub insert {
  my $self = shift;
}
=head2 remove()

=head3 Overrides:

remove in class Database::AbstractMotDBTable

=cut
sub remove {
  my $self = shift;
  Database::MotDB->resetTiedFileArray;
}

=head2 search($rowNum)

Gets the specified text row of text file.

=head3 Overrides:

search in class Database::AbstractMotDBTable

=head3 Parameters:

=over 4

=item *

$rowNum: the row number of the text file

=back


=head3 Returns:

the reference of string text.

=cut
sub search {
  my $self = shift;
  my $rowNum = shift;
  if (! defined $rowNum  || ! Scalar::Util::looks_like_number($rowNum)) {
    carp "There is no valid line number for inputing";
    return;
  }
  my $str = Database::MotDB->getTiedFileArray->[$rowNum];
  
  return \$str;
}

sub update {
  my $self = shift;
#  my $rowNum =shift;
#  my $textRef = shift;
#return $self->insert($rowNum, $textRef);
}

=head2 clear()

=head3 Overrides:

clear in class Database::AbstractMotDBTable

=cut
sub clear {
  my $self = shift;
  $self->remove;
}

=head2 query($rowNum)

Same as search method.

=head3 Overrides:

query in class Database::AbstractMotDBTable

=cut
sub query {
  my $self = shift;
  my $rowNum = shift;
  return $self->search($rowNum);
}

sub getNumAndIncr {}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package Database::MotDB_IO_Processor;
###########################################
package Database::MotDB_IO_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Utils::IO;

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];



########################
# Variables
########################
sub _build_iopipe {
  return Utils::IO->getInstance;
}

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'IO_PIPE',
);

has 'ioPipe' => (
  is => 'ro',
  isa => 'Utils::IO',
  predicate => 'hasIO',
  clearer => 'clearIO',
  reader => 'getIO',
  writer => 'setIO',
  init_arg => undef,
  lazy => 1,
  builder => '_build_iopipe',
);

########################
# methods
########################



########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $io =shift;
  
  return 1;
}
sub remove {
  my $self = shift;
  $self->clearIO;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $io =shift;
  return $self->insert($io);
}

sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  return $self->getIO;
}


sub getNumAndIncr {}


no Moose;
__PACKAGE__->meta->make_immutable;
1;



###########################################
# package Database::MotDB_TreeID_Processor;
###########################################
package Database::MotDB_TreeID_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'CUR_TREE_ID',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $treeID =shift;
  if (! defined $treeID  || $treeID eq "") {
    carp "There is no valid current Tree id for inputing";
    return;
  }

  Database::MotDB->setCurTreeID($treeID);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->resetCurTreeID;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $treeID =shift;
  return $self->insert($treeID);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  return Database::MotDB->getCurTreeID;
}


sub getNumAndIncr {}


no Moose;
__PACKAGE__->meta->make_immutable;
1;



###########################################
# package Database::MotDB_FieldPosID_Processor;
###########################################
package Database::MotDB_FieldPosID_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'CUR_FIELD_POSITION_ID',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $posID =shift;
  if (! defined $posID  || $posID eq "") {
    carp "There is no valid current field position id for inputing";
    return;
  }

  Database::MotDB->setCurFieldPosID($posID);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->resetCurFieldPosID;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $posID =shift;
  return $self->insert($posID);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  return Database::MotDB->getCurFieldPosID;
}


sub getNumAndIncr {}


no Moose;
__PACKAGE__->meta->make_immutable;
1;



###########################################
# package Database::MotDB_GraphID_Processor;
###########################################
package Database::MotDB_GraphID_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'CUR_GRAPH_ID',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $graphID =shift;
  if (! defined $graphID  || $graphID eq "") {
    carp "There is no valid current graph id for inputing";
    return;
  }

  Database::MotDB->setCurGraphID($graphID);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->resetCurGraphID;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $graphID =shift;
  return $self->insert($graphID);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  return Database::MotDB->getCurGraphID;
}


sub getNumAndIncr {}

no Moose;
__PACKAGE__->meta->make_immutable;
1;



###########################################
# package Database::MotDB_ModuleID_Processor;
###########################################
package Database::MotDB_ModuleID_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'CUR_MODULE_ID',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $modID =shift;
  if (! defined $modID  || $modID eq "") {
    carp "There is no valid current entry id for inputing";
    return;
  }

  Database::MotDB->setCurModuleID($modID);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->resetCurModuleID;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $modID =shift;
  return $self->insert($modID);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  return Database::MotDB->getCurModuleID;
}


sub getNumAndIncr {}


no Moose;
__PACKAGE__->meta->make_immutable;
1;



###########################################
# package Database::MotDB_TextpartID_Processor;
###########################################
package Database::MotDB_TextpartID_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'CUR_TEXTPART_ID',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $tpID =shift;
  if (! defined $tpID  || $tpID eq "") {
    carp "There is no valid current entry id for inputing";
    return;
  }

  Database::MotDB->setCurTextPartID($tpID);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->resetCurTextPartID;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $tpID =shift;
  return $self->insert($tpID);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  return Database::MotDB->getCurTextPartID;
}

sub getNumAndIncr {}

no Moose;
__PACKAGE__->meta->make_immutable;
1;




###########################################
# package Database::MotDB_EntryID_Processor;
###########################################
package Database::MotDB_EntryID_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'CUR_ENTRY_ID',
);
########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $entryID =shift;
  if (! defined $entryID  || $entryID eq "") {
    carp "There is no valid current entry id for inputing";
    return;
  }
  Database::MotDB->setCurEntryID($entryID);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->resetCurEntryID;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $entryID =shift;
  return $self->insert($entryID);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  return Database::MotDB->getCurEntryID;
}

sub getNumAndIncr {}

no Moose;
__PACKAGE__->meta->make_immutable;
1;




###########################################
# package Database::MotDB_FieldPosNum_Processor;
###########################################
package Database::MotDB_FieldPosNum_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Scalar::Util;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;


enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'FIELD_POSITION_NUM',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $posNum =shift;
  if (! defined $posNum  || ! Scalar::Util::looks_like_number($posNum)) {
    carp "There is no valid field position number for inputing";
    return;
  }

  Database::MotDB->setFieldPosNum($posNum);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->resetFieldPosNum;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $posNum =shift;
  return $self->insert($posNum);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  return Database::MotDB->getFieldPosNum;
}

# incr
sub incr {
  my $self = shift;
  my $posNum = Database::MotDB->getFieldPosNum;
  Database::MotDB->setFieldPosNum($self->incrNum($posNum));
  return 1;
}

# decr
sub decr {
  my $self = shift;
  my $posNum = Database::MotDB->getFieldPosNum;
  Database::MotDB->setFieldPosNum($self->decrNum($posNum));
  return 1;
}

sub getNumAndIncr {
  my $self = shift;
  my $num = $self->query;
  $self->incr;
  return $num;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;



###########################################
# package Database::MotDB_GraphNum_Processor;
###########################################

package Database::MotDB_GraphNum_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Scalar::Util;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;


enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'GRAPH_NUM',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $gNum =shift;
  if (! defined $gNum  || ! Scalar::Util::looks_like_number($gNum)) {
    carp "There is no valid graph number for inputing";
    return;
  }

  Database::MotDB->setGraphNum($gNum);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->resetGraphNum;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $gNum =shift;
  return $self->insert($gNum);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  return Database::MotDB->getGraphNum;
}

# incr
sub incr {
  my $self = shift;
  my $gNum = Database::MotDB->getGraphNum;
  Database::MotDB->setGraphNum($self->incrNum($gNum));
  return 1;
}

# decr
sub decr {
  my $self = shift;
  my $gNum = Database::MotDB->getGraphNum;
  Database::MotDB->setGraphNum($self->decrNum($gNum));
  return 1;
}

sub getNumAndIncr {
  my $self = shift;
  my $num = $self->query;
  $self->incr;
  return $num;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;



###########################################
# package Database::MotDB_TreeNum_Processor;
###########################################
package Database::MotDB_TreeNum_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Scalar::Util;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;


enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'TREE_NUM',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $treeNum =shift;
  if (! defined $treeNum  || ! Scalar::Util::looks_like_number($treeNum)) {
    carp "There is no valid tree number for inputing";
    return;
  }

  Database::MotDB->setTreeNum($treeNum);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->resetTreeNum;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $treeNum =shift;
  return $self->insert($treeNum);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  return Database::MotDB->getTreeNum;
}

# incr
sub incr {
  my $self = shift;
  my $treeNum = Database::MotDB->getTreeNum;
  Database::MotDB->setTreeNum($self->incrNum($treeNum));
  return 1;
}

# decr
sub decr {
  my $self = shift;
  my $treeNum = Database::MotDB->getTreeNum;
  Database::MotDB->setTreeNum($self->decrNum($treeNum));
  return 1;
}

sub getNumAndIncr {
  my $self = shift;
  my $num = $self->query;
  $self->incr;
  return $num;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


###########################################
# package Database::MotDB_SpecialNum_Processor;
###########################################

package Database::MotDB_SpecialNum_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Scalar::Util;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;


enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'SPECIAL_NUM',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $sNum =shift;
  if (! defined $sNum  || ! Scalar::Util::looks_like_number($sNum)) {
    carp "There is no valid special number for inputing";
    return;
  }

  Database::MotDB->setSpecialNum($sNum);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->resetSpecialNum;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $sNum =shift;
  return $self->insert($sNum);
}
sub clear {
  my $self = shift;
  $self->remove;
}
sub query {
  my $self = shift;
  return Database::MotDB->getSpecialNum;
}

# incr
sub incr {
  my $self = shift;
  my $sNum = Database::MotDB->getSpecialNum;
  Database::MotDB->setSpecialNum($self->incrNum($sNum));
  return 1;
}

# decr
sub decr {
  my $self = shift;
  my $sNum = Database::MotDB->getSpecialNum;
  Database::MotDB->setSpecialNum($self->decrNum($sNum));
  return 1;
}

sub getNumAndIncr {
  my $self = shift;
  my $num = $self->query;
  $self->incr;
  return $num;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

###########################################
# package Database::MotDB_ModuleNum_Processor;
###########################################

package Database::MotDB_ModuleNum_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Scalar::Util;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;


enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'MODULE_NUM',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $modNum =shift;
  if (! defined $modNum  || ! Scalar::Util::looks_like_number($modNum)) {
    carp "There is no valid module name for inputing";
    return;
  }

  Database::MotDB->setModNum($modNum);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->resetModNum;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $modNum =shift;
  return $self->insert($modNum);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  return Database::MotDB->getModNum;
}

# incr
sub incr {
  my $self = shift;
  my $modNum = Database::MotDB->getModNum;
  Database::MotDB->setModNum($self->incrNum($modNum));
  return 1;
}

# decr
sub decr {
  my $self = shift;
  my $modNum = Database::MotDB->getModNum;
  Database::MotDB->setModNum($self->decrNum($modNum));
  return 1;
}

sub getNumAndIncr {
  my $self = shift;
  my $num = $self->query;
  $self->incr;
  return $num;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

###########################################
# package Database::MotDB_TextPartNum_Processor;
###########################################

package Database::MotDB_TextPartNum_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Scalar::Util;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;


enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'TEXTPART_NUM',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $tpNum =shift;
  if (! defined $tpNum  || ! Scalar::Util::looks_like_number($tpNum)) {
    carp "There is no valid filename for inputing";
    return;
  }

  Database::MotDB->setTextPartNum($tpNum);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->resetTextPartNum;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $tpNum =shift;
  return $self->insert($tpNum);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  return Database::MotDB->getTextPartNum;
}


# incr
sub incr {
  my $self = shift;
  my $tpNum = Database::MotDB->getTextPartNum;
  Database::MotDB->setTextPartNum($self->incrNum($tpNum));
  return 1;
}

# decr
sub decr {
  my $self = shift;
  my $tpNum = Database::MotDB->getTextPartNum;
  Database::MotDB->setTextPartNum($self->decrNum($tpNum));
  return 1;
}

sub getNumAndIncr {
  my $self = shift;
  my $num = $self->query;
  $self->incr;
  return $num;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

###########################################
# package Database::MotDB_RowNum_Processor;
###########################################

package Database::MotDB_RowNum_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";


use Carp;
use Scalar::Util;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;


enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'ROW_NUMBER',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $rowNum =shift;
  if (! defined $rowNum  || ! Scalar::Util::looks_like_number($rowNum)) {
    carp "There is no valid filename for inputing";
    return;
  }

  Database::MotDB->setRowNum($rowNum);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->resetRowNum;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $rowNum =shift;
  return $self->insert($rowNum);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  return Database::MotDB->getRowNum;
}


# incr
sub incr {
  my $self = shift;
  my $row = Database::MotDB->getRowNum;
  Database::MotDB->setRowNum($self->incrNum($row));
  return 1;
}

# decr
sub decr {
  my $self = shift;
  my $row = Database::MotDB->getRowNum;
  Database::MotDB->setRowNum($self->decrNum($row));
}

sub getNumAndIncr {
  my $self = shift;
  my $num = $self->query;
  $self->incr;
  return $num;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

###########################################
# package Database::MotDB_ModuleName_Processor;
###########################################

package Database::MotDB_ModuleName_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'MODULE_NAME',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $modName =shift;
  if (! defined $modName  || $modName eq "") {
    carp "There is no valid module name for inputing";
    return;
  }

  Database::MotDB->setCurModName($modName);
  return 1;
}
sub remove {
  my $self = shift;
  
  Database::MotDB->clearCurModName;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $modName =shift;
  return $self->insert($modName);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  if ( $self->hasName) {
    return Database::MotDB->getCurModName;
  }
  return;
}


sub hasName {
  my $self = shift;
  return Database::MotDB->hasCurModName;
}

sub getNumAndIncr {}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

###########################################
# package Database::MotDB_TextpartName_Processor;
###########################################

package Database::MotDB_TextpartName_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'TEXTPART_NAME',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $tpName =shift;
  if (! defined $tpName  || $tpName eq "") {
    carp "There is no valid text part name for inputing";
    return;
  }

  Database::MotDB->setTextPartName($tpName);
  return 1;
}
sub remove {
  my $self = shift;
  
  Database::MotDB->clearTextPartName;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $tpName =shift;
  return $self->insert($tpName);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  if ( $self->hasName) {
    return Database::MotDB->getTextPartName;
  }
  return;
}


sub hasName {
  my $self = shift;
  return Database::MotDB->hasTextPartName;
}
sub getNumAndIncr {}
no Moose;
__PACKAGE__->meta->make_immutable;
1;

###########################################
# package Database::MotDB_Bandname_Processor;
###########################################

package Database::MotDB_Bandname_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'BAND_NAME',
);



########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $bandname =shift;
  if (! defined $bandname  || $bandname eq "") {
    carp "There is no valid filename for inputing";
    return;
  }

  Database::MotDB->setBandName($bandname);
  return 1;
}
sub remove {
  my $self = shift;
  
  Database::MotDB->clearBandName;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $bandname =shift;
  return $self->insert($bandname);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  if ( $self->hasName) {
    return Database::MotDB->getBandName;
  }
  return;
}


sub hasName {
  my $self = shift;
  return Database::MotDB->hasBandName;
}
sub getNumAndIncr {}
no Moose;
__PACKAGE__->meta->make_immutable;
1;


###########################################
# package Database::MotDB_Strecke_Processor;
###########################################

package Database::MotDB_Strecke_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'STRECKE',
);



########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $stk =shift;
  if (! defined $stk  || $stk eq "") {
    carp "There is no valid strecke for inputing";
    return;
  }

  Database::MotDB->setStrecke($stk);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->resetStrecke;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $stk =shift;
  return $self->insert($stk);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub query {
  my $self = shift;
  return Database::MotDB->getStrecke;
}
sub getNumAndIncr {}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

###########################################
# package Database::MotDB_Filename_Processor;
###########################################

package Database::MotDB_Filename_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'FILE_NAME',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $filename =shift;
  if (! defined $filename  || $filename eq "") {
    carp "There is no valid filename for inputing";
    return;
  }

  Database::MotDB->setFileName($filename);
  return 1;
}
sub remove {
  my $self = shift;
  
  Database::MotDB->clearFileName;
}
sub search {
  my $self = shift;
}

sub update {
  my $self = shift;
  my $filename =shift;
  return $self->insert($filename);
}
sub clear {
  my $self = shift;
  $self->remove;
}

sub hasName {
  my $self = shift;
  return Database::MotDB->hasFileName;
}

sub query {
  my $self = shift;
  if ( $self->hasName) {
    return Database::MotDB->getFileName;
  }
  return;
}
sub getNumAndIncr {}
no Moose;
__PACKAGE__->meta->make_immutable;
1;


############################################
## package Database::MotDB_IDMAP_Row_Processor;
############################################
#
#package Database::MotDB_IDMAP_Row_Processor;
#use Moose;
#use Moose::Util::TypeConstraints;
#use namespace::autoclean;
#
#use FindBin;
#use lib "$FindBin::Bin/../";
#
#use Carp;
#use List::MoreUtils;
#
#with 'Database::AbstractMotDBTable';
#
#use Global::Values;
#use Database::MotDB;
#
#enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];
#
#########################
## Variables
#########################
#
#has 'type' => (
#  is => 'ro',
#  isa => 'DO_DB_TABLE_TYPE',
#  default => 'IDMAP_ROW',
#);
#
#
#
#########################
## methods
#########################
#
#########################
## implements methods
#########################
#sub getMotDBTableType {
#  my $self = shift;
#  return $self->type;
#}
#
#sub insert {
#  my $self = shift;
#  my $rowNum = shift;
#  my $entryID = shift;
#  if (! defined $rowNum  || ! Scalar::Util::looks_like_number($rowNum)) {
#    carp "There is no valid row number for inputing";
#    return;
#  }
#  if (! defined $entryID || $entryID eq "") {
#    carp "There is no valid entryid for inputing";
#    return;
#  }
#  my $map = Database::MotDB->getIDMapRow;
#
#  if ( exists $map->{$rowNum}) {
#    carp "There is already a same row number: $rowNum!";
#    return;
#  } 
#  if (List::MoreUtils::any {$_ eq $entryID} values %{$map}) {
#    carp "There is alreay a same entryID: $entryID";
#    return;
#  }
#  $map->{$rowNum} = $entryID;
#  Database::MotDB->setIDMapRow($map);
#  return 1;
#}
#sub remove {
#  my $self = shift;
#  my $rowNum = shift;
#  # my $entryID = shift;
#  if (! defined $rowNum  || ! Scalar::Util::looks_like_number($rowNum)) {
#    carp "There is no valid id for inputing";
#    return;
#  }
#  
#  my $map = Database::MotDB->getIDMapRow;
#  if (! exists $map->{$rowNum}) {
#    return;
#  }
#  delete $map->{$rowNum};
#  Database::MotDB->setIDMapRow($map);
#}
#sub search {
#  my $self = shift;
#  my $rowNum = shift;
#  if (! defined $rowNum  || ! Scalar::Util::looks_like_number($rowNum)) {
#    carp "There is no valid row for searching";
#    return;
#  }
#  return Database::MotDB->getIDMapRow->{$rowNum};
#}
#
#sub update {
#  my $self = shift;
#  my $rowNum = shift;
#  my $entryID = shift;
#  if (! defined $rowNum  || ! Scalar::Util::looks_like_number($rowNum)) {
#    carp "There is no valid row number for inputing";
#    return;
#  }
#  if (! defined $entryID || $entryID eq "") {
#    carp "There is no valid entryid for inputing";
#    return;
#  }
#  my $map = Database::MotDB->getIDMapRow;
#
#  $map->{$rowNum} = $entryID;
#  Database::MotDB->setIDMapRow($map);
#}
#sub clear {
#  my $self = shift;
#  
#  Database::MotDB->resetIDMapRow;
#}
#
#sub query {
#  my $self = shift;
#  my $row = shift;
#  return $self->search();
#}
#sub getNumAndIncr {}
#no Moose;
#__PACKAGE__->meta->make_immutable;
#1;


############################################
## package Database::MotDB_IDMAP_Tree_Processor;
############################################
#
#package Database::MotDB_IDMAP_Tree_Processor;
#use Moose;
#use Moose::Util::TypeConstraints;
#use namespace::autoclean;
#
#use FindBin;
#use lib "$FindBin::Bin/../";
#
#use Carp;
#
#with 'Database::AbstractMotDBTable';
#
#use Global::Values;
#use Database::MotDB;
#
#enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];
#
#########################
## Variables
#########################
#has 'type' => (
#  is => 'ro',
#  isa => 'DO_DB_TABLE_TYPE',
#  default => 'IDMAP_TREE',
#);
#
#########################
## methods
#########################
#
#########################
## implements methods
#########################
#sub getMotDBTableType {
#  my $self = shift;
#  return $self->type;
#}
#
#sub insert {
#  my $self = shift;
#  my $entryID = shift;
#  my $id = shift;
#  if (! defined $id || $id eq "") {
#    carp "There is no valid id for inputing";
#    return;
#  }
#  if (! defined $entryID || $entryID eq "") {
#    carp "There is no valid entryid for inputing";
#    return;
#  }
#  my $map = Database::MotDB->getIDMapTree;
#  my @arr;
#  if (! exists $map->{$entryID}) {
#    @arr = ();
#    #$map->{$entryID} = \@arr;
#  } else {
#    @arr = @{$map->{$entryID}};
#  }
#  if (Utils::Tools->inList(\@arr, $id)) {
#    carp "There is already a same id in the same Entry!";
#    return;
#  }
#  push @arr, $id;
#  $map->{$entryID} = \@arr;
#  Database::MotDB->setIDMapTree($map);
#  return 1;
#}
#sub remove {
#  my $self = shift;
#  my $entryID = shift;
#  my $id = shift;
#  if (! defined $id || $id eq "") {
#    carp "There is no valid id for inputing";
#    return;
#  }
#  if (! defined $entryID || $entryID eq "") {
#    carp "There is no valid entryid for inputing";
#    return;
#  }
#  my $map = Database::MotDB->getIDMapTree;
#   my @arr;
#  if (! exists $map->{$entryID}) {
#    return;
#  } else {
#    @arr = @{$map->{$entryID}};
#  }
#  my $resArr = Utils::Tools->delListEle(\@arr, $id);
#  $map->{$entryID} = $resArr;
#  Database::MotDB->setIDMapTree($map)
#}
#sub search {
#  my $self = shift;
#  my $entryID = shift;
#  if (! defined $entryID || $entryID eq "") {
#    carp "There is no valid entryid for searching";
#    return;
#  }
#  return Database::MotDB->getIDMapTree->{$entryID};
#}
#
#sub update {
#  my $self = shift;
#}
#sub clear {
#  my $self = shift;
#  
#  Database::MotDB->resetIDMapTree;
#}
#
#sub query {
#  my $self = shift;
#  my $entryID = shift;
#  return $self->search($entryID);
#}
#sub getNumAndIncr {}
#no Moose;
#__PACKAGE__->meta->make_immutable;
#1;


############################################
## package Database::MotDB_IDMAP_Graph_Processor;
############################################
#
#package Database::MotDB_IDMAP_Graph_Processor;
#use Moose;
#use Moose::Util::TypeConstraints;
#use namespace::autoclean;
#
#use FindBin;
#use lib "$FindBin::Bin/../";
#
#use Carp;
#
#with 'Database::AbstractMotDBTable';
#
#use Global::Values;
#use Database::MotDB;
#
#enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];
#
#########################
## Variables
#########################
#
#has 'type' => (
#  is => 'ro',
#  isa => 'DO_DB_TABLE_TYPE',
#  default => 'IDMAP_GRAPH',
#);
#
#
#
#########################
## methods
#########################
#
#########################
## implements methods
#########################
#sub getMotDBTableType {
#  my $self = shift;
#  return $self->type;
#}
#
#sub insert {
#  my $self = shift;
#  my $entryID = shift;
#  my $id = shift;
#  if (! defined $id || $id eq "") {
#    carp "There is no valid id for insert";
#    return;
#  }
#  if (! defined $entryID || $entryID eq "") {
#    carp "There is no valid entryid for insert";
#    return;
#  }
#  my $map = Database::MotDB->getIDMapGraph;
#  my @arr;
#  if (! exists $map->{$entryID}) {
#    @arr = ();
#    #$map->{$entryID} = \@arr;
#  } else {
#    @arr = @{$map->{$entryID}};
#  }
#  if (Utils::Tools->inList(\@arr, $id)) {
#    carp "There is already a same id in the same Entry!";
#    return;
#  }
#  push @arr, $id;
#  $map->{$entryID} = \@arr;
#  Database::MotDB->setIDMapGraph($map);
#  return 1;
#}
#sub remove {
#  my $self = shift;
#  my $entryID = shift;
#  my $id = shift;
#  if (! defined $id || $id eq "") {
#    carp "There is no valid id for inputing";
#    return;
#  }
#  if (! defined $entryID || $entryID eq "") {
#    carp "There is no valid entryid for inputing";
#    return;
#  }
#  my $map = Database::MotDB->getIDMapGraph;
#   my @arr;
#  if (! exists $map->{$entryID}) {
#    return;
#  } else {
#    @arr = @{$map->{$entryID}};
#  }
#  my $resArr = Utils::Tools->delListEle(\@arr, $id);
#  $map->{$entryID} = $resArr;
#  Database::MotDB->setIDMapGraph($map)
#}
#sub search {
#  my $self = shift;
#  my $entryID = shift;
#  if (! defined $entryID || $entryID eq "") {
#    carp "There is no valid entryid for searching";
#    return;
#  }
#  return Database::MotDB->getIDMapGraph->{$entryID};
#}
#
#sub update {
#  my $self = shift;
#}
#sub clear {
#  my $self = shift;
#  
#  Database::MotDB->resetIDMapGraph;
#}
#
#
#sub query {
#  my $self = shift;
#  my $entryID = shift;
#  return $self->search($entryID);
#}
#
#sub getNumAndIncr {}
#no Moose;
#__PACKAGE__->meta->make_immutable;
#1;


############################################
## package Database::MotDB_IDMAP_Module_Processor;
############################################
#
#package Database::MotDB_IDMAP_Module_Processor;
#use Moose;
#use Moose::Util::TypeConstraints;
#use namespace::autoclean;
#
#use FindBin;
#use lib "$FindBin::Bin/../";
#
#use Carp;
#
#with 'Database::AbstractMotDBTable';
#
#use Global::Values;
#use Database::MotDB;
#
#enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];
#
#########################
## Variables
#########################
#
#has 'type' => (
#  is => 'ro',
#  isa => 'DO_DB_TABLE_TYPE',
#  default => 'IDMAP_MODULE',
#);
#
#
#
#########################
## methods
#########################
#
#########################
## implements methods
#########################
#sub getMotDBTableType {
#  my $self = shift;
#  return $self->type;
#}
#
#sub insert {
#  my $self = shift;
#  my $entryID = shift;
#  my $id = shift;
#  if (! defined $id || $id eq "") {
#    carp "There is no valid id for insert";
#    return;
#  }
#  if (! defined $entryID || $entryID eq "") {
#    carp "There is no valid entryid for insert";
#    return;
#  }
#  my $map = Database::MotDB->getIDMapMod;
#  my @arr;
#  if (! exists $map->{$entryID}) {
#    @arr = ();
#    #$map->{$entryID} = \@arr;
#  } else {
#    @arr = @{$map->{$entryID}};
#  }
#  if (Utils::Tools->inList(\@arr, $id)) {
#    carp "There is already a same id in the same Entry!";
#    return;
#  }
#  push @arr, $id;
#  $map->{$entryID} = \@arr;
#  Database::MotDB->setIDMapMod($map);
#  return 1;
#}
#sub remove {
#  my $self = shift;
#  my $entryID = shift;
#  my $id = shift;
#  if (! defined $id || $id eq "") {
#    carp "There is no valid id for inputing";
#    return;
#  }
#  if (! defined $entryID || $entryID eq "") {
#    carp "There is no valid entryid for inputing";
#    return;
#  }
#  my $map = Database::MotDB->getIDMapMod;
#   my @arr;
#  if (! exists $map->{$entryID}) {
#    return;
#  } else {
#    @arr = @{$map->{$entryID}};
#  }
#  my $resArr = Utils::Tools->delListEle(\@arr, $id);
#  $map->{$entryID} = $resArr;
#  Database::MotDB->setIDMapMod($map)
#}
#sub search {
#  my $self = shift;
#  my $entryID = shift;
#  if (! defined $entryID || $entryID eq "") {
#    carp "There is no valid entryid for searching";
#    return;
#  }
#  return Database::MotDB->getIDMapMod->{$entryID};
#}
#
#sub update {
#  my $self = shift;
#}
#sub clear {
#  my $self = shift;
#  
#  Database::MotDB->resetIDMapMod;
#}
#
#
#sub query {
#  my $self = shift;
#  my $entryID = shift;
#  return $self->search($entryID);
#}
#sub getNumAndIncr {}
#
#no Moose;
#__PACKAGE__->meta->make_immutable;
#1;


############################################
## package Database::MotDB_IDMAP_Textpart_Processor;
############################################
#
#package Database::MotDB_IDMAP_Textpart_Processor;
#use Moose;
#use Moose::Util::TypeConstraints;
#use namespace::autoclean;
#
#use FindBin;
#use lib "$FindBin::Bin/../";
#
#use Carp;
#
#with 'Database::AbstractMotDBTable';
#
#use Global::Values;
#use Database::MotDB;
#
#enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];
#
#########################
## Variables
#########################
#
#has 'type' => (
#  is => 'ro',
#  isa => 'DO_DB_TABLE_TYPE',
#  default => 'IDMAP_TEXTPART',
#);
#
#
#########################
## methods
#########################
#
#########################
## implements methods
#########################
#sub getMotDBTableType {
#  my $self = shift;
#  return $self->type;
#}
#
#sub insert {
#  my $self = shift;
#  my $entryID = shift;
#  my $id = shift;
#  if (! defined $id || $id eq "") {
#    carp "There is no valid id for insert";
#    return;
#  }
#  if (! defined $entryID || $entryID eq "") {
#    carp "There is no valid entryid for insert";
#    return;
#  }
#  my $map = Database::MotDB->getIDMapTP;
#  my @arr;
#  if (! exists $map->{$entryID}) {
#    @arr = ();
#    #$map->{$entryID} = \@arr;
#  } else {
#    @arr = @{$map->{$entryID}};
#  }
#  if (Utils::Tools->inList(\@arr, $id)) {
#    carp "There is already a same id in the same Entry!";
#    return;
#  }
#  push @arr, $id;
#  $map->{$entryID} = \@arr;
#  Database::MotDB->setIDMapTP($map);
#  return 1;
#}
#sub remove {
#  my $self = shift;
#  my $entryID = shift;
#  my $id = shift;
#  if (! defined $id || $id eq "") {
#    carp "There is no valid id for inputing";
#    return;
#  }
#  if (! defined $entryID || $entryID eq "") {
#    carp "There is no valid entryid for inputing";
#    return;
#  }
#  my $map = Database::MotDB->getIDMapTP;
#   my @arr;
#  if (! exists $map->{$entryID}) {
#    return;
#  } else {
#    @arr = @{$map->{$entryID}};
#  }
#  my $resArr = Utils::Tools->delListEle(\@arr, $id);
#  $map->{$entryID} = $resArr;
#  Database::MotDB->setIDMapTP($map)
#}
#sub search {
#  my $self = shift;
#  my $entryID = shift;
#  if (! defined $entryID || $entryID eq "") {
#    carp "There is no valid entryid for searching";
#    return;
#  }
#  return Database::MotDB->getIDMapTP->{$entryID};
#}
#
#sub update {
#  my $self = shift;
#}
#sub clear {
#  my $self = shift;
#  
#  Database::MotDB->resetIDMapTP;
#}
#
#
#sub query {
#  my $self = shift;
#  my $entryID = shift;
#  return $self->search($entryID);
#}
#
#sub getNumAndIncr {}
#no Moose;
#__PACKAGE__->meta->make_immutable;
#1;





###########################################
# package Database::MotDB_RegPatternEngineMap_Processor;
###########################################
package Database::MotDB_RegPatternEngineMap_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'REG_PATTERN_ENGINE_MAP',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  
  return 1;
}
sub remove {
  my $self = shift;
  return 1;
}
# patName: name of pattern
# partName:  "content", "successorTrue" "successorFalse", "type", "success", "failure", "engine", "description"

sub search {
  my $self = shift;
  my $patName = shift;
  my $partName = shift;
  if (! defined $patName || $patName eq "") {
    carp "There is no valid pattern name for searching";
    return;
  }
  return Database::MotDB->getRegPatternMap->{$patName}->{$partName};
}

sub getPatternMap {
  my $self = shift;
  return Database::MotDB->getRegPatternMap;
}

sub update {
  my $self = shift;
  return 1;
}
sub clear {
  my $self = shift;
  return 1;
}

sub query {
  my $self = shift;
  my $patName = shift;
  my $partName = shift;
  return $self->search($patName, $partName);
}

sub getNumAndIncr {}
no Moose;
__PACKAGE__->meta->make_immutable;
1;


###########################################
# package Database::MotDB_ReportReferenceMap_Processor;
###########################################
package Database::MotDB_ReportReferenceMap_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'REPORT_REFERENCE_MAP',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}
sub getNumAndIncr {}

sub insert {
  my $self = shift;
  
  return 1;
}
sub remove {
  my $self = shift;
  return 1;
}
# patName: name of pattern
# partName:  "content", "successorTrue" "successorFalse", "type", "success", "failure", "engine", "description"

sub search {
  my $self = shift;
  my $keyName = shift;
  my $partName = shift;
  if (! defined $keyName || $keyName eq "") {
    carp "There is no valid pattern name for searching";
    return;
  }
  return Database::MotDB->getRepRefMap->{$keyName}->{$partName};
}
# special for search
sub getReportDescription {
  my $self = shift;
  my $keyName = shift;
  my $partName = "description";
  return $self->search($keyName, $partName);
}
# special for search
sub getReportSeverity {
  my $self = shift;
  my $keyName = shift;
  my $partName = "severity";
  return $self->search($keyName, $partName);
}
# special for search
sub getReportType {
  my $self = shift;
  my $keyName = shift;
  my $partName = "type";
  return $self->search($keyName, $partName);
}
# special for search
sub getAction {
  my $self = shift;
  my $keyName = shift;
  my $partName = "action";
  return $self->search($keyName, $partName);
}

sub getReportReferenceMap {
  my $self = shift;
  return Database::MotDB->getRepRefMap;
}

sub update {
  my $self = shift;
  return 1;
}
sub clear {
  my $self = shift;
  return 1;
}

sub query {
  my $self = shift;
  my $patName = shift;
  my $partName = shift;
  return $self->search($patName, $partName);
}



no Moose;
__PACKAGE__->meta->make_immutable;
1;





###########################################
# package Database::MotDB_InterfaceMaps_Processor;
###########################################
package Database::MotDB_InterfaceMap_Pat_Rep_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

use Database::MotDB_ReportReferenceMap_Processor;
use Database::MotDB_RegPatternEngineMap_Processor;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'INTERFACE_MAPS_PATTERN_REPORT',
);

########################
# methods
########################


########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}
# only read
sub insert {}
sub remove {}
sub update {}
sub clear {}
sub getNumAndIncr {}

# Returns the text reference.
sub search {
  my $self = shift;
  my $patternName = shift;
  my $matched = shift;
  my $reportPart = shift;
  
  my $wholeRepInfo = 0;
  # reg pattern map
  if (! defined $patternName || $patternName eq "") {
    carp "There is no valid pattern name for searching";
    return;
  }
  # get the entire report information or the part of it
  if (! defined $reportPart || $reportPart eq "") {
    $wholeRepInfo = 1;
  }
  
  my $partName;
  if ($matched) {
    $partName = "success";
  } else {
    $partName = "failure";
  }
  
  my $reportKey =  Database::MotDB->getRegPatternMap->{$patternName}->{$partName};
  
  # report reference map
  if (! defined $reportKey || $reportKey eq '') {
    carp "There is no valid report key name for searching!";
    return;
  }
  if (! $wholeRepInfo) {
    # returns the given report information's part
    return ($reportKey, Database::MotDB->getRepRefMap->{$reportKey}->{$reportPart});
  } else {
    # returns the entire selected report information
    return ($reportKey, Database::MotDB->getRepRefMap->{$reportKey});
  }
  
}

sub query {
  my $self = shift;
  my $patternName = shift;
  my $matched = shift;
  my $reportPart = shift;
  return $self->search($patternName, $matched, $reportPart);
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

###########################################
# package Database::MotDB_Data_RowText_Processor;
###########################################
package Database::MotDB_Data_RowText_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'DATA_ROW_TEXT',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $textRef = shift;
  Database::MotDB->setCurRowText($$textRef);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->clearCurRowText;
}
sub update {
  my $self = shift;
  my $textRef = shift;
  $self->insert($$textRef);
}
sub clear {
  my $self = shift;
  $self->remove;
}
sub getNumAndIncr {}

# Returns the text reference.
sub search {
  my $self = shift;
  return Database::MotDB->getCurRowText;
}

sub query {
  my $self = shift;
  return $self->search;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;



###########################################
# package Database::MotDB_Data_EntryText_Processor;
###########################################
package Database::MotDB_Data_EntryText_Processor;
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

with 'Database::AbstractMotDBTable';

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

########################
# Variables
########################

has 'type' => (
  is => 'ro',
  isa => 'DO_DB_TABLE_TYPE',
  default => 'DATA_ENTRY_TEXT',
);

########################
# methods
########################

########################
# implements methods
########################
sub getMotDBTableType {
  my $self = shift;
  return $self->type;
}

sub insert {
  my $self = shift;
  my $entryText = shift;
  Database::MotDB->setCurEntryText($entryText);
  return 1;
}
sub remove {
  my $self = shift;
  Database::MotDB->clearCurEntryText;
}
sub update {
  my $self = shift;
  my $entryText = shift;
  $self->insert($entryText);
}
sub clear {
  my $self = shift;
  $self->remove;
}
sub getNumAndIncr {}

# Returns the Text::EntryText
sub search {
  my $self = shift;
  return Database::MotDB->getCurEntryText;
}

sub query {
  my $self = shift;
  return $self->search;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


################### Engine, App, Context#######################
#class:
package Database::MotDB_DBTableEngine;
=encoding utf8

=head1 NAME

Database::MotDB_DBTableEngine - an engine to store and control the access tables of MotDB

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

use Database::AbstractMotDBTable;

use Global::Values;
use Database::MotDB;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

=head1 ATTRIBUTES

=head2 dbTables

Hash map to store the Access tables.

key: type of MotDB table  - value: MotDB table (object)

=cut
has 'dbTables' => (
  is => 'rw',
  isa => 'HashRef[Database::AbstractMotDBTable]',
  predicate => 'hasDBTables',
  clearer => 'resetDBTables',
  default => sub {{}},
  lazy => 1,
);

=head1 METHODS

=head2 addTableProcessor($table)

Add a given access table ($table) to the dbTables.

=head3 Parameters:

=over 4

=item *

$table: the given access table, it extends from Database::AbstractMotDBTable

=back

=cut
sub addTableProcessor {
  my $self = shift;
  my $table = shift;
  
  # Engine::AbstractIDGenerator
  my $type = $table->getMotDBTableType;
  # put generator in the map
  $self->dbTables->{$type} = $table;
}
=head2 getTableProcessor($tType)

=head3 Parameters:

=over 4

=item *

$tType: the given type of access table, see also Global::Values->doDBTableType

=back

=head3 Returns:

The specified Access table.

=cut
sub getTableProcessor {
  my $self = shift;
  my $tType = shift;
  return $self->dbTables->{$tType};
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
package Database::MotDBTableApp;
=encoding utf8

=head1 NAME

Database::MotDBTableApp - The manager of the Access Tables of MotDB

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

use Global::Values;
use Database::MotDB;

use Database::MotDB_DBTableEngine;

use Database::AbstractMotDBTable;

enum 'DO_DB_TABLE_TYPE', [@{Global::Values->doDBTableType}];

=head1 CONSTRUCTORS

=head2 BUILD

After new.

=cut
sub BUILD {
  my $self = shift;
  # add build in Table Processors
  $self->addBuildInTableProcessors;
}

=head2 _build_dbtableengine

=cut
sub _build_dbtableengine {
  my $g = Database::MotDB_DBTableEngine->new;
  return $g;
}

=head1 ATTRIBUTES

=head2 dbTableEngine

Database::MotDB_DBTableEngine

=cut
has 'dbTableEngine' => (
  is => 'rw',
  isa => 'Database::MotDB_DBTableEngine',
  builder => "_build_dbtableengine",
  lazy => 1,
  handles => [qw( addTableProcessor getTableProcessor)],
);

=head1 METHODS

=head2 getInstance

Singleton.

=cut
my $singleton_MotDBTableApp;
sub getInstance {
  my $self = shift;
  if (! defined $singleton_MotDBTableApp) {
    $singleton_MotDBTableApp = $self->new;
  }
  return $singleton_MotDBTableApp;
}
=head2 addBuildInTableProcessors

Add the access tables to the main

=cut
sub addBuildInTableProcessors {
  my $self = shift;
  # data
  $self->addTableProcessor(Database::MotDB_Data_RowText_Processor->new);
  #$self->addTableProcessor(Database::MotDB_Data_EntryText_Processor->new);
  # io module
  $self->addTableProcessor(Database::MotDB_IO_Processor->new);
  $self->addTableProcessor(Database::MotDB_ElementArrayOFFile_Processor->new);
  # reg pattern engine map
  $self->addTableProcessor(Database::MotDB_RegPatternEngineMap_Processor->new);
  $self->addTableProcessor(Database::MotDB_ReportReferenceMap_Processor->new);
  $self->addTableProcessor(Database::MotDB_InterfaceMap_Pat_Rep_Processor->new);
  # id map
#  $self->addTableProcessor(Database::MotDB_IDMAP_Row_Processor->new);
#  $self->addTableProcessor(Database::MotDB_IDMAP_Tree_Processor->new);
#  $self->addTableProcessor(Database::MotDB_IDMAP_Graph_Processor->new);
#  $self->addTableProcessor(Database::MotDB_IDMAP_Module_Processor->new);
#  $self->addTableProcessor(Database::MotDB_IDMAP_Textpart_Processor->new);
  # id
  $self->addTableProcessor(Database::MotDB_TreeID_Processor->new);
  $self->addTableProcessor(Database::MotDB_GraphID_Processor->new);
  $self->addTableProcessor(Database::MotDB_ModuleID_Processor->new);
  $self->addTableProcessor(Database::MotDB_TextpartID_Processor->new);
  $self->addTableProcessor(Database::MotDB_EntryID_Processor->new);
  #num and name
  $self->addTableProcessor(Database::MotDB_Filename_Processor->new);
  $self->addTableProcessor(Database::MotDB_Bandname_Processor->new);
  $self->addTableProcessor(Database::MotDB_ModuleName_Processor->new);
  $self->addTableProcessor(Database::MotDB_TextpartName_Processor->new);
  $self->addTableProcessor(Database::MotDB_Strecke_Processor->new);
  $self->addTableProcessor(Database::MotDB_GraphNum_Processor->new);
  $self->addTableProcessor(Database::MotDB_TreeNum_Processor->new);
  $self->addTableProcessor(Database::MotDB_SpecialNum_Processor->new);
  $self->addTableProcessor(Database::MotDB_ModuleNum_Processor->new);
  $self->addTableProcessor(Database::MotDB_TextPartNum_Processor->new);
  $self->addTableProcessor(Database::MotDB_RowNum_Processor->new);
  $self->addTableProcessor(Database::MotDB_FieldPosNum_Processor->new);
  $self->addTableProcessor(Database::MotDB_FieldPosID_Processor->new);
}

=head2 getCurEntryText

Returns the current Text::EntryText. 

=cut
sub getCurEntryText {
  my $self = shift;
  $self->getTableProcessor("DATA_ENTRY_TEXT")->query;
}

=head2 setCurEntryText($entryText)

Sets the current Text::EntryText. 

=cut
sub setCurEntryText {
  my $self = shift;
  my $entryText = shift;
  $self->getTableProcessor("DATA_ENTRY_TEXT")->insert($entryText);
}
=head2 getCurRowText

Sets the current row text. 

=cut
sub getCurRowText {
  my $self = shift;
  $self->getTableProcessor("DATA_ROW_TEXT")->query;
}
=head2 setCurRowText($textRef)

Sets the current row text. 

=cut
sub setCurRowText {
  my $self = shift;
  my $textRef = shift;
  $self->getTableProcessor("DATA_ROW_TEXT")->insert($textRef);
}

=head2 updateEntryID($entryID)

Updates the new entryID with the given $entryID and resets the sub paramters i.e. module_num.

=cut
sub updateEntryID {
  my $self = shift;
  my $entryID = shift;
  
  my $succeeded = $self->getTableProcessor("CUR_ENTRY_ID")->insert($entryID);
  
  # reset treeNum, graphNum, modNum, textpartNum, textpartName, moduleName
  # reset treeid, moduleid, textpartid, graphid, 
  if($succeeded) {
    $self->getTableProcessor("TREE_NUM")->clear;
    $self->getTableProcessor("GRAPH_NUM")->clear;
    $self->getTableProcessor("MODULE_NUM")->clear;
    $self->getTableProcessor("TEXTPART_NUM")->clear;
    $self->getTableProcessor("TEXTPART_NAME")->clear;
    $self->getTableProcessor("MODULE_NAME")->clear;
    $self->getTableProcessor("FIELD_POSITION_NUM")->clear;
    
    $self->getTableProcessor("CUR_TEXTPART_ID")->clear;
    $self->getTableProcessor("CUR_MODULE_ID")->clear;
    $self->getTableProcessor("CUR_TREE_ID")->clear;
    $self->getTableProcessor("CUR_GRAPH_ID")->clear;
    $self->getTableProcessor("CUR_FIELD_POSITION_ID")->clear;
  }
}

=head2 hasBandName

Returns true if MotDB has band name.

=cut
sub hasBandName {
  my $self = shift;
  return $self->getTableProcessor("BAND_NAME")->hasName;
}
=head2 hasFileName

Returns true if MotDB has file name.

=cut
sub hasFileName {
  my $self = shift;
  return $self->getTableProcessor("FILE_NAME")->hasName;
}
=head2 updateFileName

=cut
sub updateFileName {
  my $self = shift;
  my $fn = shift;
  $self->getTableProcessor("FILE_NAME")->insert($fn);
}
=head2 updateBandName

=cut
sub updateBandName {
  my $self = shift;
  my $bn = shift;
  $self->getTableProcessor("BAND_NAME")->insert($bn);
}
=head2 updateStrecke

=cut
sub updateStrecke {
  my $self = shift;
  my $stk = shift;
  $self->getTableProcessor("STRECKE")->insert($stk);
}
=head2 getStrecke

=cut
sub getStrecke {
  my $self = shift;
  return $self->getTableProcessor("STRECKE")->query;
}
=head2 getFileName

=cut
sub getFileName {
  my $self = shift;
  return $self->getTableProcessor("FILE_NAME")->query;
}
=head2 getBandName

=cut
sub getBandName {
  my $self = shift;
  return $self->getTableProcessor("BAND_NAME")->query;
}
=head2 getRowNum

=cut
sub getRowNum {
  my $self = shift;
  return $self->getTableProcessor("ROW_NUMBER")->query;
}
=head2 updateRowNum

=cut
sub updateRowNum {
  my $self = shift;
  my $rn = shift;
  return $self->getTableProcessor("ROW_NUMBER")->insert($rn);
}

=head2 updateInputTextFile($fname)

Update the text file for analysis with the given file name.

Attention: The text file must be put in the input directory, see Global::Values->globVal->{dataPath}.

=cut
sub updateInputTextFile {
  my $self = shift;
  my $fname = shift;
  
  # take the input file by io
  my $io = $self->getTableProcessor("IO_PIPE")->query;
  $io->setCurFile($fname);
  $io->tieFile($io->curFile);
  $self->updateFileName($io->curFile);
  
  # tied file array
  $self->getTableProcessor("FILE_ELEMENT_ARRAY")->remove;
  $self->getTableProcessor("FILE_ELEMENT_ARRAY")->copyAll($io->tiedFile);
}

=head2 readNewRow

Read a new row text from MotDB->tiedFileArray with the MotDB->rowNum.

=cut
sub readNewRow {
  my $self = shift;
  
  my $rowNum = $self->getTableProcessor("ROW_NUMBER")->query;
  
  my $lineRef = $self->getTableProcessor("FILE_ELEMENT_ARRAY")->query($rowNum);
  
  if (defined $lineRef && ref($lineRef) eq "SCALAR"){
    $self->getTableProcessor("ROW_NUMBER")->incr;
  } else {
    croak "Read a new line was failed!";
  }
  return $lineRef;
}


=head2 getRegPatMap

Returns RegExp pattern map

=cut
sub getRegPatMap {
  my $self = shift;
  return $self->getTableProcessor("REG_PATTERN_ENGINE_MAP")->getPatternMap;
}

=head2 getRepRefMap

Returns report-reference map

=cut
sub getRepRefMap {
  my $self = shift;
  return $self->getTableProcessor("REPORT_REFERENCE_MAP")->getReportReferenceMap;
}

=head2 getValueFromRegPatToReport($patternName, $matched, $reportPart)

Returns the value from report-reference map by the pattern name and the matched information.

=cut
sub getValueFromRegPatToReport {
  my $self = shift;
  my $patternName = shift;
  my $matched = shift;
  my $reportPart = shift;
  
  return $self->getTableProcessor("INTERFACE_MAPS_PATTERN_REPORT")->query($patternName, $matched, $reportPart);
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

