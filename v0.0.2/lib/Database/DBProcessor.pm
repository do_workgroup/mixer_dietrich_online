package Database::DBProcessor;
=encoding utf8

=head1 NAME

Database::DBProcessor - Database interface for Dietrich Online

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use utf8;
use Encode;

#use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use v5.20;

no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
package Database::DBDao;
=encoding utf8

=head1 NAME

Database::DBDao - Data Access Object Interface

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This is abstract superclass defines the standard operations to be performed on a model objects. 

Standard behaviors like the getDBDAOType, add, update, delete, find and clear methods are defined here, 

the programmer needs only to extend this class and provide implementations for this methods. 

The non-abstract methods have their descriptions with its implementation in Detail.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose::Role;

use FindBin;
use lib "$FindBin::Bin/../";
use utf8;
use Encode;

#use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use v5.20;
########################
# requires Variables
########################

########################
# requires methods
########################
=head1 REQUIRES METHODS

=head2 getDBDAOType

Returns the DAO type

=cut
requires 'getDBDAOType';

=head2 add

Add an item to the database or table

=cut
requires 'add';
=head2 update

Update an item in the database or table

=cut
requires 'update';
=head2 delete

Remove an item in the database or table

=cut
requires 'delete';      # delete
=head2 findById

Find an item in the database or table by ID

=cut
requires 'findById';    # query?
=head2 findAll

Find the items in the database or table

=cut
requires 'findAll';     # search?
=head2 clear

=cut
requires 'clear';  # reset

no Moose;
1;

#CLASS:
package Database::DatabaseUtils;
=encoding utf8

=head1 NAME

Database::DatabaseUtils

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

Provides utility methods that can be used by access the dababase

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;
use Try::Tiny;
use DBI;
use Cwd;
use File::Find::Rule;
use File::Spec;
use DBIx::Class;

use utf8;
use Encode;

#use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use v5.20;

use Global::Values;
use Database::MotDB;
use Utils::IO;

use Database::DietrichDB::Schema;

=head1 CONSTRUCTORS

=head2 _build_curschema

Schema preparation in DBIx::Class

=cut
sub _build_curschema {
  my $self = shift;
  return $self->newSchema;
}
=head2 _build_configmap

Gets the config file for database access

=cut
sub _build_configmap {
  my $self = shift;
  return $self->getDBConfig;
}
=head2 _build_curdb

Gets the information of current database

=cut
sub _build_curdb {
  my $self = shift;
  return $self->getSQLiteDBFileName;
}

=head1 ATTRIBUTES

=head2 curSchema

The Schema class for access database (DBIx::Class)

=cut
has 'curSchema' => (
  is        => 'rw',
  isa       => 'Database::DietrichDB::Schema',
  predicate => 'hasCurSchema',
  clearer   => 'clearSchema',
  builder   => '_build_curschema',
  lazy      => 1,
);
=head2 curSchema

The current database information

=cut
has 'curDB' => (
  is        => 'rw',
  isa       => 'Str',
  predicate => 'hasCurDB',
  clearer   => 'clearDB',
  builder   => '_build_curdb',
  lazy      => 1,
);
=head2 dbh

database handle object (DBI)

=cut 
has 'dbh' => (
  is => 'rw',
  #  isa => 'Database::Schema',
  predicate => 'hasDBH',
  clearer   => 'clearDBH',
);

=head2 sth

statement handle object (DBI)

=cut 
has 'sth' => (
  is        => 'rw',
  predicate => 'hasSTH',
  clearer   => 'clearSTH',
  writer    => 'setSth',
  reader    => 'getSth',
);
=head2 configMap

The hashmap of configuration information

=cut 
has 'configMap' => (
  is      => 'ro',
  isa     => 'HashRef',
  builder => '_build_configmap',
  reader  => 'getConfigMap',
  lazy    => 1,
);

=head1 METHODS

=head2 getInstance

Singleton.

=cut
my $singleton_DatabaseUtils;
sub getInstance {
  my $self = shift;
  if ( !defined $singleton_DatabaseUtils ) {
    $singleton_DatabaseUtils = $self->new;
  }
  return $singleton_DatabaseUtils;
}
=head2 dropTableByDBI($dbh, $tableName)

Drop an tablespace($tablename) with database handle object ($dbh)

=cut
sub dropTableByDBI {
  my $self = shift;
  my ( $dbh, $tableName ) = @_;
  if ( !defined $dbh || $dbh eq "" || !defined $tableName || $tableName eq "" ) {
    croak "There is no valid input dbh or table name!";
  }
  $dbh->do("DROP TABLE IF EXISTS \'$tableName\' ");
}

=head2 createTable($tableName, $stmtRef, $isDrop)

Drop an tablespace($tablename) with database handle object ($dbh)

Attention: Close the dbh after all done!

=cut
sub createTable {
  my $self      = shift;
  my $tableName = shift;
  my $stmtRef   = shift;    # statement to create table
  my $isDrop    = shift;

  # table name, statement
  if ( !defined $tableName
    || $tableName eq ""
    || !defined $stmtRef
    || $stmtRef eq ""
    || $$stmtRef eq "" )
  {
    croak "There is no valid table name or statement to create table!";
  }

  # dbh
  my $dbh;
  if ( !$self->hasDBH ) {
    $dbh = $self->updateDBHandle;
  }

  $dbh = $self->dbh;

  # default is drop
  if ( !defined $isDrop || $isDrop eq "" ) { $isDrop = 1 }

  if ($isDrop) {
    $self->dropTableByDBI( $dbh, $tableName );
  }
  # try - catch - finally
  try {
    my $returnValue = $dbh->do($$stmtRef);
    if ( $returnValue < 0 ) {
      croak "DBI Error by create Table $tableName: " . $DBI::errstr;
    }
    else {
      print "create $tableName table is successfully!\n";
    }
  }
  finally {
    $self->disconnect_dbh;
    print "Database disconnected!\n";
  };
}
=head2 disconnect_dbh

=cut
# dbh disconnect
sub disconnect_dbh {
  my $self = shift;
  if ( $self->hasDBH ) {
    $self->dbh->disconnect();
  }
}


=head2 updateDBHandle($dbDriver)

Sets the dbh with the given database driver ($dbDriver)

=cut
sub updateDBHandle {
  my $self     = shift;
  my $dbDriver = shift;

  if ( !defined $dbDriver || $dbDriver eq "" ) {
    $dbDriver = "sqlite";
  }
  # get the configuration file
  my $configHash = $self->getConfigMap;
  my $dbh;
  given ($dbDriver) {
    when (/^sqlite/i) {
      my $dsn     = $configHash->{DBI_SQLITE_CONNECTION}->{dsn};
      my $user          = $configHash->{DBI_SQLITE_CONNECTION}->{user};
      my $password      = $configHash->{DBI_SQLITE_CONNECTION}->{password};
      my $optional_attr = $configHash->{DBI_SQLITE_CONNECTION}->{optional_attr};
      #my $dsn           = $dsnPrefix . $self->curDB;
      $dbh = DBI->connect( $dsn, $user, $password, $optional_attr );
    }
    when (/^mysql/i) {

    }
    default {
      croak "This would be imposible with this db driver: $dbDriver!";
    }
  }
#  print Dumper($dbh);
  # set it to dbh
  $self->dbh($dbh);
}

=head2 getSQLiteDBFileName()

Sets the name of sqlite database. It will be given in the Global::Values when initializing the entire system.

=cut
sub getSQLiteDBFileName {
  my $self = shift;
  my $name = File::Spec->catfile( Global::Values->globVal->{dataResultPath},
    Global::Values::ANALYSE_PARAMETERS->{DATABASE_RESULT_FILENAME_2} );
  $name = Cwd::abs_path($name);
  return $name;
}

=head2 getDBConfig()

Gets the configuration information of database access

=cut
sub getDBConfig {
  my $self = shift;
  my $io   = Utils::IO->getInstance;
  my $configHash =
    $io->readConfigFile( Global::Values::ANALYSE_PARAMETERS->{DATABASE_CONFIG_FILENAME} );
  return $configHash;
}

=head2 getDBConfig()

Sets a new Schema for data access

=cut
sub newSchema {
  my $self = shift;
  my $type = shift;

  # default;
  if ( !defined $type || $type eq "" ) {
    $type = "sqlite";
  }
  my $configHash = $self->getConfigMap;
  if ( !defined $configHash || $configHash eq "" ) {
    croak "There is no valid config file!";
  }
  my $schema;
  given ($type) {
    when (/^sqlite/i) {
      my $dsnPrefix     = $configHash->{DBIC_SQLITE_CONNECTION}->{dsn_prefix};
      my $user          = $configHash->{DBIC_SQLITE_CONNECTION}->{user};
      my $password      = $configHash->{DBIC_SQLITE_CONNECTION}->{password};
      my $optional_attr = $configHash->{DBIC_SQLITE_CONNECTION}->{optional_attr};
      # get the dsn from the current database file
      my $dsn           = $dsnPrefix . $self->curDB;
      $schema = Database::DietrichDB::Schema->connect( $dsn, $user, $password, $optional_attr );
    }
    when (/^mysql/i) {

    }
    default {
      croak "This would be imposible with this type: $type!";
    }
  }
  return $schema;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

#NTODO:
#CLASS:
package Database::EntryTextDAOImpl;
=encoding utf8

=head1 NAME

Database::EntryTextDAOImpl - DAO Class for entries 
for analyse text parts

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;

#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Clone;
use Data::Dumper;
use Scalar::Util;

use DBIx::Class::ResultClass::HashRefInflator;

use Global::Values;
use Database::DatabaseUtils;
use Database::DietrichDB::Schema;

use Text::EntryText;

with 'Database::DBDao';

enum 'DB_DAO_TYPE', [ @{ Global::Values->doDBDaoType } ];

=head1 ATTRIBUTES

=head2 entryBatch

the batch cache to store temporary entry information.

=cut
has 'entryBatch' => (
  traits  => ["Array"],
  is      => 'rw',
  isa     => 'ArrayRef',
  default => sub { [] },
  handles => {
    allEntries        => 'elements',
    addToEntryBatch   => 'push',
    countEntryBatch   => 'count',
    isEntryBatchEmpty => 'is_empty',
    resetEntryBatch   => 'clear',
  },
  trigger => \&_batch_check,
);

=head2 type

Type of the class (DB_DAO_TYPE: Global::Values->doDBDaoType)

=cut
has 'type' => (
  is      => 'ro',
  isa     => 'DB_DAO_TYPE',
  default => 'ENTRYTEXT_DAO_IMPL',
);

=head1 METHODS

=head2 severity_gt($severity)

The result items that their servertiy more than the given severity ($severity).

=cut
sub severity_gt {
  my $self = shift;
  my $severity = shift;
  
  # checks the input
  return if (! defined $severity || ! Scalar::Util::looks_like_number($severity));
  
  # get schema
  my $dbu = Database::DatabaseUtils->getInstance;
  my $schema = $dbu->curSchema;
  $schema->storage->debug(1);
  #CONSIDER: case sensitive? i.e. Entries or entries
  my $entriesModel = $schema->resultset('Entries');
  
  my $rs = $entriesModel->severity_gt($severity);
  
  return $rs;
}






=head2 _batch_check()

Check the size of batch, if it greater than Global::Values->globVal->{batchLimit}, then update the batch

to the database and clear the batch.

=cut
sub _batch_check {
  my $self = shift;
  #print "batch_check \n";
  # return if batch is empty
  return if ( $self->isEntryBatchEmpty);
  my $count = $self->countEntryBatch;
  # update batch if it more than batchLimit
  #print Dumper("count: " . $count);
  if ($count > 0 && $count >= Global::Values->globVal->{batchLimit}) {
    #print Dumper("add batch to DB");
    $self->addBatchToDB("entries");
    $self->entryBatch([]);
  }
}
=head2 getDBDAOType()

=head3 Overrides:

getDBDAOType in class Database::DBDao

=cut
sub getDBDAOType {
  my $self = shift;
  return $self->type;
}

=head2 clear()

=head3 Overrides:

clear in class Database::DBDao

=cut
sub clear {
  my $self = shift;
  $self->resetEntryBatch;
}
=head2 add()

=head3 Overrides:

add in class Database::DBDao

=cut
sub add {
  my $self = shift;
}

=head2 update()

=head3 Overrides:

update in class Database::DBDao

=cut
sub update {
  my $self = shift;
}
=head2 delete()

=head3 Overrides:

delete in class Database::DBDao

=cut
sub delete {
  my $self = shift;
}
=head2 findById()

=head3 Overrides:

findById in class Database::DBDao

=cut
sub findById {
  my $self = shift;
  my $eid = shift;
  
  # checks the input
  return if (! defined $eid || $eid eq "");
  
  # get schema
  my $dbu = Database::DatabaseUtils->getInstance;
  my $schema = $dbu->curSchema;
  $schema->storage->debug(1);
  #CONSIDER: case sensitive? i.e. Entries or entries
  my $entriesModel = $schema->resultset('Entries');
  
  return $entriesModel->by_id($eid);
  
}

=head2 findByColName()

=head3 Overrides:

findByColName in class Database::DBDao

=cut
sub findByColName {
  my $self = shift;
  my $cName = shift;
  my $str = shift;
  # checks the input
  return if (! defined $cName || $cName eq "" || ! defined $str || $str eq "");
  
  # get schema
  my $dbu = Database::DatabaseUtils->getInstance;
  my $schema = $dbu->curSchema;
  $schema->storage->debug(1);
  #CONSIDER: case sensitive? i.e. Entries or entries
  my $entriesModel = $schema->resultset('Entries');
  
  #return $entriesModel->by_col($cName, $str);
  return $entriesModel->by_col($cName, $str);
  
  
  
#  my $rs = $entriesModel->search(
#    {},             # where statement
#    {
#      page => 1,
#      rows => 3,
#    },
#  );
#  return $rs->all();
}

=head2 colsToArrRef($rs)

Returns the array reference of hashmaps of the columns of the given resultSet.

=cut
sub colsToArrRef {
  my $self = shift;
  my $rs = shift;
  
  return if (! defined $rs || $rs eq "");
  
  # DBIx::Class::ResultClass::HashRefInflator
  $rs->result_class('DBIx::Class::ResultClass::HashRefInflator');
  
  my @result = $rs->all();
  
  return \@result;
}



=head2 findAll()

=head3 Overrides:

findAll in class Database::DBDao

=cut
sub findAll {
  my $self = shift;
}
=head2 addBatchToDB($tableName)

Add the batch to the table with table name ($tablename)

=cut
sub addBatchToDB {
  my $self = shift;
  my $tableName = shift; # case sensitive
  if (! defined $tableName || $tableName eq "") {
    croak "There is no valid table name!";
  }
  my $dbu = Database::DatabaseUtils->getInstance;
  my $schema = $dbu->curSchema;
  $schema->storage->debug(1);
  # empty
  return if $self->isEntryBatchEmpty;
  # send all to the database
  my @batchArr = $self->allEntries;
  # add it to DB
  #print Dumper(@batchArr);
  $schema->populate(ucfirst($tableName),\@batchArr);
  # reset batch
  $self->resetEntryBatch;
}

=head2 addEntryToBatch($entryText)

Add the information of the given Text::EntryText into the batch

=cut
sub addEntryToBatch {
  my $self = shift;
  my $entryText = shift;
  
  if (! defined $entryText || $entryText eq "" 
  || ! Utils::Tools->isBlessed($entryText, "Text::EntryText")) {
    croak "There is no valid Text::EntryText to input";
  }
  
  my $resHash = {};
  my $entryColArrRef = Global::Values->doEntryStmts->{entryColumnsNoID};

  for my $col (@$entryColArrRef) {
    given ($col) {
      when (/^rowNum$/) {$resHash->{$col} = $entryText->getRowNumber;}
      when (/^entryID$/) {$resHash->{$col} = $entryText->getEntryID;}
      when (/^entryType$/) {$resHash->{$col} = $entryText->getEntryType;}
      when (/^severity$/) {$resHash->{$col} = $entryText->getSeverity;}
      when (/^originalText$/) {$resHash->{$col} = $entryText->getOrigEntryText;}
      when (/^header$/) {$resHash->{$col} = $self->joinTextParts($entryText, "HEADER", "textPart");}
      when (/^AttrID$/) {$resHash->{$col} = $entryText->getHeaderInfo->{id};}
      when (/^AttrBand$/) {$resHash->{$col} = $entryText->getHeaderInfo->{bd};}
      when (/^AttrSeite$/) {$resHash->{$col} = $entryText->getHeaderInfo->{se};}
      when (/^AttrSpalte$/) {$resHash->{$col} = $entryText->getHeaderInfo->{sp};}
      when (/^AttrZeile$/) {$resHash->{$col} = $entryText->getHeaderInfo->{ze};}
      when (/^lemma$/) {$resHash->{$col} = $self->joinTextParts($entryText, "LEMMA_ELEMENT", "content");}
      when (/^title$/) {$resHash->{$col} = $self->joinTextParts($entryText, "TITEL", "textPart");}
      when (/^author$/) {$resHash->{$col} = $self->joinTextParts($entryText, "VERFASSER", "content");}
      when (/^sigel$/) {$resHash->{$col} = $self->joinTextParts($entryText, "SIGEL_ELEMENT", "content");}
      when (/^collation$/) {$resHash->{$col} = $self->joinTextParts($entryText, "COLLATION_ELEMENT", "textPart");}
      when (/^verweis$/) {
        my $verweisText =  $self->joinTextParts($entryText, "VERWEIS_ELEMENT", "textPart");
        my $verweisLemmaText =  $self->joinTextParts($entryText, "VERWEIS_LEMMA_ELEMENT", "textPart");
        if ($verweisText eq "" && $verweisLemmaText eq "") {
          $resHash->{$col} = "";
        } else {
          my @resArr = ($verweisText, $verweisLemmaText);
          my $res; 
          my $sep = Global::Values::SEPARATOR->{ITEM_DB_ITEM};
          for my $temp (@resArr) {
            next if $temp eq "";
            if (! defined $res || $res eq "") {
              $res = $temp;
            } else {
              $res = join($sep, $res, $temp);
            }
          }
          $resHash->{$col} = $res;
        }
      }
      when (/^homonym$/) {$resHash->{$col} = $self->joinTextParts($entryText, "HOMONYM_ELEMENT", "content");}
      when (/^repetition$/) {$resHash->{$col} = $self->joinTextParts($entryText, "REP_ELEMENT", "content");}
      when (/^DitoElement$/) {$resHash->{$col} = $self->joinTextParts($entryText, "DITO_ELEMENT", "content");}
      when (/^DitoDescendant$/) {$resHash->{$col} = $self->joinTextParts($entryText, "DITO_DESC_PART", "textPart");}
      when (/^ignore$/) {$resHash->{$col} = $self->joinTextParts($entryText, "IGNORE_ELEMENT", "content");}
      when (/^sic$/) {$resHash->{$col} = $self->joinTextParts($entryText, "SIC_ELEMENT", "content");}
      when (/^corr$/) {$resHash->{$col} = $self->joinTextParts($entryText, "CORR_ELEMENT", "content");}
      when (/^sub$/) {$resHash->{$col} = $self->joinTextParts($entryText, "SUB_ELEMENT", "content");}
      when (/^sup$/) {$resHash->{$col} = $self->joinTextParts($entryText, "SUP_ELEMENT", "content");}
      when (/^dop$/) {$resHash->{$col} = $self->joinTextParts($entryText, "DOP_ELEMENT", "content");}
      when (/^ok$/) {$resHash->{$col} = $self->joinTextParts($entryText, "OK_ELEMENT", "content");}
      when (/^charErrors$/) {$resHash->{$col} = $self->joinTextParts($entryText, "CHAR_ERROR_ELEMENT", "textPart");}
      when (/^problems$/) {
        my $problems = $entryText->getProblemReports;
        my $sep = Global::Values::SEPARATOR->{ITEM_DB_ITEM};
        if (! defined $problems || $problems eq "" || ref($problems) ne "ARRAY" ) {
          $resHash->{$col} = "";
        }
        my $res = join($sep, @$problems);
        $resHash->{$col} = $res; 
      }
      when (/^unknownparts$/) {
        $resHash->{$col} = $self->joinTextParts($entryText, "UNKNOWN_PART", "textPart", " ");
      }
      when (/^others$/) {}
      default {
        croak "Oops, This column $col cannot be recognized!";
      }
    }
  }
  
  # add it to the batch
  $self->addToEntryBatch($resHash);
  $resHash = undef;
  return 1;
}
=head2 joinTextParts($entryText, $ptType, $textType, $separator)

Concatenates the text parts into a entire text string.

=head3 Paramters:

=over 4

=item *

$entryText: the given Text::EntryText (object)

=item *

$ptType: the given type of Text::EntryPartText (object)

=item *

$textType: the selected type of text content: textPart or content

=item *

$separator: separator character(s) in the string.

=back

=head3 Returns:

a result string.

=cut
sub joinTextParts {
  my $self = shift;
  my $entryText = shift;
  my $ptType = shift;
  my $textType = shift; # textpart or content
  my $separator = shift;
  
  if (! defined $ptType || $ptType eq "") {
    croak "There is no valid parttext type!";
  }
  if (! defined $textType || $textType eq "") {
    $textType = "textPart";
  }
  
  my $partTextArrRef = $entryText->getPartTextsFromMap($ptType);
  my @arr = ();
  for my $pt (@$partTextArrRef) {
    if ($textType eq "textPart") {
      push @arr, $pt->getTextPart;
    } elsif ($textType eq "content") {
      push @arr, $pt->getContent;
    }
  }
  # found nothing
  return "" if (! scalar @arr);
  if (! defined $separator) {
    $separator = Global::Values::SEPARATOR->{ITEM_DB_ITEM};
  }
  my $res = join($separator, @arr);
  return $res;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;



################### Engine, App, Context#######################
#class:
package Database::DaoSelectorEngine;
=encoding utf8

=head1 NAME

Database::DaoSelectorEngine - an engine to store and control DAO Models to access database

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

use Database::DBDao;

use Global::Values;
use Database::MotDBProcessor;

enum 'DB_DAO_TYPE', [ @{ Global::Values->doDBDaoType } ];


=head1 ATTRIBUTES

=head2 dbDaoMap

DAO map to store the DAO implements

key: type of DAO  - value: DAO (object)

=cut
has 'dbDaoMap' => (
  is => 'rw',
  isa => 'HashRef[Database::DBDao]',
  predicate => 'hasDBDaoMap',
  clearer => 'resetDBDaoMap',
  default => sub {{}},
  lazy => 1,
);


=head1 METHODS

=head2 addDao($dao)

Add a given DAO implement ($dao) to the dbDaoMap.

=head3 Parameters:

=over 4

=item *

$dao: the given DAO implement

=back

=cut
sub addDao {
  my $self = shift;
  my $dao = shift;
  
  # Engine::AbstractIDGenerator
  my $type = $dao->getDBDAOType;
  # put generator in the map
  $self->dbDaoMap->{$type} = $dao;
}
=head2 getDao($daoType)

=head3 Parameters:

=over 4

=item *

$daoType: the given type of DAO implement, see also Global::Values->doDBDaoType

=back

=head3 Returns:

The specified DAO implement

=cut
sub getDao {
  my $self = shift;
  my $daoType = shift;
  my $dao = $self->dbDaoMap->{$daoType};
  # make sure old data is clear
  $dao->clear;
  return $dao;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
package Database::DBDaoSelector;
=encoding utf8

=head1 NAME

Database::DBDaoSelector - a controller of the DAO implements and DAO engine

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

use Global::Values;
use Database::MotDB;

use Database::DaoSelectorEngine;

use Database::DBDao;

enum 'DB_DAO_TYPE', [ @{ Global::Values->doDBDaoType } ];

=head1 CONSTRUCTORS

=head2 BUILD

After new.

=cut
sub BUILD {
  my $self = shift;
  # add build in
  $self->addBuildIn;
}
=head2 _build_daoselectorengine

=cut
sub _build_daoselectorengine {
  my $g = Database::DaoSelectorEngine->new;
  return $g;
}

=head1 ATTRIBUTES

=head2 selectorEngine

Database::DaoSelectorEngine

=cut
has 'selectorEngine' => (
  is => 'rw',
  isa => 'Database::DaoSelectorEngine',
  builder => "_build_daoselectorengine",
  handles => [qw( addDao getDao)],
);

=head1 METHODS

=head2 getInstance

Singleton.

=cut
my $singleton_DBDaoSelector;
sub getInstance {
  my $self = shift;
  if (! defined $singleton_DBDaoSelector) {
    $singleton_DBDaoSelector = $self->new;
  }
  return $singleton_DBDaoSelector;
}
=head2 addBuildIn

Add the DAO implement to the main

=cut
sub addBuildIn {
  my $self = shift;
  $self->addDao(Database::EntryTextDAOImpl->new);
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
package Database::DatabaseManager;
=encoding utf8

=head1 NAME

Database::DatabaseManager - The manager to control the DAO and DAO engine

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;

use utf8;
use Encode;

#use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use v5.20;

use Global::Values;
use Database::MotDB;
use Utils::IO;

use Database::DietrichDB::Schema;
use Database::DatabaseUtils;

enum 'DO_DB_TABLE_TYPE', [ @{ Global::Values->doDBTableType } ];
=head1 CONSTRUCTORS

=head2 _build_dbdaoselector

=cut
sub _build_dbdaoselector {
  return Database::DBDaoSelector->getInstance;
}

=head1 ATTRIBUTES

=head2 daoSelector

Database::DBDaoSelector

=cut
has 'daoSelector' => (
  is => 'ro',
  isa => 'Database::DBDaoSelector',
  builder => '_build_dbdaoselector',
  lazy => 1,
);

=head1 METHODS

=head2 getInstance

Singleton.

=cut
my $singleton_DatabaseManager;
sub getInstance {
  my $self = shift;
  if (! defined $singleton_DatabaseManager) {
    $singleton_DatabaseManager = $self->new;
  }
  return $singleton_DatabaseManager;
}
=head2 getDaoImpl($daoType)

Returns the DAO implement with the given DAO type

=cut
sub getDaoImpl {
  my $self = shift;
  my $daoType = shift; 
  return $self->daoSelector->getDao($daoType);
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

