package Database::DietrichDB::Schema::ResultSet::Entry;
use base qw/DBIx::Class::ResultSet/;
use v5.20;

# make sure input/output Stream is UTF-8
use open ':std', 'encoding(UTF-8)';
use utf8;
no if $] >= 5.018, warnings => "experimental::smartmatch";
binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");

#use lib "../../../";
use FindBin;
use Cwd;
use Data::Dumper;
use lib "$FindBin::Bin/../../../../";
use Global::Values;

# Table columns:
# eid rowNum entryID entryType severity originalText header AttrID AttrBand AttrSeite AttrSpalte 
# AttrZeile lemma title author sigel collation verweis homonym repetition DitoElement DitoDescendant  
# ignore sic corr sub sup dop ok charErrors problems unknownparts others


sub by_id {
  my $self = shift;
  my $eid = shift;
  return $self->find({eid => $eid});
}

sub by_rowNum {
  my $self = shift;
  my $rowNum = shift;
  return shift->find({rowNum => $rowNum});
}



1;
=head1 NAME
Database::DietrichDB::Schema::ResultSet::Entry
=head1 METHODS
=head2 by_id
Find one entry by id
=head2 by_name
Find one blog entry by title
=head2 by_tags
Search blogs by related tags
=head2 latest
Most recently posted entry, chained from L<not_hidden>
=head2 hidden
Hidden entries (by 'hidden' tag)
=head2 not_hidden
Available entries
=cut