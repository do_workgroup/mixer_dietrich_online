package Database::DietrichDB::Schema::ResultSet::Entries;
use base qw/DBIx::Class::ResultSet/;
use DBIx::Class::ResultClass::HashRefInflator;
use v5.20;

# make sure input/output Stream is UTF-8
use open ':std', 'encoding(UTF-8)';
use utf8;
no if $] >= 5.018, warnings => "experimental::smartmatch";
binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");

#use lib "../../../";
use FindBin;
use Cwd;
use Data::Dumper;
use Carp;

use lib "$FindBin::Bin/../../../../";
use Global::Values;

# Table columns:
# eid rowNum entryID entryType severity originalText header AttrID AttrBand AttrSeite AttrSpalte 
# AttrZeile lemma title author sigel collation verweis homonym repetition DitoElement DitoDescendant  
# ignore sic corr sub sup dop ok charErrors problems unknownparts others



sub has_dito {
  my $self = shift;
  my $str = shift;
  if (! defined $str || $str eq "") {
    # there is nothing to input
    return;
  }
  
  if ($str =~ m/dito/i) {
    return $self->search({
       DitoElement => {'!=', undef},
       DitoDescendant => {'!=', undef},
    });
  }
  return;
}


# ok, dop, sic, corr, ignore, sub, sup, 
# charErrors, problems, unknownparts
sub has_special_col {
  my $self = shift;
  my $str = shift;
  
  if (! defined $str || $str eq "") {
    # there is nothing to input
    return;
  }
  my $colName;
  given ($str) {
    when(/^ok$/i) {$colName = 'ok';}
    when(/^dop$/i) {$colName = 'dop';}
    when(/^sic$/i) {$colName = 'sic';}
    when(/^corr$/i) {$colName = 'corr';}
    when(/^ignore$/i) {$colName = 'ignore';}
    when(/^sub$/i) {$colName = 'sub';}
    when(/^sup$/i) {$colName = 'sup';}
    when(/^homonym$/i) {$colName = 'homonym';}
    when(/^repetition$/i) {$colName = 'repetition';}
    when(/error/i) {$colName = 'charErrors';}
    when(/problem/i) {$colName = 'problems';}
    when(/unknown/i) {$colName = 'unknownparts';}
    when(/^ditoEle/i) {$colName = 'DitoElement';}
    when(/^DitoDesc/i) {$colName = 'DitoDescendant';}
    default {
      carp "This is not valid search parameter!";
      return;
    }
  }
  
  if (defined $colName && $colName ne "") {
     return $self->search({
       $colName => {'!=', undef},
    });
  }
  return;
  
}



# whether the given String equal the value of the defined column or not, with SQL-Equal
sub by_col {
  my $self = shift;
  my $colName = shift;
  my $str = shift;
  
  if (! defined $str || $str eq "" 
    || ! defined $colName || $colName eq "") {
    # there is nothing to input
    return;
  }
  given ($colName) {
    when(/^originalText$/i) {$colName = 'originalText';}
    when(/^entryid$/i) {$colName = 'entryID';}
    when(/^entrytype$/i) {$colName = 'entryType';}
    when(/^unkno$/i) {$colName = 'unknownparts';}
    when(/band/i) {$colName = 'AttrBand';}
    when(/seite/i) {$colName = 'AttrSeite';}
    when(/spalte/i) {$colName = 'AttrSpalte';}
    when(/zeile/i) {$colName = 'AttrZeile';}
    when(/^lemma$/i) {$colName = 'lemma';}
    when(/^sic$/i) {$colName = 'sic';}
    when(/^dop$/i) {$colName = 'dop';}
    when(/^corr$/i) {$colName = 'corr';}
    when(/^rowN/i) {$colName = 'rowNum';}
    when(/^repe/i) {$colName = 'repetition';}
    when(/^hom/i) {$colName = 'homonym';}
    when(/^ign/i) {$colName = 'ignore';}
    when(/^coll/i) {$colName = 'collation';}
    when(/^seve/i) {$colName = 'severity';}
    when(/^ditoele/i) {$colName = 'DitoElement';}
    when(/^ditodesc/i) {$colName = 'DitoDescendant';}
    when(/^title$/i) {$colName = 'title';}
    when(/^author$/i) {$colName = 'author';}
    when(/^sigel$/i) {$colName = 'sigel';}
    when(/^collation$/i) {$colName = 'collation';}
    when(/^verweis$/i) {$colName = 'verweis';}
    when(/^problems$/i) {$colName = 'problems';}
    default {
      carp "This is not valid search parameter!";
      return;
    }
  }
  
  if (defined $colName && $colName ne "") {
    return $self->search(
      {
        $colName => $str,   
      },
    );
  }
  return;
}

# whether the given String in the defined column or not, with SQL-LIKE
sub in_col {
  my $self = shift;
  my $colName = shift;
  my $str = shift;
  
  if (! defined $str || $str eq "" 
    || ! defined $colName || $colName eq "") {
    # there is nothing to input
    return;
  }
  given ($colName) {
    when(/^originalText$/i) {$colName = 'originalText';}
    when(/^lemma$/i) {$colName = 'lemma';}
    when(/^title$/i) {$colName = 'title';}
    when(/^author$/i) {$colName = 'author';}
    when(/^sigel$/i) {$colName = 'sigel';}
    when(/^collation$/i) {$colName = 'collation';}
    when(/^verweis$/i) {$colName = 'verweis';}
    when(/^problems$/i) {$colName = 'problems';}
    default {
      carp "This is not valid search parameter!";
      return;
    }
  }
  # valid column name
  if (defined $colName && $colName ne "") {
    return $self->search({
      $colName => {'like','%$str%'},
    });
  }
  return;
}



sub severity_gt {
  my $self = shift;
  my $severity = shift;
  return $self->search({
    severity => {'>' => $severity},
  });
}

# TEST:
sub severity_gt_eq {
  my $self = shift;
  my $severity = shift;
  return $self->search({
    severity => $severity,
    severity => {'>' => $severity},
  });
}

sub by_id {
  my $self = shift;
  my $eid = shift;
  return $self->find({eid => $eid});
}

sub by_rowNum {
  my $self = shift;
  my $rowNum = shift;
  return $self->find({rowNum => $rowNum});
}

sub by_severity {
  my $self = shift;
  my $severity = shift;
  return $self->search({severity => $severity});
}




1;
=head1 NAME
Database::DietrichDB::Schema::ResultSet::Entry
=head1 METHODS
=head2 by_id
Find one entry by id
=head2 by_name
Find one blog entry by title
=head2 by_tags
Search blogs by related tags
=head2 latest
Most recently posted entry, chained from L<not_hidden>
=head2 hidden
Hidden entries (by 'hidden' tag)
=head2 not_hidden
Available entries
=cut