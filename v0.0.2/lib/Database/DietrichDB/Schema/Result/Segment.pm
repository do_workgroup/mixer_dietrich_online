package Database::DietrichDB::Schema::Result::Segment;
use base qw/DBIx::Class::Core/;
use v5.20;

# make sure input/output Stream is UTF-8
use open ':std', 'encoding(UTF-8)';
use utf8;
no if $] >= 5.018, warnings => "experimental::smartmatch";
binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");

#use lib "../../../";
use FindBin;
use Cwd;
use Data::Dumper;
use lib "$FindBin::Bin/../../../../";
use Global::Values;


__PACKAGE__->table('segment');
__PACKAGE__->add_columns(qw/sid name/);
__PACKAGE__->set_primary_key('sid');
#__PACKAGE__->has_one('log' => 'DietrichDB::Schema::Result::Log', 'interID'); 



1;