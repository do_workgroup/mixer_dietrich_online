package Database::DietrichDB::Schema::Result::Entries;
use base qw/DBIx::Class::Core/;
use v5.20;

# make sure input/output Stream is UTF-8
use open ':std', 'encoding(UTF-8)';
use utf8;
no if $] >= 5.018, warnings => "experimental::smartmatch";
binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");

#use lib "../../../";
use FindBin;
use Cwd;
use Data::Dumper;
use lib "$FindBin::Bin/../../../../";
use Global::Values;


__PACKAGE__->table('entries');
__PACKAGE__->add_columns(@{Global::Values->doEntryStmts->{entryColumns}});
__PACKAGE__->set_primary_key('eid');



1;