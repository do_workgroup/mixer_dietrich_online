package Database::MotDB;
=encoding utf8

=head1 NAME

Database::MotDB - Inner Database for System processing

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package will be accessor from Database::MotDBProcessor

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use MooseX::Singleton;
use Moose::Util::TypeConstraints;
use FindBin;
use lib "$FindBin::Bin/../";

use utf8;
use Encode;

#use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);

#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use v5.20;

use Data::Dumper;
use Carp;
use Scalar::Util;

use Text::EntryText;

use Global::Values;
use Utils::Tools;
use Utils::IO;



=head1 CONSTRUCTORS

=head2 _build_bandname

=cut
sub _build_bandname {
  return Global::Values->globVal->bandName;
}
=head2 _build_regpatternmap

=cut
sub _build_regpatternmap {
  my $mapName = Global::Values::ANALYSE_PARAMETERS->{REGEXP_PATTERN_HASH};
  return Global::Values->regExpInfos->{$mapName};
}
=head2 _build_reportreferencemap

=cut
sub _build_reportreferencemap {
  my $mapName = Global::Values::ANALYSE_PARAMETERS->{REPORT_REFERENCE_HASH};
  return Global::Values->regExpInfos->{$mapName};
}

=head1 ATTRIBUTES

=head2 tiedFileArray

Tied files in the db waiting for analysis

=cut
has 'tiedFileArray' => (
  is => 'ro',
  isa => 'ArrayRef[Str]',
  predicate => 'hasTiedFileArray',
  clearer => 'resetTiedFileArray',
  reader => 'getTiedFileArray',
  writer => 'setTiedFileArray',
  default => sub {[]},
  lazy => 1,
  init_arg => undef,
  #trigger => \@_changedTiedArray,
);




########## current id of items ##########
# it cannot be visited directly.
=head2 curEntryID

ID of Text::EntryText

=cut
has 'curEntryID' => (
  is => 'ro',
  isa => 'Str',
  predicate => 'hasCurEntryID',
  clearer => 'resetCurEntryID',
  reader => 'getCurEntryID',
  writer => 'setCurEntryID',
  default => "",
  lazy => 1,
  init_arg => undef,
  trigger => \&_change_entryid,
);

# it cannot be visited directly.
=head2 curTextPartID

ID of Text::EntryPartText

=cut
has 'curTextPartID' => (
  is => 'ro',
  isa => 'Str',
  predicate => 'hasCurTextPartID',
  clearer => 'resetCurTextPartID',
  reader => 'getCurTextPartID',
  writer => 'setCurTextPartID',
  default => "",
  lazy => 1,
  init_arg => undef,
);

# it cannot be visited directly.
=head2 curTextPartID

ID of current analyse module

=cut
has 'curModuleID' => (
  is => 'ro',
  isa => 'Str',
  predicate => 'hasCurModuleID',
  clearer => 'resetCurModuleID',
  reader => 'getCurModuleID',
  writer => 'setCurModuleID',
  default => "",
  lazy => 1,
  init_arg => undef,
);
# it cannot be visited directly.
=head2 curGraphID

ID of current analyse graph

=cut
has 'curGraphID' => (
  is => 'ro',
  isa => 'Str',
  predicate => 'hasCurGraphID',
  clearer => 'resetCurGraphID',
  reader => 'getCurGraphID',
  writer => 'setCurGraphID',
  default => "",
  lazy => 1,
  init_arg => undef,
);
# it cannot be visited directly.
=head2 curTreeID

TODO

=cut
has 'curTreeID' => (
  is => 'ro',
  isa => 'Str',
  predicate => 'hasCurTreeID',
  clearer => 'resetCurTreeID',
  reader => 'getCurTreeID',
  writer => 'setCurTreeID',
  default => "",
  lazy => 1,
  init_arg => undef,
);
# it cannot be visited directly.
has 'curFieldPosID' => (
  is => 'ro',
  isa => 'Str',
  predicate => 'hasCurFieldPosID',
  clearer => 'resetCurFieldPosID',
  reader => 'getCurFieldPosID',
  writer => 'setCurFieldPosID',
  default => "",
  lazy => 1,
  init_arg => undef,
);


######### entryid -> id maps ######## 

# map: (row) 1 <-> (entryID) xxxx
# entryID: bandName=fileName=rowNum
#has 'IDMap_Row' => (
#  is => 'rw',
#  isa => "HashRef[Str]",
#  predicate => 'hasIDMapRow',
#  clearer => 'resetIDMapRow',
#  reader => 'getIDMapRow',
#  writer => 'setIDMapRow',
#  default => sub { {} },
#  lazy => 1,
#  #trigger => \&_size_test,
#);

# entryID => [TextPartIDs];
#has 'IDMap_TextPart' => (
#  is => 'rw',
#  isa => 'HashRef[ArrayRef[Str]]',
#  predicate => 'hasIDMapTP',
#  clearer => 'resetIDMapTP',
#  default => sub {{}},
#  lazy => 1,
#  reader => 'getIDMapTP',
#  writer => 'setIDMapTP',
#  init_arg => undef,
#);

## entryID => [ModuleIDs];
#has 'IDMap_Module' => (
#  is => 'rw',
#  isa => 'HashRef[ArrayRef[Str]]',
#  predicate => 'hasIDMapMod',
#  clearer => 'resetIDMapMod',
#  default => sub {{}},
#  lazy => 1,
#  reader => 'getIDMapMod',
#  writer => 'setIDMapMod',
#  init_arg => undef,
#);


# entryID => [GraphIDs];
#has 'IDMap_Graph' => (
#    is => 'rw',
#  isa => 'HashRef[ArrayRef[Str]]',
#  predicate => 'hasIDMapGraph',
#  clearer => 'resetIDMapGraph',
#  default => sub {{}},
#  lazy => 1,
#  reader => 'getIDMapGraph',
#  writer => 'setIDMapGraph',
#  init_arg => undef,
#);

# entryID => [TreeIDs];
#has 'IDMap_Tree' => (
#  is => 'rw',
#  isa => 'HashRef[ArrayRef[Str]]',
#  predicate => 'hasIDMapTree',
#  clearer => 'resetIDMapTree',
#  default => sub {{}},
#  lazy => 1,
#  reader => 'getIDMapTree',
#  writer => 'setIDMapTree',
#  init_arg => undef,
#);

######### environment of analysis ######## 

# it cannot be visited directly.
=head2 rowNum

Row number of current text row in the text file

=cut
has 'rowNum' => (
  is => 'ro',
  isa => 'Int',
  predicate => 'hasRowNum',
  clearer => 'resetRowNum',
  reader => 'getRowNum',
  writer => 'setRowNum',
  default => 0,
  lazy => 1,
  init_arg => undef,
);

# it cannot be visited directly.
=head2 bandName

The band name of current text file

=cut
has 'bandName' => (
  is => 'ro',
  isa => 'Str',
  predicate => 'hasBandName',
  clearer => 'clearBandName',
  reader => 'getBandName',
  writer => 'setBandName',
  init_arg => undef,
#  lazy => 1,
#  builder => '_build_bandname',
);


# file name as 'd74.txt'
=head2 fileName

The file name 

=cut
has 'fileName' => (
  is => 'ro',
  isa => 'Str',
  predicate => 'hasFileName',
  clearer => 'clearFileName',
  reader => 'getFileName',
  writer => 'setFileName',
  init_arg => undef,
);

# Strecke 
=head2 strecke

=cut
has 'strecke' => (
  is => 'ro',
  isa => 'Str',
  predicate => 'hasStrecke',
  clearer => 'resetStrecke',
  reader => 'getStrecke',
  writer => 'setStrecke',
  default => 'a',
  lazy => 1,
  init_arg => undef,
);


######## Analyse numbers and names #######

=head2 textPartNum

number of text part in a entry

=cut
has 'textPartNum' => (
  is => 'ro',
  isa => 'Int',
  predicate => 'hasTextPartNum',
  clearer => 'resetTextPartNum',
  reader => 'getTextPartNum',
  writer => 'setTextPartNum',
  default => 0,
  lazy => 1,
  init_arg => undef,
);

=head2 moduleNum

number of module in a entry

=cut
has 'moduleNum' => (
  is => 'ro',
  isa => 'Int',
  predicate => 'hasModNum',
  clearer => 'resetModNum',
  reader => 'getModNum',
  writer => 'setModNum',
  default => 0,
  lazy => 1,
  init_arg => undef,
);
=head2 specialNum

number of special part in a entry

=cut
has 'specialNum' => (
  is => 'ro',
  isa => 'Int',
  predicate => 'hasSpecialNum',
  clearer => 'resetSpecialNum',
  reader => 'getSpecialNum',
  writer => 'setSpecialNum',
  default => 0,
  lazy => 1,
  init_arg => undef,
);

=head2 graphNum

number of graph in a entry

=cut
has 'graphNum' => (
  is => 'ro',
  isa => 'Int',
  predicate => 'hasGraphNum',
  clearer => 'resetGraphNum',
  reader => 'getGraphNum',
  writer => 'setGraphNum',
  default => 0,
  lazy => 1,
  init_arg => undef,
);

=head2 treeNum

number of tree in a entry

=cut
has 'treeNum' => (
  is => 'ro',
  isa => 'Int',
  predicate => 'hasTreeNum',
  clearer => 'resetTreeNum',
  reader => 'getTreeNum',
  writer => 'setTreeNum',
  default => 0,
  lazy => 1,
  init_arg => undef,
);
# position number in a entry
has 'fieldPosNum' => (
  is => 'ro',
  isa => 'Int',
  predicate => 'hasFieldPosNum',
  clearer => 'resetFieldPosNum',
  reader => 'getFieldPosNum',
  writer => 'setFieldPosNum',
  default => 0,
  lazy => 1,
  init_arg => undef,
);

=head2 curModuleName

current module name in the entry

=cut
has 'curModuleName' => (
  is => 'ro',
  isa => 'Str',
  predicate => 'hasCurModName',
  clearer => 'clearCurModName',
  reader => 'getCurModName',
  writer => 'setCurModName',
#  default => "",
#  lazy => 1,
  init_arg => undef,
);

=head2 curTextPartName

current Text::EntryPartText name in the entry

=cut
has 'curTextPartName' => (
  is => 'ro',
  isa => 'Str',
  predicate => 'hasTextPartName',
  clearer => 'clearTextPartName',
  reader => 'getTextPartName',
  writer => 'setTextPartName',
  init_arg => undef,
);

=head2 curTextPartName

current string content of the entry

=cut
has 'curRowText' => (
  is => 'ro',
  isa => 'Str',
  predicate => 'hasCurRowText',
  clearer => 'clearCurRowText',
  reader => 'getCurRowText',
  writer => 'setCurRowText',
  init_arg => undef,
);
=head2 curEntryText

current Text::EntryText of the entry

=cut
has 'curEntryText' => (
  is => 'ro',
  isa => 'Text::EntryText',
  predicate => 'hasCurEntryText',
  clearer => 'clearCurEntryText',
  reader => 'getCurEntryText',
  writer => 'setCurEntryText',
  init_arg => undef,
);
############## Reg exp Engine ##############
# Reg exp engine map
# read only
=head2 regPatternMap

The Map of RegExp Information for text analysis

Attention: Read only!

=cut
has 'regPatternMap' => (
  is => 'ro',
  isa => 'HashRef',
  predicate => 'hasRegPatternMap',
  builder => '_build_regpatternmap',
  lazy => 1,
  reader => 'getRegPatternMap',
#  clearer => 'resetRegEngineMap',
#  writer => '',
  init_arg => undef,
);
=head2 reportReferenceMap

The Map of Report Information about analysis

Attention: Read only!

=cut
has 'reportReferenceMap' => (
  is => 'ro',
  isa => 'HashRef',
  predicate => 'hasRepRefMap',
  builder => '_build_reportreferencemap',
  lazy => 1,
  reader => 'getRepRefMap',
#  clearer => 'resetRegEngineMap',
#  writer => '',
  init_arg => undef,
);

=head1 METHODS

=head2 _change_entryid

Trigger when entry id changes.

=cut
sub _change_entryid {
  my ($self, $id, $old_id) = @_;
  $old_id = "" if (! defined $old_id);
  #print "self: $self - old id: $old_id - new id: $id \n";
  if ($old_id eq $id) {
    carp "The new interID is same as the old interID! Please check it!";
    return;
  } else {
    # reset treeNum, graphNum, modNum, textpartNum
    $self->resetTreeNum;
    $self->resetGraphNum;
    $self->resetModNum;
    $self->resetTextPartNum;    
  }
}

=head2 _size_test

Check the batch size.

=cut
sub _size_test {
  my ($self, $targetHash) = @_;
  my $outOfBoundary = 0;
  my $bound = Global::Values->globVal->batchLimit;
  
  my $size = scalar keys %$targetHash;
  # bigger than the default bound
  if ($size > $bound) {
    print "Out of default boundary Global::Value->batchLimit!";
    $outOfBoundary = 1;
  }
  return 1;
}


no Moose::Util::TypeConstraints;
no Moose;
__PACKAGE__->meta->make_immutable;
1;
