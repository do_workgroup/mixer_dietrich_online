package Coordinate::Point;
=encoding utf8

=head1 NAME

Coordinate::Point - A Point representing a location in the string text, specified in integer precision.

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Scalar::Util;

with 'Coordinate::AbstractPoint';


=head1 CONSTRUCTORS

=head2 BUILDARGS

In new.

=cut
around 'BUILDARGS' => sub {
  my $orig  = shift;
  my $class = shift;

  # There is no parameter
  if ( @_ == 0 ) {
    return $class->$orig( x => 0 );
  }
  elsif ( @_ == 1 ) {

    # There is one parameter which 'x' is.
    my ($x) = @_;    # Point->new(x);
    return $class->$orig( x => $x );
  }
  else {
    # normal input
    # Point->new(@list_or hashref)
    return $class->$orig(@_);
  }
};

=head1 METHODS

=head2 setPos()

=head3 Overrides:

setPos in class Coordinate::AbstractPoint

=cut
sub setPos {
  my $self = shift;
  my $newX = shift;

  if ( Scalar::Util::looks_like_number($newX) ) {
    $self->setX($newX);
    return 1;
  }
  return;
}

=head2 getPos()

=head3 Overrides:

getPos in class Coordinate::AbstractPoint

=cut
sub getPos {
  my $self = shift;
  return $self->getX;
}
=head2 toString()

=head3 Overrides:

toString in class Coordinate::AbstractPoint

=cut
sub toString {
  my $self = shift;
  return "Point : x -> " . $self->getPos();
}
=head2 clear()

=head3 Overrides:

clear in class Coordinate::AbstractPoint

=cut
sub clear {
  my $self = shift;
  $self->reset;
}
=head2 reset()

The position value will be reset to default value (0).

=cut
sub reset {
  my $self = shift;
  $self->setPos(0);
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

