package Coordinate::AbstractPoint;
=encoding utf8

=head1 NAME

Coordinate::AbstractPoint - location of a point

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This class is defines a abstract point, its location is for now in x coordinate system.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose::Role;

use FindBin;
use lib "$FindBin::Bin/../";


=head1 ATTRIBUTES

=head2 x

the X coordinate of this system

=cut
has 'x' => (
  is        => 'rw',
  isa       => 'Num',
  predicate => 'hasX',
  clearer   => 'clearX',
  writer    => "setX",
  reader    => "getX",
);

#has 'y' => (
#  is        => 'rw',
#  isa       => 'Num',
#  predicate => 'hasY',
#  clearer   => 'clearY',
#  writer    => "setY",
#  reader    => "getY",
#);

=head1 REQUIRES METHODS

=head2 toString

Display th point by string

=cut
requires 'toString';

=head2 clear

Resets the class to its initial, empty state.

=cut
requires 'clear';
=head2 setPos

Sets the location of this point.

=cut
requires 'setPos';
=head2 getPos

Returns the location of this point.

=cut
requires 'getPos';


no Moose;
1;
