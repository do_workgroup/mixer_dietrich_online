package Coordinate::FieldPosition;
=encoding utf8

=head1 NAME

Coordinate::FieldPosition

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

TODO

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;

use v5.16;
use FindBin;
use lib "$FindBin::Bin/../";

use Scalar::Util;
use Carp;

use Global::Values;
use Utils::Tools;
use Coordinate::Point;

use Database::MotDB;
use Database::MotDBProcessor;

use Coordinate::Point;
extends 'Coordinate::TextIndex';
# variables: beginIndex(Coordinate::Point), endIndex(Coordinate::Point), length(Int)
# methods: validParameters(void), reset(void)

########################
# variables
########################
# the id or name of source text
has 'source' =>(
  is => 'rw',
  isa => 'Str',
  writer => 'setSource',
  reader => 'getSource',
  clearer => 'resetSource',
  default => '',
  lazy => 1,
  init_arg => undef,
);

# field position id
has 'posID' => (
  is => 'rw',
  isa => 'Str',
  predicate => 'hasPosID',
  clearer => 'resetPosID',
  reader => 'getPosID',
  writer => 'setPosID',
  default => "",
  lazy => 1,
  init_arg => undef,
);


########################
# methods
########################

sub setFieldBeginIndex {
  my $self = shift;
  my $posX = shift;
  if (! defined $posX  || ! Scalar::Util::looks_like_number($posX)) {
    carp "This is no valid position index for FieldPosition";
    return;
  }
  my $p = $self->getBeginIndex;
  $p->setPos($posX);
  $self->setBeginIndex($p);
  return 1;
}
# return position with integer
sub getFieldBeginIndex {
  my $self = shift;
  return $self->getBeginIndex->getPos;
}

sub setFieldEndIndex {
  my $self = shift;
  my $posX = shift;
  if (! defined $posX  || ! Scalar::Util::looks_like_number($posX)) {
    carp "This is no valid position index for FieldPosition";
    return;
  }
  my $p = $self->getEndIndex;
  $p->setPos($posX);
  $self->setEndIndex($p);
  return 1;
}
# return position with integer
sub getFieldEndIndex {
  my $self = shift;
  return $self->getEndIndex->getPos;
}


########################
# extends methods
########################


override 'reset' => sub {
  #my $orig = shift;
  my $self = shift;
#  $self->$orig(@_);  
  super();
  
  $self->resetSource;
  $self->resetPosID;
};


no Moose;
__PACKAGE__->meta->make_immutable;
1;
