package Coordinate::TextIndex;
=encoding utf8

=head1 NAME

Coordinate::TextIndex 

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package is for making a index system of string text. It could have a begin index, end index 

and possibly the text length between the begin and end indices.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;

use v5.20;
use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;

use Scalar::Util;

use Coordinate::Point;

use Global::Values;
use Utils::Tools;


=head1 CONSTRUCTORS

=head2 BUILDARGS

In new.

=cut
around 'BUILDARGS' => sub {
  my $orig = shift;
  my $class = shift;
  
  if (@_ == 0) {
    # There is no parameter 
    my $p1 = Coordinate::Point->new();
    my $p2 = Coordinate::Point->new();
    my @arr = ();
    return $class->$orig(beginIndex=> $p1, endIndex=> $p2, length => 0);
  } elsif (@_ == 1) {
    # There is one parameter which 'x' is.
    my ($begin) = @_; # Point->new(x);
    if (! Utils::Tools::isObject($begin)) {
         die "Input Parameter is not Moose::Object!";
       }
    return $class->$orig(beginIndex=> $begin);
  } elsif (@_ == 2) {
    # There is one parameter which 'x' is.
    my ($begin, $end) = @_; # Point->new(x);
    if (! Utils::Tools::isObject($begin) || ! Utils::Tools::isObject($end)) {
         die "Input Parameters are not Moose::Object!";
       }
    if ($begin->getX > $end->getX) {
      die "beginIndex should be less than or equal to end Index";
    }
    my $len = $end->getX - $begin->getX;
    return $class->$orig(beginIndex=> $begin, endIndex=> $end, length=>$len );
  } elsif (@_ == 3) {
    # There is one parameter which 'x' is.
    my ($begin, $end, $length) = @_; # Point->new(x);
    if (! Utils::Tools::isObject($begin) || ! Utils::Tools::isObject($end)
       ) {
         die "Input Parameters are not Moose::Object!";
       }
    if ($begin->getX > $end->getX) {
      die "beginIndex should be less than or equal to end Index";
    }
    my $len = $end->getX - $begin->getX;
    if ($len != $length) {
      print STDERR "Warning: The Length paramter has some problem!\n";
    }
    return $class->$orig(beginIndex=> $begin, endIndex=> $end, length=>$len );
  }
  else {
    # normal input
    # Point->new(@list_or hashref)
    return $class->$orig(@_);
  }
};

=head1 ATTRIBUTES

=head2 beginIndex

The beginning index of the string

=cut
has 'beginIndex' => (
  is => 'rw',
  isa => 'Coordinate::Point',
  predicate => 'hasBeginIndex',
  writer => "setBeginIndex",
  reader => "getBeginIndex",
);
=head2 endIndex

The ending index of the string

=cut
has 'endIndex' => (
  is => 'rw',
  isa => 'Coordinate::Point',
  predicate => 'hasEndIndex',
  writer => "setEndIndex",
  reader => "getEndIndex",
);
=head2 length

The length of the string

=cut
has 'length' =>(
  is => 'rw',
  isa => 'Int',
  predicate => 'hasLength',
  writer => "setLength",
  reader => "getLength",
);

=head1 METHODS

=head2 isSameBeginEnd($inputIndex)

checke the input TextIndex whether has same begin-/End positions

=head3 Parameters:

=over 4

=item *

$inputIndex: the given Coordinate::TextIndex

=back

=head3 Returns:

Returns true if the beginIndex and endIndex of given TextIndex are in same point, returns false otherwise.

=cut
sub isSameBeginEnd {
  my $self = shift;
  my $inputIndex = shift;
  
  if (! defined $inputIndex || $inputIndex eq "" 
  || ! Utils::Tools->isBlessed($inputIndex, "Coordinate::TextIndex")) {
    croak "There is no valid Coordinate::TextIndex to input";
  }
  
  my $inputBeginPos = $inputIndex->getBeginIndex->getPos;
  my $inputEndPos = $inputIndex->getEndIndex->getPos;
  
  my $selfBeginPos = $self->getBeginIndex->getPos;
  my $selfEndPos = $self->getEndIndex->getPos;
  
  my $res = 0;
  if ($inputBeginPos == $selfBeginPos && $inputEndPos == $selfEndPos) {
    $res = 1;
  }
  return $res;
}

=head2 isIntervalInvolved($begin, $end)

Returns true if and only if this TextIndex contains the given $begin and $end positions.

=head3 Parameters:

=over 4

=item *

$begin: the given beginning index

=item *

$end: the given ending index

=back

=cut
sub isIntervalInvolved {
  my $self = shift;
  my $begin = shift;
  my $end = shift;
  
  my $res = 0;
  if (! defined $begin || $begin eq "" || ! Scalar::Util::looks_like_number($begin)
    || ! defined $end || $end eq "" || ! Scalar::Util::looks_like_number($end)
  ) {
    carp "This input Position indices is not valid!";
    return $res;
  }
  if ($self->isPosInvoled($begin) && $self->isPosInvolved($end)) {
    $res = 1;
  }
  return $res;
}

=head2 isPosInvolved($pos)

Returns true if this TextIndex contains the given position.

=head3 Parameters:

=over 4

=item *

$begin: the given  index

=back

=cut
sub isPosInvolved {
  my $self = shift;
  my $pos = shift;
  my $res = 0;
  if (! defined $pos || $pos eq "" || ! Scalar::Util::looks_like_number($pos)) {
    carp "This input Position index is not valid!";
    return $res;
  }
  my $beginPos = $self->getBeginIndex->getPos;
  my $endPos = $self->getEndIndex->getPos;
  
  if ($beginPos >= $pos && $pos <= $endPos) {
    $res = 1;
  }
  return $res;
}

=head2 isInOnePoint()

Returns true if this TextIndex has same beginning and ending index.

=cut
sub isInOnePoint {
  my $self = shift;
  
  my $beginPos = $self->getBeginIndex->getPos;
  my $endPos = $self->getEndIndex->getPos;
  
  my $res = 0;
  if ($beginPos == $endPos) {$res = 1;}
  
  return $res;
}
=head2 setBeginWithPos($pos)

Sets the beginning index of this TextIndex with the specified integer position

=cut
sub setBeginWithPos {
  my $self = shift;
  my $pos = shift;
  if (! defined $pos || $pos eq "" || ! Scalar::Util::looks_like_number($pos)) {
    carp "This input Position index is not valid!";
    return;
  }
  my $beginPoint = $self->getBeginIndex;
  $beginPoint->setPos($pos);
  $self->setBeginIndex($beginPoint);
}



=head2 setEndWithPos($pos)

Sets the ending index of this TextIndex with the specified integer position

=cut
sub setEndWithPos {
  my $self = shift;
  my $pos = shift;
  if (! defined $pos || $pos eq "" || ! Scalar::Util::looks_like_number($pos)) {
    carp "This input Position index is not valid!";
    return;
  }
  my $endPoint = $self->getEndIndex;
  $endPoint->setPos($pos);
  $self->setEndIndex($endPoint);
}

=head2 getTextIndices

Returns the beginning and ending indices of this TextIndex.

=cut
sub getTextIndices {
  my $self = shift;
  
  my $beginIndex = $self->getBeginIndex->getPos;
  my $endIndex = $self->getEndIndex->getPos;
#  my $len = $self->getLength;
#  return ($beginIndex, $endIndex, $len);
  return ($beginIndex, $endIndex);
}

=head2 setTextIndicesWithBeginLength($beginIndex, $len)

Sets the beginning and ending indices with the given beginning index and the text length.

This method is suit for Perl substring system.

=cut
sub setTextIndicesWithBeginLength {
  my $self = shift;
  my ($beginIndex, $len) = @_;
  
  if (! defined $beginIndex || $beginIndex eq '' 
    || ! Scalar::Util::looks_like_number($beginIndex) 
    || ! defined $len || $len eq '' 
    || ! Scalar::Util::looks_like_number($len) 
  ) {
    carp "There is no valid beginIndex or text length";
    return;
  }
  my $endIndex = $beginIndex + $len;
  #Coordinate::Point
  my $beginPoint = Coordinate::Point->new($beginIndex);
  my $endPoint = Coordinate::Point->new($endIndex);
  $self->setBeginIndex($beginPoint);
  $self->setEndIndex($endPoint);
  $self->setLength($len);
  return 1;
}

=head2 setTextIndicesWithText($textRef, $beginIndex)

Sets the beginning and ending indices with the given beginning index and the reference of string text.

=cut
sub setTextIndicesWithText {
  my $self = shift;
  my $textRef = shift;
  my $beginIndex = shift;
  
  if (! defined $textRef || $textRef eq '' || ref($textRef) ne 'SCALAR') {
    carp "There is no valid input text";
    return;
  }
  if (! defined $beginIndex || $beginIndex eq '' || ! Scalar::Util::looks_like_number($beginIndex)) {
    $beginIndex = 0;  
  }
  
  my $len = length $$textRef;
  
  $self->setTextIndicesWithBeginLength($beginIndex, $len);
  return 1;
}

=head2 validParameters()

Returns true if the beginning/ending index and text length of this TextIndex are valid.

=cut
sub validParameters {
  my $self = shift;
  my $result = 0;
  
  my $begin = $self->getBeginIndex;
  my $end = $self->getEndIndex;
  my $len = $self->getLength;
  
  if ((ref $begin eq "Coordinate::Point") && (ref $begin eq ref $end)) {
    my $tempLen = $end->getX - $begin->getX;
    if ($tempLen == $len) {
      $result = 1;
    }
  }
  return $result;
}

=head2 reset()

Resets the text length, beginning and ending indices.

=cut
sub reset {
  my $self = shift; 
  $self->setLength(0);
  $self->getBeginIndex->reset;
  $self->getEndIndex->reset;  
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;
