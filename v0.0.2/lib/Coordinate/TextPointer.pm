package Coordinate::TextPointer;
=encoding utf8

=head1 NAME

Coordinate::TextPointer - pointer for the original input text

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use Data::Dumper;
use List::MoreUtils;
use Scalar::Util;

use Coordinate::Point;
use Utils::Tools;

=head1 CONSTRUCTORS

=head2 _build_readerhead

=cut

sub _build_readerhead {
  return Coordinate::Point->new;
}

=head1 ATTRIBUTES

=head2 readerHead

The current position of TextPointer as a Reader-Head

=cut
has 'readerHead' => (
  is => 'rw',
  isa => 'Coordinate::Point',
  predicate => "hasReaderHead",
  clearer => 'resetReaderHead',
  writer => "setReaderHead",
  reader => "getReaderHead",
  builder => "_build_readerhead",
  lazy => 1,
);


=head1 METHODS

=head2 getPosition()

Returns the position of the readerHead

=cut
sub getPosition {
  my $self = shift;
  return $self->getReaderHead->getPos;
}
=head2 reset()

=cut
sub reset {
  my $self = shift;
  $self->resetReaderHead;
  #$self->resetPosHis;
}

=head2 moveTo($newPos)

Moves the TextPointer to the new position.

=head3 Parameters:

=over 4

=item *

$newPos: the given new position, it could be index number or coordinate::point

=back

=cut
sub moveTo {
  my $self = shift;
  my $newPos = shift; # Coordinate::Point or number
  if (! defined $newPos || $newPos eq "") {
    carp "There is no new position for the reader head";
    return;
  }
  my $res;
  # change the position number to object Coordinate::Point
  if (Utils::Tools->isBlessed($newPos, "Coordinate::Point")) {
    # add it to the history
    #$res = $self->addPosHis($newPos->getPos);
  } elsif ( Scalar::Util::looks_like_number($newPos)) {
    # add it to the history
    #$res = $self->addPosHis($newPos);
    if (defined $res && $res) {
      # set it to the Coordinate::Point
      $newPos = Coordinate::Point->new($newPos);
    }
  }
  # send the readerhead
  if (defined $res && $res) {
    $self->setReaderHead($newPos);
    return 1;
  }
  return;
}
# save the positions' number(0, 2, ...) or str("1,2", "3,4")
#sub addPosHis {
#  my $self = shift;
#  my $curPos = shift;
#  my $arrRef = $self->getPosHistory;
#  if (! scalar @$arrRef) {
#    push @$arrRef, $curPos;
#    $self->setPosHistory($arrRef);
#  } else {
##   if (Utils::Tools->isInArray($arrRef, \$curPos)) {
##     #carp "There is already the same Position $curPos in the position-History!";
##   }
#   push @$arrRef, $curPos;
#   $self->setPosHistory($arrRef);
#  }
#  return 1;
#}

=head2 toString

Display this TextPointer by string

=cut
sub toString {
  my $self = shift;
  my $str = "current Head is at the position: " . $self->head->getPos . "\n";
  if ($self->hasPosHis) {
    $str .= "The positon history of head is : " . join(", ", @{$self->posHistory});    
  }
  return $str;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;
