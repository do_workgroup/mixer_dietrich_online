package State::AbstractState;
=encoding utf8

=head1 NAME

State::AbstractState - The abstract class of State for state machine

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This is abstract superclass of analyse state. Standard behaviors like the 

getType, reset, handle, input and output methods are defined here, the programmer needs only to extend

this class and provide implementations for this methods. 

The non-abstract methods have their descriptions with its implementation in Detail.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose::Role;

use Carp;
use Data::Dumper;

use FindBin;
use lib "$FindBin::Bin/../";

use Text::EntryText;

=head1 ATTRIBUTES

=head2 curEntryText

Text::EntryText

=cut
has 'curEntryText' => (
  is => 'rw',
  isa => 'Text::EntryText',
  predicate => 'hasCurEntryText',
  clearer => 'clearCurEntryText',
  reader => "getCurEntryText",
  writer => "setCurEntryText",
);
=head2 output

Output object or result hashmap

=cut
has 'output' => (
  is => 'rw',
  isa => 'Object | HashRef',
  predicate => 'hasOutput',
  clearer => 'clearOutput',
  reader => "getOutput",
  writer => "setOutput",
);
=head2 checkValue

The value after state check

=cut
has 'checkValue' => (
  is => 'rw',
  isa => 'Str',
  default => 'NOT_FOUND',
  predicate => 'hasCheckValue',
  clearer => 'resetCheckValue',
  reader => "getCheckValue",
  writer => "setCheckValue",
);

=head1 REQUIRES METHODS

=head2 getStateType

Returns the state type

=cut
requires 'getStateType';
=head2 input

Adds the specified values to the class 

=cut
requires 'input';
=head2 handle

Immediately performs the analysis of the task in the class.

=cut
requires 'handle';
=head2 output

Returns the resut for the output

=cut
requires 'output';

=head1 METHODS

=head2 reset

Resets the class to its initial, empty state.

=cut
sub reset {
  my $self = shift;
  $self->resetCheckValue;
  $self->clearOutput;
  $self->clearCurEntryText;
}

=head2 addPartTextToEntryText($entryText, $partTextType, $partTextRef, $contentRef, $indices)

Generate a new Text::EntryPartText and add it to the given Text::EntryText.

=over 4

=item *

$entryText: the given Text::EntryText

=item *

$partTextType: the given type of Text::EntryPartText

=item *

$partTextRef: the given text reference of Text::EntryPartText  

=item *

$contentRef: the given text content reference of Text::EntryPartText  

=item *

$indices: the beginning and ending indices of the Text::EntryPartText in the Text::EntryText

=back

=head3 Returns:

the processed Text::EntryText.

=cut
sub addPartTextToEntryText {
  my $self = shift;
  my $entryText = shift;
  my $partTextType = shift;
  my $partTextRef = shift;
  my $contentRef = shift;
  my $indices = shift;
  
    if (! defined $entryText || $entryText eq "" 
  || ! Utils::Tools->isBlessed($entryText, "Text::EntryText")) {
    croak "There is not valid entry text!";
  }
  if (! defined $partTextRef || $partTextRef eq "" 
  || ! defined $contentRef || $contentRef eq "" 
  || ! defined $partTextType || $partTextType eq "" 
  || ! defined $indices || $indices eq "" 
  ) {
    croak "There is not valid input parameters!";
  }
  
  my $idgApp = Engine::IDGenerateApp->getInstance;
  # set a new entry Text 
  my $entryPartText =  Text::EntryPartText->new;
  # get the entry id
  my $entryPartTextID = $idgApp->generateTextPartID;
  $entryPartText->setParentEntryID($entryText->getEntryID);
  $entryPartText->setEntryPartID($entryPartTextID);
  $entryPartText->setEntryPartType($partTextType);
  
  $entryPartText->setTextPart($$partTextRef);
  if (defined $contentRef && ref($contentRef) && defined $$contentRef) {
      $entryPartText->setContent($$contentRef);
  }
  
  # set text index
  my $targetIndicesArrRef = Utils::Tools->splitIndices($indices);      
  $entryPartText->setBeginPosOfTextIndex($targetIndicesArrRef->[0]);
  $entryPartText->setEndPosOfTextIndex($targetIndicesArrRef->[1]);
  
  # 7.add it to the entry text
  $entryText->addContent($entryPartText);
  $entryText->addPartTextToMap($entryPartText);
  return $entryText;
}

=head2 addPartsToEntryText($entryText, $resHash, $partTextType, $takeFirst)

Add the textparts to the given Text::EntryText.

=over 4

=item *

$entryText: the given Text::EntryText

=item *

$resHash: the given result hashmap after analysis

=item *

$partTextType: the given type of Text::EntryPartText

=item *

$takeFirst: boolean value, take the first one or the entire array

=back

=head3 Returns:

the processed Text::EntryText.

=cut
sub addPartsToEntryText {
  my $self = shift;
  my $entryText = shift;
  my $resHash = shift;  # target analyse hash: muster Global::Values->analyseUnitResultHash
  my $partTextType = shift; # type
  my $takeFirst = shift; # take the first or the entire array
  
  if (! defined $entryText || $entryText eq "" 
  || ! Utils::Tools->isBlessed($entryText, "Text::EntryText")) {
    croak "There is not valid entry text!";
  }
  if (! defined $resHash || $resHash eq "" 
  || ! defined $partTextType || $partTextType eq "" 
  ) {
    croak "There is not valid input parameters!";
  }
  # default is take them all
  if (! defined $takeFirst || $takeFirst eq "") {
    $takeFirst = 0;
  }
  
  # if found something
  if ($resHash->{found}) {
    # how many elements
    my $eleArrRef = $resHash->{element};
    my $count = scalar @$eleArrRef;
    
    for (my $i = 0; $i<$count; $i++) {
      # textpart and content
      my $content = "";
      my $partText = $resHash->{element}->[$i];
      if (ref($resHash->{content}) eq "ARRAY" && defined $resHash->{content}->[$i]) {
        $content = $resHash->{content}->[$i];
      }
      my $indices = $resHash->{pos}->[$i];
      $self->addPartTextToEntryText($entryText, $partTextType, \$partText, \$content, $indices);
      #TEST:
      # process only once
      if ($takeFirst) {last;}
    }
  }
  return $entryText;
}

no Moose;
1;
