package State::StateProcessor;
=encoding utf8

=head1 NAME

State::StateProcessor - A state machine

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

This package implements a state machine in an object oriented way. This is based on state pattern mode.

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
package State::ErrorState;
=encoding utf8

=head1 NAME

State::ErrorState - Error State

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];

=head1 ATTRIBUTES

=head2 type

Type of the state (DO_STATE_TYPE: Global::Values->doStateType)

=cut
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'ERROR_STATE',
);

=head1 METHODS

=head2 getStateType()

=head3 Overrides:

getStateType in class State::AbstractState

=cut
sub getStateType {
  my $self = shift;
  return $self->type;
}

=head2 handle()

=head3 Overrides:

handle in class State::AbstractState

=cut
sub handle {
  my $self = shift;
  print "State Analysis has Some Error!";
  return;
}

=head2 input()

=head3 Overrides:

input in class State::AbstractState

=cut
sub input {}

=head2 output()

=head3 Overrides:

output in class State::AbstractState

=cut
sub output {}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package State::StopState
###########################################
package State::StopState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'STOP_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}

sub handle {
  my $self = shift;
#  print "State Analysis is stopped!";
  $self->setCheckValue("STOP_MACHINE");
  my $entryText = $self->getCurEntryText();
  $self->setOutput($entryText);
  return 1;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# override the super method
#around 'reset' => sub {
#  my $next = shift;
#  my $self = shift;
#  $self->$next();
#  
#};

no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package State::UnknownState
###########################################
package State::UnknownState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'UNKNOWN_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}

sub handle {
  my $self = shift;
  print "State is unknown!";
  return;
}
sub input {}

sub output {}

# override the super method
#around 'reset' => sub {
#  my $next = shift;
#  my $self = shift;
#  $self->$next();
#  
#};

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
package State::initialization;
=encoding utf8

=head1 NAME

State::initialization - A initial state.

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Data::Dumper;

use Global::Values;
use Database::MotDBProcessor;
use Engine::IDProcessor;
use Analyse::CentralAnalysisManager;

use Text::EntryText;
use Text::EntryPartText;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];

=head1 ATTRIBUTES

=head2 type

Type of the state (DO_STATE_TYPE: Global::Values->doStateType)

=cut
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'INITIALIZATION_STATE',
);

=head1 METHODS

=head2 getStateType()

=head3 Overrides:

getStateType in class State::AbstractState

=cut
sub getStateType {
  my $self = shift;
  return $self->type;
}

=head2 input()

=head3 Overrides:

input in class State::AbstractState

=cut
sub input {}

=head2 output()

Returns a new Text::EntryText.

=head3 Overrides:

output in class State::AbstractState

=cut
sub output {
  my $self = shift;
  return $self->getOutput;
}

=head2 handle()

Makes a new Text::EntryText with the row type, row number and other parameters, and sets the check value

with the row type for further analysis.

=head3 Overrides:

handle in class State::AbstractState

=cut
sub handle {
  my $self = shift;
  
  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;
  
  # 1.get the row text
  my $text = $dbtApp->getCurRowText;
  my $rowNum = $dbtApp->getRowNum;
  
  # 2.get the entry id
  my $entryID = $idgApp->generateEntryID;
  
  # 3.row type
  # process in the Analys::CentralAnalysisManager
  my $rowTypeOutputTable = $caMger->getRowLineType(\$text);
  my $rowType = $rowTypeOutputTable->{elementType};
  # 4.set a new entry Text 
  my $entryText =  Text::EntryText->new;
  # 5.set entryid and type of this entry Text 
  $entryText->setEntryID($entryID);
  $entryText->setEntryType($rowType);
  $entryText->setRowNumber($rowNum);
  $entryText->setOrigText($text);
  $entryText->setOrigEntryText($text);
  $entryText->initTextIndex;
  # 6. initializing text pointer 
  #$entryText->updateTextPointer(0);
  
  # 7.set check value to the current check value
  $self->setCheckValue($rowTypeOutputTable->{elementType});
  # 8. set output
  $self->setOutput($entryText);
  return 1;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package State::D74HeaderState
###########################################
package State::D74HeaderState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'D74_HEADER_ANALYSE_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text
  # 1.1 result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"      => 0,17
#  "found"   => 1/0,
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
  my $resHash = $caMger->getD74_Header($eText);
  
  # 4.set a new entry Text 
  my $entryPartText =  Text::EntryPartText->new;
  # 4.1.get the entry id
  my $entryPartTextID = $idgApp->generateTextPartID;
  $entryPartText->setParentEntryID($eText->getEntryID);
  $entryPartText->setEntryPartID($entryPartTextID);
  $entryPartText->setEntryPartType('HEADER');
  
  # reports for the finally analyse
  $eText->setMaxSeverity($resHash->{severity});
  $eText->addToProblemReports($resHash->{problems});
  
  $eText->getHeaderInfo->{id} = $resHash->{result}->{id};
  $eText->getHeaderInfo->{bd} = $resHash->{result}->{bd};
  $eText->getHeaderInfo->{se} = $resHash->{result}->{se};
  $eText->getHeaderInfo->{sp} = $resHash->{result}->{sp};
  
  #5. ENTRY TEXT PART
  #my $patName_getD74Header = Global::Values::MODULE_PATTERN->{GET_D74_HEADER};
  # get the position of the text part
  #my $posArrRef = $caMger->getUnitStrategy->getValueOfOutputTablesByPattern($patName_getD74Header, "pos");
  my $headerIndices = $resHash->{pos}->[0];
  
  # set text part
  my $origText = $eText->getOrigText();
  my $partTextRef =   Utils::Tools->getTextPartRefByIndices(\$origText, "", $headerIndices, "middle");
  $entryPartText->setTextPart($$partTextRef);
  
  
  my $targetIndicesArrRef = Utils::Tools->splitIndices($headerIndices);  
  # set text index
  $entryPartText->setBeginPosOfTextIndex($targetIndicesArrRef->[0]);
  $entryPartText->setEndPosOfTextIndex($targetIndicesArrRef->[1]);
  
  
  # 7.set check value to the current check value
  $self->setCheckValue("HAS_D74_HEADER");
  if($eText->isBeginEndInOnePoint) {
    $self->setCheckValue("TEXT_END");
  }
  
  # 8.add it to the entry text
  $eText->setEntryTextAttr("hasHeader", 1);
  $eText->addContent($entryPartText);
  $eText->addPartTextToMap($entryPartText);
  
  # 9.set the new read_header of textPointer
#  $eText->updateTextPointer($targetIndicesArrRef->[1]);
  $eText->setBeginPosOfTextIndex($targetIndicesArrRef->[1]);
  
  $self->setOutput($eText);
    $resHash = undef;
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;



#CLASS:
###########################################
# package State::StreckeEntryState
###########################################
package State::StreckeEntryState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'STRECKE_ANALYSE_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text
  # 1.1 result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"      => 0,17
#  "found"   => 1/0,
#  "eleNum"   => 0,
#  "element" => "",
#  "content" => "",
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
  my $resHash = $caMger->getStrecke($eText);
  
  if ($resHash->{found}) {
    $eText->setEntryTextAttr("hasStrecke", 1);
    
    # 4.set a new entry Text 
    my $entryPartText =  Text::EntryPartText->new;
    # 4.1.get the entry id
    my $entryPartTextID = $idgApp->generateTextPartID;
    $entryPartText->setParentEntryID($eText->getEntryID);
    $entryPartText->setEntryPartID($entryPartTextID);
    $entryPartText->setEntryPartType('STRECKE');
    
    # reports for the finally analyse
    $eText->setMaxSeverity($resHash->{severity});
    $eText->addToProblemReports($resHash->{problems});
    
    my $stkName = $resHash->{result};
    # motDB update the strecke
    $dbtApp->updateStrecke($stkName);
    
    #5. ENTRY TEXT PART
    #my $patName_getD74Header = Global::Values::MODULE_PATTERN->{GET_D74_HEADER};
    # get the position of the text part
    #my $posArrRef = $caMger->getUnitStrategy->getValueOfOutputTablesByPattern($patName_getD74Header, "pos");
    my $streckeIndices = $resHash->{pos}->[0];
    
    # set text part
    my $origText = $eText->getOrigText();
    my $partTextRef =   Utils::Tools->getTextPartRefByIndices(\$origText, "", $streckeIndices, "middle");
    $entryPartText->setTextPart($$partTextRef);
    
    
    my $targetIndicesArrRef = Utils::Tools->splitIndices($streckeIndices);  
    # set text index
    $entryPartText->setBeginPosOfTextIndex($targetIndicesArrRef->[0]);
    $entryPartText->setEndPosOfTextIndex($targetIndicesArrRef->[1]);
    
    
    # 8.add it to the entry text
    $eText->addContent($entryPartText);
    $eText->addPartTextToMap($entryPartText);
    
    # 9.set the new read_header of textPointer
#    $eText->updateTextPointer($targetIndicesArrRef->[1]);
    $eText->setBeginPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 7.set check value to the current check value
    $self->setCheckValue("HAS_STRECKE");
  } else {
    $self->setCheckValue("NOT_FOUND");
  }
  $self->setOutput($eText);
    $resHash = undef;
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package State::LemmaAnalyseState
###########################################
package State::LemmaAnalyseState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'LEMMA_ANALYSE_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text
  # 1.1 result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
  my $resHash = $caMger->getLemma($eText);
  # reports for the finally analyse
  $eText->setMaxSeverity($resHash->{severity});
  $eText->addToProblemReports($resHash->{problems});
  # There is a lemma area
  if ($resHash->{found}) {
    # 4.set a new entry Text 
    my $entryPartText =  Text::EntryPartText->new;
    # 4.1.get the entry id
    my $entryPartTextID = $idgApp->generateTextPartID;
    $entryPartText->setParentEntryID($eText->getEntryID);
    $entryPartText->setEntryPartID($entryPartTextID);
    $entryPartText->setEntryPartType('LEMMA_ELEMENT');
    
    #5. ENTRY TEXT PART
    #my $patName_getD74Header = Global::Values::MODULE_PATTERN->{GET_D74_HEADER};
  #  # get the position of the text part
  #  #my $posArrRef = $caMger->getUnitStrategy->getValueOfOutputTablesByPattern($patName_getD74Header, "pos");
    my $lemmaIndices = $resHash->{pos}->[0];
    
    # set text part
#    my $origText = $eText->getOrigText();
#    my $partTextRef =   Utils::Tools->getTextPartRefByIndices(\$origText, "", $lemmaIndices, "middle");
    my $partTextRef = $resHash->{element};
    my $contentRef = $resHash->{content};
    
    $entryPartText->setTextPart($$partTextRef);
    if (defined $contentRef && ref($contentRef) && defined $$contentRef) {
      $entryPartText->setContent($$contentRef);
    }
    
    my $targetIndicesArrRef = Utils::Tools->splitIndices($lemmaIndices);  
    # set text index
    $entryPartText->setBeginPosOfTextIndex($targetIndicesArrRef->[0]);
    $entryPartText->setEndPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 7.add it to the entry text
    $eText->addContent($entryPartText);
    $eText->addPartTextToMap($entryPartText);
    
    # 9.set the new read_header of textPointer
#    $eText->updateTextPointer($targetIndicesArrRef->[1]);
    $eText->setBeginPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 10. set it to ouput
    $self->setOutput($eText);
    
    # 11.set check value to the current check value
    $self->setCheckValue("HAS_LEMMA_ELEMENT");
  } else {
    # set it to ouput
    $self->setOutput($eText);
    $self->setCheckValue("NOT_FOUND");
  }
  if($eText->isBeginEndInOnePoint) {
    $self->setCheckValue("TEXT_END");
  }
  $resHash = undef;
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package State::SpecialHtmlTagsAnalyseState
###########################################
package State::SpecialHtmlTagsAnalyseState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'SPECIAL_HTML_PARTS_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text
  # 1.1 result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
  my $sicResHash = $caMger->getSic($eText);
  my $corrResHash = $caMger->getCorrection($eText);
  my $dopResHash = $caMger->getDop($eText);
  my $homResHash = $caMger->getHomonym($eText);
  my $ignoreResHash = $caMger->getIgnore($eText);
  my $supResHash = $caMger->getSup($eText);
  my $subResHash = $caMger->getSub($eText);
  my $repResHash = $caMger->getRep($eText);
  my $ditoResHash = $caMger->getDitoEle($eText);
  my $okResHash = $caMger->getOkElement($eText);
  
  # reports for the finally analyse
  $eText->setMaxSeverity($sicResHash->{severity});
  $eText->setMaxSeverity($corrResHash->{severity});
  $eText->setMaxSeverity($dopResHash->{severity});
  $eText->setMaxSeverity($repResHash->{severity});
  $eText->setMaxSeverity($homResHash->{severity});
  $eText->setMaxSeverity($ignoreResHash->{severity});
  $eText->setMaxSeverity($subResHash->{severity});
  $eText->setMaxSeverity($supResHash->{severity});
  $eText->setMaxSeverity($ditoResHash->{severity});
  $eText->setMaxSeverity($okResHash->{severity});
  
  $eText->addToProblemReports($sicResHash->{problems});
  $eText->addToProblemReports($corrResHash->{problems});
  $eText->addToProblemReports($dopResHash->{problems});
  $eText->addToProblemReports($repResHash->{problems});
  $eText->addToProblemReports($homResHash->{problems});
  $eText->addToProblemReports($ignoreResHash->{problems});
  $eText->addToProblemReports($subResHash->{problems});
  $eText->addToProblemReports($supResHash->{problems});
  $eText->addToProblemReports($ditoResHash->{problems});
  $eText->addToProblemReports($okResHash->{problems});
  
  $eText = $self->addPartsToEntryText($eText, $sicResHash, "SIC_ELEMENT");
  $eText = $self->addPartsToEntryText($eText, $corrResHash, "CORR_ELEMENT");
  $eText = $self->addPartsToEntryText($eText, $dopResHash, "DOP_ELEMENT");
  $eText = $self->addPartsToEntryText($eText, $repResHash, "REP_ELEMENT");
  $eText = $self->addPartsToEntryText($eText, $homResHash, "HOMONYM_ELEMENT");
  $eText = $self->addPartsToEntryText($eText, $ignoreResHash, "IGNORE_ELEMENT");
  $eText = $self->addPartsToEntryText($eText, $subResHash, "SUB_ELEMENT");
  $eText = $self->addPartsToEntryText($eText, $supResHash, "SUP_ELEMENT");
  $eText = $self->addPartsToEntryText($eText, $ditoResHash, "DITO_ELEMENT");
  $eText = $self->addPartsToEntryText($eText, $okResHash, "OK_ELEMENT", 1);
  
  $self->setOutput($eText);
  $self->setCheckValue("HAS_CHECKED_SPECIAL_HTML_TAGS");
  
  $sicResHash = undef;
  $corrResHash = undef;
  $dopResHash = undef;
  $homResHash = undef;
  $ignoreResHash = undef;
  $supResHash = undef;
  $subResHash = undef;
  $repResHash = undef;
  $ditoResHash = undef;
  $okResHash = undef;
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package State::HtmlLemmaEntryAnalyseState
###########################################
package State::HtmlLemmaEntryAnalyseState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'HTML_ELEMENT_LEMMA_ANALYSE_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text
  # 1.1 result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
  my $entryEleResHash = $caMger->getEntryElement($eText);
  
  # reports for the finally analyse
  $eText->setMaxSeverity($entryEleResHash->{severity});
  #$eText = $self->addPartsToEntryText($eText, $entryEleResHash, "ENTRY_ELEMENT");
  
  if ($entryEleResHash->{found}) {
    my $contentArrRef = $entryEleResHash->{content};
    
    my $attrHash = $entryEleResHash->{result};
    
    # entry attributes
    $eText->setHeaderInfo($attrHash);
    
    #ATTENTION: update the new original text and text index
    $eText->setOrigText($contentArrRef->[0]);
    $eText->initTextIndex;
    # initializing text pointer 
#    $eText->updateTextPointer(0);
    
    if ($entryEleResHash->{eleNum} > 1) {
      $self->setCheckValue("HAS_ERROR");
    } else {
      $self->setCheckValue("HAS_ENTRY_ELEMENT");
    }
  } else {
    $self->setCheckValue("NOT_FOUND");
  }
  
  $self->setOutput($eText);
  $entryEleResHash = undef;
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package State::HtmlEntryAnalyseState
###########################################
package State::HtmlEntryAnalyseState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'HTML_ELEMENT_ANALYSE_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text
  # 1.1 result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
#TODO:
  my $entryEleResHash = $caMger->getEntryElement($eText);
  
  # reports for the finally analyse
  $eText->setMaxSeverity($entryEleResHash->{severity});
  #$eText = $self->addPartsToEntryText($eText, $entryEleResHash, "ENTRY_ELEMENT");
  
  if ($entryEleResHash->{found}) {
    my $contentArrRef = $entryEleResHash->{content};
    
    # entry attributes
    $eText->setHeaderInfo($entryEleResHash->{result});
    
    #ATTENTION: update the new original text and text index
    $eText->setOrigText($contentArrRef->[0]);
    $eText->initTextIndex;
    # initializing text pointer 
#    $eText->updateTextPointer(0);
    if ($entryEleResHash->{eleNum} > 1) {
      $self->setCheckValue("HAS_ERROR");
    } else {
      $self->setCheckValue("HAS_ENTRY_ELEMENT");
    }
  } else {
    $self->setCheckValue("NOT_FOUND");
  }
  
  $self->setOutput($eText);
  $entryEleResHash = undef;
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package State::ErrorCharsAnalyseState
###########################################
package State::ErrorCharsAnalyseState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'SEARCH_ERROR_CHAR_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

sub getErrorContent {
  my $self = shift;
  my $indices = shift; # 0,12 (begin,end)
  if (! defined $indices || $indices eq "") {
    carp "There is no valid target indices!";
    return;
  }
  
  my $entryText = $self->getCurEntryText;
  my $origText = $entryText->getOrigText;
  my ($begin, $end) = $entryText->getTextIndex->getTextIndices;
  if (! defined $entryText || $entryText eq ""
    || ! defined $origText || $origText eq ""
    || ! defined $begin || $begin eq ""
    || ! defined $end || $end eq "") {
    carp "There are some problems in the parameters!";
    return;
  }
  
  # get the boundary indices
  my $boundaryIndices = Utils::Tools->setIndices($begin, $end);
  
  # get the taget text
  my $targetTextRef = Utils::Tools->getTextPartRefByIndices(\$origText, $boundaryIndices, $indices, "middle");
  return $targetTextRef;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text
  # 1.1 result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17", "x,x"]
#  "found"   => 1/0,
#  "element" => ["hallo", "gut"]; # the reference of text array
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]

  # search the entire original text
  $eText->initTextIndex; 
#  $eText->updateTextPointer(0);
  
  # check without d74 header
  if ($eText->getEntryType eq "ENTRY_TYPE_LEMMA") {
    my $partText_d74Header = $eText->getPartTextsFromMap("HEADER")->[0];
    my ($headerBegin, $headerEnd) = $partText_d74Header->getTextIndices;
#    $eText->updateTextPointer($headerEnd);
    $eText->setBeginPosOfTextIndex($headerEnd);
  }
  
  my $errorResHash = $caMger->getCharErrors($eText);
  # reports for the finally analyse
  $eText->setMaxSeverity($errorResHash->{severity});
  $eText->addToProblemReports($errorResHash->{problems});
  
  $eText = $self->addPartsToEntryText($eText, $errorResHash, "CHAR_ERROR_ELEMENT");
  
  
  my $unknownResHash = $caMger->getUnknownparts($eText);
  # reports for the finally analyse
  $eText->setMaxSeverity($unknownResHash->{severity});
  $eText->addToProblemReports($unknownResHash->{problems});
  $eText = $self->addPartsToEntryText($eText, $unknownResHash, "UNKNOWN_PART");
  $self->setOutput($eText);
  if ($errorResHash->{found} || $unknownResHash->{found} ) {
    $self->setCheckValue("HAS_CHAR_ERRORS");  
  } else {
    $self->setCheckValue("NOT_FOUND");
  }
  $unknownResHash = undef;
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;



#CLASS:
###########################################
# package State::EndpartAnalyseState
###########################################
package State::EndpartAnalyseState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;
use Data::Dumper;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'ENDPART_ANALYSE_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text
  # 1.1 result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
  my $endDopResHash = $caMger->getEndDopElement($eText);
  my $okResHash = $caMger->getOkElement($eText);
  
  # reports for the finally analyse
  $eText->setMaxSeverity($endDopResHash->{severity});
  $eText->setMaxSeverity($okResHash->{severity});

  
  $eText->addToProblemReports($endDopResHash->{problems});
  $eText->addToProblemReports($okResHash->{problems});

  $eText = $self->addPartsToEntryText($eText, $endDopResHash, "DOP_ELEMENT", 1);
  $eText = $self->addPartsToEntryText($eText, $okResHash, "OK_ELEMENT", 1);
  
  # set it to ouput
  $self->setOutput($eText);
  
  # set check value to the current check value
  if($eText->isBeginEndInOnePoint) {
    $self->setCheckValue("TEXT_END");
  } else {
    $self->setCheckValue("HAS_ENDPART_ELEMENT");
  }
  $endDopResHash = undef;
  $okResHash = undef;
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package State::TitleAnalyseState
###########################################
package State::TitleAnalyseState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'TITLE_ANALYSE_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text
  # 1.1 result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
  my $resHash = $caMger->getTitle($eText);
  # reports for the finally analyse
  $eText->setMaxSeverity($resHash->{severity});
  $eText->addToProblemReports($resHash->{problems});

  
  # There is a lemma area
  if ($resHash->{found}) {
    $eText->setEntryTextAttr("hasTitle", 1);
    # 4.set a new entry Text 
    my $entryPartText =  Text::EntryPartText->new;
    # 4.1.get the entry id
    my $entryPartTextID = $idgApp->generateTextPartID;
    $entryPartText->setParentEntryID($eText->getEntryID);
    $entryPartText->setEntryPartID($entryPartTextID);
    $entryPartText->setEntryPartType('TITEL');
    
    #5. ENTRY TEXT PART
    #my $patName_getD74Header = Global::Values::MODULE_PATTERN->{GET_D74_HEADER};
  #  # get the position of the text part
  #  #my $posArrRef = $caMger->getUnitStrategy->getValueOfOutputTablesByPattern($patName_getD74Header, "pos");
    my $titleIndices = $resHash->{pos}->[0];
    
    my $textPartRef = $resHash->{result};
    # set text part
    $entryPartText->setTextPart($$textPartRef);
    
    my $targetIndicesArrRef = Utils::Tools->splitIndices($titleIndices);  
    # set text index
    $entryPartText->setBeginPosOfTextIndex($targetIndicesArrRef->[0]);
    $entryPartText->setEndPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 7.add it to the entry text
    $eText->addContent($entryPartText);
    $eText->addPartTextToMap($entryPartText);
    
    # 9.set the new read_header of textPointer
#    $eText->updateTextPointer($targetIndicesArrRef->[1]);
    $eText->setBeginPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 10. set it to ouput
    $self->setOutput($eText);
    
    # 11.set check value to the current check value
    $self->setCheckValue("HAS_TITLE");
  } else {
    # set it to ouput
    $self->setOutput($eText);
    $self->setCheckValue("NOT_FOUND");
  }
  if($eText->isBeginEndInOnePoint) {
    $self->setCheckValue("TEXT_END");
  }
  $resHash = undef;
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package State::AuthorAnalyseState
###########################################
package State::AuthorAnalyseState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'AUTHOR_ANALYSE_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text
  # 1.1 result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
  my $resHash = $caMger->getAuthor($eText);
  # reports for the finally analyse
  $eText->setMaxSeverity($resHash->{severity});
  $eText->addToProblemReports($resHash->{problems});
  
  
  # There is a lemma area
  if ($resHash->{found}) {
    $eText->setEntryTextAttr("hasAuthor", 1);
    $eText->setEntryTextAttr("hasMoreAuthor", 1) if ($resHash->{eleNum} > 1);
    # 4.set a new entry Text 
    my $entryPartText =  Text::EntryPartText->new;
    # 4.1.get the entry id
    my $entryPartTextID = $idgApp->generateTextPartID;
    $entryPartText->setParentEntryID($eText->getEntryID);
    $entryPartText->setEntryPartID($entryPartTextID);
    $entryPartText->setEntryPartType('VERFASSER');
    
    #5. ENTRY TEXT PART
    #my $patName_getD74Header = Global::Values::MODULE_PATTERN->{GET_D74_HEADER};
  #  # get the position of the text part
  #  #my $posArrRef = $caMger->getUnitStrategy->getValueOfOutputTablesByPattern($patName_getD74Header, "pos");
    my $authorIndices = $resHash->{pos}->[0];
    
    # set text part
    my $textPartRef = $resHash->{element};
    $entryPartText->setTextPart($$textPartRef);
    my $contentRef = $resHash->{content};
    $entryPartText->setContent($$contentRef);
    
    
    my $targetIndicesArrRef = Utils::Tools->splitIndices($authorIndices);  
    # set text index
    $entryPartText->setBeginPosOfTextIndex($targetIndicesArrRef->[0]);
    $entryPartText->setEndPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 7.add it to the entry text
    $eText->addContent($entryPartText);
    $eText->addPartTextToMap($entryPartText);
    
    # 9.set the new read_header of textPointer
#    $eText->updateTextPointer($targetIndicesArrRef->[1]);
    $eText->setBeginPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 10. set it to ouput
    $self->setOutput($eText);
    
    # 11.set check value to the current check value
    $self->setCheckValue("HAS_AUTHOR");
  } else {
    $self->setOutput($eText);
    # set it to ouput
    $self->setCheckValue("NOT_FOUND");
  }
  if($eText->isBeginEndInOnePoint) {
    $self->setCheckValue("TEXT_END");
  }
  $resHash = undef;
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package State::DitoDescAnalyseState
###########################################
package State::DitoDescAnalyseState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'DITO_DESCENDANT_ANALYSE_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text
  # 1.1 result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
  my $resHash = $caMger->getDitoDesc($eText);
  # reports for the finally analyse
  $eText->setMaxSeverity($resHash->{severity});
  $eText->addToProblemReports($resHash->{problems});
  
  
  # There is a lemma area
  if ($resHash->{found}) {
    $eText->setEntryTextAttr("hasDitoDesc", 1);
#    $eText->setEntryTextAttr("hasMoreAuthor", 1) if ($resHash->{eleNum} > 1);
    # 4.set a new entry Text 
    my $entryPartText =  Text::EntryPartText->new;
    # 4.1.get the entry id
    my $entryPartTextID = $idgApp->generateTextPartID;
    $entryPartText->setParentEntryID($eText->getEntryID);
    $entryPartText->setEntryPartID($entryPartTextID);
    $entryPartText->setEntryPartType('DITO_DESC_PART');
    
    #5. ENTRY TEXT PART
    #my $patName_getD74Header = Global::Values::MODULE_PATTERN->{GET_D74_HEADER};
  #  # get the position of the text part
  #  #my $posArrRef = $caMger->getUnitStrategy->getValueOfOutputTablesByPattern($patName_getD74Header, "pos");
    my $ditoDescIndices = $resHash->{pos}->[0];
    
    # set text part
    my $textPartRef = $resHash->{element};
    $entryPartText->setTextPart($$textPartRef);
#    my $contentRef = $resHash->{content};
#    $entryPartText->setTextPart($$contentRef);
    
    
    my $targetIndicesArrRef = Utils::Tools->splitIndices($ditoDescIndices);  
    # set text index
    $entryPartText->setBeginPosOfTextIndex($targetIndicesArrRef->[0]);
    $entryPartText->setEndPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 7.add it to the entry text
    $eText->addContent($entryPartText);
    $eText->addPartTextToMap($entryPartText);
    
    # 9.set the new read_header of textPointer
#    $eText->updateTextPointer($targetIndicesArrRef->[1]);
    $eText->setBeginPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 10. set it to ouput
    $self->setOutput($eText);
    
    # 11.set check value to the current check value
    $self->setCheckValue("HAS_DITO_DESCENDANT");
  } else {
    # set it to ouput
    $self->setOutput($eText);
    $self->setCheckValue("NOT_FOUND");
  }
  if($eText->isBeginEndInOnePoint) {
    $self->setCheckValue("TEXT_END");
  }
    $resHash = undef;
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package State::PipeAnalyseState
###########################################
package State::PipeAnalyseState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'PIPE_ANALYSE_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text
  # 1.1 result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
  my $resHash = $caMger->getStartPipe($eText);
  # reports for the finally analyse
  $eText->setMaxSeverity($resHash->{severity});
  $eText->addToProblemReports($resHash->{problems});
  
  
  # There is a pipe area
  if ($resHash->{found}) {
    $eText->setEntryTextAttr("hasStartPipe", 1);
    # 4.set a new entry Text 
    my $entryPartText =  Text::EntryPartText->new;
    # 4.1.get the entry id
    my $entryPartTextID = $idgApp->generateTextPartID;
    $entryPartText->setParentEntryID($eText->getEntryID);
    $entryPartText->setEntryPartID($entryPartTextID);
    $entryPartText->setEntryPartType('START_PIPE');
    
    #5. ENTRY TEXT PART
    #my $patName_getD74Header = Global::Values::MODULE_PATTERN->{GET_D74_HEADER};
  #  # get the position of the text part
  #  #my $posArrRef = $caMger->getUnitStrategy->getValueOfOutputTablesByPattern($patName_getD74Header, "pos");
    my $startPipeIndices = $resHash->{pos}->[0];
    
    # set text part
    my $textPartRef = $resHash->{element};
    $entryPartText->setTextPart($$textPartRef);    
    
    my $targetIndicesArrRef = Utils::Tools->splitIndices($startPipeIndices);  
    # set text index
    $entryPartText->setBeginPosOfTextIndex($targetIndicesArrRef->[0]);
    $entryPartText->setEndPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 7.add it to the entry text
    $eText->addContent($entryPartText);
    $eText->addPartTextToMap($entryPartText);
    
    # 9.set the new read_header of textPointer
#    $eText->updateTextPointer($targetIndicesArrRef->[1]);
    $eText->setBeginPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 10. set it to ouput
    $self->setOutput($eText);
    
    # 11.set check value to the current check value
    $self->setCheckValue("HAS_START_PIPE");
  } else {
    # set it to ouput
    $self->setOutput($eText);
    $self->setCheckValue("NOT_FOUND");
  }
  if($eText->isBeginEndInOnePoint) {
    $self->setCheckValue("TEXT_END");
  }
  $resHash = undef;
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


#CLASS:
###########################################
# package State::CollationAnalyseState
###########################################
package State::CollationAnalyseState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'COLLATION_ANALYSE_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text
  # 1.1 result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]

  my $resHash = $caMger->getCollation($eText);
  # reports for the finally analyse
  $eText->setMaxSeverity($resHash->{severity});
  $eText->addToProblemReports($resHash->{problems});
  
  # There is a collation area
  if ($resHash->{found}) {
    $eText->setEntryTextAttr("hasCollation", 1);
    # 4.set a new entry Text 
    my $entryPartText =  Text::EntryPartText->new;
    # 4.1.get the entry id
    my $entryPartTextID = $idgApp->generateTextPartID;
    $entryPartText->setParentEntryID($eText->getEntryID);
    $entryPartText->setEntryPartID($entryPartTextID);
    $entryPartText->setEntryPartType('COLLATION_ELEMENT');
    
    #5. ENTRY TEXT PART
    #my $patName_getD74Header = Global::Values::MODULE_PATTERN->{GET_D74_HEADER};
  #  # get the position of the text part
  #  #my $posArrRef = $caMger->getUnitStrategy->getValueOfOutputTablesByPattern($patName_getD74Header, "pos");
    my $collationIndices = $resHash->{pos}->[0];
    
    # set text part
    my $textPartRef = $resHash->{element};
    $entryPartText->setTextPart($$textPartRef);
    my $contentRef = $resHash->{content};
    $entryPartText->setContent($$contentRef);
    
    
    my $targetIndicesArrRef = Utils::Tools->splitIndices($collationIndices);  
    # set text index
    $entryPartText->setBeginPosOfTextIndex($targetIndicesArrRef->[0]);
    $entryPartText->setEndPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 7.add it to the entry text
    $eText->addContent($entryPartText);
    $eText->addPartTextToMap($entryPartText);
    
    # 9.set the new read_header of textPointer
#    $eText->updateTextPointer($targetIndicesArrRef->[1]);
    $eText->setBeginPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 10. set it to ouput
    $self->setOutput($eText);
    
    # 11.set check value to the current check value
    $self->setCheckValue("HAS_COLLATION");
  } else {
    # set it to ouput
    $self->setOutput($eText);
    $self->setCheckValue("NOT_FOUND");
  }
#  if($eText->isBeginEndInOnePoint) {
#    $self->setCheckValue("TEXT_END");
#  }
  $resHash = undef;
  return 1;
}




no Moose;
__PACKAGE__->meta->make_immutable;
1;



#CLASS:
###########################################
# package State::ReferenceAnalyseState
###########################################
package State::ReferenceAnalyseState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'REFERENCE_ANALYSE_STATE',
);
# verweis, vl, regexp verweis
########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text
  
  $eText->updateTempIndex($eText->getTextIndex());
  
  # 1.1 result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
  my $resHash = $caMger->getReference($eText);
  
  
  #NTODO: change the reference process to after checked the pipe and lemma.
  
  # reports for the finally analyse
  $eText->setMaxSeverity($resHash->{severity});
  $eText->addToProblemReports($resHash->{problems});

  # There is a reference area
  # !! Attention: Loop !!
  while ($resHash->{found}) {
    # 4.set a new entry Text 
    my $entryPartText =  Text::EntryPartText->new;
    # 4.1.get the entry id
    my $entryPartTextID = $idgApp->generateTextPartID;
    $entryPartText->setParentEntryID($eText->getEntryID);
    $entryPartText->setEntryPartID($entryPartTextID);
    $entryPartText->setEntryPartType($resHash->{elementType});

    
    #5. ENTRY TEXT PART
    #my $patName_getD74Header = Global::Values::MODULE_PATTERN->{GET_D74_HEADER};
  #  # get the position of the text part
  #  #my $posArrRef = $caMger->getUnitStrategy->getValueOfOutputTablesByPattern($patName_getD74Header, "pos");
    my $referenceIndices = $resHash->{pos}->[0];
    
    # set text part
    my $textPartRef = $resHash->{element};
    $entryPartText->setTextPart($$textPartRef);
    my $contentRef = $resHash->{content};
    if (defined $contentRef && ref($contentRef) eq "SCALAR" ) {
      $entryPartText->setContent($$contentRef);
    }
    
    my $targetIndicesArrRef = Utils::Tools->splitIndices($referenceIndices);  
    # set text index
    $entryPartText->setBeginPosOfTextIndex($targetIndicesArrRef->[0]);
    $entryPartText->setEndPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 7.add it to the entry text
    $eText->addContent($entryPartText);
    $eText->addPartTextToMap($entryPartText);
    
    # 9.set the new read_header of textPointer
#    $eText->updateTextPointer($targetIndicesArrRef->[1]);
    $eText->setBeginPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 10. set it to ouput
    $self->setOutput($eText);

    # 11.set check value to the current check value
    #$self->setCheckValue("HAS_REFERENCE");
    
    # beginning/ending indices are same.
    last if ($eText->isBeginEndInOnePoint);
    # for while
    $resHash = $caMger->getReference($eText);
  } 
  
  #NTODO:
  # if programm has found some reference and reference lemma, change the ending index of Text::EntryText,
  # because references are mostly at the end of a entry.
  if ($eText->getEntryTextAttr("hasReference") || $eText->getEntryTextAttr("hasReferenceLemma")) {
    # find the beginning index of first reference.
    my $beginIdxOfRef = $eText->beginIndexOfFirstReference();
    $eText->setEndPosOfTextIndex($beginIdxOfRef);
    my ($oldBegin, $oldEnd) = $eText->getTempIndices();
    $eText->setBeginPosOfTextIndex($oldBegin);
  }
  # 10. set it to ouput
  $self->setOutput($eText);
  # There is no reference or no more references
  $self->setCheckValue("NOT_FOUND");

  if($eText->isBeginEndInOnePoint) {
    $self->setCheckValue("TEXT_END");
  }
  
  $resHash = undef;
  return 1;
}



no Moose;
__PACKAGE__->meta->make_immutable;
1;




#CLASS:
###########################################
# package State::SigelAnalyseState
###########################################
package State::SigelAnalyseState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'SIGEL_ANALYSE_STATE',
);

########################
# methods
########################

########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text
  # 1.1 result hash reference
#  "severity" => 1,2,3,4,5,6,
#  "pos"     => ["0,17"]
#  "found"   => 1/0,
#  "element" => $lemElement;
#  "content" => $lemContent;
#  "result" => $headerInfoHash;
#  "problems" => [rpt_info_name_1, rpt_info_name_3]     # severty > 3
#  "reports" => [rpt_info_name_1, rpt_info_name_2, rpt_info_name_3 ...]
  my $resHash = $caMger->getSigel($eText);
  # reports for the finally analyse
  $eText->setMaxSeverity($resHash->{severity});
  $eText->addToProblemReports($resHash->{problems});
  
  # There is a sigel area
  if ($resHash->{found}) {
    # 4.set a new entry Text 
    $eText->setEntryTextAttr("hasSigel", 1);
    $eText->setEntryTextAttr("hasMoreSigel", 1) if ($resHash->{eleNum} > 1);
    my $entryPartText =  Text::EntryPartText->new;
    # 4.1.get the entry id
    my $entryPartTextID = $idgApp->generateTextPartID;
    $entryPartText->setParentEntryID($eText->getEntryID);
    $entryPartText->setEntryPartID($entryPartTextID);
    $entryPartText->setEntryPartType('SIGEL_ELEMENT');
    
    #5. ENTRY TEXT PART
    #my $patName_getD74Header = Global::Values::MODULE_PATTERN->{GET_D74_HEADER};
  #  # get the position of the text part
  #  #my $posArrRef = $caMger->getUnitStrategy->getValueOfOutputTablesByPattern($patName_getD74Header, "pos");
    my $sigelIndices = $resHash->{pos}->[0];
    
    # set text part
    my $textPartRef = $resHash->{element};
    $entryPartText->setTextPart($$textPartRef);
    my $contentRef = $resHash->{content};
    $entryPartText->setContent($$contentRef);
    
    my $targetIndicesArrRef = Utils::Tools->splitIndices($sigelIndices);  
    # set text index
    $entryPartText->setBeginPosOfTextIndex($targetIndicesArrRef->[0]);
    $entryPartText->setEndPosOfTextIndex($targetIndicesArrRef->[1]);
    
    #FIXME: bracket will be removed
    # 7.add it to the entry text
    $eText->addContent($entryPartText);
    $eText->addPartTextToMap($entryPartText);
    
    # 9.set the new read_header of textPointer
#    $eText->updateTextPointer($targetIndicesArrRef->[1]);
    $eText->setBeginPosOfTextIndex($targetIndicesArrRef->[1]);
    
    # 10. set it to ouput
    $self->setOutput($eText);
    
    # 11.set check value to the current check value
    $self->setCheckValue("HAS_SIGEL");
  } else {
    # 10. set it to ouput
    $self->setOutput($eText);
    $self->setCheckValue("NOT_FOUND");
  }
  if($eText->isBeginEndInOnePoint) {
    $self->setCheckValue("TEXT_END");
  }
  $resHash = undef;
  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package State::ReportAnalyseState
###########################################
package State::ReportAnalyseState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

use Algorithms::DORuleEngineClass;
use Algorithms::DOSets;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'REPORT_ANALYSE_STATE',
);

########################
# methods
########################



########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
sub input {
  my $self = shift;
  my $entryText = shift;
  $self->setCurEntryText($entryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  my $eText = $self->getCurEntryText;   # entry text

  my $objArrRef = $caMger->checkSeverityByRuleEngine($eText);
  my $entryText = $objArrRef->[0];  
  #CONSIDER: or do nothing??
  # not pass the status checking
  if (! defined $objArrRef || $objArrRef eq "" || ! scalar @$objArrRef) {
    # eText is harmless, severity is under baseline
    # take it to the report object
    $self->setCheckValue("HAS_ERROR");
  } else  {
    $self->setCheckValue("ANALYSE_END");
  }
  $self->setOutput($eText);

  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

#CLASS:
###########################################
# package State::CompareEntriesState
###########################################
package State::CompareEntriesState;
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

# make sure input/output Stream is UTF-8
use Encode;
use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape quote);
use File::Basename;
use utf8;
use 5.020;
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
no if $] >= 5.018, warnings => "experimental::smartmatch";

use Carp;
use Test::More;
use Clone;

use Global::Values;
use Database::MotDBProcessor;
use Coordinate::TextIndex;

use Algorithms::DORuleEngineClass;
use Algorithms::DOSets;

with 'State::AbstractState';

enum 'DO_STATE_TYPE', [@{Global::Values->doStateType}];
enum 'DO_STATE_CHECK_VALUE_TYPE', [@{Global::Values->doStateCheckValueType}];
########################
# constructor
########################

########################
# Variables
########################
has 'type' => (
  is => 'ro',
  isa => 'DO_STATE_TYPE',
  default => 'ENTRIES_COMPARISON_STATE',
);

has 'preEntryText' =>(
  is => 'rw',
  isa => 'Text::EntryText | Undef',
  predicate => 'hasPreEntryText',
  clearer => 'clearPreEntryText',
  reader => "getPreEntryText",
  writer => "setPreEntryText",
);

########################
# methods
########################



########################
# implements methods
########################
sub getStateType {
  my $self = shift;
  return $self->type;
}
# double
sub input {
  my $self = shift;
  my $preEntryText = shift;
  my $curEntryText = shift;
  $self->setPreEntryText($preEntryText);
  $self->setCurEntryText($curEntryText);
}
# outpout entry Text with entry Text part 
sub output {
  my $self = shift;
  return $self->getOutput;
}

# entry Text
sub handle {
  my $self = shift;

  # 0.
  my $dbtApp = Database::MotDBTableApp->getInstance;
  my $idgApp = Engine::IDGenerateApp->getInstance;
  my $caMger = Analyse::CentralAnalysisManager->getInstance;

  # 1.get the entry text 
  if (defined $self->getPreEntryText && defined $self->getCurEntryText) {
    my $objArrRef = $caMger->compareEntries($self->getPreEntryText, $self->getCurEntryText);
    my $curEntryText = $objArrRef->[0];
    
    # last check the entire entry text
    $objArrRef = $caMger->checkIntegrityByRuleEngine($curEntryText);
    
    my $newCurEText = $objArrRef->[0];
  }
  
  $self->setOutput($self->getCurEntryText);

  return 1;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;


########### > Engine, Context and App > ##############

#CLASS:
package State::DOStateEngine;
=encoding utf8

=head1 NAME

State::DOStateEngine - an engine to store and control the states in the state machine

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use FindBin;
use lib "$FindBin::Bin/../";

use Carp;
use State::AbstractState;

use Global::Values;
use Database::MotDB;
use Database::MotDBProcessor;

enum 'DO_STATE_TYPE', [ @{ Global::Values->doStateType } ];

=head1 ATTRIBUTES

=head2 stateMap

State map to store the states with their types.

key: type of state  - value: state (object)

=cut
has 'stateMap' => (
  is => 'rw',
  isa => 'HashRef[State::AbstractState]',
  predicate => 'hasStateMap',
  clearer => 'resetStateMap',
  default => sub {{}},
  lazy => 1,
);

=head1 METHODS

=head2 addDOState($state)

Add a given state ($state) to the stateMap.

=head3 Parameters:

=over 4

=item *

$state: the given state, it extends from State::AbstractState

=back

=cut
sub addDOState {
  my $self = shift;
  my $state = shift;
  
  my $type = $state->getStateType;
  # put state in the map
  $self->stateMap->{$type} = $state;
}
=head2 getDOState($stateType)

=head3 Parameters:

=over 4

=item *

$stateType: the given type of state, see also Global::Values->doStateType

=back

=head3 Returns:

The specified state object

=cut
sub getDOState {
  my $self = shift;
  my $stateType = shift;
  return $self->stateMap->{$stateType};
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;



#CLASS: 
package State::StateManagerContext;
=encoding utf8

=head1 NAME

State::StateManagerContext - a controller of the states and their engine

=head1 VERSION

version 0.02

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

Xin Zhou, C<zhouxin@uni-trier.de>

=head1 BUGS

=head1 COPYRIGHT

Copyright 2016, Xin Zhou, UB Trier. All Rights Reserved.

=cut
use Moose;
use namespace::autoclean;
use Moose::Util::TypeConstraints;

use FindBin;
use lib "$FindBin::Bin/../";

use utf8;
use Encode;
#use Mojo::Util qw(b64_decode b64_encode url_escape url_unescape xml_escape);
#base64, escape, md5, sha1,
use open ':std', 'encoding(UTF-8)';
use open ':utf8';
no if $] >= 5.018, warnings => "experimental::smartmatch";
use v5.20;

use Carp;

use Global::Values;
use Database::MotDBProcessor;

# positon
use Coordinate::TextPointer;

use Text::EntryText;

use State::AbstractState;

enum 'DO_STATE_TYPE', [ @{ Global::Values->doStateType } ];

=head1 CONSTRUCTORS

=head2 BUILD

After new.

=cut
sub BUILD {
  my $self = shift;
  # build in states
  $self->addBuildInStates;
}

=head2 _build_stateengine

Returns a new State::DOStateEngine.

=cut
sub _build_stateengine {
  return State::DOStateEngine->new();
}

=head1 ATTRIBUTES

=head2 stateEngine

=cut
has 'stateEngine' => (
  is => 'rw',
  isa => 'State::DOStateEngine',
  builder => "_build_stateengine",
  handles => [qw( addDOState getDOState)],
);

=head2 curState

the current state

=cut
has 'curState' => (
  is        => 'rw',
  isa       => 'State::AbstractState',
  predicate => 'hasCurState',
  writer    => 'setCurState',
  reader    => 'getCurState',
  clearer   => 'clearCurState',
);


=head1 METHODS

=head2 getInstance

Singleton.

=cut
my $singleton_StateManagerContext;
sub getInstance {
  my $self = shift;
  if ( !defined $singleton_StateManagerContext ) {
    $singleton_StateManagerContext = $self->new;
  }
  return $singleton_StateManagerContext;
}
=head2 addBuildInStates

Add the states to the main

=cut
sub addBuildInStates {
  my $self = shift;
  $self->addDOState(State::ErrorState->new);
  $self->addDOState(State::StopState->new);
  $self->addDOState(State::UnknownState->new);
  #
  $self->addDOState(State::initialization->new);
  $self->addDOState(State::D74HeaderState->new);
  $self->addDOState(State::LemmaAnalyseState->new);
  $self->addDOState(State::TitleAnalyseState->new);
  $self->addDOState(State::AuthorAnalyseState->new);
  $self->addDOState(State::SigelAnalyseState->new);
  $self->addDOState(State::CollationAnalyseState->new);
  $self->addDOState(State::ReferenceAnalyseState->new);
  $self->addDOState(State::EndpartAnalyseState->new);
  $self->addDOState(State::SpecialHtmlTagsAnalyseState->new);
  $self->addDOState(State::ErrorCharsAnalyseState->new);
  $self->addDOState(State::ReportAnalyseState->new);
  $self->addDOState(State::DitoDescAnalyseState->new);
  $self->addDOState(State::PipeAnalyseState->new);
  $self->addDOState(State::HtmlLemmaEntryAnalyseState->new);
  $self->addDOState(State::HtmlEntryAnalyseState->new);
  $self->addDOState(State::CompareEntriesState->new);
  $self->addDOState(State::StreckeEntryState->new);
}

=head2 runStates

Runs the state machine to analyse the original text.

=head3 Returns:

the processed Text::EntryText.

=cut
sub runStates {
  my $self = shift;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  
  # 0. set the initialization state
  $self->setStateWithType('INITIALIZATION_STATE');
  $self->getCurState->handle;
  
  # !! ATTENTION: LOOP 
  while($self->getCurState->getCheckValue ne "STOP_MACHINE") {
    $self->changeState;
    $self->getCurState->handle;
  }
  return $self->getCurState->output;
}

=head2 setState($state)

Checks and sets the current state. See 'curState'.

=cut
sub setState {
  my $self = shift;
  my $state = shift;
  if (! defined $state || $state eq '') {
    carp "This given state is not valid!";
    return;
  }
  $self->setCurState($state);
}

=head2 setState($stateType)

Checks and sets the current state with state type. See 'curState'.

=cut
sub setStateWithType {
  my $self = shift;
  my $stateType = shift;
  if (! defined $stateType || $stateType eq '') {
    carp "This given state type is not valid!";
    return;
  }
  # get the new State
  my $state = $self->getDOState($stateType);
  # make sure the new state clean
  $state->reset();
  # set it
  $self->setCurState($state);
}

#sub updateStateHistoryList {
#  my $self = shift;
#  my $state = shift;
#  
#  my $checkValue = $state->getCheckValue;
#  my $stateType = $state->getStateType;
#  # set the tiny hash ref
#  my $hash->{$stateType} = $checkValue;
#  my $arr = $self->getStateHisList;
#  push @$arr, $hash;
#  $self->setStateHisList($arr);
#  return 1;
#}

=head2 changeState()

The current state will be changed according the checked value and the defined rules.

=cut
sub changeState {
  my $self = shift;
  #my $curState = shift;
  
  my $mdbtApp = Database::MotDBTableApp->getInstance;
  # get current state
  my $curState = $self->getCurState;
  
  my $curStateType = $curState->getStateType;
  my $checkValue = $curState->getCheckValue;
  #$self-> updateStateHistoryList($curState);
  
  #CONSIDER:?? entry text
  my $curEntryText = $curState->output;
#  $mdbtApp->setCurEntryText();
  #ATTENTION: STOP_MACHINE
  given ($curStateType) {
    when("INITIALIZATION_STATE") {
      # check the entry text type
      given ($checkValue) {
        when ("ENTRY_TYPE_LEMMA") {
          $self->setStateWithType("D74_HEADER_ANALYSE_STATE");
        }
        when ("ENTRY_TYPE_ENTRY") {
          #i.e.: | Aus d. Musikleben ~s (H. Jacobs) <s>1344.</s> 235
          $self->setStateWithType("PIPE_ANALYSE_STATE");
        }
        when ("ENTRY_TYPE_LEMMA_HTML") {
          #i.e.: <entry id="a00001" bd="75" se="077" sp="li" ze="01"><l>Aachen</l>:...</entry>
          $self->setStateWithType("HTML_ELEMENT_LEMMA_ANALYSE_STATE");
        }
        when ("ENTRY_TYPE_ENTRY_HTML") {
          #i.e.: <entry id="a00001" bd="75" se="077" sp="li" ze="02">| 1 J. ~er Mis...</entry>
          $self->setStateWithType("HTML_ELEMENT_ANALYSE_STATE");
        }
        when ("ENTRY_TYPE_STRECKE") {
          #i.e.:<strecke=a> <seite=77>
          $self->setStateWithType("STRECKE_ANALYSE_STATE");
        }
        when ("ENTRY_TYPE_SEPARATOR") {
          #i.e.:----
          $self->setStateWithType("STOP_STATE");
        }
        when ("ENTRY_TYPE_UNKNOWN") {
          # something wrong!
          $self->setStateWithType("REPORT_ANALYSE_STATE");
        }
        default {
          croak "This check value is impossible for getting entry type!";
        }
      }
    }
    when("D74_HEADER_ANALYSE_STATE") {
      given ($checkValue) {
        when ("TEXT_END") {$self->setStateWithType("REPORT_ANALYSE_STATE");}
        when ("NOT_FOUND") {$self->setStateWithType("STOP_STATE");}
        when ("HAS_D74_HEADER") {$self->setStateWithType("REFERENCE_ANALYSE_STATE");}
        #when ("HAS_D74_HEADER") {$self->setStateWithType("LEMMA_ANALYSE_STATE");}
      }
    }
    when("LEMMA_ANALYSE_STATE") {
      given ($checkValue) {
        when ("TEXT_END") {$self->setStateWithType("SPECIAL_HTML_PARTS_STATE");}
        when ("NOT_FOUND") {$self->setStateWithType("DITO_DESCENDANT_ANALYSE_STATE");}
        when ("HAS_LEMMA_ELEMENT") {$self->setStateWithType("DITO_DESCENDANT_ANALYSE_STATE");}
      }
    }
    when("PIPE_ANALYSE_STATE") {
      given ($checkValue) {
        when ("TEXT_END") {$self->setStateWithType("SPECIAL_HTML_PARTS_STATE");}
        when ("HAS_ERROR") {$self->setStateWithType("REPORT_ANALYSE_STATE");}
        when ("NOT_FOUND") {$self->setStateWithType("REPORT_ANALYSE_STATE");}
        when ("HAS_START_PIPE") {$self->setStateWithType("REFERENCE_ANALYSE_STATE");}
      }
    }
    when("DITO_DESCENDANT_ANALYSE_STATE") {
      given ($checkValue) {
        when ("TEXT_END") {$self->setStateWithType("SPECIAL_HTML_PARTS_STATE");}
        when ("NOT_FOUND") {$self->setStateWithType("TITLE_ANALYSE_STATE");}
        when ("HAS_DITO_DESCENDANT") {$self->setStateWithType("TITLE_ANALYSE_STATE");}
      }
    }
    when("TITLE_ANALYSE_STATE") {
      given ($checkValue) {
        when ("TEXT_END") {$self->setStateWithType("SPECIAL_HTML_PARTS_STATE");}
        when ("NOT_FOUND") {$self->setStateWithType("AUTHOR_ANALYSE_STATE");}
        when ("HAS_TITLE") {$self->setStateWithType("AUTHOR_ANALYSE_STATE");}
      }
    }
    when("AUTHOR_ANALYSE_STATE") {
       given ($checkValue) {
         when ("TEXT_END") {$self->setStateWithType("SPECIAL_HTML_PARTS_STATE");}
         when ("NOT_FOUND") {$self->setStateWithType("SIGEL_ANALYSE_STATE");}
         when ("HAS_AUTHOR") {$self->setStateWithType("SIGEL_ANALYSE_STATE");}
      }
    }
    when("SIGEL_ANALYSE_STATE") {
      given ($checkValue) {
        when ("TEXT_END") {$self->setStateWithType("SPECIAL_HTML_PARTS_STATE");}
        when ("NOT_FOUND") {$self->setStateWithType("COLLATION_ANALYSE_STATE");}
        when ("HAS_SIGEL") {$self->setStateWithType("COLLATION_ANALYSE_STATE");}
      }
    }
    when("COLLATION_ANALYSE_STATE") {
      given ($checkValue) {
#         when ("NOT_FOUND") {$self->setStateWithType("ENDPART_ANALYSE_STATE");}
#         when ("HAS_COLLATION") {$self->setStateWithType("ENDPART_ANALYSE_STATE");}
         when ("NOT_FOUND") {$self->setStateWithType("SPECIAL_HTML_PARTS_STATE");}
         when ("HAS_COLLATION") {$self->setStateWithType("SPECIAL_HTML_PARTS_STATE");}
#         when ("TEXT_END") {$self->setStateWithType("SPECIAL_HTML_PARTS_STATE");}
      }
    }
    when("REFERENCE_ANALYSE_STATE") {
      # a small loop in the reference check
      given ($checkValue) {
        when ("NOT_FOUND") {$self->setStateWithType("LEMMA_ANALYSE_STATE");}
        when ("TEXT_END") {$self->setStateWithType("SPECIAL_HTML_PARTS_STATE");}
      }
    }
#    when("ENDPART_ANALYSE_STATE") {
#      given ($checkValue) {
##        when ("STOP_MACHINE") {$self->setStateWithType("STOP_STATE");}
#        when ("NOT_FOUND") {$self->setStateWithType("SPECIAL_HTML_PARTS_STATE");}
#        when ("HAS_ENDPART_ELEMENT") {$self->setStateWithType("SPECIAL_HTML_PARTS_STATE");}
#        when ("TEXT_END") {$self->setStateWithType("SPECIAL_HTML_PARTS_STATE");}
#      }
#    }
    when("SPECIAL_HTML_PARTS_STATE") {
      given ($checkValue) {
        when ("NOT_FOUND") {$self->setStateWithType("SEARCH_ERROR_CHAR_STATE");}
        when ("HAS_CHECKED_SPECIAL_HTML_TAGS") {$self->setStateWithType("SEARCH_ERROR_CHAR_STATE");}
      }
    }
    when("SEARCH_ERROR_CHAR_STATE") {
      given ($checkValue) {
        when ("NOT_FOUND") {$self->setStateWithType("REPORT_ANALYSE_STATE");}
        when ("HAS_CHAR_ERRORS") {$self->setStateWithType("REPORT_ANALYSE_STATE");}
      }
    }
    when("HTML_ELEMENT_LEMMA_ANALYSE_STATE") {
      given ($checkValue) {
        when ("HAS_ERROR") {$self->setStateWithType("STOP_STATE");}
        when ("NOT_FOUND") {$self->setStateWithType("REPORT_ANALYSE_STATE");}
        when ("HAS_ENTRY_ELEMENT") {$self->setStateWithType("REFERENCE_ANALYSE_STATE");}
      }
    }
    when("HTML_ELEMENT_ANALYSE_STATE") {
       given ($checkValue) {
        when ("HAS_ERROR") {$self->setStateWithType("REPORT_ANALYSE_STATE");}
        when ("NOT_FOUND") {$self->setStateWithType("REPORT_ANALYSE_STATE");}
        when ("HAS_ENTRY_ELEMENT") {$self->setStateWithType("PIPE_ANALYSE_STATE");}
      }
    }
    when("STRECKE_ANALYSE_STATE") {
      given ($checkValue) {
        when ("HAS_ERROR") {$self->setStateWithType("REPORT_ANALYSE_STATE");}
        when ("NOT_FOUND") {$self->setStateWithType("REPORT_ANALYSE_STATE");}
        when ("HAS_STRECKE") {$self->setStateWithType("STOP_STATE");}
      }
    }
    when("REPORT_ANALYSE_STATE") {$self->setStateWithType("STOP_STATE");}
    when("STOP_STATE") {
#      return $curEntryText;
    }
    default {
      croak "This would be imposible with this current state type: $curStateType !";
    }
  }
  # set the input of the new current state
  $self->getCurState->input($curEntryText);
  
  return 1;
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;

########### < Context and App < ##############
